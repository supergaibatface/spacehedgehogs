package jobs;

import java.util.ArrayList;

import events.EventManager;
import events.EventTagName;
import factories.IAutoMake;
import factories.MainFactory;
import other.StaticFunctions;
import skills.Skill;
import skills.SkillManager;
import skills.SkillName;
import sreenElementsSockets.ScrollCharSocket;

//TODO this is almost obsolete and should be either removed or redefined
//Central manager object for all the job objects
public class JobManager implements IAutoMake {
	private MainFactory mainFactory;
	private Job testJob;
	private ArrayList<Job> jobs;
	private ArrayList<ScrollCharSocket> registeredSockets;
	private boolean debug = false;

	//Setter for the main factory
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
	}

	//bootup method for a new object
	@Override
	public void bootUp() {
		this.jobs = new ArrayList<Job>();
		this.registeredSockets = new ArrayList<ScrollCharSocket>();
	}
	
	//NB TEMPORARY! makes test job for the test interface
	private void makeTestJob() {
		Job newJob = mainFactory.getElemFactory().getJob(JobName.Test);
		newJob.addNewSkillGain(SkillName.Scouting, 10);
		newJob.addTag(EventTagName.TST);
		newJob.setStaminaLimiter(20);
		this.testJob = newJob;
	}
	
	//Makes jovs required for the travel tutorial mode
	public void makeTravelTutorialJobs() {
		makeFoodSearchJob();
		makeRestingJob();
	}
	
	//Makes foor search job
	private void makeFoodSearchJob() {
		Job foodJob = this.makeJob(JobName.SearchFood, SkillName.Scouting, 2, EventTagName.SearchForFood);
		this.jobs.add(foodJob);
	}
	
	//Makes resting job
	private void makeRestingJob() {
		Job restingJob = this.mainFactory.getElemFactory().getJob(JobName.Resting);
		restingJob.addTag(EventTagName.Resting);
		this.jobs.add(restingJob);
	}
	
	/*
	 * Makes a new job object
	 * Inputs:
	 * 	jobName: enum type of a job name, this is used to distinguish between jobs
	 * 	skillName: the name of the skill that doing this job improves
	 * 	expValue: how much exp does this job give at one time
	 * 	eventTag: the tag that this job uses for its events
	 */
	private Job makeJob(JobName jobName, SkillName skillName, int expValue, EventTagName eventTag) {
		Job newJob = mainFactory.getElemFactory().getJob(jobName);
		newJob.addNewSkillGain(skillName, expValue);
		newJob.addTag(eventTag);
		return newJob;
	}
	
	//Method for registering a job socket at the job manager
	public boolean registerSocket(ScrollCharSocket newSocket) {
		this.registeredSockets.add(newSocket);
		return true;
	}
	
	//calls doJob command on all registered job sockets
	public boolean doAllRegisteredJobs() {
		StaticFunctions.writeOutDebug(debug, "Filling jobs on " + this.registeredSockets.size() + " registered sockets");
		for(ScrollCharSocket oneSocket : this.registeredSockets) {
			if(oneSocket.hasJob()) {
				oneSocket.doJobs();
			}
			else {
				StaticFunctions.writeOutDebug(debug, "There is a socket in the list without a job");
			}
		}
		return true;
	}
	
	//NB ONLY TEMPORARY! getter for the test job
	public Job getTestJob() {
		if(this.testJob == null) {
			makeTestJob();
		}
		return testJob;
	}
	
	//Finds a job based on JobName enum
	public Job findJob(JobName jobName) {
		for(Job oneJob: this.jobs) {
			if(oneJob.equals(jobName)) {
				return oneJob;
			}
		}
		return null;
	}
	

}

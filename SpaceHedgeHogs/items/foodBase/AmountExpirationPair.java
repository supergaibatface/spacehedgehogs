package foodBase;

//Class that can represent a certaina mount of food with a certain expiration time
public class AmountExpirationPair {
	private int amount;
	private int expirationTurn;
	
	/*
	 * constructor
	 * Input:
	 * 	amount: amount of food with this expiration date
	 * 	expirationTurn: the nr of turns till this amount of food expires
	 */
	public AmountExpirationPair(int amount, int expirationTurn) {
		this.amount = amount;
		this.expirationTurn = expirationTurn;
	}

	//getter for the amount
	public int getAmount() {
		return amount;
	}

	//setter for amount
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	//method for adding a certain amount ontop of the already set amount
	public int addAmount(int addition) {
		this.amount = this.amount + addition;
		return this.amount;
	}
	
	//removes a certain amount from the stack
	public int removeAmount(int amountToRemove) {
		return addAmount(-amountToRemove);
	}
	
	//lowers the expiration date by one
	public int expireByOne() {
		this.expirationTurn--;
		return this.expirationTurn;
	}
	
	//check function that replies if this stack of food has already expired
	public boolean hasExpired() {
		return expirationTurn < 1;
	}

	//getter for the expiration value
	public int getExpirationTurn() {
		return expirationTurn;
	}

	//setter for the expiration value
	public void setExpirationTurn(int expirationTurn) {
		this.expirationTurn = expirationTurn;
	}

}

package itemChange;

import factories.IAutoMake;
import factories.MainFactory;

//Base class for item listeners
public abstract class BaseItemListener implements IAutoMake, IItemListener{
	protected MainFactory mainFactory;

	@Override
	public void itemChangeEventOccured(ItemChangeEvent evt) {
		if(reactCheck(evt)) {
			this.changeReacting(evt);
		}
	}
	
	//method for reacting to an item change event
	public abstract void changeReacting(ItemChangeEvent evt);

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public abstract void bootUp();
	
	//check method that says respons if the listener should react to an event
	protected abstract boolean reactCheck(ItemChangeEvent evt);

}

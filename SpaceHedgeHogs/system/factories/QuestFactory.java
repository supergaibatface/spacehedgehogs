package factories;

import dialogueCharacters.IDialogueCharacter;
import dialogueFlags.DialogueFlag;
import itemBase.IGameItem;
import loupeQuests.LoupeMainQuest;
import loupeQuests.PreparingWeaponsJack;
import loupeQuests.RestingInLoupe;
import questBase.IQuest;
import questManager.QuestManager;
import questReward.GainDialogueFlagReward;
import questReward.GainItemsAsReward;
import questReward.GainLocationAccessReward;
import questReward.GainQuestAsReward;
import questRewardBase.IQuestReward;
import questStepBase.IQuestStep;
import questStepBase.StandardQuestStep;
import settlementLocation.ISettlementLocation;
import stepObjective.TalkToNpcObjective;
import stepObjective.VisitLocationObjective;
import stepObjectiveBase.IStepObjective;

public class QuestFactory extends BaseMinorFactory {
	private QuestManager questManager;
	private IQuest loupeMainQuest;
	private IQuest restingInLoupe;
	private IQuest preparingWeaponsJack;

	/*@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}*/

	/*@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}*/
	
	//private method for making a object that implements IAutoMake interface
	/*private <T extends IAutoMake> T setUpNewElement(T obj) {
		obj.setMainFactory(this.mainFactory);
		obj.bootUp();
		return obj;
	}*/
	
	public StandardQuestStep getStandardQuestStep(String name, IQuest masterQuest) {
		return setUpNewElement(new StandardQuestStep(name, masterQuest));
	}
	
	private void makeQuestFactory() {
		this.questManager = setUpNewElement(new QuestManager());
	}
	
	public QuestManager getQuestManager() {
		if(this.questManager == null) {
			this.makeQuestFactory();
		}
		return this.questManager;
	}
	
	private void makeLoupeMainQuest() {
		this.loupeMainQuest = setUpNewElement(new LoupeMainQuest());
	}
	
	public IQuest getLoupeMainQuest() {
		if(this.loupeMainQuest == null) {
			this.makeLoupeMainQuest();
		}
		return this.loupeMainQuest;
	}
	
	public IStepObjective getTalkToNpcObjective(IQuestStep masterStep, IDialogueCharacter targetCharacter) {
		return setUpNewElement(new TalkToNpcObjective(masterStep, targetCharacter));
	}
	
	public IQuestReward getGainLocationAccessReward(ISettlementLocation rewardLocation) {
		return setUpNewElement(new GainLocationAccessReward(rewardLocation));
	}
	
	public IStepObjective getVisitLocationObjective(IQuestStep masterStep, ISettlementLocation targetLocation) {
		return setUpNewElement(new VisitLocationObjective(masterStep, targetLocation));
	}
	
	private void makeRestingInLoupeQuest() {
		this.restingInLoupe = setUpNewElement(new RestingInLoupe());
	}
	
	public IQuest getRestingInLoupeQuest() {
		if(this.restingInLoupe == null) {
			this.makeRestingInLoupeQuest();
		}
		return this.restingInLoupe;
	}
	
	public IQuestReward getGainQuestASReward(IQuest rewardQuest) {
		return setUpNewElement(new GainQuestAsReward(rewardQuest));
	}
	
	public IQuestReward getGainDialogueFlagReward(DialogueFlag gainedFlag, IDialogueCharacter targetCharacter) {
		return setUpNewElement(new GainDialogueFlagReward(gainedFlag, targetCharacter));
	}
	
	private void makePreparingWeaponsJackQuest() {
		this.preparingWeaponsJack = setUpNewElement(new PreparingWeaponsJack());
	}
	
	public IQuest getPreparingWeaponsJackQuest() {
		if(this.preparingWeaponsJack == null) {
			this.makePreparingWeaponsJackQuest();
		}
		return this.preparingWeaponsJack;
	}
	
	public GainItemsAsReward getGainItemsAsReward(IGameItem itemType, int amount) {
		return setUpNewElement(new GainItemsAsReward(itemType, amount));
	}

}

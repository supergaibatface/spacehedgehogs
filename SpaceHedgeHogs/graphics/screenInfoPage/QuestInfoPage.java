package screenInfoPage;

import java.util.ArrayList;
import java.util.List;

import frameworks.ButtonFrame;
import frameworks.TextFrame;
import generalGraphics.ButtonLookType;
import questBase.IQuest;
import questRewardBase.IQuestReward;
import questStepBase.IQuestStep;
import screenElements.ScreenButton;
import stepObjectiveBase.IStepObjective;

public class QuestInfoPage extends BaseInfoPage {
	private IQuest master;
	private TextFrame questStepList;
	private ButtonFrame questStepButtonList;
	private TextFrame questStatus;
	private TextFrame currentStepInfo;
	private IQuestStep displayedStep;

	public QuestInfoPage(IQuest master) {
		super(1000, 900);
		this.master = master;
	}
	
	public void bootUp() {
		super.bootUp();
		this.setCoordinates(200, 50);
		this.makeQuestStepList();
		makeQuestStepButtonList();
		this.makeCurrentStepInfo();
		this.makeQuestStatus();
	}

	@Override
	protected void setLocationForCloseButton(ScreenButton closeButton) {
		closeButton.setCoordinates(950, 20);
		
	}

	@Override
	protected void setLocationForNameField(TextFrame nameField) {
		nameField.setLocation(50, 80);
		
	}

	@Override
	protected void writeNameFieldLines(List<String> lines) {
		lines.add("Quest: "+this.master.getName());	
	}
	
	protected void makeQuestStatus() {
		this.questStatus = this.mainFactory.getElemFactory().getTextFrame(this);
		this.questStatus.setLocation(550, 120);
		this.updateQuestStatusText();
		this.addNewElement(questStatus);
	}
	
	protected void updateQuestStatusText() {
		this.questStatus.fillTexts(this.getQuestStatusLines());
	}
	
	protected List<String> getQuestStatusLines() {
		List<String> lines = new ArrayList<String>();
		this.addQuestSituationLine(lines);
		return lines;
	}
	
	protected void makeQuestStepList() {
		this.questStepList = this.mainFactory.getElemFactory().getTextFrame(this);
		this.questStepList.setLocation(550, 160);
		this.updateQuestStepListText();
		this.addNewElement(questStepList);
	}
	
	protected void makeQuestStepButtonList() {
		this.questStepButtonList = this.mainFactory.getElemFactory().getButtonFrame(this);
		this.questStepButtonList.setLocation(550, 190);
		updateQuestStepButtonList();
		this.addNewElement(questStepButtonList);
	}
	
	protected void updateQuestStepListText() {
		this.questStepList.fillTexts(this.getQuestStepLines());
	}
	
	protected void updateQuestStepButtonList() {
		this.questStepButtonList.removeAllButtons();
		this.addButtonsForSteps(this.master.getVisibleQuestSteps());
	}
	
	protected void addButtonsForSteps(List<IQuestStep> stepList) {
		for(IQuestStep oneStep : stepList) {
			this.questStepButtonList.addElement(this.makeButtonFromQuestStep(oneStep));
		}
	}
	
	protected ScreenButton makeButtonFromQuestStep(IQuestStep targetStep) {
		ScreenButton newButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.QUESTSSTEPSELECT, targetStep.getQuestStepName());
		newButton.addButtonEventListener(this.mainFactory.getElemFactory().getSelectInfoQuestStepListener(this, targetStep));
		return newButton;
	}
	
	protected List<String> getQuestStepLines() {
		List<String> lines = new ArrayList<String>();
		lines.add("Quest steps:");
		/*int nr = 0;
		for(IQuestStep oneStep : this.master.getQuestSteps()) {
			nr++;
			lines.add(this.makeOneStepString(nr, oneStep));
			if(!oneStep.isCompleted()) {
				break;
			}
		}*/
		return lines;
	}
	
	protected String makeOneStepString(int stepNr, IQuestStep oneStep) {
		return stepNr + ". "+oneStep.getQuestStepName();
	}
	
	protected void makeCurrentStepInfo() {
		this.currentStepInfo = this.mainFactory.getElemFactory().getTextFrame(this);
		this.currentStepInfo.setLocation(50, 120);
		//this.updateCurrentStepText();
		this.selectCurrentStep();
		this.addNewElement(currentStepInfo);
	}
	
	public void selectCurrentStep() {
		this.selectQuestStep(this.master.getCurrentQuestStep());
	}
	
	public void selectQuestStep(IQuestStep selectedStep) {
		if(!this.master.hasStep(selectedStep)) {
			return;
		}
		this.displayedStep = selectedStep;
		this.updateCurrentStepText();
	}
	
	protected void updateCurrentStepText() {
		//this.currentStepInfo.fillTexts(this.getCurrentStepLines());
		this.currentStepInfo.fillTexts(this.makeDisplayedStepLines(this.displayedStep));
	}
	
	/*protected List<String> getCurrentStepLines() {
		if(this.master.isCompleted()) {
			return this.getCompletedQuestCurrentStepLines();
		}
		return this.getUnCompletedQuestCurrentStepLines();
	}*/
	
	/*protected List<String> getCompletedQuestCurrentStepLines() {
		List<String> lines = new ArrayList<String>();
		lines.add("Quest status: finished");
		return lines;
	}*/
	
	/*protected List<String> getUnCompletedQuestCurrentStepLines() {
		List<String> lines = new ArrayList<String>();
		lines.add("Quest status: underway");
		IQuestStep currentStep = this.master.getCurrentQuestStep();
		lines.add("Current step: "+ currentStep.getQuestStepName());
		addObjectiveLines(lines, this.master.getCurrentQuestStep());
		this.dealWithStepRewardsPart(lines, currentStep);
		return lines;
	}*/
	
	
	protected List<String> makeDisplayedStepLines(IQuestStep displayedStep) {
		List<String> lines = new ArrayList<String>();
		//this.addQuestSituationLine(lines);
		if(displayedStep != null) {
			this.addObjectiveLines(lines, displayedStep);
			this.dealWithStepRewardsPart(lines, displayedStep);
		}
		return lines;
	}
	
	protected void addQuestSituationLine(List<String> addLocation) {
		if(!this.master.isCompleted()) {
			addLocation.add("Quest status: underway");
		}
		else {
			addLocation.add("Quest status: completed");
		}
	}
	
	protected void dealWithSelectedStep(List<String> addLocation, IQuestStep selectedStep) {
		addLocation.add("Quest step name: "+ selectedStep.getQuestStepName());
		
	}
	
	
	protected void addObjectiveLines(List<String> addLocation, IQuestStep oneStep) {
		addLocation.add("  Objectives:");
		for(IStepObjective oneObjective : oneStep.getObjectives()) {
			addLocation.add("  *."+oneObjective.getObjectiveDescription()+", "+this.getCompletionLinePart(oneObjective));
		}
	}
	
	protected void dealWithStepRewardsPart(List<String> addLocation, IQuestStep oneStep) {
		if(!oneStep.getRewards().isEmpty()) {
			addLocation.add("  Rewards:");
			this.addStepRewardLines(addLocation, oneStep);
		}
	}
	
	protected void addStepRewardLines(List<String> addLocation, IQuestStep oneStep) {
		for(IQuestReward oneReward : oneStep.getRewards()) {
			addLocation.add("  *."+oneReward.getRewardDescription());
		}
	}
	
	protected String getCompletionLinePart(IStepObjective objective) {
		if(objective.isCompleted()) {
			return "done";
		}
		return "underway";
	}
	
	public void callDataUpdate() {
		this.updateQuestStepListText();
		this.selectCurrentStep();
		this.updateQuestStatusText();
		updateQuestStepButtonList();
		//this.updateCurrentStepText();
	}
	
	public IQuest getMasterQuest() {
		return this.master;
	}

	

}

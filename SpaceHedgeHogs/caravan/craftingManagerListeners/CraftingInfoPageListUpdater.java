package craftingManagerListeners;

import craftingBluePrints.CraftingProject;
import craftingCenter.CraftingInfoPage;

public class CraftingInfoPageListUpdater extends BaseCraftingManagerListener {
	private CraftingInfoPage masterPage;
	
	public CraftingInfoPageListUpdater(CraftingInfoPage masterPage) {
		this.masterPage = masterPage;
	}

	@Override
	protected void newCraftingProjectScript(CraftingProject newProject) {
		this.masterPage.callUpdateOnWorkList();
		
	}

	@Override
	protected void oldProjectCancelledScript(CraftingProject oldProject) {
		this.masterPage.callUpdateOnWorkList();
		
	}

	@Override
	protected void craftingProjectCompletedScript(CraftingProject completedProject) {
		this.masterPage.callUpdateOnWorkList();
		
	}

	@Override
	protected void craftingListChangedScript() {
		this.masterPage.callUpdateOnWorkList();
		
	}

}

package battleManagement;

import java.util.ArrayList;

import factories.IAutoMake;
import factories.MainFactory;

//Object for keeping values that limit players ability to spawn characters
public class SpawnLimiting implements IAutoMake{
	private MainFactory mainFactory;
	private int spawnCount;
	private int spawnCountMax;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	oneTurnSpawnCount: nr value for how many characters player should be able to spawn each turn
	 */
	public SpawnLimiting(int oneTurnSpawnCount) {
		this.spawnCountMax = oneTurnSpawnCount;
		refreshSpawnCount();
	}
	
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}
	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	//Refreshes one turn allowed spawns to max value
	public void refreshSpawnCount() {
		this.spawnCount = this.spawnCountMax;
	}
	
	//Counts down one turn allowed spawn count by one
	public int countDownSpawn() {
		this.spawnCount--;
		return this.spawnCount;
	}
	
	//Checks if player is allowed to spawn atleast one more char
	public boolean allowedToSpawn() {
		return this.spawnCount > 0;
	}
	
	//Method that will fill a string array with current limiting info
	public void addInfoToArray(ArrayList<String> lines) {
		lines.add("Player spawns left: "+this.spawnCount);
	}

}

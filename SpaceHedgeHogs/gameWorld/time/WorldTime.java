package time;

import java.util.ArrayList;
import java.util.List;

import factories.IAutoMake;
import factories.MainFactory;
import screenElements.ScreenDataBox;
import timeListeners.ITimeListener;

//Class that represents the time in the game world
public class WorldTime implements IAutoMake{
	private MainFactory mainFactory;
	private int day;
	private TimeOfDay currentMoment;
	private List<ITimeListener> timeListeners;
	
	//constructor, input startTurn, number that the time counter starts with
	public WorldTime() {
		
	}
	
	//Boot up method, meant for starting up the object after creation
	public void bootUp() {
		this.day = 1;
		this.currentMoment = TimeOfDay.DAWN;
		this.timeListeners = new ArrayList<ITimeListener>();
	}
	
	//setter for main factory object
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
	}
	
	//Getter for turn showing databox lines
	public ArrayList<String> getLines() {
		ArrayList<String> lines = new ArrayList<String>();
		lines.add("Day: " + this.day);
		lines.add("Time: " + this.getMomentName());
		return lines;
	}
	
	//Method for advancing the time by one day
	private void nextDay() {
		this.day++;
	}
	
	//switch for moving through different parts of the day
	public void nextTurn() {
		this.triggerTimeListeners();
		this.mainFactory.getEventCaller().callEvents();
		switch(currentMoment) {
		case NIGHT:
			this.currentMoment = TimeOfDay.DAWN;
			break;
		case DAWN:
			this.currentMoment = TimeOfDay.NOON;
			break;
		case NOON:
			this.currentMoment = TimeOfDay.DUSK;
			break;
		case DUSK:
			this.currentMoment = TimeOfDay.NIGHT;
			this.nextDay();
			break;
		default:
			break;
		}
	}
	
	//getter for current day
	public int getDay() {
		return this.day;
	}
	
	//getter for current moment of the day
	public TimeOfDay getMoment() {
		return this.currentMoment;
	}
	
	//getter for the string value of the current part of the day
	public String getMomentName() {
		switch(currentMoment) {
		case NIGHT:
			return "Night";
		case DAWN:
			return "Dawn";
		case NOON:
			return "Noon";
		case DUSK:
			return "Dusk";
		default:
			return "Hedgehog time";
		}
	}
	
	//setter for day
	public void setDay(int dayNr) {
		this.day = dayNr;
	}
	
	//for notifying all time listeners that the turn changed
	private void triggerTimeListeners() {
		for(ITimeListener oneTimeListener : this.timeListeners) {
			oneTimeListener.timeAdvanced(false);
		}
	}
	
	//Adds a listener that listens for turn changes
	public void addTimeListener(ITimeListener newListener) {
		this.timeListeners.add(newListener);
	}
	
	//Removes a listener that waits for turn changes
	public void removeTimeListener(ITimeListener oldListener) {
		this.timeListeners.remove(oldListener);
	}

}

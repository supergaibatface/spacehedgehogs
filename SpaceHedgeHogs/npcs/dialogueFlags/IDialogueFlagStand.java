package dialogueFlags;

import dialogueFlagListeners.IDialogueFlagListener;

public interface IDialogueFlagStand {
	
	public boolean raiseFlag(DialogueFlag flag);
	
	public boolean checkFlagState(DialogueFlag flag);
	
	public boolean takeDownFlag(DialogueFlag flag);
	
	public boolean addListener(IDialogueFlagListener newListener);
	
	public boolean removeListener(IDialogueFlagListener oldListener);

}

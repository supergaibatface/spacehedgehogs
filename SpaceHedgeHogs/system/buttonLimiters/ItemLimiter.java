package buttonLimiters;

import itemBase.IGameItem;

//Limiter that limits the action by the possession of a specific trade item
public class ItemLimiter extends BaseButtonLimiter {
	private IGameItem limitItem;
	
	//constructor
	public ItemLimiter(boolean notifyOnFailure, boolean instantPopUp, IGameItem limitItem) {
		super(notifyOnFailure, instantPopUp);
		this.limitItem = limitItem;
	}

	@Override
	public boolean check() {
		IGameItem checkItem = this.mainFactory.getInventory().findItem(this.limitItem.getName());
		if(checkItem == null) {
			return false;
		}
		if(checkItem.getSupply() < this.limitItem.getSupply()) {
			return false;
		}
		return true;
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void doCost() {
		this.mainFactory.getInventory().tryToRemoveItemWithAmount(this.limitItem);
		//this.mainFactory.getInventory().removeTradeItemAmount(this.limitItem);
	}

	@Override
	public String reasonForFail() {
		return "Inventory has less then "+this.limitItem.getSupply() + " of "+this.limitItem.getName();
	}

}

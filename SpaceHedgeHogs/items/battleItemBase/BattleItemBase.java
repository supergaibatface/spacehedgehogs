package battleItemBase;

import itemBase.ItemTagName;
import itemBase.SingleStackItemBase;
import itemBase.TradeItemName;
import screenInfoPage.ItemInfoPage;

public abstract class BattleItemBase extends SingleStackItemBase {

	public BattleItemBase(TradeItemName name) {
		super(name);
	}
	
	@Override
	protected ItemInfoPage bootGiveInfoPage() {
		return this.mainFactory.getElemFactory().getItemInfoPage(this, "Orange");
	}
	
	public void bootUp() {
		super.bootUp();
		this.addTags();
	}
	
	private void addTags() {
		addAnotherItemTag(ItemTagName.Battle);
	}
}

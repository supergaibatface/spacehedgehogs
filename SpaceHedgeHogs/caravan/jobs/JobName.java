package jobs;

//Enum for naming jobs
public enum JobName {
	Test,
	SearchFood,
	SearchSupply,
	PathFinding,
	TrainingFighting,
	Resting,
	BarricadeCrafting,
	ItemRepairing
}

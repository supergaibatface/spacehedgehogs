package loupeQuests;

import java.util.List;

import questBase.BaseQuest;
import questStepBase.IQuestStep;

public class LoupeMainQuest extends BaseQuest {

	@Override
	protected String returnQuestName() {
		// TODO Auto-generated method stub
		return "Seeking safety from Loupe";
	}

	@Override
	protected void addQuestSteps(List<IQuestStep> questStepList) {
		questStepList.add(this.makeFirstLoupeQuestStep());
		questStepList.add(this.makeSecondLoupeQuestStep());
		questStepList.add(this.makeThirdLoupeQuestStep());
		questStepList.add(this.makeFourthLoupeQuestStep());
	}
	
	private IQuestStep makeFirstLoupeQuestStep() {
		IQuestStep newStep = this.mainFactory.getQuestFactory().getStandardQuestStep("Gain entrance to the fort", this);
		newStep.addNewStepObjective(this.mainFactory.getQuestFactory().getTalkToNpcObjective(newStep, this.mainFactory.getNPCFactory().getNPCLoupeGateGuard()));
		newStep.addQuestStepReward(this.mainFactory.getQuestFactory().getGainLocationAccessReward(this.mainFactory.getLogicalLocationFactory().getLoupeKeep()));
		return newStep;
	}
	
	private IQuestStep makeSecondLoupeQuestStep() {
		IQuestStep newStep = this.mainFactory.getQuestFactory().getStandardQuestStep("Talk to the captain", this);
		newStep.addNewStepObjective(this.mainFactory.getQuestFactory().getTalkToNpcObjective(newStep, this.mainFactory.getNPCFactory().getNPCLoupeCaptain()));
		newStep.addQuestStepReward(this.mainFactory.getQuestFactory().getGainLocationAccessReward(this.mainFactory.getLogicalLocationFactory().getLoupeCamp()));
		newStep.addQuestStepReward(this.mainFactory.getQuestFactory().getGainQuestASReward(this.mainFactory.getQuestFactory().getRestingInLoupeQuest()));
		newStep.addQuestStepReward(this.mainFactory.getQuestFactory().getGainLocationAccessReward(this.mainFactory.getLogicalLocationFactory().getLoupeDungeon()));
		return newStep;
	}
	
	private IQuestStep makeThirdLoupeQuestStep() {
		IQuestStep newStep = this.mainFactory.getQuestFactory().getStandardQuestStep("Fight in the dungeons", this);
		newStep.addNewStepObjective(this.mainFactory.getQuestFactory().getVisitLocationObjective(newStep, this.mainFactory.getLogicalLocationFactory().getLoupeDungeon()));
		return newStep;
	}
	
	private IQuestStep makeFourthLoupeQuestStep() {
		IQuestStep newStep = this.mainFactory.getQuestFactory().getStandardQuestStep("Talk to docile prisoners", this);
		newStep.addNewStepObjective(this.mainFactory.getQuestFactory().getTalkToNpcObjective(newStep, this.mainFactory.getNPCFactory().getNPCDocilePrisoner()));
		return newStep;
	}

}

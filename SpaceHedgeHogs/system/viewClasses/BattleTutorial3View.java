package viewClasses;

import generalGraphics.ButtonLookType;
import propertyBars.PropertyBarName;
import screenElements.ScreenButton;
import screenElements.ScreenDataBox;
import screenElements.ScreenScrollPanel;
import socketStoring.CharSocketBuilder;
import socketStoring.FullCharacterListProviding;
import sreenElementsSockets.ScrollCharSocket;
import view.BaseGameView;
import view.GameView;
import view.ViewName;

public class BattleTutorial3View extends BaseGameView {

	public BattleTutorial3View() {
		super(ViewName.Battle);
	}

	@Override
	protected String bootGiveBackgroundImageName() {
		return "TestBG";
	}

	@Override
	protected void bootElements() {
		bootAssignGameTimeCoords();
		makeIdleForTabDirectory();
		bootAddFieldCharCommandControl();
		makePlayerLimitingDataBox();
		makeEnemyInfoDataBox(1500, 125);
		this.buttons.add(this.getNextTurnButton());
		bootMakeScrollPanel();
	}
	
	private void bootAssignGameTimeCoords() {
		this.setGameTimeCoord(1600, 20);
	}
	
	//Makes idle socket that is located in the tab directory
	private void makeIdleForTabDirectory() {
		ScrollCharSocket socket = this.mainFactory.getElemFactory().getScrollCharSocket(0, 0, 
				this.mainFactory.getLengthAssigner().getSocketLengthForSize(1), 
				this.mainFactory.getLengthAssigner().getSocketLengthForSize(3), "Orange");
		socket.setTitle("Idle");
		socket.designateAsIdleSocket();
		FullCharacterListProviding provider = this.mainFactory.getElemFactory().getFullCharacterListProvding();
		CharSocketBuilder socketBuilder = this.mainFactory.getElemFactory().getCharSocketBuilder(socket, provider);
		this.newSocketBuildersForTabs.add(socketBuilder);
	}
	
	//Adds a fieldCharCommandControl to a view
	private void bootAddFieldCharCommandControl() {
		this.addDataBox(getSimpleFieldCharCommandControl(), 1150, 760);
	}
	
	//Getter for a fieldCharCommandControl
	private ScreenDataBox getSimpleFieldCharCommandControl() {
		return this.mainFactory.getFieldCharCommandControl().getDataBox();
	}
	
	//Adds a player limiting databox to a view
	private void makePlayerLimitingDataBox() {
		this.addDataBox(this.mainFactory.getPlayerLimiting().getLimiterDataBox(), 1500, 85);
	}
	
	//adds enemy info databox to a view
	private void makeEnemyInfoDataBox(int x, int y) {
		this.addDataBox(this.mainFactory.getMainAIHub().getEnemyInfo(), x, y);
	}
	
	private ScreenButton getNextTurnButton() {
		ScreenButton nextTurn = this.mainFactory.getElemFactory().getScreenButton(1610, 900, ButtonLookType.NEXT, "Next Turn");
		nextTurn.addButtonEventListener(this.mainFactory.getElemFactory().getTurnChangeListener());
		nextTurn.addButtonEventListener(this.mainFactory.getElemFactory().getCombatBarValueRefresh(PropertyBarName.Movement));
		nextTurn.addButtonEventListener(this.mainFactory.getElemFactory().getCombatBarValueRefresh(PropertyBarName.Attacks));
		return nextTurn;
	}
	
	//Makes a scrollpanel meant for battleview
	public void bootMakeScrollPanel() {
		ScreenScrollPanel newPanel = this.mainFactory.getElemFactory().getNewScrollPanel(10, 10, 1400, 700, "20Opacity");
		this.mainScreen = newPanel;
		this.buttons.add(newPanel.getAScrollButtonForThisPanel(true, true));
		this.buttons.add(newPanel.getAScrollButtonForThisPanel(true, false));
		this.buttons.add(newPanel.getAScrollButtonForThisPanel(false, true));
		this.buttons.add(newPanel.getAScrollButtonForThisPanel(false, false));
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

}

package settlementLocationListeners;

import stepObjective.VisitLocationObjective;

public class VisitLocationQuestListener extends BaseSettlementLocationListener {
	private VisitLocationObjective objective;
	
	public VisitLocationQuestListener(VisitLocationObjective masterObjective) {
		this.objective = masterObjective;
	}

	@Override
	public void callVistingEvent() {
		objective.announceObjectiveCompletion();
		
	}

}

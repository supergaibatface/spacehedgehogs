package eventModules;

import java.util.ArrayList;

import events.GameEvent;

//interface for event modules
public interface IEventModule {
	
	//returns all the events contained in the specific event module
	public ArrayList<GameEvent> getEvents();

}

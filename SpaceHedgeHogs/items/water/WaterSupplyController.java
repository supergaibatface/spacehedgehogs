package water;

import java.awt.Color;
import java.util.ArrayList;

import factories.IAutoMake;
import factories.MainFactory;
import party.PartyStance;
import propertyBars.PropertyBar;
import propertyBars.PropertyBarName;
import screenElements.ScreenDataBox;
import screenElements.ScreenPopUp;

//Controller for the caravans water supply
public class WaterSupplyController implements IAutoMake {
	private MainFactory mainFactory;
	private PropertyBar waterSupply;
	private ScreenDataBox dataBox;
	private int dehydrationCounter;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.makeWaterSupplyBar();
		this.dataBox = this.mainFactory.getElemFactory().getScreenDataBox(0, 0, "LightBlue");
		this.writeInfoToDataBox();
		this.dehydrationCounter = this.getMaxDehydrationCounter();
	}
	
	//getter for the max water supply
	private int getMaxWaterSupplyValue() {
		return 100;
	}
	
	//getter for the max dehyration counter
	private int getMaxDehydrationCounter() {
		return 3;
	}
	
	//Makes a bar that represents current water supply for the caravan
	private void makeWaterSupplyBar() {
		this.waterSupply = mainFactory.getElemFactory().getPropertyBar(PropertyBarName.WaterSupply);
		this.waterSupply.setMaxValue(this.getMaxWaterSupplyValue());
		this.waterSupply.fillUp();
		this.waterSupply.addListener(this.mainFactory.getElemFactory().getWaterBarListener());
	}
	
	//writes info about water supply to databox
	private void writeInfoToDataBox() {
		ArrayList<String> lines = new ArrayList<String>();
		lines.add("Water: " + this.waterSupply.getCurrentValue() + "/" + this.waterSupply.getMaxValue());
		writeWeightCarryline(lines);
		lines.add("Current amount of food: "+this.mainFactory.getFoodManager().currentFoodAmount());
		lines.add("one turn consumption: " );
		lines.add("Water: " + this.getOneTurnWaterConsuption());
		if(this.mainFactory.getParty().getPartyStance() == PartyStance.Travel) {
			lines.add("Food: " + this.getOneTurnFoodConsumption());
		}
		this.dataBox.fillDataBoxWithTexts(lines);
		this.setColorForWaterAmount(0);
		this.setColorForWeightCarryLine(1);

		
		this.dataBox.updateSizeForTime();
	}
	
	//checks if the info box also needs to add lines about weight carry
	private boolean needsWeightCarryLine() {
		return this.mainFactory.getParty().getPartyStance() == PartyStance.Travel;
	
	}
	
	//For writing lines under the weight carry segment, input is a list of the lines that will be written there
	private void writeWeightCarryline(ArrayList<String> lines) {
		if(this.needsWeightCarryLine()) {
			lines.add("Weight carry: "+this.mainFactory.getInventoryWeightController().getFullCarryingAbility()+"/"+this.mainFactory.getInventory().getInventoryFullWeight());
		}
	}
	
	//Method for setting the right color for the weight carry line, input is the specific line nr where weight carry is written
	private void setColorForWeightCarryLine(int lineNr) {
		if(this.needsWeightCarryLine()) {
			this.decideOnWeightCarryColor(lineNr);
		}
	}
	
	//Method that checks what color needs to be used for weight carry line, input is the specific line nr where weight carry is written
	private void decideOnWeightCarryColor(int lineNr) {
		if(!this.mainFactory.getInventoryWeightController().hasEnoughCarryingAbility()) {
			this.dataBox.setColorForALine(Color.red, lineNr);
		}
		else {
			this.dataBox.setColorForALine(Color.black, lineNr);
		}
	}
	
	//Method for setting a color for water amount line
	private void setColorForWaterAmount(int lineNr) {
		if(this.waterSupply.isEmpty()) {
			this.dataBox.setColorForALine(Color.red, lineNr);
		}
		else {
			this.dataBox.setColorForALine(Color.black, lineNr);
		}
	}
	
	//For removing a turns worth of consumed water from caravans water supply
	public void removeATurnsWorthOfWater() {
		this.oneTurnDehydrationCheck();
		this.waterSupply.decreaseByValue(getOneTurnWaterConsuption());
	}
	
	//Getter for how much water the caravan consumes in one turn
	private int getOneTurnWaterConsuption() {
		return this.mainFactory.getCharacterManager().getPartySize() * this.getOneCharWaterConsumption();
	}
	
	//Getter for one characters water consumption amount for a turn
	private int getOneCharWaterConsumption() {
		return 5;
	}
	
	//getter for one characters food consumption amount for a turn
	private int getOneTurnFoodConsumption() {
		return this.mainFactory.getFoodManager().getFullPartyFoodConsumption();
	}
	
	//updates the info on water supply databox
	public void updateData() {
		this.writeInfoToDataBox();
	}
	
	//getter for the databox that displays info on water supply
	public ScreenDataBox getWaterDisplayDataBox() {
		return this.dataBox;
	}
	
	//Checks if the game should start counting down to dehydration or instead reset the counter for it
	public void oneTurnDehydrationCheck() {
		if(this.waterSupply.isEmpty()) {
			this.outOfWaterLogic();
		}
		else {
			this.dehydrationCounter = this.getMaxDehydrationCounter();
		}
	}
	
	//Logic that gets activated when party is out of water
	private void outOfWaterLogic() {
		if(this.dehydrationCounter < 1) {
			this.triggerGameOverByDehydration();
		}
		else {
			this.dehydrationCounter --;
			this.sendWarningNoticeToPlayerAboutWater();
		}
	}
	
	//calls game over message once party reached critical dehydration
	private void triggerGameOverByDehydration() {
		this.mainFactory.getGElementManager().addQuickPopUp(this.getEndGamePopUp());
	}
	
	//getter for end game popup
	private ScreenPopUp getEndGamePopUp() {
		return this.mainFactory.getGameStateController().makeAndGetEndGamePopUp(
				"Your party died from dehydration", 
				"Back to menu", 
				"100X100Mark");
	}
	
	//send a notice to the player about being out of water and also in how many turns the player would lose if this continues
	private void sendWarningNoticeToPlayerAboutWater() {
		this.mainFactory.getNoticeController().addNotice("Warning, out of water, if it continues, you will lose in " + this.dehydrationCounter + "turns", true);
	}
	
	//getter for the property bar that represents water supply
	public PropertyBar getWaterPropertyBar() {
		return this.waterSupply;
	}


}

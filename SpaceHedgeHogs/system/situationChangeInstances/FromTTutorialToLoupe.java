package situationChangeInstances;

import cutsceneInstances.ReachingLoupeCutscene;
import situationChange.BaseSituationChange;
import view.ViewName;

public class FromTTutorialToLoupe extends BaseSituationChange {
	private ReachingLoupeCutscene loupeCutscene;

	@Override
	protected void inBetweenLogic() {
		this.setUpLoupeCutscene();
		
	}
	
	private void setUpLoupeCutscene() {
		loupeCutscene = this.mainFactory.getCutsceneFactory().getReachingLoupeCutscene();
		loupeCutscene.addButtonListenersToEnd(this.mainFactory.getElemFactory().getViewChangeListener(ViewName.Loupe));
	}

	@Override
	protected void sendIntoNextViewLogic() {
		this.startLoupeQuest();
		this.startCutscene(loupeCutscene);
		
	}
	
	private void startLoupeQuest() {
		this.mainFactory.getQuestFactory().getQuestManager().addNewQuest(this.mainFactory.getQuestFactory().getLoupeMainQuest());
	}

}

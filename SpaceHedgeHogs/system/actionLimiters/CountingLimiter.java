package actionLimiters;

//Counter class that keeps track of actions
public class CountingLimiter {
	private int limitNr;
	private int currentCount;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	limit: numeral limit to something being done
	 */
	public CountingLimiter(int limit) {
		setNewLimit(limit);
		this.currentCount = 0;
	}
	
	//Method that checks if the limit hasn't been meet
	public boolean canDoAction() {
		if(currentCount < limitNr) {
			return true;
		}
		else {
			return false;
		}
	}
	
	//Advances the counter by one
	public void countUp() {
		this.currentCount++;
	}
	
	//Resets the counter
	public void resetCount() {
		this.currentCount = 0;
	}
	
	//Sets new limit to this counter
	public void setNewLimit(int limit) {
		this.limitNr = limit;
	}

}

package questStepBase;

import java.util.List;

import questRewardBase.IQuestReward;
import stepObjectiveBase.IStepObjective;

public interface IQuestStep {
	
	public String getQuestStepName();
	
	public boolean isCompleted();
	
	public boolean isActive();
	
	//public void setActive();
	
	public void addNewStepObjective(IStepObjective newObjective);
	
	public void recieveObjectiveUpdate();
	
	public List<IStepObjective> getObjectives();
	
	public void addQuestStepReward(IQuestReward newReward);
	
	public List<IQuestReward> getRewards();

}

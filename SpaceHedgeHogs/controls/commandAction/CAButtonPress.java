package commandAction;

import screenElements.ScreenButton;
import screenElements.ScreenElement;

//CommandAction for reacting to a button press
public class CAButtonPress extends CABaseClass<ScreenButton> {

	@Override
	public boolean inputIsCorrect() {
		return this.standardClickCheck();
	}

	@Override
	public void actionImplement() {	
		ScreenButton button = (ScreenButton) conditions.getClickedItem(this.hasItemTypeClicked());
		button.activate();
		conditions.clearValues();
	}

	@Override
	public Class<ScreenButton> setClickTargetClass() {
		return ScreenButton.class;
	}

	@Override
	protected int giveMouseButtonNr() {
		return this.giveLeftMouseButtonValue();
	}

	

}

package npcListeners;

import view.CutsceneDataCollection;

public interface INpcListener {
	
	public void callEndOfADialogue(CutsceneDataCollection endedDialogue);

}

package buttonLimiters;

import questBase.IQuest;

public class QuestLimiter extends BaseButtonLimiter {
	private IQuest limitingQuest;

	public QuestLimiter(IQuest limitingQuest) {
		super(false, false);
		this.limitingQuest = limitingQuest;
	}

	@Override
	public boolean check() {
		return this.limitingQuest.isCompleted();
	}

	@Override
	public String reasonForFail() {
		return "Quest: "+this.limitingQuest.getName()+" is not yet complete";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doCost() {
		// TODO Auto-generated method stub
		
	}

}

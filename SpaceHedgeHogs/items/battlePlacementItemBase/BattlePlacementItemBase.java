package battlePlacementItemBase;

import battleItemBase.BattleItemBase;
import itemBase.IGameItem;
import itemBase.ItemTagName;
import itemBase.TradeItemName;

public abstract class BattlePlacementItemBase extends BattleItemBase {

	public BattlePlacementItemBase(TradeItemName name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	
	public void bootUp() {
		super.bootUp();
		this.addTags();
	}
	
	private void addTags() {
		addAnotherItemTag(ItemTagName.FieldPlacement);
	}

}

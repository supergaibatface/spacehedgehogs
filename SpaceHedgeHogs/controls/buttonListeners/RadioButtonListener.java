package buttonListeners;

import generalControls.RadioButtonFunction;

/*
 * Listener that will trigger a change in a radio functionality
 */
public class RadioButtonListener extends BaseButtonListener {
	private RadioButtonFunction radioFunction;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	function: the radio functionality set that this listener will belong to
	 */
	public RadioButtonListener(RadioButtonFunction function) {
		this.radioFunction = function;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.radioFunction.switchActivity(evt.getButton());
	}

	@Override
	public String toInfo() {
		return "switches selected value";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

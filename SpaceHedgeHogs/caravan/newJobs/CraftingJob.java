package newJobs;

import java.util.List;

import barChange.BarGain;
import barChange.BarLimiter;
import craftingCenter.CraftingManager;
import eventSuccessFactors.IEventSuccessFactor;
import events.EventTagName;
import gameCharacters.IGameCharacter;
import generalGraphics.ButtonLookType;
import jobs.SkillGain;
import propertyBars.PropertyBarName;
import screenElements.ScreenButton;
import skills.SkillName;

public class CraftingJob extends BaseJob {
	private CraftingManager connectedManager;
	
	public CraftingJob(CraftingManager connectedManager) {
		this.connectedManager = connectedManager;
	}

	@Override
	public void newElementAdded(IGameCharacter newChar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void oldElementRemoved(IGameCharacter oldChar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void safeDelete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void setWorkTags(List<EventTagName> tagList) {
		tagList.add(EventTagName.Crafting);
		
	}

	@Override
	protected void setBarCost(List<BarLimiter> limiterList) {
		limiterList.add(this.mainFactory.getElemFactory().getPropertyBarLimiter(PropertyBarName.Stamina, 20));
		
	}

	@Override
	protected void setBarGains(List<BarGain> gainList) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void setSkillGains(List<SkillGain> gainList) {
		gainList.add(this.mainFactory.getElemFactory().getSkillGain(SkillName.Crafting, 3));
		
	}

	@Override
	protected void addEventSuccessFactors(List<IEventSuccessFactor> factorList) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void socketGoingOnScreenCodeSpace() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void addButtonsToExtraCommandsList(List<ScreenButton> saveLocationList) {
		saveLocationList.add(makeOpenCraftingPageButton());
		
	}
	
	private ScreenButton makeOpenCraftingPageButton() {
		ScreenButton newButton = this.mainFactory.getElemFactory().getScreenButton(0, 9, ButtonLookType.POPUPSELECT, "Open crafting page");
		newButton.addButtonEventListener(this.mainFactory.getElemFactory().getOpenElementOnTopLayerListener(this.connectedManager.getCraftingPage()));
		return newButton;
	}

	@Override
	protected void doExtraCodePerWorkingCharacter(IGameCharacter target) {
		this.connectedManager.addWorkValue(15);
		
	}

}

package mouseControls;

import java.util.ArrayList;
import java.util.List;

import commandAction.CABaseClass;
import commandAction.ICommandAction;
import controllers.ActivityController;
import factories.IAutoMake;
import factories.MainFactory;
import frameworks.IStackable;
import mouseSituationSet.IMouseSituationSet;
import other.StaticFunctions;
import playerActionListener.IPlayerActionListener;
import screenElements.ScreenElement;
import screenManagement.GElementManager;
import screenManagement.IGElementManager;

//Class that possesses the logic for reacting to mouse clicks
public class MouseCommands implements IAutoMake{
	private MainFactory mainFactory;
	private ActivityController selection;
	//private GElementManager gManager;
	private ArrayList<CABaseClass> commandArray;
	private MouseActionConditionSet conditions;
	private ArrayList<Class> searchClasses;
	private ArrayList<IMouseSituationSet> mouseSituations;
	private List<IPlayerActionListener> playerActionListeners;
	private boolean debug = false;
	
	//Constructor Input: screen that will take commands
	public MouseCommands() {
		
	}
	
	//setter for main factory object
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
	}
	
	//Boot up method, meant for starting up the object after creation
	public void bootUp() {
		this.selection = this.mainFactory.getActivityController();
		//this.gManager = this.mainFactory.getGElementManager();
		this.conditions = this.mainFactory.getMouseConditionSet();
		this.commandArray = new ArrayList<CABaseClass>();
		this.mouseSituations = new ArrayList<IMouseSituationSet>();
		this.playerActionListeners = new ArrayList<IPlayerActionListener>();
		addMouseSituations();
	}
	
	/*
	 * Fills up mouse situation list 
	 */
	private void addMouseSituations() {
		this.mouseSituations.add(this.mainFactory.getMSSBattleActiveSocketChar());
		this.mouseSituations.add(this.mainFactory.getMSSBattleActiveSocketItem());
		this.mouseSituations.add(this.mainFactory.getMSSBattleActiveFieldCharMove());
		this.mouseSituations.add(this.mainFactory.getMSSBattleActiveFieldCharAttack());
		this.mouseSituations.add(this.mainFactory.getMSSBattleActiveFieldCharItem());
		this.mouseSituations.add(this.mainFactory.getMSSBattleDefault());
		this.mouseSituations.add(this.mainFactory.getMSSTravelActiveSocketChar());
		this.mouseSituations.add(this.mainFactory.getMSSDefault());
	}
	
	//adds a new player action listener
	public void addPlayerActionListener(IPlayerActionListener newListener) {
		this.playerActionListeners.add(newListener);
	}
	
	//removes a player action listener
	public void removePlayerActionListener(IPlayerActionListener oldListener) {
		this.playerActionListeners.remove(oldListener);
	}
	
	//informs listeners about what mouse button was pressed
	private void informPlayerListenersMouse(int button) {
		for(IPlayerActionListener oneListener : this.playerActionListeners) {
			oneListener.playerMouseActionHappened(button);
		}
	}
	
	/*
	 * Master mouse click command that is called out by the mouse listener
	 * all actions that start from mouse click will come out of this method
	 * Inputs:
	 * 	x: x-coordinate on the screen where the mouse was clicked
	 * 	y: y-coordinate on the screen where the mouse was clicked
	 */
	public void mouseLeftClick(int x, int y) {
		this.mouseSituationLogic(x, y, 1);
		this.informPlayerListenersMouse(1);
	}
	
	/*
	 * Master mouse click command that is called out by the mouse listener
	 * all actions that start from mouse click will come out of this method
	 * Inputs:
	 * 	x: x-coordinate on the screen where the mouse was clicked
	 * 	y: y-coordinate on the screen where the mouse was clicked
	 * 	mouseButton: int value of which mouse button was pressed
	 */
	public void mouseClick(int x, int y, int moustButton) {
		this.mouseSituationLogic(x, y, moustButton);
		this.informPlayerListenersMouse(moustButton);
	}
	
	//Command that will deal with resetting command counters
	public void turnChangeResets() {
		
	}
	
	/*
	 * Logic for reacting to a mouse situation
	 * Inputs:
	 * 	clickX: x coordinate of where the player clicked
	 * 	clickY: y coordinate of where the player clicked
	 * 	mouseButton: int value of which mouse button was pressed
	 */
	private void mouseSituationLogic(int clickX, int clickY, int mouseButton) {
		this.conditions.updateCondition();
		this.conditions.setNewClickCoords(clickX, clickY);
		IMouseSituationSet currentSituation = findMouseSituation();
		currentSituationReacting(currentSituation, clickX, clickY, mouseButton);

	}
	
	/*
	 * Logic that deals with result to the search for MouseSituationSet
	 * Inputs:
	 * 	currentSituation: result of the intial search for a mouse situation logic
	 * 	clickX: x coordinate of where the player clicked
	 * 	clickY: y coordinate of where the player clicked
	 * 	mouseButton: int value of which mouse button was pressed
	 */
	private void currentSituationReacting(IMouseSituationSet currentSituation, int clickX, int clickY, int mouseButton) {
		if(currentSituation == null) {
			StaticFunctions.writeOutDebug(debug, "System didn't find a mouse situation for current situation!");
		}
		else {
			currentSituationFoundReacting(currentSituation, clickX, clickY, mouseButton);
		}
	}
	
	/*
	 * Logic after its certain there is an actual MouseSituationSet
	 * Inputs:
	 * 	currentSituation: result of the intial search for a mouse situation logic
	 * 	clickX: x coordinate of where the player clicked
	 * 	clickY: y coordinate of where the player clicked
	 * 	mouseButton: int value of which mouse button was pressed
	 */
	private void currentSituationFoundReacting(IMouseSituationSet currentSituation, int clickX, int clickY, int mouseButton) {
		StaticFunctions.writeOutDebug(debug, "Current mouse situation: " + currentSituation.getClass());
		findClickedElement(clickX, clickY, currentSituation, mouseButton);
		ICommandAction commandForNow= findCACommand(currentSituation);
		if(commandForNow == null) {
			StaticFunctions.writeOutDebug(debug, "System didn't find a CommandAction for current situation!");
		}
		else {
			commandForNow.fullFillAction();
			StaticFunctions.writeOutDebug(debug, "activated command " + commandForNow.getClass());
		}
	}
	
	/*
	 * Method that performas a search for possible mouse situations
	 */
	private IMouseSituationSet findMouseSituation() {
		for(IMouseSituationSet oneSet : this.mouseSituations) {
			if(oneSet.checkSituation()) {
				return oneSet;
			}
		}
		return null;
	}
	
	/*
	 * Performs a search for acceptable clicked elements
	 * Inputs:
	 * 	clickX: x coordinate of where the player clicked
	 * 	clickY: y coordinate of where the player clicked
	 * 	situationSet: current mouse situation
	 */
	private void findClickedElement(int clickX, int clickY, IMouseSituationSet situationSet, int mouseButton) {
		ArrayList<ScreenElement> result = new ArrayList<ScreenElement>();
		//ArrayList<IStackable> toCheck = this.gManager.getAllClickables();
		List<IStackable> toCheck = this.getAccessToGEManager().getAllElementsFromLayers();

		for(IStackable oneElement : toCheck) {
			oneElement.selfSearch(clickX, clickY, situationSet.getSearchClasses(mouseButton), result);
		}
		if(!result.isEmpty()) {
			this.conditions.setNewClickedItems(result, false, mouseButton);
		}
	}
	
	/*private List<ScreenElement> findClickedElementsForRightClick(int clickX, int clickY, IMouseSituationSet situationSetRight) {
		List<ScreenElement> result = new ArrayList<ScreenElement>();
		for(IStackable oneElement : this.gManager.getAllClickables()) {
			oneElement.selfSearch(clickX, clickY, situationSetRight.getSearchClasses(), result);
		}
		return result;
	}*/
	
	/*
	 * Search for a Command action from the current mouseSituation set, that matches player action
	 * Inputs:
	 * 	situationSet: current mouse situation
	 */
	private ICommandAction findCACommand(IMouseSituationSet situationSet) {
		for(CABaseClass oneCommand : situationSet.getCommands()) {
			if(oneCommand.fullCheck()) {
				return oneCommand;
			}
		}
		return null;
	}
	
	private IGElementManager getAccessToGEManager() {
		return this.mainFactory.getScreen().getCurrentGEManager();
	}
	
	

}

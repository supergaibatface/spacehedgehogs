package itemChange;

import frameworks.SimpleItemFrame;
import sreenElementsSockets.ScreenSimpleItem;

//Listener that listens for itemChangeEvents, is placed on screen versions of those items
public class ScreenInventoryItemUpdateListener extends BaseItemListener {
	protected ScreenSimpleItem item;
	
	//constructor
	public ScreenInventoryItemUpdateListener(ScreenSimpleItem item) {
		this.item = item;
	}
	
	//Method for deleting screen verion of the item, for when inventory is out of this specific item
	private void deleteItemInstance() {
		item.getMaster().removeItemChangeEventListener(this);
		SimpleItemFrame locationFrame = (SimpleItemFrame) item.getWrapper();
		locationFrame.removeElement(item);
		this.item = null;
	}

	@Override
	public void bootUp() {
		
	}

	@Override
	public void changeReacting(ItemChangeEvent evt) {
		if(this.item.getMaster().hasPossesion()) {
			item.updateLabelValue();
		}
		else {
			deleteItemInstance();
		}
		
	}

	@Override
	protected boolean reactCheck(ItemChangeEvent evt) {
		if(!evt.isAmountChanged()) {
			return false;
		}
		return true;
	}

}

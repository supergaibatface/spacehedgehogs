package craftingManagerListeners;

import craftingBluePrints.CraftingProject;
import factories.IAutoMake;
import factories.MainFactory;

public abstract class BaseCraftingManagerListener implements IAutoMake, ICraftingManagerListener {
	protected MainFactory mainFactory;
	
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
	}

	@Override
	public void bootUp() {
		
	}

	@Override
	public void newCraftingProjectAdded(CraftingProject newProject) {
		newCraftingProjectScript(newProject);
	}
	
	protected abstract void newCraftingProjectScript(CraftingProject newProject);
	

	@Override
	public void oldCraftingProjectCancelled(CraftingProject oldProject) {
		oldProjectCancelledScript(oldProject);
	}
	
	protected abstract void oldProjectCancelledScript(CraftingProject oldProject);

	@Override
	public void craftingProjectCompleted(CraftingProject completedProject) {
		craftingProjectCompletedScript(completedProject);
	}
	
	protected abstract void craftingProjectCompletedScript(CraftingProject completedProject);

	@Override
	public void craftingListChanged() {
		craftingListChangedScript();
	}
	
	protected abstract void craftingListChangedScript();

}

package buttonListeners;

import settlementLocation.ISettlementLocation;

public class SettlementLocationListener extends BaseButtonListener {
	private ISettlementLocation location;
	
	public SettlementLocationListener(ISettlementLocation location) {
		this.location = location;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		if(this.location.isAccessable()) {
			this.location.reactToClick();
		}
	}

	@Override
	public String toInfo() {
		return "custom location reaction";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

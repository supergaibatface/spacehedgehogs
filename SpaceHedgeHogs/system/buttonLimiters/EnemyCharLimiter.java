package buttonLimiters;

/*
 * Button limiter that can limit button clicking either when there are enemies or when there are not
 * When value positive limiting is true then button cant be pressed if there are enemies
 * when the value is negative the button cant be pressed when there aren't any enemies
 */
public class EnemyCharLimiter extends BaseButtonLimiter {
	private boolean positiveLimiting;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	notifyOnFailure: if true, a notice is generated when this limiter stops action
	 * 	limitWhenExist: if true buttons action is stopped when there are enemies, if false
	 * 		its stopped when there aren't any enemies
	 */
	public EnemyCharLimiter(boolean notifyOnFailure, boolean instantPopUp, boolean limitWhenExist) {
		super(notifyOnFailure, instantPopUp);
		this.positiveLimiting = limitWhenExist;
	}

	@Override
	public boolean check() {
		if(this.mainFactory.getGameStateController().needsBattle() == positiveLimiting) {
			return false;
		}
		return true;
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doCost() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String reasonForFail() {
		if(this.positiveLimiting) {
			return "there are enemies";
		}
		return "there aren't any enemies";
	}

}

package buttonListeners;

/*
 * ButtonListener that will trigger an update to jobFrame
 */
public class UpdateJobFrameListener extends BaseButtonListener {

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.mainFactory.getJobChoiceController().updateValues();
		
	}

	@Override
	public String toInfo() {
		return null;
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

package gameState;

import java.util.ArrayList;

import central.AICharacterSet;
import dataValues.CharValue;
import screenElements.ScreenButton;
import skills.SkillName;
import view.CutsceneData;
import view.CutsceneDataCollection;
import view.GameView;
import view.IGameView;
import view.ViewName;

public class BattleTutorialState3 extends BaseGameState {

	@Override
	protected void loadInPlayerCharacters(ArrayList<CharValue> listOfChars) {
		CharValue newCharValue = new CharValue("James", "charspot");
		newCharValue.addSavedSkillValue(SkillName.PhysicalForm, 20);
		newCharValue.addSavedSkillValue(SkillName.MeleeCombat, 20);;
		listOfChars.add(newCharValue);
		
		newCharValue = new CharValue("Jack", "charspot");
		newCharValue.addSavedSkillValue(SkillName.PhysicalForm, 20);
		newCharValue.addSavedSkillValue(SkillName.MeleeCombat, 20);;
		listOfChars.add(newCharValue);
		
	}

	@Override
	protected void bootLoadFillViewList() {
		/*GameView newView =  mainFactory.getViewManager().makeAndGetDebugView();
		this.travelView = newView.getName();*/
		this.travelView = ViewName.Debug;
		
	}

	@Override
	protected void loadInBattleViewBase(IGameView viewSaveLocation) {
		//viewSaveLocation = this.mainFactory.getViewManager().makeAndGetThirdTutorialBattleView();
		viewSaveLocation = this.mainFactory.getViewFactory().getBattleTutorial3View();
		this.mainFactory.getViewManager().addNewView(viewSaveLocation);
		
	}
	
	/*@Override
	protected void loadInOtherViews() {
		// TODO Auto-generated method stub
		
	}*/

	@Override
	protected void loadInInventory() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void loadInTravelOptions() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void loadInCaravanJobs() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startGameScript() {
		ArrayList<AICharacterSet> enemies = new ArrayList<AICharacterSet>();
		enemies.add(this.mainFactory.getCharacterManager().getEnemyCharacterSets().get(0));
		enemies.add(this.mainFactory.getCharacterManager().getEnemyCharacterSets().get(0));
		enemies.add(this.mainFactory.getCharacterManager().getEnemyCharacterSets().get(0));
		enemies.add(this.mainFactory.getCharacterManager().getEnemyCharacterSets().get(0));
		this.mainFactory.getGameStateController().addEnemysForAFight(enemies);
		
		this.mainFactory.getGameStateController().setBattleTutorialFlag(true);
		this.mainFactory.getGameStateController().startBattle(this.mainFactory.getBattleMapManager().getTutorialMovementField3(), 1, this.mainFactory.getElemFactory().getTimeSpawnKeepNrOfCharsOnField(2));
		this.mainFactory.getGameStateController().getBattleController().insertSpecialWinPopUp(makeNextModulePopUp());
		
		this.mainFactory.getViewManager().changeToView(ViewName.Battle);
		
		this.mainFactory.getCutsceneBuilder().startACutscene(this.makeStartCutscene());
		
	}

	@Override
	protected void addNextPartButtonListeners(ScreenButton nextPartButton) {
		nextPartButton.addButtonEventListener(this.mainFactory.getElemFactory().getImportAllCharsToModuleListener(this.mainFactory.getTravelTutorialState()));
		nextPartButton.addButtonEventListener(this.mainFactory.getElemFactory().getLoadInGameStateListener(this.mainFactory.getTravelTutorialState()));
		
	}
	
	//Makes tutorial cutscene for the game
	private CutsceneDataCollection makeStartCutscene() {
		CutsceneDataCollection tutorial = this.mainFactory.getElemFactory().getCutsceneDataCollection("Intro2");
		CutsceneData newFrame = new CutsceneData("travelBG", "This is good, you are no longer alone in this hellscape.");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "With enough trusted people at your side, every obstacle can be conquered");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "And so the palace is getting closer");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "There, in front of you... people, people for sure.");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "Its James and Jack more of your lost soldiers, but why are they coming this way?");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "There is no time for plesantries, they are chased. More monsters are coming.");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "They are spread out, use it against them!");
		tutorial.addNewCutScene(newFrame);
		//tutorial.addFinalButtonEventListener(this.mainFactory.getElemFactory().getViewChangeListener(ViewName.Battle));
		return tutorial;
	}

}

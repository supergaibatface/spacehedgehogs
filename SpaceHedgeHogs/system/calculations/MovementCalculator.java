package calculations;

import java.util.ArrayList;

import combatFieldElements.MovementSquare;
import factories.IAutoMake;
import factories.MainFactory;
import generalControls.TravelPath;

//Calculator for making possible paths for characters
public class MovementCalculator implements IAutoMake {
	private MainFactory mainFactory;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	/*
	 * Main method for making an array of paths
	 * Inputs:
	 * 	startSquare: the square from where the character starts moving from
	 * 	travelDistance: the distance that a character can travel
	 */
	public ArrayList<TravelPath> makePossiblePaths(MovementSquare startSquare, int travelDistance) {
		ArrayList<TravelPath> paths = new ArrayList<TravelPath>();
		ArrayList<MovementSquare> reachedSquares = new ArrayList<MovementSquare>();
		TravelPath firstPath = new TravelPath(startSquare);
		reachedSquares.add(startSquare);
		calculateOneStep(firstPath, travelDistance, reachedSquares, paths);
		return paths;
	}
	
	/*
	 * Calculates one possible new step from a square
	 * Inputs:
	 * 	currentPath: the path that possibly got the character here
	 * 	travelDistance: the distance that character can still travel
	 * 	reachedSquares: array of squares the already have paths calculated to them
	 * 	paths: saved paths that have already been calculated
	 */
	public void calculateOneStep(TravelPath currentPath, int travelDistance, ArrayList<MovementSquare> reachedSquares, ArrayList<TravelPath> paths) {
		if(travelDistance > 0) {		
			makeNewPath(currentPath, travelDistance, currentPath.getDestination().getTopSquare(), reachedSquares, paths);
			makeNewPath(currentPath, travelDistance, currentPath.getDestination().getRightSquare(), reachedSquares, paths);
			makeNewPath(currentPath, travelDistance, currentPath.getDestination().getBotSquare(), reachedSquares, paths);
			makeNewPath(currentPath, travelDistance, currentPath.getDestination().getLeftSquare(), reachedSquares, paths);
		}
	}
	
	/*
	 * Method for finalising and new possible path
	 * Inputs:
	 * 	startPath: the path character woulod have used to get to a new square
	 * 	travelDistance: the amount of squares a character could still move
	 * 	nextStep: the square that this new version of the path would take
	 * 	reachedSquares: array of squares that already have a path to them
	 * 	acceptedPaths: array of already saved possible paths
	 */
	private void makeNewPath(TravelPath startPath, int travelDistance, MovementSquare nextStep, ArrayList<MovementSquare> reachedSquares, ArrayList<TravelPath> acceptedPaths) {
		if(nextStep != null) {
			TravelPath newPath = new TravelPath(startPath);
			newPath.addStep(nextStep);
			boolean result = addingNewPath(reachedSquares, newPath, acceptedPaths);
			if(result) {
				calculateOneStep(newPath, travelDistance -1, reachedSquares, acceptedPaths);
			}
		}
	}
	
	/*
	 * Method that saves a new path to accepted paths array
	 * Inputs:
	 * 	reachedSquares: squares that have already been reached with paths
	 * 	pathUnderQuestion: travel path that will be saved
	 * 	acceptedPaths: paths that have already been previously saved
	 */
	private boolean addingNewPath(ArrayList<MovementSquare> reachedSquares, TravelPath pathUnderQuestion, ArrayList<TravelPath> acceptedPaths) {
		if(pathAcceptanceCheck(reachedSquares, pathUnderQuestion)) {
			reachedSquares.add(pathUnderQuestion.getDestination());
			acceptedPaths.add(pathUnderQuestion);
			return true;
		}
		else {
			return false;
		}
	}
	
	/*
	 * Check method that makes sure a path should be saved
	 * Inputs:
	 * 	reachedSquares: squares that already have paths to them
	 * 	pathUnderQuestion: path that might be saved
	 */
	private boolean pathAcceptanceCheck(ArrayList<MovementSquare> reachedSquares, TravelPath pathUnderQuestion) {
		MovementSquare destination = pathUnderQuestion.getDestination();
		if(reachedSquares.contains(destination)) {
			return false;
		}
		if(destination.isOccupied()) {
			return false;
		}
		return true;
	}

}

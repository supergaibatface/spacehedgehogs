package view;

import java.util.ArrayList;

import factories.IAutoMake;
import factories.MainFactory;
import graphicalWrapping.DataBoxWrapper;
import jobChoiceController.JobChoiceController;
import screenElements.ScreenBackground;
import screenElements.ScreenButton;
import screenElements.ScreenDataBox;
import screenElements.ScreenScrollPanel;
import socketStoring.INewSocketBuilder;
import sreenElementsSockets.BaseScrollSocket;
import tabInterface.TabDirectory;
import tabInterface.TabWindow;

//Class that represents one set of unqiue objects that would be used together on one view
public class GameView extends BaseGameView {
	
	//Constructor for the gameview, input is a string name for the view
	public GameView(ViewName name) {
		super(name);
		//this.name = name;
	}
	
	//boot up method, used after constructor
	protected void bootElements() {
		
	}
	
	@Override
	protected String bootGiveBackgroundImageName() {
		return "TestBG";
	}
	
	//Saves a socket builder to this view
	public void addSocketBuilder(INewSocketBuilder oneSocketBuilder) {
		this.newSocketBuildersForTabs.add(oneSocketBuilder);
	}
	
	//Sets a scrollpanel to this view
	public void setAScrollPanel(ScreenScrollPanel newPanel) {
		this.mainScreen = newPanel;
	}
	
	
	//Method for adding new buttons to the view, input is the new button
	public void addButton(ScreenButton newButton) {
		this.buttons.add(newButton);
	}
	
	//Getter for the current game time
	public ScreenDataBox getGameTime() {
		ScreenDataBox timeBox = this.mainFactory.getTurnController().getDataBox();
		timeBox.setCoordinates(gameTimeX, gameTimeY);
		return timeBox;
	}
	
	//setter for the background image of this view
	public void setBackground(String imgName) {
		this.background = this.mainFactory.getElemFactory().getBackground(imgName);
	}
	
	//setter for job choice controller
	/*public void setJobChoiceController(JobChoiceController jobChoiceController) {
		this.jobChoiceController = jobChoiceController;
	}*/
	
	//checks if the view name equals this views name
	public boolean equals(ViewName name) {
		return this.name.equals(name);
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

}

package events;

import java.util.ArrayList;
import java.util.List;

import eventSuccessFactors.IEventSuccessFactor;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;

/*
 * Class that stores all necessary values needed for requesting a work event
 */
public class EventRequest {
	private List<IGameCharacter> characters;
	private List<EventTagName> tags;
	private List<IEventSuccessFactor> successFactors;
	
	//constructor
	public EventRequest() {
		this.makeArrays();
	}
	
	//constructors with event tags
	public EventRequest(List<EventTagName> tags) {
		this.makeArrays();
		this.tags.addAll(tags);
		
	}
	
	//constructor with tags and characters
	public EventRequest(List<IGameCharacter> characters, List<EventTagName> tags) {
		this.makeArrays();
		this.characters.addAll(characters);
		this.tags.addAll(tags);
	}
	
	//adds a character value to the request
	public void addCharacter(IGameCharacter newCharacter) {
		this.characters.add(newCharacter);
	}
	
	//adds a factor that governs the success chance of the event request
	public void addEventSuccessFactor(IEventSuccessFactor newSuccessFactor) {
		this.successFactors.add(newSuccessFactor);
	}
	
	//makes storing arrays for the event request
	private void makeArrays() {
		this.characters = new ArrayList<IGameCharacter>();
		this.tags = new ArrayList<EventTagName>();
		this.successFactors = new ArrayList<IEventSuccessFactor>();
	}

	//get all characters stored in this request
	public List<IGameCharacter> getCharacters() {
		return characters;
	}

	//get all tags assigned to this event request
	public List<EventTagName> getTags() {
		return tags;
	}
	
	//get all success factors assigned to this event request
	public List<IEventSuccessFactor> getSuccessFactors() {
		return this.successFactors;
	}

}

package jobChoiceController;

import java.util.ArrayList;

import jobs.JobName;
import jobs.JobTopic;

//JobTopicController that adds jobs about basic survival
public class BasicSurvivalTopicController extends BaseJobTopicController {

	//constructor
	public BasicSurvivalTopicController(JobTopic newTopic) {
		super(newTopic);
	}

	@Override
	protected ArrayList<Object> makeModifierValuePackage() {
		ArrayList<Object> modifierPackage = new ArrayList<Object>();
		return modifierPackage;
	}

	@Override
	protected void addSockets() {
		prepareANewSocket(2, "LightGreen", "Water search", this.mainFactory.getElemFactory().getWaterSearchJob());
		prepareANewSocket(2, "LightGreen", "Food search", this.mainFactory.getElemFactory().getFoodSearchJob());
		
	}

}

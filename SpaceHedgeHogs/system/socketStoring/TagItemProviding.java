package socketStoring;

import java.util.ArrayList;

import itemBase.IGameItem;
import itemBase.ItemTagName;
import sreenElementsSockets.ScreenSimpleItem;

//Content provider that will provide items that possess certain tag
public class TagItemProviding extends BaseContentProviding<ScreenSimpleItem> {
	private ItemTagName filterTag;
	
	//Constructor
	public TagItemProviding(ItemTagName filterTag) {
		this.filterTag = filterTag;
	}

	@Override
	public ArrayList<ScreenSimpleItem> getArrayOfSupplies() {
		ArrayList<ScreenSimpleItem> items = new ArrayList<ScreenSimpleItem>();
		for (IGameItem oneItem : this.mainFactory.getInventory().filterForTag(this.filterTag)) {
			items.add(this.mainFactory.getElemFactory().getScreenSimpleItem(oneItem, oneItem.getImageName()));
		}
		return items;
	}




}

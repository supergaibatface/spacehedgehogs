package buttonListeners;

import screenElements.ScreenButton;

/*
 * Button listener that will skip current active cutscene
 */
public class SkipCutsceneListener extends BaseButtonListener {

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.mainFactory.getCutsceneBuilder().skipCutscene((ScreenButton) evt.getSource());
	}

	@Override
	public String toInfo() {
		return "Skips cutscene";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

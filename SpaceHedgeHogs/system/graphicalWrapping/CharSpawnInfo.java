package graphicalWrapping;

import central.AICharPatterns;
import central.AICharacterSet;
import combatFieldElements.ControlFaction;
import combatFieldElements.MovementField;
import combatFieldElements.MovementLine;
import combatFieldElements.MovementSquare;
import factories.IAutoMake;
import factories.MainFactory;
import gameCharacters.GameCharacter;

/*
 * Method for the computer to spawn characters on the battlefield
 */
public class CharSpawnInfo implements IAutoMake{
	private MainFactory mainFactory;
	private AICharacterSet thisCharacter;
	private AICharPatterns aiCharPattern;
	private int xBoardCoord;
	private int yBoardCoord;
	
	/*
	 * constructor
	 * Inputs:
	 * 	oneCharater: game character version of the char that will be spawned
	 * 	xBoardLocation: x coordinate on where the char will be spawned
	 * 	yBoardLocation: y coordinate on where the char will be spawned
	 */
	public CharSpawnInfo(AICharacterSet oneCharacter, int xBoardLocation, int yBoardLocation) {
		this.thisCharacter = oneCharacter;
		this.xBoardCoord = xBoardLocation;
		this.yBoardCoord = yBoardLocation;
	}

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	//Getter for the character
	public AICharacterSet getCharacter() {
		return this.thisCharacter;
	}
	
	//getter for the x board coordinate
	public int getBoardXCoord() {
		return this.xBoardCoord;
	}
	
	//getter for the y board coordinate
	public int getBoardYCoord() {
		return this.yBoardCoord;
	}

}

package activityListeners;

import screenElements.ScreenElement;

//Interface for listners that wait for activity change
public interface IActivityListener {
	
	//Method that is called when activated element changes, input is the new element
	public void activityChange(ScreenElement activatedElement);

}

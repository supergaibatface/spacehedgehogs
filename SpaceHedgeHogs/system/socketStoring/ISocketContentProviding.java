package socketStoring;

import java.util.ArrayList;

import screenElements.ScreenElement;

//Interface for socket content providers
public interface ISocketContentProviding<ElementType extends ScreenElement> {
	
	//getter for array of elements that this content provider needs to give
	public ArrayList<ElementType> getArrayOfSupplies();
	
	

}

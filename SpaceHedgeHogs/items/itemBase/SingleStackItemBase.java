package itemBase;

import java.util.List;

import battleItemBase.BattleItemBase;
import screenInfoPage.BaseInfoPage;
import screenInfoPage.ItemInfoPage;

public abstract class SingleStackItemBase extends BaseItem {
	private int supply;
	private ItemInfoPage infoPage;

	public SingleStackItemBase(TradeItemName name) {
		super(name);
	}
	
	@Override
	public void bootUp() {
		super.bootUp();
		supply = 0;
		this.makeItemInfoPage();
	}
	
	//makes an info page for the item
	private void makeItemInfoPage() {
		this.infoPage = this.bootGiveInfoPage();
	}
	
	protected abstract ItemInfoPage bootGiveInfoPage();

	@Override
	public IGameItem makeCopy() {
		return makeCopyElementScript();
	}
	
	private IGameItem makeCopyElementScript() {
		IGameItem newStack = giveBaseClass();
		newStack.addAmount(this.supply);
		return newStack;
	}
	
	protected abstract IGameItem giveBaseClass();

	@Override
	public BaseInfoPage getInfoPage() {
		return this.infoPage;
	}

	@Override
	public void addAmountLogic(int addition) {
		this.supply = this.supply + addition;
		
	}

	@Override
	public boolean removeAmountLogic(int amount) {
		if(canRemoveAmount(amount)) {
			this.supply = this.supply - amount;
			//amountDecreaseEventCalls();
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public IGameItem SplitItem(int amount) {
		int takeValue = 0;
		if(canRemoveAmount(amount)) {
			takeValue = amount;
		}
		else {
			takeValue = this.supply;
		}
		IGameItem newStack = giveBaseClass();
		newStack.addAmount(takeValue);
		this.removeAmount(takeValue);
		return newStack;
	}

	@Override
	public int getSupply() {
		return this.supply;
	}

	@Override
	protected void addExtraLines(List<String> lines) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected boolean mergeLogic(IGameItem item) {
		this.addAmount(item.getSupply());
		return true;
	}

}

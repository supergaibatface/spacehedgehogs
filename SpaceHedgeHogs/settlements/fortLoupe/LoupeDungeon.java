package fortLoupe;

import java.util.ArrayList;
import java.util.List;

import dialogueCharacters.IDialogueCharacter;
import generalGraphics.ButtonLookType;
import screenElements.ScreenButton;
import screenElements.ScreenDataBox;
import screenElements.ScreenPopUp;
import settlementLocation.BaseSettlementLocation;

public class LoupeDungeon extends BaseSettlementLocation {
	private boolean fightFlag = false;

	public LoupeDungeon() {
		super("Loupe dungeon");
	}

	@Override
	protected void clickScript() {
		if(!fightFlag) {
			this.mainFactory.getGElementManager().addPopUp(getFightPlaceHolder());
			fightFlag = true;
		}
		else {
			System.out.println("Out of fight now");
			this.mainFactory.getGElementManager().openElementOnTopLayer(actionDisplayList);
		}
		
	}

	@Override
	protected int giveButtonX() {
		return 700;
	}

	@Override
	protected int giveButtonY() {
		return 100;
	}

	@Override
	protected int giveButtonLength() {
		return 100;
	}

	@Override
	protected int giveButtonHeight() {
		return 100;
	}

	@Override
	protected String giveButtonImgName() {
		return "100X100Mark";
	}

	@Override
	protected String giveButtonLabel() {
		return "Dungeon";
	}

	@Override
	protected void setUpLocationCharacters(List<IDialogueCharacter> addLocation) {
		addLocation.add(this.mainFactory.getNPCFactory().getNPCDocilePrisoner());
		
	}

	@Override
	protected boolean giveStartAccessabilityValue() {
		return false;
	}
	
	private ScreenPopUp getFightPlaceHolder() {
		ScreenPopUp placeholderPopUp = this.mainFactory.getElemFactory().getScreenPopUp("Orange");
		placeholderPopUp.fillDataBoxWithTexts(this.fightPlaceholderLines());
		ScreenButton closeButton = placeholderPopUp.getNewCloseButton(ButtonLookType.POPUPSELECT, "ok");
		placeholderPopUp.addButton(closeButton);
		placeholderPopUp.autoSize();
		
		return placeholderPopUp;
	}
	
	private List<String> fightPlaceholderLines() {
		List<String> lines = new ArrayList<String>();
		lines.add("You just entered a fight");
		lines.add("that has not been implemented yet");
		lines.add("In the future there will be alot of");
		lines.add("awesome stuff here with epic combat!");
		lines.add("but its not here yet, so you win for now.");
		return lines;
	}

}

package factories;

import dialogueCharacters.IDialogueCharacter;
import dialogueFlags.DialogueFlagStand;
import dialogueFlags.IDialogueFlagStand;
import loupeNpcs.LoupeArmoryChief;
import loupeNpcs.LoupeCaptain;
import loupeNpcs.LoupeDocilePrisoner;
import loupeNpcs.LoupeGateGuard;
import npcListeners.EndDialogueQuestNPCListener;
import stepObjective.TalkToNpcObjective;

public class NPCFactory extends BaseMinorFactory {
	private LoupeGateGuard loupeGateGuard;
	private LoupeCaptain loupeCaptain;
	private IDialogueCharacter loupeDocilePrisoner;
	private IDialogueCharacter loupeArmoryChief;
	
	private void makeLoupeGateGuard() {
		this.loupeGateGuard = this.setUpNewElement(new LoupeGateGuard());
	}
	
	public LoupeGateGuard getNPCLoupeGateGuard() {
		if(this.loupeGateGuard == null) {
			this.makeLoupeGateGuard();
		}
		return this.loupeGateGuard;
	}
	
	private void makeLoupeCaptain() {
		this.loupeCaptain = this.setUpNewElement(new LoupeCaptain());
	}
	
	public LoupeCaptain getNPCLoupeCaptain() {
		if(this.loupeCaptain == null) {
			this.makeLoupeCaptain();
		}
		return this.loupeCaptain;
	}
	
	public EndDialogueQuestNPCListener getEndDialogueQuestNPCListener(TalkToNpcObjective targetObjective) {
		return setUpNewElement(new EndDialogueQuestNPCListener(targetObjective));
	}
	
	private void makeDocilePrisoner() {
		this.loupeDocilePrisoner = this.setUpNewElement(new LoupeDocilePrisoner());
	}
	
	public IDialogueCharacter getNPCDocilePrisoner() {
		if(this.loupeDocilePrisoner == null) {
			this.makeDocilePrisoner();
		}
		return this.loupeDocilePrisoner;
	}
	
	public IDialogueFlagStand getNewDialogueFlagStand() {
		return setUpNewElement(new DialogueFlagStand());
	}
	
	private void makeLoupeArmoryChief() {
		this.loupeArmoryChief = this.setUpNewElement(new LoupeArmoryChief());
	}
	
	public IDialogueCharacter getNPCLoupeArmoryChief() {
		if(this.loupeArmoryChief == null) {
			this.makeLoupeArmoryChief();
		}
		return this.loupeArmoryChief;
	}

}

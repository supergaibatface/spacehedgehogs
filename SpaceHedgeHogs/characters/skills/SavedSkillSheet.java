package skills;

import java.util.ArrayList;
import java.util.List;

import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;

//For collecting all the saved skills meant for a specific character
public class SavedSkillSheet {
	private List<SavedSkill> savedSkills;
	
	//constructor
	public SavedSkillSheet() {
		this.savedSkills = new ArrayList<SavedSkill>();
	}
	
	//for adding a new saved skill for this element
	public void addNewSavedSkill(SavedSkill skillValue) {
		this.savedSkills.add(skillValue);
	}
	
	//Calls on setting all saved skill on this sheet on one character
	public void fillSkillsOnCharacter(IGameCharacter character) {
		for(SavedSkill oneSkill : this.savedSkills) {
			oneSkill.fillSkill(character);
		}
	}

}

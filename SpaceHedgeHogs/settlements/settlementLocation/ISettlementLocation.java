package settlementLocation;

import screenElements.ScreenButton;
import settlementLocationListeners.ISettlementLocationListener;

public interface ISettlementLocation {
	
	public void reactToClick();
	
	public boolean isAccessable();
	
	public ScreenButton getLocationButton();
	
	public boolean changeAccessabilityTo(boolean newValue);
	
	public String getName();
	
	public boolean addListener(ISettlementLocationListener newListener);
	
	public boolean removeListener(ISettlementLocationListener oldListener);

}

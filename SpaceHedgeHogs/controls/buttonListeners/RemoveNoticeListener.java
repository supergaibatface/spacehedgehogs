package buttonListeners;

import announcements.Notice;

/*
 * Button listener that will delete a notice on click
 */
public class RemoveNoticeListener extends BaseButtonListener {
	private Notice notice;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	notice: notice that will be deleted on click
	 */
	public RemoveNoticeListener(Notice notice) {
		this.notice = notice;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.mainFactory.getNoticeController().deleteANotice(notice);
		
	}

	@Override
	public String toInfo() {
		return "removes a notice from notice controller";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

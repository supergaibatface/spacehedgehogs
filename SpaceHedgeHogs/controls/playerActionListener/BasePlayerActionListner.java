package playerActionListener;

import factories.IAutoMake;
import factories.MainFactory;

//Base class for player action listeners
public abstract class BasePlayerActionListner implements IAutoMake, IPlayerActionListener {
	protected MainFactory mainFactory;

	@Override
	public abstract boolean playerMouseActionHappened(int mouseClick);

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public abstract void bootUp();
}

package stepObjectiveBase;

import factories.IAutoMake;
import factories.MainFactory;
import questStepBase.IQuestStep;

public abstract class BaseStepObjective implements IAutoMake, IStepObjective {
	protected MainFactory mainFactory;
	protected String objectiveDescription;
	protected IQuestStep masterStep;
	protected boolean checkFlag = false;
	
	public BaseStepObjective(IQuestStep masterStep) {
		this.masterStep = masterStep;
	}

	@Override
	public String getObjectiveDescription() {
		return this.objectiveDescription;
	}
	
	protected abstract String returnObjectiveDescription();

	@Override
	public boolean isCompleted() {
		return this.performCompletionCheck();
	}
	
	protected boolean performCompletionCheck() {
		return this.checkFlag;
	}
	

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.objectiveDescription = this.returnObjectiveDescription();
		makeListeners();
	}
	
	protected void announceChange() {
		this.masterStep.recieveObjectiveUpdate();
	}
	
	protected void setCompletionFlag(boolean value) {
		this.checkFlag = value;
	}
	
	@Override
	public void announceObjectiveCompletion() {
		if(!this.masterStep.isActive()) {
			return;
		}
		setCompletionFlag(true);
		this.announceChange();
	}
	
	protected abstract void makeListeners();

}

package moving;

import java.util.ArrayList;

import calculations.MovementCalculator;
import combatFieldElements.FieldCharacter;
import combatFieldElements.MovementSquare;
import factories.IAutoMake;
import factories.MainFactory;
import generalControls.TravelPath;
import other.StaticFunctions;
import propertyBars.PropertyBarName;

//AI that deals with moving characters on the field
public class MovementAI implements IAutoMake{
	private MainFactory mainFactory;
	private MovementCalculator moveCalc;
	private boolean debug = true;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.moveCalc = this.mainFactory.getMovementCalculator();
		
	}
	
	//Method that finds a way to move closer to a designated targeted character
	public void moveMode(FieldCharacter controlledChar, FieldCharacter target) {
		movementPatternSwitch(controlledChar, target);
	}
	
	//switch for choosing AI movement logic
	private void movementPatternSwitch(FieldCharacter controlledChar, FieldCharacter target) {
		if(!controlledChar.hasAIPatterns()) {
			return;
		}
		switch(controlledChar.getAIPattern().getMovementPattern()) {
		case Standard:
			movingCloser(controlledChar, target);
			break;
		case None:
			noneMoving();
			break;
		}
	}
	
	//logic for moving closer to a specific targeted character
	private void movingCloser(FieldCharacter controlledChar, FieldCharacter target) {
		ArrayList<TravelPath> paths = this.getPaths(controlledChar);
		TravelPath chosenPath = this.findClosestPath(paths, target);
		if(chosenPath != null) {
			this.moveChar(controlledChar, chosenPath);
		}
	}
	
	//logic for empty movement command
	private void noneMoving() {
		return;
	}
	
	
	//Method that actually makes a character moved along designated path
	private void moveChar(FieldCharacter controlledChar, TravelPath path) {
		controlledChar.moveByPath(path);
	}
	
	//Getter for one chars possible move paths
	private ArrayList<TravelPath> getPaths(FieldCharacter controlledChar) {
		MovementSquare startSquare = controlledChar.getLocation();
		ArrayList<TravelPath> paths = this.moveCalc.makePossiblePaths(startSquare, controlledChar.getMasterCharacter().getPropertyBar(PropertyBarName.Movement).getCurrentValue());
		return paths;
	}
	
	//Method that sorts out the best bath from a list of possible options
	private TravelPath findClosestPath(ArrayList<TravelPath> paths, FieldCharacter target) {
		MovementSquare destinationSquare = target.getLocation();
		TravelPath chosenPath = pathChoosing(destinationSquare, paths);
		if(chosenPath == null) {
			StaticFunctions.writeOutDebug(debug, "Couldn't decide on a path for AI char");
			return null;
		}
		return chosenPath;
		
	}
	
	/*
	 * Chooses path that is the closest to the destination square
	 * Inputs:
	 * 	destinationSquare: square that char wants to get next to
	 * 	paths: possible paths the char can move on
	 */
	private TravelPath pathChoosing(MovementSquare destinationSquare, ArrayList<TravelPath> paths) {
		int smallestDistance = 999;
		TravelPath chosenPath = null;
		for(TravelPath onePath : paths) {
			if(onePath.getDestination().getDistanceFrom(destinationSquare) < smallestDistance) {
				chosenPath = onePath;
				smallestDistance = onePath.getDestination().getDistanceFrom(destinationSquare);
			}
		}
		return chosenPath;
	}
	

}

package commandAction;

import java.util.List;

import actionLimiters.CountingLimiter;
import combatFieldElements.MovementSquare;
import controllers.ActivityController;
import factories.IAutoMake;
import factories.MainFactory;
import mouseControls.MouseActionConditionSet;
import other.StaticFunctions;
import screenElements.ScreenElement;
import screenManagement.GElementManager;

//Abstract base class for all command actions (Actions taken by the player)
public abstract class CABaseClass <T extends ScreenElement> implements ICommandAction, IAutoMake {
	protected MainFactory mainFactory;
	protected MouseActionConditionSet conditions;
	protected ActivityController selection;
	protected CountingLimiter limiter;
	protected GElementManager GEManager;
	protected Class<T> clickTargetClass;
	protected int designatedMouseButton;
	
	//Setter for mainFactory element
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	//BootUp, triggered by factory after creation
	@Override
	public void bootUp() {
		this.conditions = mainFactory.getMouseConditionSet();
		this.selection = this.mainFactory.getActivityController();
		this.GEManager = this.mainFactory.getGElementManager();
		this.clickTargetClass = this.setClickTargetClass();
		this.designatedMouseButton = this.giveMouseButtonNr();
	}
	
	//method that has to return what type of mouse button press is this command action tied to
	protected abstract int giveMouseButtonNr();
	
	//shortcut for getting the int value for right mouse click
	protected int giveRightMouseButtonValue() {
		return 3;
	}
	
	//shortcut for getting the in value for left mouse click
	protected int giveLeftMouseButtonValue() {
		return 1;
	}
	
	//Checks if all the conditions for this action have been met
	@Override
	public boolean fullCheck() {
		if(!this.checkForCorrectButton()) {
			return false;
		}
		if(!inputIsCorrect()) {
			return false;
		}
		if(!counterCheck()) {
			return false;
		}
		return true;
	}
	
	//Method for triggering this actions script
	@Override
	public void fullFillAction() {
		if(hasCountLimiter()) {
			this.limiter.countUp();
		}
		actionImplement();
	}
	
	//Abstract method that will cover the implementations of the action
	public abstract void actionImplement();
	
	//Checks if latest mouse click button matches the designated button for this command action
	protected boolean checkForCorrectButton() {
		if(conditions.getLatestMoustButton() == this.designatedMouseButton) {
			return true;
		}
		else {
			return false;
		}
	}
	
	//Checks that there are no active popUps
	protected boolean notOnPopup() {
		if(!conditions.isActivePopup()) {
			return true;
		}
		else {
			return false;
		}
	}
	
	//Checks if there is an active popUp
	protected boolean isOnPopup() {
		return !notOnPopup();
	}
	
	//Checks if this command action has an count limiter set
	public boolean hasCountLimiter() {
		return this.limiter != null;
	}
	
	/*
	 * Sets a action count limit
	 * Inputs:
	 * 	nrOfUsagePerTurn: int value of how many times this action can be triggered
	 */
	public void setCountLimiter(int nrOfUsagePerTurn) {
		this.limiter = new CountingLimiter(nrOfUsagePerTurn);
	}
	
	//Checks if the counter allows for this action to be taken
	public boolean counterCheck() {
		if(hasCountLimiter()) {
			if(!this.limiter.canDoAction()) {
				return false;
			}
		}
		return true;
	}
	
	//Resets the counter limiters count
	public void resetCounter() {
		if(hasCountLimiter()) {
			this.limiter.resetCount();
		}
	}
	
	/*
	 * Method that will return the class of the element that player needs to click on for triggering the command action
	 * if there is no specific click target then this function needs to return null
	 */
	public abstract Class<T> setClickTargetClass();
	
	/*
	 * Method that checks if a specific ScreenElement is a specific subclass
	 * Inputs:
	 * 	element: any screenElement subclass object that will be checked
	 * 	screenElementClass: a class type of object that element will be checked against
	 * Output:
	 * 	true: element is of class input screenElementClass
	 * 	false: element is not of that class
	 */
	protected <T2 extends ScreenElement> boolean screenElementClassMatch(ScreenElement element, Class<T2> screenElementClass) {
		return StaticFunctions.isElementOfClass(element, screenElementClass);
	}
	
	/*
	 * Method for checking is specific square is among movementFields active square list
	 * Inputs:
	 * 	oneSquare: the square that will be checked
	 * Output:
	 * 	true: square is among movementFields active square list
	 * 	false: square is not amont movementFields active square list
	 */
	protected boolean checkSquareFromActiveSquares(MovementSquare oneSquare) {
		if(this.mainFactory.getGameStateController().getBattleController().hasActiveSquares()) {
			if(this.mainFactory.getGameStateController().getBattleController().getActiveSquares().contains(oneSquare)) {
				return true;
			}
		}
		return false;
	}
	
	/*
	 * Extension of screenElementClassMatch that will use clicked item from conditions and tries to match it with
	 * this command actions click target class
	 */
	protected boolean standardClickCheck() {
		if(hasItemTypeClicked() >= 0) {
			return true;
		}
		return false;
	}

	/*
	 * method that gets click check logic for elements fetched from conditions
	 */
	protected int hasItemTypeClicked() {
		return itemTypeClickLogic(conditions.getClickedItems());
	}
	
	/*
	 * Method that looks for click target class element from a list of input screenElements
	 * Inputs:
	 * 	elements: list of elements that were clicked
	 * Output:
	 * 	if found, returns the elements location in the list
	 * 	if not found, returns -1
	 */
	protected int itemTypeClickLogic(List<ScreenElement> elements) {
		for(int i= 0; i < elements.size(); i++) {
			if(this.screenElementClassMatch(elements.get(i), clickTargetClass)) {
				return i;
			}
		}
		return -1;
	}
	
	/*
	 * Checks if there is a set clickTarget Class
	 * Outputs:
	 * 	true: if a click target has a class
	 * 	false if click target class has null value
	 */
	public boolean hasClickTargetClass() {
		return this.clickTargetClass != null;
	}
	
	/*
	 * Getter for ClickTargetClass, can return null
	 */
	public Class<T> getClickTargetClass() {
		return this.clickTargetClass;
	}
	
	//shorthand method for checking if this is a right click command action
	public boolean isRightClickCA() {
		return this.designatedMouseButton == this.giveRightMouseButtonValue();
	}
	
	//shorthand method for checking if this is a left click command action
	public boolean isLeftClickCA() {
		return this.designatedMouseButton == this.giveLeftMouseButtonValue();
	}
	
	//Method for casting an screenElement to clickTargetClass
	protected T castToClickTargetClass(ScreenElement element) {
		T result = null;
		try {
			result = (T) element;
		}
		catch(Exception e) {
			System.out.println("Failed at casting");
		}
		return result;
		
	}

}

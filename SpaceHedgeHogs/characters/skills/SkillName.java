package skills;

//Skill names
public enum SkillName {
	PhysicalForm("Physical form"), 
	MeleeCombat("Melee combat"),
	RangedCombat("Ranged combat"),
	Scouting("Scouting"),
	Crafting("Crafting"),
	BaggageManagement("Baggage management");
	
	private String nameValue;
	
	/*
	 * Construtor
	 * Inputs:
	 * 	nameValue: string name for the skill
	 */
	SkillName(String nameValue) {
		this.nameValue = nameValue;
	}
	
	//toString method
	public String toString() {
		return this.nameValue;
	}
}

package questStepBase;

import java.util.ArrayList;
import java.util.List;

import factories.IAutoMake;
import factories.MainFactory;
import questBase.IQuest;
import questRewardBase.IQuestReward;
import stepObjectiveBase.IStepObjective;

public abstract class BaseQuestStep implements IAutoMake, IQuestStep {
	protected MainFactory mainFactory;
	protected String stepName;
	protected IQuest masterQuest;
	protected List<IStepObjective> objectives;
	protected List<IQuestReward> rewardList;
	protected boolean gaveRewards;
	//protected boolean isActive;
	
	public BaseQuestStep(String name, IQuest masterQuest) {
		this.stepName = name;
		this.masterQuest = masterQuest;
		this.gaveRewards = false;
		//this.isActive = false;
	}

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.objectives = new ArrayList<IStepObjective>();
		this.rewardList = new ArrayList<IQuestReward>();
	}
	
	@Override
	public String getQuestStepName() {
		return this.stepName;
	}
	
	@Override
	public boolean isCompleted() {
		return this.checkListOfObjectives();
	}
	
	public boolean isActive() {
		//return this.isActive;
		return this.findOutActivity();
	}
	
	private boolean findOutActivity() {
		if(!this.masterQuest.isActive()) {
			return false;
		}
		if(this.masterQuest.isCompleted()) {
			return false;
		}
		if(!this.masterQuest.getCurrentQuestStep().equals(this)) {
			return false;
		}
		return true;
	}
	
	/*public void setActive() {
		this.isActive = true;
	}*/
	
	protected boolean checkListOfObjectives() {
		for(IStepObjective oneObjective : this.objectives) {
			if(!oneObjective.isCompleted()) {
				return false;
			}
		}
		return true;
	}
	
	public void addNewStepObjective(IStepObjective newObjective) {
		this.objectives.add(newObjective);
	}
	
	public void recieveObjectiveUpdate() {
		this.sendUpdateAnnouncementToMaster();
		callSelfFinishCheck();
	}
	
	protected void sendUpdateAnnouncementToMaster() {
		this.masterQuest.recieveQuestStepProgressChange(this);
	}
	
	public List<IStepObjective> getObjectives() {
		return new ArrayList<IStepObjective>(this.objectives);
	}
	
	protected void callSelfFinishCheck() {
		if(this.isCompleted()) {
			if(!this.gaveRewards) {
				this.giveRewards();
				this.gaveRewards = true;
			}
		}
	}
	
	protected void giveRewards() {
		for(IQuestReward oneReward : this.rewardList) {
			oneReward.fullFillReward();
		}
	}
	
	@Override
	public void addQuestStepReward(IQuestReward newReward) {
		this.rewardList.add(newReward);
	}
	
	@Override
	public List<IQuestReward> getRewards() {
		return new ArrayList<IQuestReward>(this.rewardList);
	}

}

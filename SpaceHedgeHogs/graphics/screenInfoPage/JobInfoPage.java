package screenInfoPage;

import java.util.List;

import frameworks.TextFrame;
import newJobs.IJob;
import screenElements.ScreenButton;

public class JobInfoPage extends BaseInfoPage {
	private IJob masterJob;

	public JobInfoPage(IJob masterElement) {
		super(1000, 900);
		this.masterJob = masterElement;
	}
	
	public void bootUp() {
		super.bootUp();
		this.setCoordinates(200, 50);
	}

	@Override
	protected void setLocationForCloseButton(ScreenButton closeButton) {
		closeButton.setCoordinates(950, 20);
		
	}

	@Override
	protected void setLocationForNameField(TextFrame nameField) {
		nameField.setLocation(50, 80);
		
	}

	@Override
	protected void writeNameFieldLines(List<String> lines) {
		lines.add("Job info page for " + masterJob.getClass());
		
	}

}

package viewClasses;

import java.util.ArrayList;

import generalGraphics.ButtonLookType;
import propertyBars.PropertyBar;
import propertyBars.PropertyBarName;
import screenElements.ScreenButton;
import screenElements.ScreenDataBox;
import screenElements.ScreenElement;
import screenElements.ScreenScrollPanel;
import view.BaseGameView;
import view.IGameView;
import view.ViewName;

public class BattleTutorial1View extends BaseGameView {

	public BattleTutorial1View() {
		super(ViewName.Battle);
	}

	@Override
	protected String bootGiveBackgroundImageName() {
		return "TestBG";
	}

	@Override
	protected void bootElements() {
		this.bootAssignGameTimeCoords();
		bootAddFieldCharCommandControl();
		this.buttons.add(this.getNextTurnButton());
		bootMakeScrollPanel();
	}
	
	private void bootAssignGameTimeCoords() {
		this.setGameTimeCoord(1600, 20);
	}
	
	//Adds a fieldCharCommandControl to a view
	private void bootAddFieldCharCommandControl() {
		this.addDataBox(getSimpleFieldCharCommandControl(), 1150, 760);
	}
	
	//Getter for a fieldCharCommandControl
	private ScreenDataBox getSimpleFieldCharCommandControl() {
		return this.mainFactory.getFieldCharCommandControl().getDataBox();
	}
	
	private ScreenButton getNextTurnButton() {
		ScreenButton nextTurn = this.mainFactory.getElemFactory().getScreenButton(1400, 800, ButtonLookType.NEXT, "Next Turn");
		nextTurn.addButtonEventListener(this.mainFactory.getElemFactory().getTurnChangeListener());
		nextTurn.addButtonEventListener(this.mainFactory.getElemFactory().getCombatBarValueRefresh(PropertyBarName.Movement));
		nextTurn.addButtonEventListener(this.mainFactory.getElemFactory().getCombatBarValueRefresh(PropertyBarName.Attacks));
		return nextTurn;
	}
	
	//Makes a scrollpanel meant for battleview
	public void bootMakeScrollPanel() {
		ScreenScrollPanel newPanel = this.mainFactory.getElemFactory().getNewScrollPanel(10, 10, 1400, 700, "20Opacity");
		this.mainScreen = newPanel;
		this.buttons.add(newPanel.getAScrollButtonForThisPanel(true, true));
		this.buttons.add(newPanel.getAScrollButtonForThisPanel(true, false));
		this.buttons.add(newPanel.getAScrollButtonForThisPanel(false, true));
		this.buttons.add(newPanel.getAScrollButtonForThisPanel(false, false));
	}
	
	private void setUpVisibilityPart() {
		ScreenButton nextTurnButton = this.buttons.get(0);
		nextTurnButton.makeInvisible();
		ArrayList<ScreenElement> elementsToTurnVisible = new ArrayList<ScreenElement>();
		elementsToTurnVisible.add(nextTurnButton);
		PropertyBar propertyBar = this.mainFactory.getCharacterManager().getCharacters().get(0).getPropertyBar(PropertyBarName.Movement);
		propertyBar.addListener(this.mainFactory.getElemFactory().getTutorialMakeElementsVisibleOnBarValue(0, elementsToTurnVisible, propertyBar));

	}

	@Override
	protected void goingOnScreenScript() {
		setUpVisibilityPart();
		
	}
	
}

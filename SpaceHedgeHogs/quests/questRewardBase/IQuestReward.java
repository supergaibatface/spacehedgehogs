package questRewardBase;

public interface IQuestReward {
	
	public void fullFillReward();
	
	public String getRewardDescription();

}

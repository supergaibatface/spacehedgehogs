package buttonListeners;

import craftingBluePrints.IBlueprint;
import craftingCenter.CraftingInfoPage;

public class ActivateBlueprintListener extends BaseButtonListener {
	private CraftingInfoPage masterPage;
	private IBlueprint blueprintToActivate;
	
	public ActivateBlueprintListener(CraftingInfoPage masterPage, IBlueprint blueprintToActivate) {
		this.masterPage = masterPage;
		this.blueprintToActivate = blueprintToActivate;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.masterPage.activateBlueprint(blueprintToActivate);
		
	}

	@Override
	public String toInfo() {
		return "Selects a saved blueprint on crafting page";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

package events;

import java.util.ArrayList;
import java.util.List;

import factories.IAutoMake;
import factories.MainFactory;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import generalGraphics.ButtonLookType;
import screenElements.ScreenButton;
import screenElements.ScreenPopUp;
import screenElements.ScreenText;

//this class represent one game event that would be displayed by a pop up
public class GameEvent implements IAutoMake{
	private MainFactory mainFactory;
	private String text;
	private ArrayList<EventChoice> choices;
	private ArrayList<EventTagName> tags;
	private int numberOfChars;
	private String eventImgName;
	
	//constructor
	public GameEvent(int numberOfChars) {
		this.numberOfChars = numberOfChars;
	}
	
	//constructor that defaults to 1 participant
	public GameEvent() {
		this.numberOfChars = 1;
	}

	//setter for the mainFactory
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	//Boot up method that sets up a new objected
	@Override
	public void bootUp() {
		this.choices = new ArrayList<EventChoice>();
		this.tags = new ArrayList<EventTagName>();
	}
	
	//method for adding a line of text to the event, input is the string version of that line
	public void addText(String text) {
		this.text = text;
	}
	
	//method for adding an image to the triggered event
	public void addEventImg(String imgName) {
		this.eventImgName = imgName;
	}
	
	//adds choice to the event
	public void addChoice(EventChoice newChoice) {
		this.choices.add(newChoice);
	}
	
	//adds a tag to the event for sorting, input is the tag
	public void addTag(EventTagName tag) {
		this.tags.add(tag);
	}
	
	/*
	 * Checks if the event has the specific tag
	 * Inputs:
	 * 	name: string name of the tag that is looked for
	 * Ouputs:
	 * 	true: event has the tag
	 * 	false: event does not have the tag
	 */
	public boolean hasTag(EventTagName name) {
		for(EventTagName oneTag: this.tags) {
			if(oneTag.equals(name)) {
				return true;
			}
		}
		return false;
	}
	
	//getter for the participant number in this event
	public int getCharAmount() {
		return this.numberOfChars;
	}
	
	//triggers the pop up to appear on screen
	public ScreenPopUp triggerPopUp(IGameCharacter oneChar) {
		List<IGameCharacter> array= new ArrayList<IGameCharacter>();
		array.add(oneChar);
		ScreenPopUp newPopUp = makePopUp(array);
		return newPopUp;
	}
	
	//tiggers a pop up to appear on the screen, for events with multiple chars
	public ScreenPopUp triggerPopUp(List<IGameCharacter> characters) {
		ScreenPopUp newPopUp = makePopUp(characters);
		return newPopUp;
	}
	
	//triggers a pop up to appear on the screen
	public ScreenPopUp triggerPopUp() {
		ScreenPopUp newPopUp = makePopUpWithoutChars();
		return newPopUp;
	}
	
	//make method for the first version of the pop up
	public ScreenPopUp makePopUp(List<IGameCharacter> gameCharacters) {
		ScreenPopUp newPopUp = this.mainFactory.getElemFactory().getScreenPopUp("50red");
		eventImageAdding(newPopUp);
		textAdding(newPopUp, gameCharacters);
		addButtons(newPopUp, gameCharacters);
		newPopUp.updateSize();
		return newPopUp;
	}
	
	//makes a popup without any chars included in it
	public ScreenPopUp makePopUpWithoutChars() {
		ScreenPopUp newPopUp = this.mainFactory.getElemFactory().getScreenPopUp("50red");
		eventImageAdding(newPopUp);
		textAdding(newPopUp);
		addButtons(newPopUp);
		newPopUp.updateSize();
		return newPopUp;
	}
	
	//Checks and adds image to the pop up
	private void eventImageAdding(ScreenPopUp newPopUp) {
		if(hasEventImg()) {
			newPopUp.setEventImage(this.eventImgName);
		}
	}
	
	/*
	 * Adds text to the pop up
	 * Inputs:
	 * 	newPopUp: the pop up that texts will be added to
	 * 	gameCharacters: the list of game characters that triggered the event
	 */
	private void textAdding(ScreenPopUp newPopUp, List<IGameCharacter> gameCharacters) {
		ArrayList<String> lines = this.getLines();
		for(IGameCharacter oneChar: gameCharacters) {
			lines.add(0, "triggered by: " + oneChar.getName());
		}
		newPopUp.fillDataBoxWithTexts(lines);
	}
	
	/*
	 * Adds text to the pop up
	 * Inputs:
	 * 	newPopUp: the pop up that texts will be added to
	 */
	private void textAdding(ScreenPopUp newPopUp) {
		ArrayList<String> lines = this.getLines();
		newPopUp.fillDataBoxWithTexts(lines);
	}
	
	/*
	 * method for adding all the buttons from this event to a pop up
	 * Input:
	 * 	newPopUp: the pop up where the info is added
	 */
	private void addButtons(ScreenPopUp popUp, List<IGameCharacter> characters) {
		for (EventChoice oneChoice : this.choices) {
			oneChoice.makeButton(popUp, characters);
		}
	}
	
	/*
	 * method for adding all the buttons from this event to a pop up
	 * Input:
	 * 	newPopUp: the pop up where the info is added
	 */
	private void addButtons(ScreenPopUp popUp) {
		for (EventChoice oneChoice : this.choices) {
			oneChoice.makeButton(popUp);
		}
	}
	
	//Getter for the events text lines
	private ArrayList<String> getLines() {
		return this.mainFactory.getStringToScreenConv().stringSplitter(text);
	}
	
	//Checks if the event has a image added to it
	public boolean hasEventImg() {
		return this.eventImgName != null;
	}
	
	//Event choice getter method based on nr, can return null if there is no choice for the number
	public EventChoice getEventChoiceByNr(int nr) {
		if(this.choices.size() > nr) {
			return choices.get(nr);
		}
		return null;
	}
	
	/*
	 * Adds new choice for the event and returns the number of that new event
	 * Inputs:
	 * 	newEvent: event that will be triggered after the new choice is selected
	 * 	buttonText: text on the button of the new choice
	 * Output:
	 * 	int: event choice number of the new choice
	 */
	public int addNewChoiceForEvent(GameEvent newEvent, String buttonText) {
		EventChoice newChoice = this.mainFactory.getElemFactory().getEventChoice(this, buttonText);
		newChoice.setResultEvent(newEvent);
		return this.choices.indexOf(newChoice);
	}
	
	/*
	 * Adds new choice for the event and returns the number of that new event
	 * Inputs:
	 * 	buttonText: text on the button of the new choice
	 * Output:
	 * 	int: event choice number of the new choice
	 */
	public int addNewChoiceWithText(String buttonText) {
		EventChoice newChoice = this.mainFactory.getElemFactory().getEventChoice(this, buttonText);
		return this.choices.indexOf(newChoice);
	}
	
	//getter for event text
	public String getText() {
		return this.text;
	}

}

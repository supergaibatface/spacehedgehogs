package barChange;

import factories.IAutoMake;
import factories.MainFactory;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import propertyBars.PropertyBarName;

//Class that can trigger a gain change in a bar
public class BarGain implements IAutoMake {
	private MainFactory mainFactory;
	private PropertyBarName propertyName;
	private int amount;
	
	//if amount is set to 0, it will fill the bar to full
	public BarGain(PropertyBarName name, int amount) {
		this.propertyName = name;
		this.amount = amount;
	}

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	/*
	 * Public command that does this saved gains bar change to a input charters propertybar value
	 */
	public void doGain(IGameCharacter oneChar) {
		if(this.amount > 0) {
			oneChar.getPropertyBar(propertyName).increaseByValue(amount);
		}
		else {
			oneChar.getPropertyBar(propertyName).fillUp();
		}
	}
	

}

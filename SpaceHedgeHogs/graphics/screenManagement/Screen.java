package screenManagement;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import factories.IAutoMake;
import factories.MainFactory;
import frameworks.IStackable;
import mouseControls.ScreenMouseListener;
import other.StaticFunctions;
import screenElements.ScreenElement;
import screenElements.ScreenText;
import timedSystems.SystemTimer;

/*
 * method that extends canvas and represents the screen that everything is drawn on
 * uses graphical element manager for keeping track of what to draw
 */
public class Screen extends JPanel implements IAutoMake{
	private boolean debug = false;
	private MainFactory mainFactory;
	//private GElementManager elementManager;
	private IGElementManager elementManager;
	private Graphics2D g2;
	private SystemTimer timer;
	
	
	//constructor
	public Screen() {
		
	}
	
	//Boot up method, meant for starting up the object after creation
	public void bootUp() {
		this.setSize(this.mainFactory.getLengthAssigner().getScreenLength(), this.mainFactory.getLengthAssigner().getScreenHeight());
		this.setPreferredSize(new Dimension(this.mainFactory.getLengthAssigner().getScreenLength(), this.mainFactory.getLengthAssigner().getScreenHeight()));
		this.elementManager = this.mainFactory.getGElementManager();
		ScreenMouseListener mouse1 = this.mainFactory.getMouseListener();
		this.addMouseListener(mouse1);
		this.timer = this.mainFactory.getSystemTimer();
		StaticFunctions.writeOutDebug(debug, "Screen class has been made");
		this.mainFactory.getGameStateController().bootUpMenu();
	}
	
	public void changeElementManager(IGElementManager newManager) {
		this.elementManager = newManager;
	}
	
	public IGElementManager getCurrentGEManager() {
		return this.elementManager;
	}

	//paint function for drawing items
	//current canvas size (1000,800);
	public void paint(Graphics g) {
		this.g2 = (Graphics2D) g;
		dynamicDraw(g2);
		drawActivityCheck(g2);
	}
	
	/*
	 * Dynamic draw function that takes all elements in without knowing their specific classes beyond screenElement
	 */
	private void dynamicDraw(Graphics2D g2) {
		/*for(ScreenElement oneElement : this.elementManager.getAllDrawables()) {
			this.dealWithIStackable(g2, oneElement);
		}*/
		for(IStackable oneElement : this.elementManager.getAllElementsFromLayers()) {
			this.dealWithIStackable(g2, oneElement);
		}
	}
	
	/*
	 * Dynamic one element draw function that doesn't know their class beyond screenElement
	 */
	private void drawNoTypeElement(Graphics2D g2, ScreenElement element) {
		if(element.getDisplayData().isUsesData()) {
			drawNoTypElementWithDisplayData(g2, element);
		}
		else {
			drawElemOnElem(g2, element);
		}
		
	}
	
	/*
	 * Method for dealing with children elements of an element
	 */
	private void dealWithChildren(Graphics2D g2, IStackable stackable) {
		for(IStackable newElement : stackable.getOnTopElements()) {
			dealWithIStackable(g2, newElement);
		}
	}
	
	/*
	 * Figures out if a stackable element is screenElement and then deals with it
	 */
	private void dealWithIStackable(Graphics2D g2, IStackable element) {
		if(element.isScreenElement()) {
			if(!checkVisibility((ScreenElement) element)) {
				return;
			}
			drawNoTypeElement(g2, (ScreenElement) element);
		}
		this.drawTextsOnElem(g2, element.getOnTopTexts());
		dealWithChildren(g2, element);
		
	}
	
	/*
	 * Draws no type element that uses displayData
	 */
	private void drawNoTypElementWithDisplayData(Graphics2D g2, ScreenElement element) {
		drawNoTypElementWithDisplayData(g2, element, element.getImage());
	}
	
	/*
	 * Draws a no type element that uses displayData and can use a different image
	 */
	private void drawNoTypElementWithDisplayData(Graphics2D g2, ScreenElement element, Image imageToDraw) {
		g2.drawImage(imageToDraw, 
				element.getOffWrapperPointX(element.getDisplayData().getDstx1()), 
				element.getOffWrapperPointY(element.getDisplayData().getDsty1()), 
				element.getOffWrapperPointX(element.getDisplayData().getDstx2()), 
				element.getOffWrapperPointY(element.getDisplayData().getDsty2()),
				element.getDisplayData().getSrcx1(), 
				element.getDisplayData().getSrcy1(),
				element.getDisplayData().getSrcx2(), 
				element.getDisplayData().getSrcy2(),
				null);
	}
	
	//Checks if the element is visibile
	private boolean checkVisibility(ScreenElement element) {
		if(element.isVisible()) {
			return true;
		}
		return false;
	}
	
	//private function for drawing image that shows active char
	private void drawActivityCheck(Graphics2D g2) {
		if(this.mainFactory.getActivityController().getActivityState()) {
			if(this.mainFactory.getActivityController().isActivityInSocket()) {
				drawSimpleScreenCharActivity(g2);
			}
			else {
				drawImgOnElem(g2, this.mainFactory.getScreenSelection(), this.mainFactory.getScreenSelection().getSelectedChar());
			}
			
		}
	}
	
	/*
	 * Method for drawing activity on scimple screen characters
	 */
	private void drawSimpleScreenCharActivity(Graphics2D g2) {
		if(!checkVisibility(this.mainFactory.getScreenSelection().getSelectedChar())) {
			return;
		}
		drawSimpleScreenCharDataBranch(g2, this.mainFactory.getScreenSelection().getSelectedChar());
		
	}
	
	/*
	 * Branch code for choosing between normal draw vs drawing with display data
	 */
	private void drawSimpleScreenCharDataBranch(Graphics2D g2, ScreenElement masterElement) {
		if(masterElement.getDisplayData().isUsesData()) {
			drawNoTypElementWithDisplayData(g2, masterElement, this.mainFactory.getScreenSelection().getImage());
		}
		else {
			drawImgOnElem(g2, this.mainFactory.getScreenSelection(), masterElement);
		}
	}
	
	//BaseDraw for drawing an image that uses wrappers for location
	private void drawImgOnElem(Graphics2D g2, ScreenElement img, ScreenElement onElement) {
		if(img.isImgLoaded()) {
			g2.drawImage(img.getImage(), onElement.getOffWrapperX(), onElement.getOffWrapperY(), null);
		}
	}
	
	//setter for main factory object
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
	}
	
	/*
	 * Method for drawing texts on elem
	 * Inputs:
	 * 	g2: the drawing object
	 * 	texts: an array list of all the texts that have to be drawn
	 */
	private void drawTextsOnElem(Graphics2D g2, List<ScreenText> texts) {
		for(ScreenText oneText : texts) {
			if(oneText.isVisibile()) {
				drawOneTextOnElem(g2, oneText);
			}
		}
	}
	
	
	/*
	 * Method for drawing graphical elements that also considers wrapper elements
	 * Inputs:
	 * 	g2: graphics element that can be drawn through
	 * 	topElement: Element that will be drawn
	 */
	private void drawElemOnElem(Graphics2D g2, ScreenElement topElement) {
		if(topElement.isImgLoaded()) {
			g2.drawImage(topElement.getImage(), topElement.getOffWrapperX(), topElement.getOffWrapperY(), null);
		}
	}
	
	//Method for drawing text on element
	private void drawOneTextOnElem(Graphics2D g2, ScreenText text) {
		changeFont(g2, text.getSize());
		changeColor(g2, text.getColor());
		g2.drawString(text.getText(), text.getOffWrapperX(), text.getOffWrapperY());
	}
	
	//For changing writing font
	private void changeFont(Graphics2D g2, int fontSize) {
		if(!g2.getFont().equals(new Font("Tahoma", Font.PLAIN, fontSize))) {
			g2.setFont(new Font("Tahoma", Font.PLAIN, fontSize));
		}
	}
	
	//method for changing currently used color when drawing elements
	private void changeColor(Graphics2D g2, Color newColor) {
		if(g2.getColor() != newColor) {
			g2.setColor(newColor);
		}
	}
	

}

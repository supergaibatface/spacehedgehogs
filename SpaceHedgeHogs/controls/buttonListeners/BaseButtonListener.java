package buttonListeners;

import factories.IAutoMake;
import factories.MainFactory;

//abstract base for all buttonListeners
public abstract class BaseButtonListener implements IAutoMake, ISButtonListener {
	protected MainFactory mainFactory;

	//Method that is triggered when listener is activated
	@Override
	public abstract void screenButtonEventOccured(ScreenButtonEvent evt);

	//Method that gives info on what the listener does
	@Override
	public abstract String toInfo();

	//Setter for mainFactory
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
	}

	//boot up method, triggered by Factory elements after creation
	@Override
	public abstract void bootUp();

}

package screenInfoPage;

import java.util.ArrayList;
import java.util.List;

import frameworks.TextFrame;
import itemBase.IGameItem;
import screenElements.ScreenButton;
import screenElements.ScreenSoloImage;

//Page for standard items that dont have extra classifications
public class ItemInfoPage extends ItemBasePage {
	protected IGameItem masterItem;

	public ItemInfoPage(IGameItem masterItem) {
		super();
		this.masterItem = masterItem;
	}

	@Override
	protected IGameItem getMasterAsInterface() {
		return this.masterItem;
	}
	

}

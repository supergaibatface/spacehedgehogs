package generalControls;

import java.util.ArrayList;

import combatFieldElements.MovementSquare;
import other.StaticFunctions;

/*
 * Class that represents a possible travel path on a movement field
 * contains an array of squares that will be moved on, first square is the location where char is
 * last square is where the char will end up after finishing with the path
 */
public class TravelPath {
	ArrayList<MovementSquare> path;
	
	//Constructor
	public TravelPath() {
		this.path = new ArrayList<MovementSquare>();
	}
	
	/*
	 * Constructor with startSquare
	 * Inputs:
	 * 	startSquare: the square that the paths will start from
	 */
	public TravelPath(MovementSquare startSquare) {
		this.path = new ArrayList<MovementSquare>();
		path.add(startSquare);
	}
	
	/*
	 * Constructor that expands on a previous path
	 * Inputs:
	 * 	previousPath: path that represents previously steps on the new path
	 */
	public TravelPath(TravelPath previousPath) {
		this.path = StaticFunctions.copyArray(previousPath.getPath());
	}

	/*
	 * Adds a step to this path
	 * Inputs:
	 * 	mewDestination: ads this square as the new final step
	 */
	public void addStep(MovementSquare newDestination) {
		this.path.add(newDestination);
	}
	
	//Getter for that path length
	public int getPathLength() {
		return this.path.size();
	}
	
	//getter for the final square in the path
	public MovementSquare getDestination() {
		if(path.isEmpty()) {
			return null;
		}
		else {
			return path.get(getPathLength() - 1);
		}
	}
	
	//Getter for the paths start square (first square)
	public MovementSquare getStart() {
		if(path.isEmpty()) {
			return null;
		}
		else {
			return path.get(0);
		}
	}
	
	//Getter for the first square that will be moved to (second square in the list)
	public MovementSquare getFirstStep() {
		if(path.size() < 2) {
			return null;
		}
		else {
			return path.get(1);
		}
	}
	
	/*
	 * Updates the path for the case where first step has been taken
	 * Char moved from (0) square to (1) square
	 */
	public void updatePathForFirstStepTaken() {
		if(!path.isEmpty()) {
			this.path.remove(0);
		}
	}
	
	//Getter for the full list of squares saved in this path
	public ArrayList<MovementSquare> getPath() {
		return this.path;
	}

}

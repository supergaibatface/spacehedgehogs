package timedSystems;

import java.util.TimerTask;

import factories.IAutoMake;
import factories.MainFactory;
import screenManagement.Screen;

/*
 * Timer task that will trigger a repaint on the screen
 */
public class RepaintTask extends TimerTask implements IAutoMake{
	private MainFactory mainFactory;
	private Screen screen;

	/*
	 * method that is triggered when the timer reaches the designated moment
	 */
	@Override
	public void run() {
		screen.repaint();
		
	}

	//setter for main factory
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	//bootup method
	@Override
	public void bootUp() {
		this.screen = mainFactory.getScreen();
		
	}

}

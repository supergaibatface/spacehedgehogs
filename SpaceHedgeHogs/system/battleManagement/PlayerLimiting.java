package battleManagement;

import java.util.ArrayList;

import factories.IAutoMake;
import factories.MainFactory;
import screenElements.ScreenDataBox;

//Class that will limit player actions in a battle
public class PlayerLimiting implements IAutoMake {
	private MainFactory mainFactory;
	private ScreenDataBox dataBox;
	private SpawnLimiting spawnLimiter;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.makeDataBox();
		this.mainFactory.getBattleTime().addTimeListener(this.mainFactory.getElemFactory().getPlayerLimiterRefreshListener());
		
	}
	
	/*
	 * For setting a number limit on spawning characters per turn
	 * Inputs:
	 * 	spawnLimiter: spawnLimiting type of object containing necessary values
	 */
	public void setSpawnLimiter(SpawnLimiting spawnLimiter) {
		this.spawnLimiter = spawnLimiter;
	}
	
	//Checks if spawn limiter has been set
	public boolean hasSpawnLimiter() {
		return this.spawnLimiter != null;
	}
	
	//removes currently saved spawn limtier
	public void deleteCurrentSpawnLimiter() {
		this.spawnLimiter = null;
	}
	
	//getter for spawn limiter
	public SpawnLimiting getSpawnLimiter() {
		return this.spawnLimiter;
	}
	
	//makes databox for displaying current player limitation values
	private void makeDataBox() {
		this.dataBox = mainFactory.getElemFactory().getScreenDataBox(0, 0, "LightBlue");
	}
	
	//getter for limitation databox
	public ScreenDataBox getLimiterDataBox() {
		return this.dataBox;
	}
	
	//public method that is called when turn changes
	public void turnChangeRefresh() {
		if(this.hasSpawnLimiter()) {
			this.spawnLimiter.refreshSpawnCount();
		}
		this.writeNewText();
	}
	
	//Text writing method that asks info from every set limiter and writes it to the databox
	public void writeNewText() {
		ArrayList<String> lines = new ArrayList<String>();
		if(this.hasSpawnLimiter()) {
			this.spawnLimiter.addInfoToArray(lines);
		}
		if(!lines.isEmpty()) {
			this.dataBox.fillDataBoxWithTexts(lines);
			this.dataBox.updateSizeForTime();
		}
	}
	

}

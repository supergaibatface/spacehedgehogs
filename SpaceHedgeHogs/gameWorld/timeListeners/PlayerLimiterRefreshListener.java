package timeListeners;

//time listener that refreshes player limiter situations
public class PlayerLimiterRefreshListener extends BaseTimeListener {

	@Override
	public void timeAdvanced(boolean inBattle) {
		if(inBattle) {
			this.mainFactory.getPlayerLimiting().turnChangeRefresh();
		}
	}

}

package jobChoiceController;

import java.util.ArrayList;

import jobs.Job;
import jobs.JobName;
import jobs.JobTopic;
import sreenElementsSockets.ScrollCharSocket;

/*
 * Controller for default jobs that appear all the time
 */
public class ProtoShowDefaultController extends BaseJobTopicController {

	//constructor
	public ProtoShowDefaultController(JobTopic newTopic) {
		super(newTopic);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected ArrayList<Object> makeModifierValuePackage() {
		ArrayList<Object> modifierPackage = new ArrayList<Object>();
		return modifierPackage;
	}

	@Override
	protected void addSockets() {
		prepareANewSocket(2, "LightGreen", "Food search", JobName.SearchFood);
		
		prepareANewSocket(2, "LightGreen", "Supply search", JobName.SearchSupply);
		
		prepareANewSocket(2, "LightGreen", "Path finding", JobName.PathFinding);
	}

}

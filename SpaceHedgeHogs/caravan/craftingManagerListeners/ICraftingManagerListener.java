package craftingManagerListeners;

import craftingBluePrints.CraftingProject;

public interface ICraftingManagerListener {
	
	public void newCraftingProjectAdded(CraftingProject newProject);
	
	public void oldCraftingProjectCancelled(CraftingProject oldProject);
	
	public void craftingProjectCompleted(CraftingProject completedProject);
	
	public void craftingListChanged();

}

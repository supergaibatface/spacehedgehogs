package jobChoiceController;

import java.util.ArrayList;

import jobs.Job;
import jobs.JobName;
import jobs.JobTopic;
import party.PartyStance;
import sreenElementsSockets.ScrollCharSocket;

/*
 * Controller that sets sockets based on current party stance
 */
public class PartyStanceTopicController extends BaseJobTopicController {

	/*
	 * Constructor
	 */
	public PartyStanceTopicController(JobTopic newTopic) {
		super(newTopic);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected ArrayList<Object> makeModifierValuePackage() {
		ArrayList<Object> modifierPackage = new ArrayList<Object>();
		modifierPackage.add(findCurrentPartyStance());
		return modifierPackage;
	}
	
	/*
	 * Finds current party stance
	 */
	private PartyStance findCurrentPartyStance() {
		return this.mainFactory.getParty().getPartyStance();
	}

	@Override
	protected void addSockets() {
		PartyStance partyStance = this.findCurrentPartyStance();
		if(partyStance == PartyStance.Travel) {
			ScrollCharSocket newSocket = this.makeBaseSocket(2, "LightGreen", "First Test");
			Job newJob = this.mainFactory.getJobManager().getTestJob();
			newSocket.setJob(newJob);
			assignedTopic.AddNewSocket(newSocket);
			newSocket = this.makeBaseSocket(2, "LightGreen", "Second Test");
			newJob = this.mainFactory.getJobManager().getTestJob();
			newSocket.setJob(newJob);
			assignedTopic.AddNewSocket(newSocket);
		}
		if(partyStance == PartyStance.Camp) {
			ScrollCharSocket newSocket = this.makeBaseSocket(2, "LightGreen", "Camp Test");
			Job newJob = this.mainFactory.getJobManager().getTestJob();
			newSocket.setJob(newJob);
			assignedTopic.AddNewSocket(newSocket);
		}
		
	}

}

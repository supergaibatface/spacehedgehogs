package buttonListeners;

import land.LandArea;

//Listener that when triggered adds a new land option the player can travel on
public class AddLandAreaListener extends BaseButtonListener {
	private LandArea landArea;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	landArea: the land area that will be added to the travel options
	 */
	public AddLandAreaListener(LandArea landArea) {
		this.landArea = landArea;
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.mainFactory.getTravelManager().addTravelOption(landArea);
		
	}

	@Override
	public String toInfo() {
		return "Adds a new land option to travel on " + landArea.toString();
	}

}

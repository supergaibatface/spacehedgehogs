package generalGraphics;

import java.util.ArrayList;

import factories.IAutoMake;
import factories.MainFactory;
import frameworks.FieldAndValue;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import propertyBars.PropertyBar;
import screenElements.ScreenDataBox;
import skills.Skill;

//Class that manages a databox that contains the info about the currently activated character
public class ActivityInfo implements IAutoMake{
	private MainFactory mainFactory;
	private ScreenDataBox dataBox;
	private FieldAndValue name;
	private ArrayList<FieldAndValue> skills;
	private ArrayList<FieldAndValue> bars;
	
	//constructor
	public ActivityInfo() {
		
	}

	//setter for the main factory
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	//BootUp method used after the constructor
	@Override
	public void bootUp() {
		this.dataBox = this.mainFactory.getElemFactory().getScreenDataBox(0, 0, "LightBlue");
		this.skills = new ArrayList<FieldAndValue>();
		this.bars = new ArrayList<FieldAndValue>();
		setUpTextValues();
		writeValues();
		this.dataBox.updateSizeForTime();
	}
	
	/*
	 * Sets up text values for the first bootup, includes skill names
	 * that wont be changed with the change of activity
	 */
	private void setUpTextValues() {
		this.name = this.mainFactory.getElemFactory().getFieldAndValue("Name");
		for (Skill oneSkill : this.mainFactory.getSkillManager().getSkillList()) {
			this.skills.add(this.mainFactory.getElemFactory().getFieldAndValue(oneSkill.getName().toString()));
		}
		setUpBarNames();
		setInactiveValues();
	}
	
	/*
	 * Adds default string values to properties that represent property bars
	 */
	private void setUpBarNames() {
		for(PropertyBar oneBar: this.mainFactory.getCharacterManager().getPropertyBars()) {
			FieldAndValue newField = this.mainFactory.getElemFactory().getFieldAndValue(oneBar.toString());
			newField.setFieldValue(oneBar.getMinValue() + " / " + oneBar.getMaxValue());
			this.bars.add(newField);
		}
	}
	
	/*
	 * method for setting active values
	 * Input:
	 * 	oneChar: GameCharacter who's info will be displayed as active
	 */
	public void setActiveValues(IGameCharacter oneChar) {
		this.name.setFieldValue(oneChar.getName());
		setActiveSkillValues(oneChar);
		setActiveBarValues(oneChar);
		writeValues();
	}
	
	/*
	 * Method for setting active skill values
	 * Input:
	 * 	oneChar: GameCharater who's skills will be displayed in activity
	 */
	private void setActiveSkillValues(IGameCharacter oneChar) {
		ArrayList<Skill> activeSkills = oneChar.getAllSkill();
		for(int i = 0; i < activeSkills.size(); i++) {
			Skill oneSkill = activeSkills.get(i);
			this.skills.get(i).setFieldValue("level: " + oneSkill.getLevel() + " / experience: " + oneSkill.getExp());
		}
	}
	
	/*
	 * Method for setting active bar values
	 * Input:
	 * 	oneChar: GameCharater who's bar values will be displayed in activity
	 */
	private void setActiveBarValues(IGameCharacter oneChar) {
		ArrayList<PropertyBar> activeBars = oneChar.getAllPropertyBars();
		for(int i = 0; i < activeBars.size(); i++) {
			PropertyBar oneBar = activeBars.get(i);
			this.bars.get(i).setFieldValue(oneBar.getCurrentValue() + " / " + oneBar.getMaxValue());
		}
	}
	
	//method for setting inactive
	public void setInactiveValues() {
		this.name.setFieldValue("");
		setInactiveSkillValues();
		setInactiveBarValues();
		writeValues();
	}
	
	//Method for setting inactive skill values
	private void setInactiveSkillValues() {
		for(FieldAndValue oneValue : skills) {
			oneValue.setFieldValue("");
		}
	}
	
	//Method for setting inactive bar values
	private void setInactiveBarValues() {
		for(FieldAndValue oneValue : bars) {
			oneValue.setFieldValue("");
		}
	}
	
	//method for getting the databox that this object uses to display info
	public ScreenDataBox getDataBox() {
		return this.dataBox;
	}
	
	//method for writing the currently used values onto a ScreenDataBox
	private void writeValues() {
		ArrayList<String> lines = combineText();
		this.dataBox.fillDataBoxWithTexts(lines);
	}
	
	//combines saved values into lines in an array meant for a databox, returns it as ArrayList<String>
	private ArrayList<String> combineText() {
		ArrayList<String> lines = new ArrayList<String>();
		lines.add(name.getFullLine());
		lines = makeBarLines(lines);
		lines = makeSkillLines(lines);
		return lines;
	}
	
	/*
	 * Makes writtable description lines about char skills
	 */
	private ArrayList<String> makeSkillLines(ArrayList<String> lines) {
		for(FieldAndValue oneSkill : this.skills) {
			lines.add(oneSkill.getFullLine());
		}
		return lines;
		
	}
	
	/*
	 * Combines saved bar values into lines in an array
	 * Inputs:
	 * 	lines: array list where the bar value string lines will be added
	 * Output:
	 * 	the input array with the added lines
	 */
	private ArrayList<String> makeBarLines(ArrayList<String> lines) {
		for(FieldAndValue oneBar : this.bars) {
			lines.add(oneBar.getFullLine());
		}
		return lines;
		
	}

}

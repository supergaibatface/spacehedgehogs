package socketStoring;

import factories.IAutoMake;
import factories.MainFactory;
import sreenElementsSockets.BaseScrollSocket;

//Base class for socket builders
public abstract class NewBaseSocketBuilder<ElementType extends BaseScrollSocket> implements INewSocketBuilder<ElementType>, IAutoMake {
	protected MainFactory mainFactory;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public abstract ElementType getFullSocket();

}

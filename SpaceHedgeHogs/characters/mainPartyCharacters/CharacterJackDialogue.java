package mainPartyCharacters;

import dialogueCharacters.BaseDialogueCharacter;
import dialogueFlags.DialogueFlag;
import view.CutsceneData;
import view.CutsceneDataCollection;

public class CharacterJackDialogue extends BaseDialogueCharacter {

	@Override
	protected void getCurrentDialogueInstance() {
		if(this.flagStand.checkFlagState(DialogueFlag.LoupeMainPartyPersonalQuests)) {
			this.mainFactory.getCutsceneBuilder().startACutscene(makeLoupeQuestCutscene());
			this.flagStand.takeDownFlag(DialogueFlag.LoupeMainPartyPersonalQuests);
			return;
		}
		this.mainFactory.getCutsceneBuilder().startACutscene(makeDefaultCutscene());
		
	}

	@Override
	protected boolean hasAllowedDialogue() {
		return true;
	}

	@Override
	protected String giveNameToChar() {
		return "Jack";
	}
	
	private CutsceneDataCollection makeDefaultCutscene() {
		CutsceneDataCollection cusceneBase = this.getNewDialogueBaseCutscene("generic chat");
		CutsceneData newFrame = new CutsceneData("TestBG", "yes, captain?");
		cusceneBase.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "Just wanted to see how are you doing.");
		cusceneBase.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "Just getting used to the new situation captain");
		cusceneBase.addNewCutScene(newFrame);
		return cusceneBase;
	}
	
	private CutsceneDataCollection makeLoupeQuestCutscene() {
		CutsceneDataCollection cusceneBase = this.getNewDialogueBaseCutscene("Jack quest start");
		CutsceneData newFrame = new CutsceneData("TestBG", "yes, captain?");
		cusceneBase.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "What's your take on our situation?");
		cusceneBase.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "The walls of this place are grumbling, this wont keep us safe long term");
		cusceneBase.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "So whats your idea for best course of action.");
		cusceneBase.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "Other then finding a safer location, i think we should find the armory of this place.");
		cusceneBase.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "This place is rundown for certain but a good guess is that they should still have weapons laying around.");
		cusceneBase.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "and you know me, id rather starve to death then go down without a fight.");
		cusceneBase.addNewCutScene(newFrame);
		return cusceneBase;
	}

}

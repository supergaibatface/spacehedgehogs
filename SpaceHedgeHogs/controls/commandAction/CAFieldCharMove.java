package commandAction;

import combatFieldElements.FieldCharacter;
import combatFieldElements.MovementSquare;
import fieldCharCommand.FieldCharCommandStateName;
import generalControls.TravelPath;
import screenElements.ScreenElement;

//Command action for moving a field character on the field
public class CAFieldCharMove extends CABaseClass<MovementSquare> {

	@Override
	public boolean inputIsCorrect() {
		int nr = this.hasItemTypeClicked();
		if(nr<0) {
			return false;
		}
		MovementSquare selectedSquare = this.castToClickTargetClass(conditions.getClickedItem(nr));
		if(!this.checkSquareFromActiveSquares(selectedSquare)) {
			return false;
		}
		return true;
	}

	@Override
	public void actionImplement() {
		MovementSquare destination = this.castToClickTargetClass(conditions.getClickedItem(this.hasItemTypeClicked()));
		TravelPath neededPath = this.mainFactory.getGameStateController().getBattleController().findPathForDestination(destination);
		this.selection.getSelectedCharAsFieldChar().moveByPath(neededPath);
		this.mainFactory.getGameStateController().getBattleController().clearVisuals();
		this.mainFactory.getFieldCharCommandControl().reactivateCurrentSelection();
	}

	@Override
	public Class<MovementSquare> setClickTargetClass() {
		return MovementSquare.class;
	}

	@Override
	protected int giveMouseButtonNr() {
		return this.giveLeftMouseButtonValue();
	}

}

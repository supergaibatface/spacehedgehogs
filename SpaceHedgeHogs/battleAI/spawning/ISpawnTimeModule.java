package spawning;

//interface for modules that control ai spawn timing 
public interface ISpawnTimeModule {
	
	//method that returns true if ai should spawn a character right now
	public boolean doWeSpawn();

}

package buttonLimiters;

import dialogueCharacters.IDialogueCharacter;

public class DialogueElementLimiter extends BaseButtonLimiter {
	private IDialogueCharacter masterCharacter;

	public DialogueElementLimiter(IDialogueCharacter limitingCharacter) {
		super(false, false);
		this.masterCharacter = limitingCharacter;
	}

	@Override
	public boolean check() {
		return this.masterCharacter.canStartDialogue();
	}

	@Override
	public String reasonForFail() {
		return "Dialogue element refuses dialogues";
	}

	@Override
	public void bootUp() {
		
	}

	@Override
	public void doCost() {
		
	}

}

package eventModules;

import events.EventTagName;
import events.GameEvent;
import itemBase.TradeItemName;

public class SettlementWaterSearchEvents extends BaseEventModule {

	@Override
	protected void makeEvents() {
		this.makeFirstSetEvents();
		
	}
	
	private void makeFirstSetEvents() {
		this.makeWellEvent();
		this.makeEasyWaterEvent();
		this.makeNearbyStreamEvent();
		this.makeAnotherWellEvent();
	}
	
	private void makeWellEvent() {
		GameEvent newEvent = this.mainFactory.getElemFactory().getGameEvent(1);
		newEvent.addTag(EventTagName.SearchForWaterSettlement);
		newEvent.addText("Well, well, well, its a well, this was easier then expected.");
		newEvent.addEventImg("100X100Mark");
		this.events.add(newEvent);
		int choiceNr = newEvent.addNewChoiceWithText("Fill up on water");
		newEvent.getEventChoiceByNr(choiceNr).addWaterGainToFull();
	}
	
	private void makeEasyWaterEvent() {
		GameEvent newEvent = this.mainFactory.getElemFactory().getGameEvent(1);
		newEvent.addTag(EventTagName.SearchForWaterSettlement);
		newEvent.addText("Hmm, settlements have easy access to water, it almost feels like cheating");
		newEvent.addEventImg("100X100Mark");
		this.events.add(newEvent);
		int choiceNr = newEvent.addNewChoiceWithText("Get as much as you need");
		newEvent.getEventChoiceByNr(choiceNr).addWaterGainToFull();
	}
	
	private void makeNearbyStreamEvent() {
		GameEvent newEvent = this.mainFactory.getElemFactory().getGameEvent(1);
		newEvent.addTag(EventTagName.SearchForWaterSettlement);
		newEvent.addText("Locals pointed out a nearby stream, and its there, time to get all the water you need");
		newEvent.addEventImg("100X100Mark");
		this.events.add(newEvent);
		int choiceNr = newEvent.addNewChoiceWithText("Let the stream flow, right into the your flasks");
		newEvent.getEventChoiceByNr(choiceNr).addWaterGainToFull();
	}
	
	private void makeAnotherWellEvent() {
		GameEvent newEvent = this.mainFactory.getElemFactory().getGameEvent(1);
		newEvent.addTag(EventTagName.SearchForWaterSettlement);
		newEvent.addText("It sure is nice that people invented wells");
		newEvent.addEventImg("100X100Mark");
		this.events.add(newEvent);
		int choiceNr = newEvent.addNewChoiceWithText("Get your water from the well");
		newEvent.getEventChoiceByNr(choiceNr).addWaterGainToFull();
	}

}

package calculations;

import java.util.ArrayList;

import combatFieldElements.MovementField;
import combatFieldElements.MovementLine;
import combatFieldElements.MovementSquare;
import factories.IAutoMake;
import factories.MainFactory;
import squareFilters.ISquareFilter;

/*
 * Calculator for finding all the squares with a direct distance check
 */
public class RangeCalculator implements IAutoMake {
	private MainFactory mainFactory;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	/*
	 * Main function that will find all the squares in the a certain range radius
	 * Inputs:
	 * 	range: int value range that squares are searched in
	 * 	centerSquare: square where the range search is applied
	 * 	field: field that the calculations are done on
	 * 	filter: possible further filter for checking for squares
	 */
	public ArrayList<MovementSquare> getTargets(int range, MovementSquare centerSquare, MovementField field, ISquareFilter filter) {
		ArrayList<MovementSquare> squares = new ArrayList<MovementSquare>();
		for(MovementLine oneLine : field.getLines()) {
			for(MovementSquare oneSquare : oneLine.getSquares()) {
				oneSquareCalc(oneSquare, centerSquare, range, filter, squares);
			}
		}
		return squares;
	}
	
	/*
	 * Checks if a specific square should be added to the range calculation result list
	 * Inputs:
	 * 	oneSquare: square that is checked
	 * 	centerSquare: square where the range search is applied
	 * 	range: int value range that squares are searched in
	 * 	filter: possible further filter for checking for squares
	 * 	squares: array where found squares will be added
	 */
	private void oneSquareCalc(MovementSquare oneSquare, MovementSquare centerSquare, int range, ISquareFilter filter, ArrayList<MovementSquare> squares) {
		int distance = oneSquare.getDistanceFrom(centerSquare);
		if(distanceCheck(range, distance)) {
			if(filter.filterSquare(oneSquare)) {
				squares.add(oneSquare);
			}
		}
	}
	
	/*
	 * Checks if the range is correct
	 * Inputs:
	 * 	rangeLimit: allowed distance
	 * 	distance: calculated distance
	 */
	private boolean distanceCheck(int rangeLimit, int distance) {
		if(distance > rangeLimit) {
			return false;
		}
		if(distance == 0) {
			return false;
		}
		return true;
	}
	

}

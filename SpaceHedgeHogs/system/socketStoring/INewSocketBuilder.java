package socketStoring;

import sreenElementsSockets.BaseScrollSocket;

//Interface for socket builders
public interface INewSocketBuilder<ElementType extends BaseScrollSocket> {
	
	//getter for the full socket that contains elements provided to it
	public ElementType getFullSocket();

}

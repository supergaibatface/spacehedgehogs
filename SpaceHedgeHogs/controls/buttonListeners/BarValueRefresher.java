package buttonListeners;

import java.util.ArrayList;

import gameCharacters.GameCharacter;
import other.StaticFunctions;
import propertyBars.PropertyBarName;

//Button listener that when triggered fills up the property bar on characters that are listed with the listener
public class BarValueRefresher extends BaseButtonListener {
	private PropertyBarName propertyName;
	private ArrayList<GameCharacter> characters;

	/*
	 * constructor
	 * Inputs:
	 * 	propertyName: property bar name that will be refreshed when triggered
	 * 	characters: ArrayList of characters whos property bars will be refreshed
	 */
	public BarValueRefresher(PropertyBarName propertyName, ArrayList<GameCharacter> characters) {
		this.propertyName = propertyName;
		this.characters = StaticFunctions.copyArray(characters);
	}
	
	//Method for adding characters whos property bars will be refilled when triggered
	public void addCharacter(GameCharacter oneCharacter) {
		this.characters.add(oneCharacter);
	}
	
	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		for(GameCharacter oneChar : this.characters) {
			oneChar.getPropertyBar(propertyName).fillUp();
		}
		
	}

	@Override
	public String toInfo() {
		return "Fills up the " + this.propertyName + " property value on characters";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub	
	}

}

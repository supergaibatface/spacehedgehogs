package screenElements;

import java.util.ArrayList;

import frameworks.IStackable;
import timedSystems.ITimedObject;

/*
 * DataBox that will dissapear after certain time has passed
 */
public class ScreenTimedDataBox extends ScreenDataBox implements ITimedObject {
	private int selfDestructTimer;
	private boolean needsToBeCleared = false;
	private String textValue;

	/*
	 * Constructor
	 * Inputs:
	 * 	textValue: text displayed on this dataBox
	 * 	bottomElem: element this timedataBox is displayed ontop of
	 * 	timeValue: the time this element will be visible for
	 */
	public ScreenTimedDataBox(String textValue, IStackable bottomElem, int timeValue) {
		super(30, 30);
		this.setWrapper(bottomElem);
		this.selfDestructTimer = timeValue;
		this.textValue = textValue;
	}
	
	public void bootUp() {
		super.bootUp();
		this.setSize(20, 20);
		this.xDataStart = 0;
		this.yDataStart = 0;
		this.fillTextFromOneText(textValue);
		this.setUpLifeCycle();
	}
	
	/*
	 * Method for setting up the lifecycle of this element
	 */
	private void setUpLifeCycle() {
		this.mainFactory.getGElementManager().addTimedObject(this);
		this.mainFactory.getSystemTimer().setNewObjectTimerTask(this);
	}

	@Override
	public void countDown() {
		this.countDownLogic();
		
	}
	
	/*
	 * Logic for counting down to this elements deleting
	 */
	private void countDownLogic() {
		if(this.selfDestructTimer > 0) {
			this.selfDestructTimer--;
		}
		else {
			checkForDelete();
		}
	}
	
	/*
	 * Sends this element to the list for deleting
	 */
	private void checkForDelete() {
		this.needsToBeCleared = true;
		this.mainFactory.getGElementManager().removeTimedObject(this);
	}

	/*
	 * Checks if element needs to be deleted
	 */
	@Override
	public boolean checkIfDelete() {
		return this.needsToBeCleared;
	}
	
	/*
	 * Fills text part on timedDataBox from one line
	 */
	private void fillTextFromOneText(String text) {
		ArrayList<String> lines = new ArrayList<String>();
		lines.add(text);
		this.fillDataBoxWithTexts(lines);
	}

}

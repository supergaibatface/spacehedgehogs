package travelGoals;

import events.GameEvent;
import factories.IAutoMake;
import factories.MainFactory;
import other.StaticFunctions;
import screenElements.ScreenPopUp;

//Object that represents a travel goal
public class GoalMarker implements IAutoMake{
	private MainFactory mainFactory;
	private double travelGoal;
	private GameEvent goalEvent;
	private String destinationDescription;
	private boolean showDestination;
	private boolean debug = false;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	goalDistance: double value for how much distance needs to be traveled to reach this goal
	 * 	resultEvent: event that is triggered when the goal is reached
	 */
	public GoalMarker(double goalDistance, GameEvent resultEvent) {
		this.travelGoal = goalDistance;
		this.goalEvent = resultEvent;
	}
	
	/*
	 * Checks if the goal has been reached
	 * Inputs:
	 * 	travelDistance: already traveled distance
	 */
	public boolean hasReachedGoal(double traveledDistance) {
		if(traveledDistance >= this.travelGoal) {
			this.triggerSuccess();
			return true;
		}
		return false;
	}
	
	//Method that is called when the goal is reached
	private void triggerSuccess() {
		StaticFunctions.writeOutDebug(debug, "Reached travel goal");
		ScreenPopUp testPopUp = this.goalEvent.triggerPopUp();
		this.mainFactory.getGElementManager().openElementOnPopupLayer(testPopUp);
		
	}

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.destinationDescription = "";
		this.showDestination = false;
	}

	//getter for destination description
	public String getDestinationDescription() {
		return destinationDescription;
	}

	//setter for destination description
	public void setDestinationDescription(String destinationDescription) {
		this.destinationDescription = destinationDescription;
		this.showDestination = true;
	}
	
	//Checks if this destination should be visible to the player
	public boolean isShowDestination() {
		return this.showDestination;
	}
	
	//getter for this goals distance value
	public double getGoalDistance() {
		return this.travelGoal;
	}

}

package announcements;

import java.util.ArrayList;

import factories.IAutoMake;
import factories.MainFactory;
import generalGraphics.ButtonLookType;
import screenElements.ScreenButton;
import screenElements.ScreenDataBox;

/*
 * Controller class for notices, deals with beringing together a databox where notices are
 * displayed and the actual list of notices
 */
public class NoticeController implements IAutoMake {
	private MainFactory mainFactory;
	private ArrayList<Notice> notices;
	private ScreenDataBox dataBox;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.notices = new ArrayList<Notice>();
		this.dataBox = this.mainFactory.getElemFactory().getScreenDataBox(0, 0, "LightBlue");
		this.dataBox.setVertical(false);
		//Test notice
		//this.addNotice("this is the first notice", false);
		
	}
	
	/*
	 * Makes and adds a new notice for the player from plain text
	 * Inputs:
	 * 	text: text message of the notice
	 */
	public Notice addNotice(String text, boolean autoPopUp) {
		Notice newNotice = this.mainFactory.getElemFactory().getNotice(text);
		this.notices.add(newNotice);
		this.triggerUpdate();
		if(autoPopUp) {
			newNotice.triggerPopUp();
		}
		return newNotice;
	}
	
	//starts an update on the notice controller
	public void triggerUpdate() {
		this.updateTextBox();
	}
	
	//Command for updating the visual databox
	private void updateTextBox() {
		this.refreshButtons();
	}
	
	//refreshes list of notice buttons on the databox
	private void refreshButtons() {
		this.dataBox.hasButtons();
		if(this.dataBox.hasButtons()) {
			this.dataBox.removeAllButtons();
		}
		this.makeAllButtons();
	}
	
	//Makes buttons from all the notices that can be used to open specific notices
	private void makeAllButtons() {
		for(Notice oneNotice : this.notices) {
			ScreenButton newButton = makeButtonForNotice(oneNotice);
			this.dataBox.addButton(newButton);
		}
	}
	
	//Makes one button for a specific notice, the button, when clicked, opens the notice as a popup
	private ScreenButton makeButtonForNotice(Notice oneNotice) {
		ScreenButton newButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.NOTICE, "N");
		newButton.addButtonEventListener(this.mainFactory.getElemFactory().getOpenNoticeButtonListener(oneNotice));
		return newButton;
	}
	
	//getter for the databox that lists all the notices
	public ScreenDataBox getDataBox() {
		return this.dataBox;
	}
	
	//Deletes all notices listed in this controller
	public void clearAllNotices() {
		this.notices.clear();
		this.triggerUpdate();
	}
	
	//Deletes a specific notices in this controller
	public void deleteANotice(Notice notice) {
		this.notices.remove(notice);
		this.triggerUpdate();
	}
	
	

}

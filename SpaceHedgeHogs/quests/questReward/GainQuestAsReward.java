package questReward;

import questBase.IQuest;
import questRewardBase.BaseQuestReward;

public class GainQuestAsReward extends BaseQuestReward {
	private IQuest rewardQuest;
	
	public GainQuestAsReward(IQuest rewardQuest) {
		this.rewardQuest = rewardQuest;
	}

	@Override
	public String getRewardDescription() {
		return "Starts \""+rewardQuest.getName()+"\" quest.";
	}

	@Override
	protected void rewardScript() {
		this.mainFactory.getQuestFactory().getQuestManager().addNewQuest(rewardQuest);
		
	}

}

package mouseSituationSet;

import screenElements.ScreenElement;
import sreenElementsSockets.ScreenSimpleItem;

//Represents battle view with an active item in a socket
public class MSSBattleActiveSocketItem extends BaseMouseSituationSet {

	@Override
	protected void makeAllCAElements() {
		this.addNewCAElement(this.mainFactory.getCAActivateSelectedItem());
		this.addNewCAElement(this.mainFactory.getCACharDeselect());
		
	}
	
	@Override
	public boolean checkSituation() {
		if(!this.mainFactory.getGameStateController().inBattle()) {
			return false;
		}
		ScreenElement selectedItem = conditions.getSelectedItem();
		if(selectedItem == null) {
			return false;
		}
		if(!selectedItem.getClass().equals(ScreenSimpleItem.class)) {
			return false;
		}
		return true;
	}

}

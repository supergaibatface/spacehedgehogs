package mouseSituationSet;

import screenElements.ScreenElement;
import sreenElementsSockets.ScreenSimpleChar;

//Represents battle view with an active char in a socket
public class MSSBattleActiveSocketChar extends BaseMouseSituationSet{

	@Override
	protected void makeAllCAElements() {
		this.addNewCAElement(this.mainFactory.getCACharSpawn());
		this.addNewCAElement(this.mainFactory.getCACharDeselect());
		
	}

	@Override
	public boolean checkSituation() {
		if(!this.mainFactory.getGameStateController().inBattle()) {
			return false;
		}
		ScreenElement selectedItem = conditions.getSelectedItem();
		if(selectedItem == null) {
			return false;
		}
		if(!selectedItem.getClass().equals(ScreenSimpleChar.class)) {
			return false;
		}
		return true;
	}

}

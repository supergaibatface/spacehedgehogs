package itemExceptions;

public class CantStartCraftingProjectException extends Exception {
	
	public CantStartCraftingProjectException(String craftingItemName) {
		super("Cant start crafting project for "+craftingItemName);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}

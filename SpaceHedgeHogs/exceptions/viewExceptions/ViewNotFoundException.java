package viewExceptions;

import view.ViewName;

public class ViewNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5624187158865760764L;
	
	public ViewNotFoundException(ViewName name) {
		super("Didn't find view named: "+name);
	}

}

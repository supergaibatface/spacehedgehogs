package jobs;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import barChange.BarLimiter;
import events.EventManager;
import events.EventRequest;
import events.EventTagName;
import events.GameEvent;
import factories.IAutoMake;
import factories.MainFactory;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import generalGraphics.ButtonLookType;
import newJobs.IJob;
import propertyBars.PropertyBarName;
import screenElements.ScreenButton;
import skills.Skill;
import skills.SkillName;
import sreenElementsSockets.ScrollCharSocket;

//Class that represents a job in game that a character can be assigned to
public class Job implements IAutoMake, IJob{
	private JobName name;
	private MainFactory mainFactory;
	private ArrayList<EventTagName> workTags;
	private EventManager eventManager;
	private ArrayList<BarLimiter> barRequirments;
	private ArrayList<BarLimiter> costBar;
	private ArrayList<SkillGain> skillGains;
	
	
	//constructor
	public Job(JobName name) {
		this.name = name;
	}
	
	//bootup method for the object
	@Override
	public void bootUp() {
		this.workTags = new ArrayList<EventTagName>();
		this.eventManager = this.mainFactory.getEventManager();
		this.barRequirments = new ArrayList<BarLimiter>();
		this.costBar = new ArrayList<BarLimiter>();
		this.skillGains = new ArrayList<SkillGain>();
	}
	
	/*
	 * Does a full check if an assigned character matches all character and item requirements to do this job
	 * Input:
	 * 	gameChar: game character that will be checked
	 */
	public boolean fullReqCheck(IGameCharacter gameChar) {
		if(!this.matchesCharacterRequirements(gameChar)) {
			return false;
		}
		if(!this.matchesItemRequirements()) {
			return false;
		}
		return true;
	}
	
	//Checks if character matches character based job requirements 
	private boolean matchesCharacterRequirements(IGameCharacter gameChar) {
		//Bar limiting
		for(BarLimiter oneLimiter : this.barRequirments) {
			if(!oneLimiter.checkChar(gameChar)) {
				return false;
			}
		}
		return true;
	}
	
	//Checks if player matches the item requirements of doing this job
	private boolean matchesItemRequirements() {
		
		return true;
	}
	
	//Method that enacts job requirement prices for doing it
	public void doResults(IGameCharacter target) {
		doBarCosts(target);
		doSkillResults(target);
	}
	
	//Does required bar costs on a char
	private void doBarCosts(IGameCharacter target) {
		for(BarLimiter oneBar : this.costBar) {
			oneBar.doCost(target);
		}
	}
	
	//Assigns new skill gains to a character
	private void doSkillResults(IGameCharacter target) {
		for(SkillGain oneSkillGain : this.skillGains) {
			oneSkillGain.giveExp(target);
		}
	}
	
	//Method for making a list of characters assigned to a job work through it
	public void workThrough(List<IGameCharacter> characters) {
		for(IGameCharacter oneChar: characters) {
			doResults(oneChar);
		}
		this.sendEventRequest(characters);
		//getEvents(characters);
	}
	
	private void sendEventRequest(List<IGameCharacter> characters) {
		if(characters.isEmpty()) {
			return;
		}
		this.mainFactory.getEventCaller().sendEventRequest(new EventRequest(characters, this.workTags));
	}
	
	/*
	 * Method for adding a new skill gain to this job
	 * Inputs:
	 * 	skillName: name of the skill that will be advanced
	 * 	expValue: how much exp for this skill is given for one time doing this job
	 */
	public void addNewSkillGain(SkillName skillName, int expValue) {
		this.skillGains.add(this.mainFactory.getElemFactory().getSkillGain(skillName, expValue));
	}
	
	//setter for the main factory
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}
	
	//setter for the event tag this job can trigger
	public void addTag(EventTagName workTag) {
		this.workTags.add(workTag);
	}
	
	//comparing method that compares the jobs name
	public boolean equals(JobName name) {
		return this.name.equals(name);
	}
	
	//Checks if there has been a stamine limiter assigned to this job
	public boolean hasStamineLimiter() {
		for(BarLimiter oneBar : this.barRequirments) {
			if(oneBar.getLimitedBarName() == PropertyBarName.Stamina) {
				return true;
			}
		}
		return false;
	}
	
	//Getter for the stamina limiter
	public BarLimiter getStaminaLimiter() {
		for(BarLimiter oneBar : this.barRequirments) {
			if(oneBar.getLimitedBarName() == PropertyBarName.Stamina) {
				return oneBar;
			}
		}
		return null;
	}
	
	//setting a bar limiter on this job that will be taken from a character once they perform this job
	public void setBarLimiterWithCost(PropertyBarName propertyName, int amount) {
		BarLimiter newLimiter= this.mainFactory.getElemFactory().getPropertyBarLimiter(propertyName, amount);
		this.barRequirments.add(newLimiter);
		this.costBar.add(newLimiter);
	}
	
	//Sets the stamine limiter for this job
	public void setStaminaLimiter(int amount) {
		this.setBarLimiterWithCost(PropertyBarName.Stamina, amount);
	}

	@Override
	public void newElementAdded(IGameCharacter newChar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void oldElementRemoved(IGameCharacter oldChar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void registerSocketForJob(ScrollCharSocket socket) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void safeDelete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void socketGoingOnScreenScriptPoint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<ScreenButton> getExtraCommandButtons() {
		return new ArrayList<ScreenButton>();
	}

}

package activityListeners;

import combatFieldElements.FieldCharacter;
import jobPanels.IJobPanel;
import sreenElementsSockets.ScreenSimpleChar;
import sreenElementsSockets.ScreenSimpleItem;

//Listener that will paint job sockets in a job frame as available or unavailable by who is selected
public class JobFrameVisualListener extends BaseActivityListener {
	private IJobPanel savedFrame;
	
	//construtor, input is jobframe that is commanded by the listener
	public JobFrameVisualListener(IJobPanel frame) {
		this.savedFrame = frame;
	}

	@Override
	protected void deselected() {
		this.savedFrame.callCleanHighlights();
		
	}

	@Override
	protected void screenSimpleCharActivated(ScreenSimpleChar activatedChar) {
		this.savedFrame.callSocketHighlightUpdate();
		
	}

	@Override
	protected void screenSimpleItemActivated(ScreenSimpleItem activateditem) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void fieldCharActivated(FieldCharacter activatedChar) {
		// TODO Auto-generated method stub
		
	}

}

package screenElements;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;

import factories.IAutoMake;
import factories.MainFactory;
import frameworks.IStackable;
import generalGraphics.ClickBox;
import other.StaticFunctions;

//Method for representing a string on the screen
public class ScreenText implements IAutoMake{
	private MainFactory mainFactory;
	private int x;
	private int y;
	private int size;
	private Color color;
	private String text;
	private IStackable wrapper;
	private FontMetrics fontMetrics;
	private boolean isVisibile;
	private boolean forceNonScroll;
	private boolean debug = false;
	
	/*
	 * constructor
	 * Inputs:
	 * 	x: x coordinate for the text (bottom left corner of the box that the text is in)
	 * 	y: y coordinate for the text (bottom left corner of the box that the text is in)
	 * 	text: String representation of what will be written on the screen
	 */
	public ScreenText(int x, int y, String text) {
		setCoordinates(x, y);
		this.text = text;
	}
	
	//constructor with only the text value, sets default location of (0,0)
	public ScreenText(String text) {
		setCoordinates(0,0);
		this.text = text;
	}
	
	//Getter method for font metrics used on this Screentext
	private void getFontMetrics() {
		this.fontMetrics = this.mainFactory.getScreen().getFontMetrics(new Font("Tahoma", Font.PLAIN, this.size));
	}
	
	//Setter for the font size of this text
	public void setSize(int size) {
		this.size = size;
		getFontMetrics();
	}
	
	//Setter method for the written text, input: text in a string format
	public void setText(String newText) {
		this.text = newText;
		this.setDefaultColor();
	}
	
	/*
	 * Setter for the location of the text box
	 * Inputs:
	 *  x: x coordinate for the text (bottom left corner of the box that the text is in)
	 * 	y: y coordinate for the text (bottom left corner of the box that the text is in)
	 */
	public void setCoordinates(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	//setter for the wrapper element, input screen element that the text is located on
	public void setWrapper(IStackable wrapperElem) {
		this.wrapper = wrapperElem;
	}
	
	//Getter for the x coordinate for the text
	public int getX() {
		return this.x;
	}
	
	//Getter for the y coordinate of the text
	public int getY() {
		return this.y;
	}
	
	//Getter for the string form of the text value
	public String getText() {
		return this.text;
	}
	
	/*
	 * Method that checks the visibility of this text
	 * Inputs: 
	 * 	visibleBox: area that is visible on the same coordinate system as this element
	 */
	public boolean checkVisibility(ClickBox visibleBox) {
		checkVisibilityDebugLines(visibleBox);
		if(!this.checkYVisibility(visibleBox)) {
			this.setVisibile(false);
			return false;
		}
		if(!this.checkXVisibility(visibleBox)) {
			this.setVisibile(false);
			return false;
		}
		this.setVisibile(true);
		return true;
	}
	
	private void checkVisibilityDebugLines(ClickBox visibleBox) {
		StaticFunctions.writeOutDebug(this.debug, "ForOverlap got box: ");
		StaticFunctions.writeOutDebug(this.debug, "VisibleBox topY: "+visibleBox.getTopY());
		StaticFunctions.writeOutDebug(this.debug, "VisibleBox botY: "+visibleBox.getBottomY());
		StaticFunctions.writeOutDebug(this.debug, "VisibleBox rightX: "+visibleBox.getRightX());
		StaticFunctions.writeOutDebug(this.debug, "VisibleBox leftX: "+visibleBox.getLeftX());
	}
	
	
	/*
	 * Method that checks the Y coordinate visibility of this text
	 * Inputs: 
	 * 	visibleBox: area that is visible on the same coordinate system as this element
	 */
	private boolean checkYVisibility(ClickBox visibleBox) {
		int topLine = visibleBox.getTopY();
		int botLine = visibleBox.getBottomY();
		int nameTop = this.getY() - this.getOnScreenHeight();
		int nameBot = this.getY();
		if(topLine <= nameTop && botLine >= nameBot) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/*
	 * Method that checks the X coordinate visibility of this text
	 * Inputs: 
	 * 	visibleBox: area that is visible on the same coordinate system as this element
	 */
	private boolean checkXVisibility(ClickBox visibleBox) {
		int rightLine = visibleBox.getRightX();
		int leftLine = visibleBox.getLeftX();
		int nameRight = this.getX() + this.getOnScreenLength();
		int nameLeft = this.getX();
		if(leftLine <= nameLeft && rightLine >= nameRight) {
			return true;
		}
		else {
			//StaticFunctions.writeOutDebug(this.debug, "failed X visibilityCheck on: "+this.text);
			return false;
		}
	}
	
	/*
	 * Saves a hard coded visibility value
	 */
	public void setVisibile(boolean value) {
		this.isVisibile = value;
	}
	
	/*
	 * Asks for the hard coded visiblity value
	 */
	public boolean isVisibile() {
		return this.isVisibile;
	}
	
	//getter for master x coordinate by using the wrapper system
	public int getOffWrapperX() {
		if(wrapper != null) {
			return forceScrollCheck(wrapper.getOffWrapperX() + wrapper.getXDataStart() + this.getX(), wrapper.getXScroll());
		}
		else {
			return this.getX();
		}
	}
	
	//getter for master y coordinate by using the wrapper system
	public int getOffWrapperY() {
		if(wrapper != null) {
			return forceScrollCheck(wrapper.getOffWrapperY() + wrapper.getYDataStart() + this.getY(), wrapper.getYScroll());
		}
		else {
			return this.getY();
		}
	}
	
	/*
	 * Method that returns real coordinate value depending if the forceNonScroll flag is set
	 * Inputs:
	 * 	coordinate: normal coordinate calculation value (has scroll value added already)
	 * 	wrapperScrollValue: scroll value of the wrapper element
	 */
	private int forceScrollCheck(int coordinate, int wrapperScrollValue) {
		if(!this.forceNonScroll) {
			return coordinate - wrapperScrollValue;
		}
		else {
			return coordinate;
		}
	}

	//setter for main factory
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	//bootup method
	@Override
	public void bootUp() {
		this.setSize(this.mainFactory.getLengthAssigner().getOneTextSize());
		this.setDefaultColor();
		this.forceNonScroll = false;
		this.isVisibile = true;
	}
	
	//setter for text defauly colour value
	private void setDefaultColor() {
		if(Color.black != this.color) {
			this.color = Color.black;
		}
	}
	
	//getter for the font size of this element
	public int getSize() {
		return this.size;
	}
	
	//Getter for the length of this element on screen
	public int getOnScreenLength() {
		if(this.fontMetrics != null) {
			return fontMetrics.stringWidth(text);
		}
		else {
			return 0;
		}
	}
	
	//Getter for the height of this element on screen
	public int getOnScreenHeight() {
		if(this.fontMetrics != null) {
			return this.fontMetrics.getAscent();
		}
		else {
			return 0;
		}
	}

	//getter for text color
	public Color getColor() {
		return color;
	}

	//setter for text color
	public void setColor(Color color) {
		this.color = color;
	}

	//checks if forceNonScroll has been set
	public boolean isForceNonScroll() {
		return forceNonScroll;
	}

	//setter for forceNonScroll flag
	public void setForceNonScroll(boolean forceNonScroll) {
		this.forceNonScroll = forceNonScroll;
	}

	//setter for debug value
	public void setDebug(boolean debug) {
		this.debug = debug;
	}

}

package timeListeners;

//Interface for time listeners
public interface ITimeListener {
	
	//method that is triggered when time (turn) advances
	public void timeAdvanced(boolean inBattle);

}

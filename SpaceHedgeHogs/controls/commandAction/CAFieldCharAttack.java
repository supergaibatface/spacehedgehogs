package commandAction;

import calculations.FightCalc;
import combatFieldElements.FieldCharacter;
import combatFieldElements.MovementSquare;
import fieldCharCommand.FieldCharCommandStateName;
import screenElements.ScreenElement;
import screenManagement.GElementManager;

//Command action for attacking an enemy char
public class CAFieldCharAttack extends CABaseClass<FieldCharacter> {
	private FightCalc fightCalculator;

	@Override
	public void bootUp() {
		super.bootUp();
		this.fightCalculator = this.mainFactory.getFightCalculator();
	}

	@Override
	public boolean inputIsCorrect() {
		int nr = this.hasItemTypeClicked();
		if(nr<0) {
			return false;
		}
		MovementSquare clickedSquare = this.castToClickTargetClass(conditions.getClickedItem(nr)).getLocation();
		if(!this.checkSquareFromActiveSquares(clickedSquare)) {
			return false;
		}
		return true;
	}

	@Override
	public void actionImplement() {
		FieldCharacter attacker = (FieldCharacter) conditions.getSelectedItem();
		FieldCharacter defender = (FieldCharacter) conditions.getClickedItem(this.hasItemTypeClicked());
		int distance = attacker.getLocation().getDistanceFrom(defender.getLocation());
		this.fightCalculator.throwIntoBattle(attacker, defender, distance);
		this.mainFactory.getGameStateController().getBattleController().checkForWin();
		this.mainFactory.getGameStateController().getBattleController().clearVisuals();
		this.mainFactory.getFieldCharCommandControl().reactivateCurrentSelection();
		
		
	}

	@Override
	public Class<FieldCharacter> setClickTargetClass() {
		return FieldCharacter.class;
	}

	@Override
	protected int giveMouseButtonNr() {
		return this.giveLeftMouseButtonValue();
	}

}

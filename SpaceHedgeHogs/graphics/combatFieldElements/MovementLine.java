package combatFieldElements;

import java.util.ArrayList;
import java.util.List;

import frameworks.BaseFrame;
import frameworks.IStackable;
import screenElements.ScreenElement;
import screenElements.ScreenText;

//Object that represents a line of squares in a movementField
public class MovementLine extends BaseFrame {
	private int lineNr;
	private ArrayList<MovementSquare> squares;

	/*
	 * constructor
	 * Inputs:
	 * 	wrapper: IStackable element that this element is placed on
	 * 	lineNr: nr that represents the location of the line
	 */
	public MovementLine(IStackable wrapper, int lineNr) {
		super(wrapper);
		this.lineNr = lineNr;
	}

	@Override
	public void bootUp() {
		super.bootUp();
		this.setLocation(0, lineNr);
		this.squares = new ArrayList<MovementSquare>();
		
	}
	
	/*
	 * Method for making a new square in the line
	 * Inputs:
	 * 	locationNr: location of the square in the line
	 * 	size: visual size of the square
	 * 	image: string name of the squares picture
	 * 	betweenSize: visual space between two squares
	 */
	private void makeNewSquare(int locationNr, int size, String image, int betweenSize) {
		MovementSquare newSquare = this.mainFactory.getElemFactory().getMovementSquare(locationNr, size, image);
		newSquare.setCoordinates(calculateX(locationNr, size, betweenSize), 0);
		newSquare.setWrapper(this);
		this.squares.add(newSquare);
	}
	
	/*
	 * Method for making an amount of squares
	 * Inputs:
	 * 	nr: the amount of squares to be made
	 * 	size: visual size of one square
	 * 	image: the name of the image used for the square
	 * 	betweenSize: visual size of the space between each square
	 */
	public void makeNrOfSquares(int nr, int size, String image, int betweenSize) {
		for(int i = 0; i < nr; i++) {
			makeNewSquare(i, size, image, betweenSize);
		}
	}
	
	/*
	 * Method for making a line of squares between two x coordinates
	 * Inputs:
	 * 	fromX: x coordiante where the new squares will start
	 * 	toX: x coordinate for the last new square
	 * 	size: visual size of one square
	 * 	image: the name of the image used for the square
	 * 	betweenSize: visual size of the space between each square
	 */
	public void makeNrOfSquares(int fromX, int toX, int size, String image, int betweenSize) {
		for(int i = fromX; i < toX+1; i++) {
			makeNewSquare(i, size, image, betweenSize);
		}
	}
	
	/*
	 * Makes one new square on X location
	 * Inputs:
	 * 	x: x coordiante for the new square
	 * 	size: visual size of the square
	 * 	image: name of the image used for squares
	 */
	public void makeOneSquare(int x, int size, String image, int betweenSize) {
		this.makeNewSquare(x, size, image, betweenSize);
	}
	
	/*
	 * Calculates the x coordinate for a new square
	 * Inputs:
	 * 	locationNr: squares number in the line
	 * 	size: visual size of one square
	 * 	betweenSize: visual size  of the space between two squares
	 */
	private int calculateX(int locationNr, int size, int betweenSize) {
		return (size+betweenSize) * locationNr;
	}
	
	//Getter for all the squares in the line
	public ArrayList<MovementSquare> getSquares() {
		return this.squares;
	}
	
	//Getter for full visual height of the line (counts as same size as one square height)
	protected int getFullHeight() {
		return lenAss.getFieldSquareSize();
	}
	
	//Getter for this frames full length
	protected int getFullLength() {
		if(!this.hasSquares()) {
			return 0;
		}
		//System.out.println("this is line nr: "+this.lineNr);
		MovementSquare lastSquare = this.squares.get(this.squares.size() -1);
		return lastSquare.getLength()+lastSquare.getX();
	}
	
	//Checks if there are any squares saved into this line
	public boolean hasSquares() {
		return this.squares.size() != 0;
	}
	
	/*
	 * Getter for a specific square by its number in the line
	 * Inputs:
	 * 	squareNr: squares number in line
	 */
	public MovementSquare getSquareNr(int squareNr) {
		for(MovementSquare oneSquare : this.squares) {
			if(oneSquare.getLocationNr() == squareNr) {
				return oneSquare;
			}
		}
		return null;
	}
	
	//Getter for the lines number
	public int getLineNr() {
		return this.lineNr;
	}
	
	//Getter for the line above this line
	public MovementLine getTopLine() {
		MovementLine topLine = getSideLine(this.lineNr - 1);
		return topLine;
	}
	
	//Getter for the line below this line
	public MovementLine getBotLine() {
		MovementLine botLine = getSideLine(this.lineNr + 1);
		return botLine;
	}
	
	/*
	 * Getter for any line other line through this line with a line number
	 * Inputs:
	 * 	lineNr: lines number
	 */
	private MovementLine getSideLine(int lineNr) {
		MovementField masterField = getMovementField();
		MovementLine line = masterField.getLineNr(lineNr);
		return line;
	}
	
	//Getter for the movementField that this line belongs to
	private MovementField getMovementField() {
		MovementField masterField = (MovementField) this.wrapper;
		return masterField;
	}
	
	/*
	 * Changes the images of all the squares in this line
	 * Inputs:
	 * 	name: name of the new image
	 */
	public void paintAllSquaresTo(String name) {
		for(MovementSquare oneSquare : this.squares) {
			oneSquare.setImage(name);
		}
	}

	@Override
	public void fillTopElementList(List<IStackable> list) {
		list.addAll(this.squares);
	}

	@Override
	public void fillTopTextList(List<ScreenText> list) {
		
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOffScreenScript() {
		// TODO Auto-generated method stub
		
	}

}

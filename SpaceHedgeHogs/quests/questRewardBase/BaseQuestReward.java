package questRewardBase;

import factories.IAutoMake;
import factories.MainFactory;

public abstract class BaseQuestReward implements IAutoMake, IQuestReward {
	protected MainFactory mainFactory;

	@Override
	public void fullFillReward() {
		this.rewardScript();
		
	}
	
	protected abstract void rewardScript();

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

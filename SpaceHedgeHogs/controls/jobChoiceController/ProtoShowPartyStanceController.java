package jobChoiceController;

import java.util.ArrayList;

import jobs.Job;
import jobs.JobName;
import jobs.JobTopic;
import party.PartyStance;
import sreenElementsSockets.ScrollCharSocket;

/*
 * Controller for job topics that appear with the changing of party stance
 */
public class ProtoShowPartyStanceController extends BaseJobTopicController {

	//constructor
	public ProtoShowPartyStanceController(JobTopic newTopic) {
		super(newTopic);
	}

	@Override
	protected ArrayList<Object> makeModifierValuePackage() {
		ArrayList<Object> modifierPackage = new ArrayList<Object>();
		modifierPackage.add(findCurrentPartyStance());
		return modifierPackage;
	}
	
	private PartyStance findCurrentPartyStance() {
		return this.mainFactory.getParty().getPartyStance();
	}

	@Override
	protected void addSockets() {
		PartyStance partyStance = this.findCurrentPartyStance();
		if(partyStance == PartyStance.Camp) {
			prepareANewSocket(2, "LightGreen", "Fighting training", JobName.TrainingFighting);
			
			prepareANewSocket(2, "LightGreen", "Resting", JobName.Resting);
			
			prepareANewSocket(2, "LightGreen", "Barricade making", JobName.BarricadeCrafting);
			
			prepareANewSocket(2, "LightGreen", "Item repairing", JobName.ItemRepairing);
		}
		
	}

}

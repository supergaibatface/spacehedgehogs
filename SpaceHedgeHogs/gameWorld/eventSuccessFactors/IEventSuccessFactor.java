package eventSuccessFactors;

import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;

//interface for all event success factors
public interface IEventSuccessFactor {
	
	//returns if this is a positive factor when taking account a character or not
	public boolean getValueType(IGameCharacter oneChar);
	
	//gets the power of this success factor as an integer
	public int getValueFactor(IGameCharacter oneChar);

}

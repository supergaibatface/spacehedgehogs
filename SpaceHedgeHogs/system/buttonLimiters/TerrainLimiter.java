package buttonLimiters;

//Limiter that checks if travel managers terrain allows movement
public class TerrainLimiter extends BaseButtonLimiter{

	//constructor, same inputs as super class
	public TerrainLimiter(boolean notifyOnFailure, boolean instantPopUp) {
		super(notifyOnFailure, instantPopUp);
	}

	@Override
	public boolean check() {
		if(this.mainFactory.getTravelManager().canAdvance()) {
			return true;
		}
		return false;
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doCost() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String reasonForFail() {
		return "Terrain doesn't allow advancing";
	}

}

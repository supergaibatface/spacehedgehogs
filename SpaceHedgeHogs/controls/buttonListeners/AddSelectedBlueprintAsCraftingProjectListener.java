package buttonListeners;

import craftingCenter.CraftingInfoPage;

public class AddSelectedBlueprintAsCraftingProjectListener extends BaseButtonListener {
	private CraftingInfoPage targetPage;
	
	public AddSelectedBlueprintAsCraftingProjectListener(CraftingInfoPage targetPage) {
		this.targetPage = targetPage;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		targetPage.addSelectedBlueprintToProjects();
		
	}

	@Override
	public String toInfo() {
		return "Lists selected blueprint as crafting project";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

package spawning;

//Time spawn controller module that has a max amount of enemy chars that can be on the field at max
public class TimeSpawnKeepNrOfCharsOnField extends BaseSpawnTimeModule {
	private int charAmount;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	charAmount: the max amount of enemy chars that can be on the field at all time
	 */
	public TimeSpawnKeepNrOfCharsOnField(int charAmount) {
		this.charAmount = charAmount;
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean doWeSpawn() {
		if(this.charAmount > this.getNrOfCharsOnField()) {
			return true;
		}
		return false;
	}
	
	//getter for how many characters there are currently on the map
	private int getNrOfCharsOnField() {
		return this.mainFactory.getMainAIHub().getControlledCharList().size();
	}

}

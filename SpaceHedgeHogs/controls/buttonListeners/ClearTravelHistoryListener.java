package buttonListeners;

//Clears travel history on activation
public class ClearTravelHistoryListener extends BaseButtonListener {

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.mainFactory.getTravelManager().clearTravelHistory();
		
	}

	@Override
	public String toInfo() {
		return "clears travel history";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

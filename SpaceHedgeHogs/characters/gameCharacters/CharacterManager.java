package gameCharacters;

import java.util.ArrayList;
import java.util.List;

import attacking.AIAttackPatterns;
import central.AICharPatterns;
import central.AICharacterSet;
import dataValues.CharValue;
import factories.IAutoMake;
import factories.MainFactory;
import moving.AIMovementPatterns;
import propertyBars.PropertyBar;
import propertyBars.PropertyBarName;
import skills.Skill;
import targetChoosing.AITargetingPatterns;

//Manager class for GameCharactes, contains a list of all the GameCharacters in the game
public class CharacterManager implements IAutoMake{
	private MainFactory mainFactory;
	private List<IGameCharacter> characters;
	private List<IGameCharacter> enemyCharacters;
	private List<AICharacterSet> enemyCharacterSets;
	private List<AICharacterSet> tempCopiedCharSets;
	private List<IGameCharacter> tempCopiedChars;
	private List<PropertyBar> charPropertyBars;
	
	//constructor
	public CharacterManager() {
	}
	
	/*
	 * Boot up method, meant for starting up the object after creation
	 * sets up the array list for all characters for the player
	 */
	public void bootUp() {
		this.characters = new ArrayList<IGameCharacter>();
		this.enemyCharacters = new ArrayList<IGameCharacter>();
		this.tempCopiedChars = new ArrayList<IGameCharacter>();
		
		this.enemyCharacterSets = new ArrayList<AICharacterSet>();
		this.tempCopiedCharSets = new ArrayList<AICharacterSet>();
		
		this.makeBasePropertyBars();
		makeEnemyCharacters();
	}
	
	//Makes test enemy character
	private void makeEnemyCharacters() {
		enemyCharacters.add(mainFactory.getElemFactory().getGameCharacter("Corona", "50x50Mark"));
		enemyCharacterSets.add(new AICharacterSet(mainFactory.getElemFactory().getGameCharacter("Corona", "50x50Mark"), this.getStandardAICommandPattern()));
		enemyCharacterSets.add(new AICharacterSet(mainFactory.getElemFactory().getGameCharacter("Tutotrial1", "50x50Mark"), this.getNoneAICommandPattern()));
	}
	
	/*
	 * Generates a set of standard AI command patterns
	 */
	private AICharPatterns getStandardAICommandPattern() {
		return new AICharPatterns(AIAttackPatterns.Standard, AIMovementPatterns.Standard , AITargetingPatterns.Standard);
	}
	
	/*
	 * Generates a set of none AI command patterns (passive enemy that only stands and counter attacks)
	 */
	private AICharPatterns getNoneAICommandPattern() {
		return new AICharPatterns(AIAttackPatterns.None, AIMovementPatterns.None , AITargetingPatterns.None);
	}
	
	/*
	 * Makes a new copy version of an already existing character (used for enemy mobs)
	 * Inputs:
	 * 	base: template character that will be copied
	 */
	public GameCharacter copyCharacter(GameCharacter base) {
		GameCharacter copy = mainFactory.getElemFactory().getGameCharacter(base.getName(), base.getImgName());
		this.tempCopiedChars.add(copy);
		return copy;
	}
	
	/*
	 * Copies a characterSet
	 */
	public AICharacterSet copyCharacterSet(AICharacterSet base) {
		AICharacterSet copy = new AICharacterSet(
				mainFactory.getElemFactory().getGameCharacter(
						base.getCharacterInfo().getName(),
						base.getCharacterInfo().getImgName()), 
				base.getCommandDetails());
		this.tempCopiedCharSets.add(copy);
		return copy;
	}
	
	/*
	 * Method for deleting list of made copy characters after they are no longer used in battle
	 */
	public void flushCopyList() {
		this.tempCopiedChars.clear();
		this.tempCopiedCharSets.clear();
	}
	
	//Returns the temporary copy character list
	public List<IGameCharacter> getTempChars() {
		return this.tempCopiedChars;
	}
	
	/*
	 * Getter for the list of temp char sets
	 */
	public List<AICharacterSet> getTempCharSets() {
		return this.tempCopiedCharSets;
	}
	
	/*
	 * Gets all temp chars that are stores as char sets
	 */
	public ArrayList<GameCharacter> getTempCharSetChars() {
		ArrayList<GameCharacter> characters = new ArrayList<GameCharacter>();
		for(AICharacterSet oneSet : this.tempCopiedCharSets) {
			characters.add(oneSet.getCharacterInfo());
		}
		return characters;
	}
	
	//Makes actual characters from a list of charValues
	public void setCharactersWithCharValues(ArrayList<CharValue> charValues) {
		for(CharValue oneValue : charValues) {
			makeCharFromValues(oneValue);
		}
	}
	
	//Makes one character from a specific character value
	private void makeCharFromValues(CharValue oneValue) {
		//IGameCharacter newCharacter = mainFactory.getElemFactory().getGameCharacter(oneValue.getName(), oneValue.getImgName());
		//IGameCharacter newCharacter = this.dealWithBob(oneValue);
		IGameCharacter newCharacter = this.mainFactory.getCharacterFactory().getMatchClassFor(oneValue);
		if(oneValue.hasSavedSkillSheet()) {
			oneValue.getSavedSkillSheet().fillSkillsOnCharacter(newCharacter);
		}
		if(oneValue.hasSavedPropertySheet()) {
			oneValue.getSavedPropertySheet().fillBars(newCharacter);
		}
		characters.add(newCharacter);
	}
	
	private IGameCharacter dealWithBob(CharValue oneValue) {
		if(oneValue.getName().equals("Bob")) {
			return this.mainFactory.getCharacterFactory().getCharacterBob();
		}
		return this.mainFactory.getElemFactory().getGameCharacter(oneValue.getName(), oneValue.getImgName());
	}
	
	//Deletes all saved characters and calls safe delete on them
	public void clearCharacterValues() {
		for(IGameCharacter oneChar : this.characters) {
			oneChar.safeDelete();
		}
		this.characters.clear();
		this.flushCopyList();
	}
	
	/*
	 * Makes default property bars that will be used by all chars
	 */
	private void makeBasePropertyBars() {
		this.charPropertyBars = new ArrayList<PropertyBar>();
		makeHealthBar();
		makeStaminaBar();
		makeMovementBar();
		makeAttackBar();
	}
	
	//Makes and adds health bars to the list of values needed for characters
	private void makeHealthBar() {
		PropertyBar healthBar = this.mainFactory.getElemFactory().getPropertyBar(PropertyBarName.Health);
		healthBar.setMaxValue(100);
		healthBar.setMinValue(0);
		charPropertyBars.add(healthBar);
	}
	
	//Makes the default stamina bar
	private void makeStaminaBar() {
		PropertyBar staminaBar = this.mainFactory.getElemFactory().getPropertyBar(PropertyBarName.Stamina);
		staminaBar.setMaxValue(100);
		staminaBar.setMinValue(0);
		charPropertyBars.add(staminaBar);
	}
	
	//Makes the default movement bar
	private void makeMovementBar() {
		PropertyBar movementBar = this.mainFactory.getElemFactory().getPropertyBar(PropertyBarName.Movement);
		movementBar.setMaxValue(2);
		movementBar.setMinValue(0);
		charPropertyBars.add(movementBar);
	}
	
	//Makes the default attack bar
	private void makeAttackBar() {
		PropertyBar attackBar = this.mainFactory.getElemFactory().getPropertyBar(PropertyBarName.Attacks);
		attackBar.setMaxValue(2);
		attackBar.setMinValue(0);
		charPropertyBars.add(attackBar);
	}
	
	//setter for main factory object
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
	}
	
	//getter for all the characters
	public List<IGameCharacter> getCharacters() {
		return new ArrayList<IGameCharacter>(this.characters);
	}
	
	//getter for all the enemy characters
	public List<IGameCharacter> getEnemyCharacters() {
		return this.enemyCharacters;
	}
	
	//getter for enemy character sets
	public List<AICharacterSet> getEnemyCharacterSets() {
		return this.enemyCharacterSets;
	}
	
	//Adds instance of all the property bars to a char
	/*public void getPropertyBarsForChar(GameCharacter oneChar) {
		propertyBarCheck();
		for(PropertyBar oneExample: this.charPropertyBars) {
			PropertyBar newBar = makeCopy(oneExample);
			newBar.fillUp();
			oneChar.addPropertyBar(newBar);
		}
	}*/
	
	public void getPropertyBarsForChar(IGameCharacter oneChar) {
		propertyBarCheck();
		for(PropertyBar oneExample: this.charPropertyBars) {
			PropertyBar newBar = makeCopy(oneExample);
			newBar.fillUp();
			oneChar.addPropertyBar(newBar);
		}
	}
	
	//Turns a GameCharacter into a charValue
	public CharValue makeACharValueOutOfChar(IGameCharacter oneChar) {
		CharValue newCharValue = new CharValue(oneChar.getName(), oneChar.getImgName());
		for(Skill oneSkill: oneChar.getAllSkill()) {
			newCharValue.addSavedSkillValue(oneSkill.getName(), oneSkill.getExp());
		}
		addHealthBarValue(newCharValue, oneChar);
		return newCharValue;
	}
	
	/*
	 * Adds health bar value to a charValue
	 * Inputs:
	 * 	valueSet: charValues where the health bar value will be saved
	 * 	oneChar: Game character where the health bar value will be taken from
	 */
	private void addHealthBarValue(CharValue valueSet, IGameCharacter oneChar) {
		PropertyBar healthBar = oneChar.getPropertyBar(PropertyBarName.Health);
		if(healthBar != null) {
			valueSet.addSavedPropertyValue(healthBar.getName(), healthBar.getCurrentValue());
		}
	}
	
	/*
	 * Turns all saved characters into characterValues and returns the arrayList
	 */
	public ArrayList<CharValue> makeCharValuesOutOfAll() {
		ArrayList<CharValue> values = new ArrayList<CharValue>();
		for(IGameCharacter oneChar : this.characters) {
			values.add(this.makeACharValueOutOfChar(oneChar));
		}
		return values;
	}
	
	//Getter for all the default property bars
	public List<PropertyBar> getPropertyBars() {
		propertyBarCheck();
		return this.charPropertyBars;
	}
	
	//Checker for property bars, if there are none, will make new default ones
	public void propertyBarCheck() {
		if(this.charPropertyBars == null) {
			this.makeBasePropertyBars();
		}
	}
	
	//Method for copying a property bar from an example
	private PropertyBar makeCopy(PropertyBar example) {
		PropertyBar newBar = this.mainFactory.getElemFactory().getPropertyBar(example.getName());
		newBar.setMaxValue(example.getMaxValue());
		newBar.setMinValue(example.getMinValue());
		return newBar;
	}
	
	//method for commanding all the sockets with jobs to make chars do them
	public void fullFillRoles() {
		this.mainFactory.getJobManager().doAllRegisteredJobs();
	}
	
	//getter for current party size
	public int getPartySize() {
		return this.characters.size();
	}

}

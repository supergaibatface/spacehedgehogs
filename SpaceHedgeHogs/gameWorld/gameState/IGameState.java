package gameState;

import java.util.ArrayList;

import dataValues.CharValue;
import screenElements.ScreenButton;
import view.GameView;
import view.IGameView;

//interface for game states
public interface IGameState {
	
	//Method for loading in all details unique for the game state
	public void loadInDetails();
	
	//Getter for battle view
	public IGameView getStandardBattleView();
	
	//Method for loading in travel view
	public void startUpTravelView();
	
	//Getter for next part button, this button is not meant for displaying
	public ScreenButton getNextPartButton();
	
	//for adding extra char values from outside the game state
	public void addExtraCharValues(ArrayList<CharValue> extraValues);

}

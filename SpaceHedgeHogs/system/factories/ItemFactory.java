package factories;

import baggageCarryItemBase.BaggageAnimalItemBase;
import baggageCarryItems.HorseCarryItem;
import battleItems.StandardSwordEquipItem;
import foodBase.FoodItemBase;
import foodItems.AppleFoodItem;
import foodItems.BerryFoodItem;
import foodItems.FreshMeatFoodItem;
import foodItems.HerbsFoodItem;
import foodItems.MushroomFoodItem;
import foodItems.RootsFoodItem;
import ingredientItemBase.IngredientItemBase;
import ingredientItems.BrokenSwordIngredientItem;
import itemBase.IGameItem;

public class ItemFactory extends BaseMinorFactory {
	
	public FoodItemBase getBerryItem() {
		return this.setUpNewElement(new BerryFoodItem());
	}
	
	public FoodItemBase getMushroomItem() {
		return this.setUpNewElement(new MushroomFoodItem());
	}
	
	public FoodItemBase getRootsItem() {
		return this.setUpNewElement(new RootsFoodItem());
	}
	
	public FoodItemBase getFreshMeatItem() {
		return this.setUpNewElement(new FreshMeatFoodItem());
	}
	
	public FoodItemBase getHerbsFoodItem() {
		return this.setUpNewElement(new HerbsFoodItem());
	}
	
	public FoodItemBase getAppleFoodItem() {
		return this.setUpNewElement(new AppleFoodItem());
	}
	
	public StandardSwordEquipItem getSwordEquipItem() {
		return this.setUpNewElement(new StandardSwordEquipItem());
	}
	
	public BaggageAnimalItemBase getHorseCarryitem() {
		return this.setUpNewElement(new HorseCarryItem());
	}
	
	public IngredientItemBase getBrokenSwordIngredientItem() {
		return this.setUpNewElement(new BrokenSwordIngredientItem());
	}

}

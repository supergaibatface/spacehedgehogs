package skills;

import java.util.ArrayList;
import java.util.List;

import factories.IAutoMake;
import factories.MainFactory;
import skillListeners.ISkillChangeListener;

//Class that represents one skill that belongs to a specific character
public class Skill implements IAutoMake {
	private MainFactory mainFactory;
	private SkillName name;
	private int level;
	private int experience;
	private List<ISkillChangeListener> listeners;
	
	//constructor
	public Skill(SkillName name) {
		this.name = name;
	}
	
	//getter for skill name
	public SkillName getName() {
		return this.name;
	}
	
	//getter for skill level
	public int getLevel() {
		return this.level;
	}
	
	//getter for skill experience
	public int getExp() {
		return this.experience;
	}
	
	//setter for experience lvl
	public void setExp(int newExperiance) {
		this.experience = newExperiance;
		updateLvl(newExperiance);
		this.informListeners();
	}
	
	//method for comparing skills by their name
	public boolean equals(Skill otherSkill) {
		return this.equals(otherSkill.getName());
	}
	
	//method for comparing skills by their name
	public boolean equals(SkillName name) {
		return this.name.equals(name);
	}
	
	//method for adding new experience ontop of old
	public void addExp(int newExperiance) {
		setExp(this.experience + newExperiance);
	}
	
	//method for updating lvl according to new experience
	private void updateLvl(int newExperiance) {
		this.level = this.mainFactory.getSkillManager().askForLevel(newExperiance);
	}
	
	//Adds a listener that listens for skill exp changes
	public void addListener(ISkillChangeListener newListener) {
		this.listeners.add(newListener);
	}
	
	//removes a listener that listenes for skill exp changes
	public void removeListener(ISkillChangeListener oldListener) {
		this.listeners.remove(oldListener);
	}
	
	//informs all listeners about a skill exp change
	private void informListeners() {
		for(ISkillChangeListener oneListener : this.listeners) {
			oneListener.skillChangeEvent();
		}
	}

	//setter for mainfactory
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	//method for booting up the object after constructing
	@Override
	public void bootUp() {
		this.level = 1;
		this.experience = 0;
		this.listeners = new ArrayList<ISkillChangeListener>();
	}
	

}

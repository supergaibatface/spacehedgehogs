package spawning;

//Time spawn module that will spawn one character every turn
public class TimeSpawnEveryTurnOne extends BaseSpawnTimeModule {
	private int lastTurn;

	@Override
	public void bootUp() {
		lastTurn = -1;
		
	}

	@Override
	public boolean doWeSpawn() {
		if(lastTurn != this.getCurrentTurn()) {
			lastTurn = this.getCurrentTurn();
			return true;
		}
		else {
			return false;
		}
	}
	
	//gets current turn value
	private int getCurrentTurn() {
		return this.mainFactory.getBattleTime().getCurrenTurn();
	}

}

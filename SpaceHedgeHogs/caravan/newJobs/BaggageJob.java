package newJobs;

import java.util.ArrayList;
import java.util.List;

import barChange.BarGain;
import barChange.BarLimiter;
import eventSuccessFactors.IEventSuccessFactor;
import events.EventTagName;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import inventory.WeightCarryingAbility;
import jobs.SkillGain;
import propertyBars.PropertyBarName;
import screenElements.ScreenButton;
import skills.SkillName;
import sreenElementsSockets.ScreenSimpleChar;

//class that represents a baggage carrying job
public class BaggageJob extends BaseJob {
	private WeightCarryingAbility weightCarry;
	
	public void bootUp() {
		super.bootUp();
	}

	@Override
	protected void setWorkTags(List<EventTagName> tagList) {
		tagList.add(EventTagName.BaggageManagement);
		
	}

	@Override
	protected void setBarCost(List<BarLimiter> limiterList) {
		limiterList.add(this.mainFactory.getElemFactory().getPropertyBarLimiter(PropertyBarName.Stamina, 5));
		
	}
	
	@Override
	protected void setBarGains(List<BarGain> gainList) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void setSkillGains(List<SkillGain> gainList) {
		gainList.add(this.mainFactory.getElemFactory().getSkillGain(SkillName.BaggageManagement, 3));
		
	}
	
	//getter for all the chars in the socket that has this job
	public List<IGameCharacter> getSocketCharacters() {
		List<IGameCharacter> fullList = new ArrayList<IGameCharacter>();
		if(this.hasRegisteredSocket()) {
			fullList.addAll(this.registeredSocket.getMasterElements());
		}
		return fullList;
	}
	
	//method that calls for a refresh on carry ability of this socket
	public void refreshCarryAmount() {
		this.addWeightCarry(this.getSocketCharacters());
	}
	
	//method for changing the carry value of this jobs carrying ability, input is the list of characters designated to this job
	private void addWeightCarry(List<IGameCharacter> characters) {
		this.weightCarry.setCapability(this.getCharListCarryValue(characters));
		
	}
	
	//Method for getting the total carry power of a list of characters, input is the list
	private int getCharListCarryValue(List<IGameCharacter> characters) {
		int endValue = 0;
		for(IGameCharacter oneChar : characters) {
			endValue = endValue + getOneCharsCarryValue(oneChar);
		}
		return endValue;
	}
	
	//getter for one characters carrying ability, input is the character
	private int getOneCharsCarryValue(IGameCharacter oneChar) {
		int endValue = oneChar.findSkill(SkillName.BaggageManagement).getLevel() * this.getLvlMultiplier();
		endValue = endValue + this.getBaseCarryValue();
		return endValue;
	}
	
	//getter for base carrying ability
	private int getBaseCarryValue() {
		return 60;
	}
	
	//getter for how much more carrying ability is gained with each lvl
	private int getLvlMultiplier() {
		return 5;
	}

	@Override
	public void newElementAdded(IGameCharacter newChar) {
		refreshCarryAmount();
		
	}

	@Override
	public void oldElementRemoved(IGameCharacter oldChar) {
		refreshCarryAmount();
		
	}

	@Override
	public void safeDelete() {
		this.weightCarry.safeDelete();
		this.weightCarry = null;
		
	}

	@Override
	protected void addEventSuccessFactors(List<IEventSuccessFactor> factorList) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void socketGoingOnScreenCodeSpace() {
		this.makingNewWeightCarryValue();
		
	}
	
	private void makingNewWeightCarryValue() {
		this.weightCarry = this.mainFactory.getElemFactory().getWeightCarryingAbility();
	}

	@Override
	protected void addButtonsToExtraCommandsList(List<ScreenButton> saveLocationList) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void doExtraCodePerWorkingCharacter(IGameCharacter target) {
		// TODO Auto-generated method stub
		
	}


}

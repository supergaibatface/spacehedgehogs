package events;

import java.util.ArrayList;
import java.util.List;

import buttonListeners.ISButtonListener;
import central.AICharacterSet;
import factories.IAutoMake;
import factories.MainFactory;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import generalGraphics.ButtonLookType;
import itemBase.IGameItem;
import land.LandArea;
import screenElements.ScreenButton;
import screenElements.ScreenPopUp;

//represents one choice in an event, usually is represented by one button on screen
public class EventChoice implements IAutoMake {
	private MainFactory mainFactory;
	private ArrayList<IGameItem> itemGain;
	private ArrayList<AICharacterSet> enemies;
	private LandArea newLandArea;
	private GameEvent masterEvent;
	private String buttonLabel;
	private GameEvent resultEvent;
	private ArrayList<ISButtonListener> buttonListeners;
	
	/*
	 * constructor
	 * Inpputs:
	 * 	masterEvent: the event that this choice belongs to
	 * 	label: text that will be displayed on the button that represents this choice
	 */
	public EventChoice(GameEvent masterEvent, String label) {
		this.masterEvent = masterEvent;
		this.buttonLabel = label;
	}

	//setter for main factory
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	//bootup method
	@Override
	public void bootUp() {
		this.masterEvent.addChoice(this);
		this.buttonListeners = new ArrayList<ISButtonListener>();
	}
	
	/*
	 * adds a trade item that acts as what and how much will be added to inventory,
	 * when the listener is triggered
	 * Inputs:
	 * 	item: trade item that stores the item name and amount
	 */
	public void addItemGain(IGameItem item) {
		if(this.itemGain == null) {
			this.itemGain = new ArrayList<IGameItem>();
		}
		this.itemGain.add(item);
	}
	
	/*
	 * Adds an enemy to event choice that has to be defeated in battle
	 * Inputs:
	 * 	enemy: game character version of the enemy
	 */
	public void addEnemy(AICharacterSet enemy) {
		if(this.enemies == null) {
			this.enemies = new ArrayList<AICharacterSet>();
		}
		this.enemies.add(enemy);
	}
	
	/*
	 * Adds a new land area finding result to this choice
	 * Inputs:
	 * 	landArea: the land area added when choosing this option
	 */
	public void addNewLandArea(LandArea landArea) {
		this.newLandArea = landArea;
	}
	
	//Setter for a possible result event
	public void setResultEvent(GameEvent resultEvent) {
		this.resultEvent = resultEvent;
	}
	
	//Adds button listeners that will be directly added to this choices representing button on event popup
	public void addNewCustomButtonListener(ISButtonListener listener) {
		this.buttonListeners.add(listener);
	}
	
	//Make function for the button that represents this choice
	public ScreenButton makeButton(ScreenPopUp popUp, List<IGameCharacter> characters) {
		ScreenButton newButton = this.getButtonBase(popUp);
		itemGainBranch(newButton);
		landAreaBranch(newButton);
		resultEventBranch(newButton, characters);
		enemyAddingBranch(newButton);
		buttonListenerAddingBranch(newButton);
		newButton.makeInfo();
		return newButton;
	}
	
	//Make function for the button that represents this choice
	public ScreenButton makeButton(ScreenPopUp popUp) {
		ScreenButton newButton = this.getButtonBase(popUp);
		itemGainBranch(newButton);
		landAreaBranch(newButton);
		resultEventBranch(newButton);
		enemyAddingBranch(newButton);
		buttonListenerAddingBranch(newButton);
		newButton.makeInfo();
		return newButton;
	}
	
	//Checks if there is an item gain and if there is, adds its listener to the button
	private void itemGainBranch(ScreenButton newButton) {
		if(this.itemGain != null) {
			for(IGameItem oneGain: this.itemGain) {
				newButton.addButtonEventListener(this.mainFactory.getElemFactory().getItemAddListener(oneGain));
			}
		}
	}
	
	/*
	 * Adds water gain to the event choice
	 * Input:
	 * 	amount: how many units of water will be gained
	 */
	public void addWaterGain(int amount) {
		this.addNewCustomButtonListener(this.mainFactory.getElemFactory().getAddValueToBarListener(
						this.mainFactory.getWaterSupplyController().getWaterPropertyBar(), amount));
	}
	
	/*
	 * Adds a water gain that fills up players entire water supply
	 */
	public void addWaterGainToFull() {
		this.addNewCustomButtonListener(this.mainFactory.getElemFactory().getAddValueToBarListener(
						this.mainFactory.getWaterSupplyController().getWaterPropertyBar()));

	}
	
	//Checks if there is a result event, and if there is adds its listener to the button
	private void resultEventBranch(ScreenButton newButton, List<IGameCharacter> characters) {
		if(hasResultEvent()) {
			newButton.addButtonEventListener(this.mainFactory.getElemFactory().getResultEventListener(resultEvent, characters));
		}
	}
	
	//Checks if there is a result event, and if there is adds its listener to the button
	private void resultEventBranch(ScreenButton newButton) {
		if(hasResultEvent()) {
			newButton.addButtonEventListener(this.mainFactory.getElemFactory().getResultEventListener(resultEvent));
		}
	}
	
	 //function adding branch that checks and if needed adds the land area discovering option to the button
	private void landAreaBranch(ScreenButton newButton) {
		if(this.hasNewLandArea()) {
			newButton.addButtonEventListener(this.mainFactory.getElemFactory().getAddLandAreaListener(newLandArea));
		}
	}
	
	//function adding branch that checks and if needed adds the enemy option to the button
	private void enemyAddingBranch(ScreenButton newButton) {
		if(this.hasEnemies()) {
			newButton.addButtonEventListener(this.mainFactory.getElemFactory().getAddEnemiesListener(this.enemies));
		}
	}
	
	//function adding branch that adds all custom button listeners saved to this choice, to the input button
	private void buttonListenerAddingBranch(ScreenButton newButton) {
		for(ISButtonListener oneListener : this.buttonListeners) {
			newButton.addButtonEventListener(oneListener);
		}
	}
	
	//Makes the minimal button that has a look and closes the pop up
	private ScreenButton getButtonBase(ScreenPopUp popUp) {
		ScreenButton newButton = popUp.getNewCloseButton(ButtonLookType.POPUPSELECT, this.buttonLabel);
		return newButton;
	}
	
	//Checks if the EventChoice has resulting event
	private boolean hasResultEvent() {
		return this.resultEvent != null;
	}
	
	//Checks if the EventChoice has resulting new land
	private boolean hasNewLandArea() {
		return this.newLandArea != null;
	}
	
	//Checks if the EventChoice has resulting enemies
	private boolean hasEnemies() {
		return this.enemies != null;
	}
	
}

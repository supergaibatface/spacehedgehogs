package foodItems;

import foodBase.FoodItemBase;
import itemBase.TradeItemName;

public class BerryFoodItem extends FoodItemBase {

	public BerryFoodItem() {
		super(TradeItemName.Berry);
	}

	@Override
	protected int bootGiveBaseExpiration() {
		return 8;
	}

	@Override
	protected FoodItemBase giveBaseClass() {
		return (BerryFoodItem) this.mainFactory.getItemFactory().getBerryItem();
	}

	@Override
	protected String bootGiveItemImageName() {
		return this.bootGivePlaceholderImage();
	}

	@Override
	protected int bootGiveOneUnitWeight() {
		return 1;
	}

}

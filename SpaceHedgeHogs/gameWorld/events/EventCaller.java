package events;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import eventSuccessFactors.IEventSuccessFactor;
import factories.IAutoMake;
import factories.MainFactory;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import generalRNG.LotteryBox;
import other.StaticFunctions;

//Central class that takes in event requests and stores them, those events can also be called through this class
public class EventCaller implements IAutoMake {
	private MainFactory mainFactory;
	private List<EventRequest> eventRequests;
	private EventManager eventManager;
	private LotteryBox lotteryBox;
	private boolean debug = false;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.eventRequests = new ArrayList<EventRequest>();
		this.eventManager = this.mainFactory.getEventManager();
		this.lotteryBox = this.mainFactory.getElemFactory().getLotteryBox();
	}
	
	//public method for sending in an event request, input is the request
	public void sendEventRequest(EventRequest newRequest) {
		this.eventRequests.add(newRequest);
	}
	
	//public method for calling out all stored event requests as actual events
	public void callEvents() {
		StaticFunctions.writeOutDebug(debug, "Calling for events");
		this.printCurrentValues();
		this.callPopUps(firstStageLottery(this.eventRequests));
		this.clearOldRequests();
		
	}
	
	/*
	 * Calls first stage lottery phase on first requests, this lottery figures out if an event request is succesful for a working character
	 * if it succeeds there is a successful event lottery phase if not then there is no event
	 * Input:
	 * 	initialRequests: list of event requests that will go through the lottery
	 */
	private List<EventRequest> firstStageLottery(List<EventRequest> initialRequests) {
		StaticFunctions.writeOutDebug(debug, "Doing first stage event rolls");
		List<EventRequest> firstTurnSuccesses = new ArrayList<EventRequest>();
		for(EventRequest oneRequest : initialRequests) {
			EventRequest tempRequest = oneRequestFirstStageRoller(oneRequest);
			if(!tempRequest.getCharacters().isEmpty()) {
				firstTurnSuccesses.add(tempRequest);
			}
		}
		StaticFunctions.writeOutDebug(debug, "--------------------------");
		return firstTurnSuccesses;
	}
	
	/*
	 * First stage lottery command for a singular event request
	 * Input:
	 * 	fullRequest: event request that gets rolled for
	 * Output:
	 * 	output is always an event request but on failure its an empty request, on success it will have the character listed on it
	 */
	private EventRequest oneRequestFirstStageRoller(EventRequest fullRequest) {
		EventRequest firstResult = new EventRequest(fullRequest.getTags());
		for(IGameCharacter oneChar : fullRequest.getCharacters()) {
			if(oneCharSuccessCalculation(oneChar, fullRequest)) {
				firstResult.addCharacter(oneChar);
				StaticFunctions.writeOutDebug(debug, "Char: "+oneChar.getName()+" succeded in first stage roll");
			}
			else {
				StaticFunctions.writeOutDebug(debug, "Char: "+oneChar.getName()+" failed in first stage roll");
			}
		}
		return firstResult;
	}
	
	/*
	 * Lottery success calculation for one character
	 * Input:
	 * 	oneChar: character who is participating in the event request
	 * 	fullRequest: request for which the success is calculated for
	 * output:
	 * 	boolean, true if successful, false if not
	 */
	private boolean oneCharSuccessCalculation(IGameCharacter oneChar, EventRequest fullRequest) {
		this.lotteryBox.clearBox();
		for(IEventSuccessFactor oneFactor : fullRequest.getSuccessFactors()) {
			StaticFunctions.writeOutDebug(debug, "getting a "+oneFactor.getValueType(oneChar)+ "value factor of "+ oneFactor.getValueFactor(oneChar));
			this.lotteryBox.addAnotherItem(oneFactor.getValueType(oneChar), oneFactor.getValueFactor(oneChar));
		}
		this.lotteryBox.addAnotherItem(true, 1);
		this.lotteryBox.addAnotherItem(false, 2);
		if(this.debug) {
			this.lotteryBox.pringOutContents();
		}
		return (boolean) this.lotteryBox.pullRandomObject();
	}
	
	//debug function for printing out amount of current event requests
	private void printCurrentValues() {
		//System.out.println("Stored requests: "+this.eventRequests.size());
		StaticFunctions.writeOutDebug(debug, "Stored requests: "+this.eventRequests.size());
	}
	
	//private method for dealing with calling out a list of event requests
	private void callPopUps(List<EventRequest> requestList) {
		for(EventRequest oneRequest : requestList) {
			this.callOnePopUp(oneRequest);
		}
	}
	
	//private method for calling out a single event request
	private void callOnePopUp(EventRequest oneRequest) {
		decideOneEvent(oneRequest);
	}
	
	/*
	 * Decides on the actual events that represent one succesfull event request
	 * Input:
	 * 	oneRequest: successful event request
	 */
	private void decideOneEvent(EventRequest oneRequest) {
		List<IGameCharacter> charsToAssign = new ArrayList<IGameCharacter>();
		charsToAssign.addAll(oneRequest.getCharacters());
		while(charsToAssign.size() > 0) {
			callForOneEvent(charsToAssign, oneRequest.getTags());
		}

	}
	
	/*
	 * Calls out one event for a specific character or characters that represents certain event tags
	 * Input:
	 * 	charsToAssign: list of characters that need events
	 *	eventTags: list of event tags that the event needs to be matched to 	
	 */
	private void callForOneEvent(List<IGameCharacter> charsToAssign, List<EventTagName> eventTags) {
		List<GameEvent> events = this.getTaggedEvents(eventTags, charsToAssign.size());
		GameEvent winEvent = makeAndDrawOneEvent(events);
		if(winEvent == null) {
			this.drawOneCharFromList(charsToAssign);
		}
		else {
			if(winEvent.getCharAmount() == 0) {
				this.callEventWithZeroParticpants(winEvent);
			}
			else if(winEvent.getCharAmount() == 1) {
				this.callEventWithOneParticipant(winEvent, charsToAssign);
			}
			else {
				this.callEventWithMultipleParticipants(winEvent, charsToAssign);
			}
		}
	}
	
	/*
	 * Sets up lottery box with a list of events and draws one, that it also returns
	 * Input:
	 * 	events: list of events that can win
	 * Outpu:
	 * 	GameEvent that came out of the lottery box as winner
	 */
	private GameEvent makeAndDrawOneEvent(List<GameEvent> events) {
		this.lotteryBox.clearBox();
		for(GameEvent oneEvent : events) {
			this.lotteryBox.addAnotherItem(oneEvent, 3);
		}
		if(this.lotteryBox.isEmpty()) {
			System.out.println("Bug: lottery box for deciding events is empty");
			this.addNullSituations(1);
		}
		Object winner = this.lotteryBox.pullRandomObject();
		if(winner == null) {
			return null;
		}
		else {
			return (GameEvent) winner;
		}
	}
	
	/*
	 * Method for triggering the popup for a event with zero participants
	 * Input:
	 * 	chosenEvent: event getting displayed on the screen
	 */
	private void callEventWithZeroParticpants(GameEvent chosenEvent) {
		this.mainFactory.getGElementManager().openElementOnPopupLayer(chosenEvent.triggerPopUp());
	}
	
	/*
	 * Method for triggering a popup for event with one participant
	 * Input:
	 * 	chosenEvent: event getting displayed on the screen
	 * 	charsToAssign: list of characters where first character will be the participant, also removes that char from the list
	 */
	private void callEventWithOneParticipant(GameEvent chosenEvent, List<IGameCharacter> charsToAssign) {
		this.mainFactory.getGElementManager().openElementOnPopupLayer(chosenEvent.triggerPopUp(this.drawOneCharFromList(charsToAssign)));
	}
	
	/*
	 * Method for triggering a popup for event with multiple participants
	 * Input:
	 * 	chosenEvent: event getting displayed on the screen
	 * 	charsToAssign: list of characters that will be participating in the event
	 */
	private void callEventWithMultipleParticipants(GameEvent chosenEvent, List<IGameCharacter> charsToAssign) {
		this.mainFactory.getGElementManager().openElementOnPopupLayer(chosenEvent.triggerPopUp(this.makeListForEvent(chosenEvent, charsToAssign)));
	}
	
	//removes and returns the first character in the input list of characters
	private IGameCharacter drawOneCharFromList(List<IGameCharacter> charsToAssign) {
		return charsToAssign.remove(0);
	}
	
	/*
	 * Makes a required amount of chars list for a specific event
	 * Input:
	 * 	chosentEvent: event that needs the characters
	 * 	charsToAssign: list of characters where the characters will be removed from and assigned to the new list
	 */
	private List<IGameCharacter> makeListForEvent(GameEvent chosenEvent, List<IGameCharacter> charsToAssign) {
		List<IGameCharacter> newCharList = new ArrayList<IGameCharacter>();
		for(int i = 0; i < chosenEvent.getCharAmount(); i++) {
			newCharList.add(this.drawOneCharFromList(charsToAssign));
		}
		return newCharList;
	}
	
	/*
	 * Used for adding an amount of null result tickets to the lotterybox
	 * Input:
	 * 	nrOfTimes: amount of null ticket situations added
	 */
	private void addNullSituations(int nrOfTimes) {
		this.lotteryBox.addAnotherItem(null, nrOfTimes);
	}
	
	//Clears the list of all saved event requests
	private void clearOldRequests() {
		this.eventRequests.clear();
	}
	
	//Method for getting events for chars that are assigned to this job
	public void getEvents(EventRequest eventRequest) {
		if(eventRequest.getCharacters().size() > 0) {
			List<GameEvent> events = this.getTaggedEvents(eventRequest.getTags());
			if(events.size() > 0) {
				int particNr = getHighParticNr(events);
				charListEvtAssigner(eventRequest, particNr, events);
			}
		}
		
	}
	
	//gets a list of possible events for this jobs tag
	private List<GameEvent> getTaggedEvents(List<EventTagName> tags) {
		List<GameEvent> events = this.eventManager.findEventsWithTag(tags.get(0));
		if(tags.size() > 1) {
			for(int i = 1; i < tags.size(); i++) {
				events = this.eventManager.addAnotherTagEvents(tags.get(i), events);
			}
		}
		return events;
	}
	
	//gets a list of possible events for this jobs tag
	private List<GameEvent> getTaggedEvents(List<EventTagName> tags, int maxParticipation) {
		List<GameEvent> events = this.eventManager.findEventsWithTag(tags.get(0), maxParticipation);
		if(tags.size() > 1) {
			for(int i = 1; i < tags.size(); i++) {
				events = this.eventManager.addAnotherTagEvents(tags.get(i), events, maxParticipation);
			}
		}
		return events;
	}
	
	//Method for figuring out the highest char participation number from an array of events
	public int getHighParticNr(List<GameEvent> events) {
		int amount = 0;
		for(GameEvent oneEvent: events) {
			if(oneEvent.getCharAmount() > amount) {
				amount = oneEvent.getCharAmount();
			}
		}
		return amount;
	}
	
	/*
	 * Private method that starts assigning events to each character participating in the job
	 * Chooses multiple char events by how many chars can be assigned to the largest event or
	 * by how mand chars are still looking for events, depending what number is bigger
	 * Inputs:
	 * 	eventChars: GameCharacters that have been assigned to the job
	 * 	particNr: highest char participation number in the largest event
	 */
	private void charListEvtAssigner(EventRequest eventRequest, int particNr, List<GameEvent> events) {
		List<IGameCharacter> assigningList = new ArrayList<IGameCharacter>();
		assigningList.addAll(eventRequest.getCharacters());
		while(assigningList.size() > 0) {
			if(assigningList.size() > particNr) {
				eventChoosingBranch(particNr, assigningList, events);
			}
			else {
				eventChoosingBranch(assigningList.size(), assigningList, events);
			}
		}
	}
	
	/*
	 * Branch of event choosing, when its clear what is the max number for participants
	 * Inputs:
	 * 	randomChoosingNr: max number for event participants
	 * 	eventChars: list of Chars that still need an event
	 */
	private void eventChoosingBranch(int randomChoosingNr, List<IGameCharacter> eventChars, List<GameEvent> eventList) {
		int chosenNr = new Random().nextInt(randomChoosingNr) + 1;
		List<IGameCharacter> chosenChars  = charSelecting(chosenNr, eventChars);
		eventDecider(chosenChars, eventList);
	}
	
	/*
	 * Builds a list of chars that will participate in an event
	 * Inputs:
	 * 	chosenNr: the amount of chars we need to choose
	 * 	eventChars: the chars that are available for selecting
	 * Output:
	 * 	chosenChars: a list of game characters that were chosen for the event
	 */
	private List<IGameCharacter> charSelecting(int chosenNr, List<IGameCharacter> eventChars) {
		List<IGameCharacter> chosenChars = new ArrayList<IGameCharacter>();
		while(chosenChars.size() < chosenNr) {
			chosenChars.add(eventChars.get(0));
			eventChars.remove(0);
		}
		return chosenChars;
	}
	
	//decides on what event will be triggered for a list of game characters, uses the jobs tags to limit events
	private void eventDecider(List<IGameCharacter> gameChars, List<GameEvent> eventList) {
		//List<GameEvent> eventList = this.getTaggedEvents();
		List<GameEvent> newList = this.eventManager.getByCharNr(gameChars.size(), eventList);
		GameEvent oneEvent = getOneEvent(newList);
		this.mainFactory.getGElementManager().openElementOnPopupLayer(oneEvent.triggerPopUp(gameChars));
	}
	
	//method for getting one random event from a list of events
	private GameEvent getOneEvent(List<GameEvent> events) {
		int highest = events.size();
		int eventNr = new Random().nextInt(highest);
		return events.get(eventNr);
	}

}

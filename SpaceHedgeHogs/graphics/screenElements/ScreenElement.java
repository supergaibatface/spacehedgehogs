package screenElements;

import java.awt.Image;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import displayElements.DisplayData;
import factories.IAutoMake;
import factories.MainFactory;
import frameworks.IStackable;
import generalGraphics.ClickBox;
import other.StaticFunctions;
import screenManagement.GraphicalLengthAssigner;
import screenManagement.ImageLoader;

/*
 * Class that represents a random item on the screen, combines the cliking and displaying logic
 * needed for this type of object, mostly meant as superior class to more specific displayable objects
 */
public abstract class ScreenElement implements IAutoMake, IStackable {
	protected MainFactory mainFactory;
	static protected int nr = 0;
	protected String id;
	protected Rectangle2D avatar;
	protected ClickBox clickBox;
	protected GraphicalLengthAssigner lenAss;
	protected String imageName;
	protected Image img;
	protected ImageLoader imgLoad;
	protected int xDataStart;
	protected int yDataStart;
	protected IStackable wrapper;
	protected boolean visible;
	protected ScreenSoloImage grayElement;
	protected DisplayData displayBuffer;
	protected boolean forceNonScroll;
	protected List<ScreenButton> extraCommands;
	
	/*
	 * Constructor for ScreenElement as this class is meant to be a parent class
	 * it only sets a location and not size by itself
	 * Inputs:
	 * 	x: x-coordinate for screen elements location (top left corner)
	 * 	y: y-coordinate for screen elements location (top left corner)
	 */
	public ScreenElement(int x, int y) {
		this.clickBox = new ClickBox(x, y);
		this.avatar = new Rectangle2D.Double(
				x, y, 0, 0);
	}
	
	/*Checks if a certain point is inside the clickbox's square
	 * input:
	 * 	x: certain points x coordinate
	 * 	y: certain points y coordinate
	 * output:
	 * 	true: point is inside the clickbox
	 * 	false: point is outside the clickbox
	*/
	public boolean isThePointOnElement(int x, int y) {
		int myX = this.getX();
		int myOtherX = myX + this.getLength();
		if(isBetween(x, myX, myOtherX)) {
			int myY = this.getY();
			int myOtherY = myY + this.getHeight();
			if(isBetween(y, myY, myOtherY)) {
				return true;
			}
		}
		return false;
	}
	
	//getter for the temporary square that represents the element on the screen, output: Rectangle2d type of element
	public Rectangle2D getAvatar() {
		return this.avatar;
	}
	
	//getter for the screen elements X coordinate, gets it from the clickbox, output: x-coordinate
	public int getX() {
		return this.clickBox.getX();
	}
	
	//getter for the screen element Y coordinate, gets it from the clickbox, output: y-coordinate
	public int getY() {
		return this.clickBox.getY();
	}
	
	//Getter for x dimension scroll value
	public int getXScroll() {
		return 0;
	}
	
	//Getter for y dimension scroll value
	public int getYScroll() {
		return 0;
	}
	
	//getter for the screen elements horizontal size, gets it from the clickbox, output: horizontal size
	public int getLength() {
		return this.clickBox.getLength();
	}
	
	//getter for the screen elements vertical size, gets it from the clickbox, output: vertical size
	public int getHeight() {
		return this.clickBox.getHeight();
	}
	
	//getter for the screen elements right side x coordinate
	public int getRightX() {
		return this.clickBox.getX() + this.clickBox.getLength(); 
	}
	
	//getter for the screen elements bottom side y coordinate
	public int getBottomY() {
		return this.clickBox.getY() + this.clickBox.getHeight();
	}
	
	/*
	 * Setter for coordinates, changes coordinates for both the clickbox and graphical avatar element
	 * Inputs:
	 * 	x: x-coordinate on the screen
	 * 	y: y-coordinate on the screen
	 */
	public void setCoordinates(int x, int y) {
		avatar.setRect(x, y, this.getLength(), this.getHeight());
		this.clickBox.setCoords(x, y);
	}
	
	/*
	 * Setter for size, changes size for clickbox and graphical avatar element
	 * and also the imgage if it has been set
	 * Inputs:
	 * 	length: length of the element on the screen
	 * 	height: height of the element on the screen
	 */
	public void setSize(int length, int height) {
		avatar.setRect(this.getX(), this.getY(), length, height);
		this.clickBox.setSize(length, height);
		if(this.isImgLoaded()) {
			this.img = this.imgLoad.scaleImage(this.img, this);
		}
		if(this.hasGrayElement()) {
			this.grayElement.setSize(length, height);
		}
	}
	
	//setter for a wrapper element, this means that this items location will be on the wrapper elements interior coordinates
	public void setWrapper(IStackable wrapperElem) {
		this.wrapper = wrapperElem;
	}
	
	/*
	 * Method for calculating if a number is between two different points on same dimension
	 * Inputs:
	 * 	number: the number that's location is under question
	 * 	firstPoint: the smaller of the two numbers that get compared against
	 * 	secondPoint: the larger of the two numbers that get compared against
	 * Ouputs:
	 * 	true: the number is larger then the first point AND smaller then the second point
	 * 	false: the number is either not larger then the first or not smaller then the second point
	 */
	private boolean isBetween(int number, int firstPoint, int secondPoint) {
		if(firstPoint < number && number < secondPoint) {
			return true;
		}
		return false;
	}
	
	//Setter for Image that uses string image name
	public void setImage(String imgName) {
		if(!isAlreadyThisImage(imgName)) {
			this.img = imgLoad.getAndScale(imgName, this);
			this.imageName = imgName;
		}
	}
	
	//Gate keeper function that checks if this element already has the imput image assigned as its look
	private boolean isAlreadyThisImage(String imgName) {
		if(this.imageName.equals(imgName)) {
			return true;
		}
		return false;
	}
	
	//Method for switching the image of this element
	public void switchImage(Image img) {
		this.img = img;
		this.imageName = "";
	}
	
	//getter that lets program check if an image has been set for the element
	public boolean isImgLoaded() {
		return this.img != null;
	}
	
	//getter for the elements image, use only if isImgLoaded() value is true
	public Image getImage() {
		return this.img;
	}
	
	//(NOT PERMANET) getter for the element id, returns the string type id
	public String getId() {
		return this.id;
	}
	
	//Static function for getting unique names for elements, output a string type of a name
	private static String getNewName() {
		String name = "Element nr " + nr;
		nr++;
		return name;
		
	}
	
	//Setter for a custon ID value for debugging
	public void setCustomId(String name) {
		this.id = name;
	}

	//setter for the main factory object
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
	}

	//Boot up method, meant for starting up the object after creation
	@Override
	public void bootUp() {
		this.id = getNewName();
		this.lenAss = mainFactory.getLengthAssigner();
		this.imgLoad = mainFactory.getImageLoader();
		this.setCoordinates(getX(), getY());
		this.xDataStart = lenAss.getExtraRoomLength();
		this.yDataStart = lenAss.getExtraRoomLength();
		this.visible = true;
		this.imageName = "";
		this.displayBuffer = new DisplayData(this);
		this.forceNonScroll = false;
	}
	
	/*
	 * Method for getting the elements X coordinate in its masters location coordinates
	 * Input:
	 * 	ScreenElement, element thats x coordinate will be calculated
	 */
	public int getXOffElem(ScreenElement elem) {
		return getXOffElem(elem.getX());
	}
	
	/*
	 * Method for getting the elements X coordinate in its masters location coordinates
	 * Input:
	 * 	itemX: x coordinate on this items coordinate system
	 */
	public int getXOffElem(int itemX) {
		return startOfElemX() + itemX - this.getXScroll();
	}
	
	//Method for getting the elements Y coordinate in its masters location coordinates
	public int getYOffElem(ScreenElement elem) {
		return getYOffElem(elem.getY());
	}
	
	//Method for getting the elements Y coordinate in its masters location coordinates
	public int getYOffElem(int itemY) {
		return  startOfElemY() + itemY - this.getYScroll();
	}
	
	//Method for getting a X coordinate of a click on this elements coordinate system
	public int getXOnElem(int clickX) {
		return clickX - startOfElemX() + this.getXScroll();
	}
	
	//Method for Getting a Y coordinate of a click on this elements coordinate system
	public int getYOnElem(int clickY) {
		return clickY - startOfElemY() + this.getYScroll();
	}
	
	//getting an X coordinate for this element off a possible bottom element
	public int getMyXOffElem(ScreenElement botElem) {
		return botElem.getXOffElem(this);
	}
	
	//getting an Y coordinate for this element off a possible bottom element
	public int getMyYOffElem(ScreenElement botElem) {
		return botElem.getYOffElem(this);
	}
	
	//Wrapper system for getting absolute X coordinates on the screen
	public int getOffWrapperX() {
		return this.getOffWrapperPointX(this.getX());
	}
	
	//Wrapper system for getting absolute X coordinates on the screen
	public int getOffWrapperPointX(int xPoint) {
		if(wrapper != null) {
			return forceScrollCheck(wrapper.getOffWrapperX() + wrapper.getXDataStart() + xPoint, wrapper.getXScroll());
		}
		else {
			return xPoint;
		}
	}
	
	//Wrapper system for getting absolute X coordinates on the screen
	public int getOffWrapperY() {
		return this.getOffWrapperPointY(this.getY());
	}
	
	//Wrapper system for getting absolute X coordinates on the screen
	public int getOffWrapperPointY(int yPoint) {
		if(wrapper != null) {
			return forceScrollCheck(wrapper.getOffWrapperY() + wrapper.getYDataStart() + yPoint, wrapper.getYScroll());
		}
		else {
			return yPoint;
		}
	}
	
	/*
	 * Method that will take in normal coordinate calculation and wrapper scroll value and then give the right coordinate
	 * value, depending on if forceNonScroll flag has been set (can be used for both x and y coordinate)
	 * Inputs:
	 * 	coordinate: normally calculated coordinate value (has scroll value added on already)
	 * 	wrapperScrollValue: wrapper elements scroll value
	 */
	protected int forceScrollCheck(int coordinate, int wrapperScrollValue) {
		if(!this.forceNonScroll) {
			return coordinate - wrapperScrollValue;
		}
		else {
			return coordinate;
		}
	}
	
	//x coordinate for where the displayable chars start
	public int startOfElemX() {
		return this.getX() + this.xDataStart;
	}
		
	//y coordinate for where the displayable chars start
	public int startOfElemY() {
		return this.getY() + this.yDataStart;
	}
	
	//Method for getting data start x coordinate on this element
	public int getXDataStart() {
		return this.xDataStart;
	}
	
	//Method for getting data start y coordinate on this element
	public int getYDataStart() {
		return this.yDataStart;
	}
	
	/*
	 * Getter for saved grayElement
	 */
	public ScreenSoloImage getGrayElement() {
		return this.grayElement;
	}
	
	//Checks if the element has a gray element
	public boolean hasGrayElement() {
		return this.grayElement != null;
	}
	
	/*
	 * getter for the elements wrapper
	 */
	public IStackable getWrapper() {
		return this.wrapper;
	}
	
	//Getter for elements clickBox
	public ClickBox getClickBox() {
		return this.clickBox;
	}
	
	//Checks if this element is visibile
	public boolean isVisible() {
		return this.visible;
	}
	
	//Makes element visible
	public void makeVisible() {
		this.visible = true;
	}
	
	//makes element invisibile
	public void makeInvisible() {
		this.visible = false;
	}
	
	/*
	 * Getter for displayData
	 */
	public DisplayData getDisplayData() {
		return this.displayBuffer;
	}
	
	//Getter for elements on top of this element
	public List<IStackable> getOnTopElements() {
		ArrayList<IStackable> topElements = new ArrayList<IStackable>();
		fillTopElementList(topElements);
		return topElements;
	}
	
	//abstract method for filling the list of top elements, input is the list to fill
	public abstract void fillTopElementList(List<IStackable> list);
	
	//Getter for texts on top of this element
	public List<ScreenText> getOnTopTexts() {
		List<ScreenText> topTexts = new ArrayList<ScreenText>();
		fillTopTextList(topTexts);
		return topTexts;
	}
	
	//Abstract method for filling the list of top text elements, input is the list to fill
	public abstract void fillTopTextList(List<ScreenText> list);

	
	/*
	 * Self click search for finding elements that might have been clicked on
	 * Inputs:
	 * 	clickX: x coordinate of the mouse click
	 * 	clickY: y coordinate of the mouse click
	 * 	searchClasses: list of classes that are allowed to be click on
	 * 	fillList: list where the clicked elements will be added to
	 */
	public List<ScreenElement> selfSearch(int clickX, int clickY, List<Class> searchClasses, List<ScreenElement> fillList) {
		if(!clickCheck()) {
			return fillList;
		}
		if(this.isThePointOnElement(clickX, clickY)) {
			matchClass(this, searchClasses, fillList);
		}
		List<IStackable> childList = new ArrayList<IStackable>();
		this.fillTopElementList(childList);
		for(IStackable oneElement : childList) {
			oneElement.selfSearch(this.getXOnElem(clickX), this.getYOnElem(clickY), searchClasses, fillList);
		}
		
		return fillList;
	}
	
	/*
	 * Method for checking if an element is of some class
	 * Inputs:
	 * 	oneElement: element that is checked
	 * 	filterClasses: classes that are searched for
	 * 	listToFill: list where searched class element would be added
	 */
	protected boolean matchClass(ScreenElement oneElement, List<Class> filterClasses, List<ScreenElement> listToFill) {
		for(Class oneClass : filterClasses) {
			if(oneElement.getClass().equals(oneClass)) {
				listToFill.add(oneElement);
				return true;
			}
		}
		return false;
	}
	
	//Check function that checks if a click is allowed on this element
	private boolean clickCheck() {
		if(!this.visible) {
			return false;
		}
		if(this.displayBuffer.isUsesData()) {
			if(!this.displayBuffer.isVisible()) {
				return false;
			}
		}
		return true;
	}
	
	//AutoSize function that automatically sets height and length for this element
	public void autoSize() {
		calculateSizeByOnTopElements();
	}
	
	//Calculates elements size by the elements on top of it
	protected void calculateSizeByOnTopElements() {
		int calcLength = findLengthByElements();
		int calcHeight = findHeightByElements();
		int newLength = this.getLength();
		int newHeight = this.getHeight();
		if(calcLength > 0) {
			newLength = calcLength + this.getFrameDoubleSize();
		}
		if(calcHeight > 0) {
			newHeight = calcHeight + this.getFrameDoubleSize();
		}
		this.setSize(newLength, newHeight);
		
	}
	
	//Basically returns doubled value of the designated frame size
	protected int getFrameDoubleSize() {
		return this.mainFactory.getLengthAssigner().getExtraRoomLength() * 2;
	}
	
	//Getter for list of elements that can impact this elements autosize
	protected List<IStackable> getSizeElements() {
		return this.getOnTopElements();
	}
	
	//Makes a gray out element for this button
	protected void makeGrayOut() {
		this.grayElement = this.mainFactory.getElemFactory().getSoloImage(getLength(), getHeight(), "grayOut");
		this.grayElement.setCoordinates(-this.xDataStart, -this.yDataStart);
		this.grayElement.setWrapper(this);
		this.grayElement.makeInvisible();
	}
	
	//Calculates this elements length by elements on top of it
	private int findLengthByElements() {
		int elementsLength = findBiggestLengthFromElements(this.getSizeElements());
		int textsLength = findBiggestLengthFromTexts(this.getOnTopTexts());
		if(elementsLength > textsLength) {
			return elementsLength;
		}
		return textsLength;
	}
	
	//Finds the length of the longest element on this element, input is a list of all elements on top of this element
	private int findBiggestLengthFromElements(List<IStackable> elementsToConsider) {
		int largestLength = 1;
		for(IStackable oneElement: elementsToConsider) {
			if(oneElement.getLength() > largestLength) {
				largestLength = oneElement.getLength();
			}
		}
		return largestLength;
	}
	
	//Finds the length of the longest text element on this element, input is a list of all text elements on top of this element
	private int findBiggestLengthFromTexts(List<ScreenText> textsToConsider) {
		int largestLength = 1;
		for(ScreenText oneText: textsToConsider) {
			if(oneText.getOnScreenLength() > largestLength) {
				largestLength = oneText.getOnScreenLength();
			}
		}
		return largestLength;
	}
	
	//Calculates this elements height by elements on top of it
	private int findHeightByElements() {
		List<IStackable> elementsToConsider = this.getSizeElements();
		List<ScreenText> textsToConsider = this.getOnTopTexts();
		int totalElementHeight = 1;
		int oneBufferUnit = this.lenAss.getExtraRoomLength();
		for(IStackable oneElement: elementsToConsider) {
			totalElementHeight = totalElementHeight + oneElement.getHeight();
		}
		
		for(ScreenText oneText: textsToConsider) {
			totalElementHeight = totalElementHeight + oneText.getOnScreenHeight();
		}
		
		int bufferMultiplier = (elementsToConsider.size() - 1);
		if((!elementsToConsider.isEmpty()) && (!textsToConsider.isEmpty())) {
			bufferMultiplier = bufferMultiplier + 1;
		}
		if(bufferMultiplier > 0) {
			totalElementHeight = totalElementHeight + (oneBufferUnit * bufferMultiplier );
		}
		
		return totalElementHeight;
	}
	
	/*
	 * Method for getting visible are on this element as ClickBox
	 */
	public ClickBox getDisplayBox() {
		if(this.wrapper == null) {
			return new ClickBox(this.getXScroll() + this.xDataStart, this.getYScroll() + this.yDataStart , this.getLength(), this.getHeight());
		}
		else {
			return this.wrapper.getDisplayBox().getOverlap(clickBox).localiseClickBox(this);
		}
	}

	/*
	 * Master command for calculating visible area with ClickBox of visible area in this elements location coordinates
	 * Input:
	 * 	visibleBox: ClickBox that represents the visible area
	 */
	public void overLapMaster(ClickBox visibleBox) {
		if(!visibleBox.sameSquare(clickBox)) {
			this.displayBuffer.fillDataWithOverlap(visibleBox);
			this.displayBuffer.setUsesData(true);
			incompleteOverlapOnChildren(visibleBox.localiseClickBox(this));
		}
		else {
			fullOverlapOnChildren(visibleBox.localiseClickBox(this));
		}
		
	}
	
	/*
	 * Command called on child elements when this elements is not completly visible
	 */
	protected void incompleteOverlapOnChildren(ClickBox localVisibleBox) {
		callOverlapOnChildren(localVisibleBox);
		callOverlapOnText(localVisibleBox);
	}
	
	/*
	 * Command called on child elements when this element is completly visible
	 */
	protected void fullOverlapOnChildren(ClickBox localVisibleBox) {
		this.displayBuffer.setUsesData(false);
		callOverlapOnChildren(localVisibleBox);
		callFullOverLapOnText();
	}
	
	/*
	 * Method for calling overlap calculation on child elements
	 */
	protected void callOverlapOnChildren(ClickBox localVisibleBox) {
		ClickBox overLap;
		for(IStackable oneElement : this.getOnTopElements()) {
			overLap = localVisibleBox.getOverlap(oneElement.getClickBox());
			oneElement.overLapMaster(overLap);
		}
	}
	
	/*
	 * Method for calling overlap calculation on text elements
	 */
	protected void callOverlapOnText(ClickBox localVisibleBox) {
		for(ScreenText oneText : this.getOnTopTexts()) {
			oneText.checkVisibility(localVisibleBox);
		}
	}
	
	/*
	 * Method for calling overlap calculattion on text when this element is completly visible
	 */
	protected void callFullOverLapOnText() {
		for(ScreenText oneText : this.getOnTopTexts()) {
			oneText.setVisibile(true);
		}
	}
	
	//Checks if this element inherits screenElement class
	public boolean isScreenElement() {
		return true;
	}
	
	/*
	 * Debug method for priting out all wrapper element types
	 */
	public void printOutStackingHierarchy() {
		System.out.println(this.getClass());
		if(this.wrapper != null) {
			this.wrapper.printOutStackingHierarchy();
		}
	}
	
	public void safeDelete() {
		
	}

	public boolean isForceNonScroll() {
		return forceNonScroll;
	}

	//setter for the forceNonScroll flag
	public void setForceNonScroll(boolean forceNonScroll) {
		this.forceNonScroll = forceNonScroll;
	}
	
	public void goingOnScreenAlert() {
		this.informChildrenGoingOnScreen();
		this.goingOnScreenScript();
	}
	
	private void informChildrenGoingOnScreen() {
		for(IStackable oneElement : this.getOnTopElements()) {
			oneElement.goingOnScreenAlert();
		}
	}
	
	protected abstract void goingOnScreenScript();
	
	public void goingOffScreenAlert() {
		this.informChildrenGoingOffScreen();
		this.goingOffScreenScript();
	}
	
	private void informChildrenGoingOffScreen() {
		for(IStackable oneElement : this.getOnTopElements()) {
			oneElement.goingOffScreenAlert();
		}
	}
	
	protected abstract void goingOffScreenScript();
	
	//creates the object used as a list of extra commands (extra commands can be reached by right click)
	protected void makeExtraCommandsList() {
		this.extraCommands = new ArrayList<ScreenButton>();
	}
	
	//checks if elements had extra commands added
	public boolean hasExtraCommands() {
		if(this.extraCommands== null) {
			return false;
		}
		if(this.extraCommands.isEmpty()) {
			return false;
		}
		return true;
	}
	
	//Used for adding a new extra command to this element
	public void addExtraCommand(ScreenButton newCommand) {
		if(!this.hasExtraCommands()) {
			this.makeExtraCommandsList();
		}
		this.extraCommands.add(newCommand);
	}
	
	//Used for removing a specific extra command from this element
	public boolean removeExtraCommand(ScreenButton oldCommand) {
		if(this.hasExtraCommands()) {
			if(this.extraCommands.contains(oldCommand)) {
				this.extraCommands.remove(oldCommand);
				return true;
			}
		}
		return false;
	}
	
	//Used to clear extra command list
	protected void deleteExtraCommands() {
		if(this.hasExtraCommands()) {
			this.extraCommands.clear();
		}
	}
	
	//getter for extra commands of this element
	public List<ScreenButton> getExtraCommands() {
		return this.extraCommands;
	}
	

}

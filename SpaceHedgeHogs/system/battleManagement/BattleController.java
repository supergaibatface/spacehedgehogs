package battleManagement;

import java.util.ArrayList;

import battle.SpawnArea;
import central.AICharacterSet;
import central.MainAIHub;
import combatFieldElements.ControlFaction;
import combatFieldElements.FieldCharacter;
import combatFieldElements.MovementField;
import combatFieldElements.MovementLine;
import combatFieldElements.MovementSquare;
import factories.IAutoMake;
import factories.MainFactory;
import fieldCharCommand.FieldCharCommandControl;
import gameCharacters.GameCharacter;
import generalControls.TravelPath;
import generalGraphics.ButtonLookType;
import graphicalWrapping.CharSpawnInfo;
import other.StaticFunctions;
import screenElements.ScreenButton;
import screenElements.ScreenPopUp;
import spawning.ISpawnTimeModule;

//Controller class for battle situations with a movementField
public class BattleController implements IAutoMake {
	private MainFactory mainFactory;
	private MovementField battleField;
	private FieldCharCommandControl fieldCommandControl;
	private ArrayList<TravelPath> activePaths;
	private ArrayList<MovementSquare> activeSquares;
	private ArrayList<AICharacterSet> fieldEnemyInfo;
	private String activeMovementImgName;
	private String suitableAttackSquare;
	private String notSuitableAttackSquare;
	private MainAIHub mainAi;
	private ScreenPopUp specialWinPopUp;
	private boolean debug = false;
	
	//constructor
	public BattleController() {
	}
	
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}
	@Override
	public void bootUp() {
		this.fieldCommandControl = this.mainFactory.getFieldCharCommandControl();
		this.writeVisualImageNames();
		this.fieldEnemyInfo = new ArrayList<AICharacterSet>();
		this.setUpSpawnLimiterForPlayer();
	}
	
	//Bootup method that assignes images names for different situations
	private void writeVisualImageNames() {
		this.activeMovementImgName = "50Pink";
		this.suitableAttackSquare = "100Lime";
		this.notSuitableAttackSquare = "50purple";
	}
	
	//setter for the movement field of current battle
	public void setBattleField(MovementField battleField) {
		this.battleField = battleField;
	}
	
	/*
	 * Method for starting up the AI
	 */
	public void setUpAIForBattle(ISpawnTimeModule spawnTimeModule) {
		this.mainAi = this.mainFactory.getMainAIHub();
		this.mainAi.gearUpForBattle(this, spawnTimeModule);
	}
	
	/*
	 * Method for preturn spawning
	 * Inputs:
	 * 	enemySpawns: nr of enemies that should be spawned
	 */
	public void doPreTurnAction(int enemySpawns) {
		for(int i = 0; i < enemySpawns; i++) {
			this.mainAi.givePreTurn();
		}
	}
	
	/*
	 * Method for setting up limiter for player character spawns
	 */
	private void setUpSpawnLimiterForPlayer() {
		this.mainFactory.getPlayerLimiting().setSpawnLimiter(this.mainFactory.getElemFactory().getSpawnLimiting(2));
		this.mainFactory.getPlayerLimiting().writeNewText();
	}
	
	/*
	 * Sets a list of active squares for the movement field, these will be highlighted and can be interacted with
	 * Inputs:
	 * 	squares: list of squares that will be made active
	 */
	public void setActiveSquares(ArrayList<MovementSquare> squares) {
		this.activeSquares = StaticFunctions.copyArray(squares);
		this.battleField.setActiveColor(this.activeMovementImgName);
		this.battleField.paintSquaresReset(squares);
	}
	
	/*
	 * Sets a list of movement paths for the movement field, this will also set destinations as active squares
	 * Inputs:
	 * 	paths: List of travel paths that an active character can take
	 */
	public void setMovementPaths(ArrayList<TravelPath> paths) {
		this.activePaths = StaticFunctions.copyArray(paths);
		this.setActiveSquares(getDestinationSquares(paths));
	}
	
	/*
	 * Will set attack squares, this will filter out possible attack targets as active squares
	 * Inputs:
	 * 	squares: squares that are in range of targeting of the active char
	 */
	public void setAttackSquares(ArrayList<MovementSquare> squares) {
		this.activeSquares = filterOutActiveTargets(squares);
		this.battleField.setActiveColor(notSuitableAttackSquare);
		this.battleField.paintSquaresReset(squares);
		this.battleField.setActiveColor(suitableAttackSquare);
		this.battleField.paintSquaresNoReset(activeSquares);
	}
	
	/*
	 * Finds and paints the spawn area for input faction
	 * Inputs:
	 * 	faction: faction whos spawn area will be painted
	 */
	public void setVisualSpawnSquares(ControlFaction faction) {
		SpawnArea area = this.findSpawnArea(faction);
		if(area == null) {
			return;
		}
		this.battleField.setActiveColor(activeMovementImgName);
		this.battleField.paintSquaresReset(area.getSquares());
		
	}
	
	/*
	 * Filters and returns squares that have enemies on them
	 * Inputs:
	 * 	attackSquares: list of squares that will be filtered to find ones that are occupied by chars
	 */
	private ArrayList<MovementSquare> filterOutActiveTargets(ArrayList<MovementSquare> attackSquares) {
		ArrayList<MovementSquare> filteredSquares = new ArrayList<MovementSquare>();
		for(MovementSquare oneSquare : attackSquares) {
			if(squareAttackCheck(oneSquare)) {
				filteredSquares.add(oneSquare);
			}
		}
		return filteredSquares;
	}
	
	/*
	 * Checks if the square has a character on it
	 */
	private boolean squareAttackCheck(MovementSquare oneSquare) {
		if(!oneSquare.isOccupied()) {
			return false;
		}
		return true;
	}
	
	/*
	 * Makes a list of destinations squares out of a list of paths
	 * Inputs:
	 * 	paths: arrayList of paths that a char can move by
	 */
	private ArrayList<MovementSquare> getDestinationSquares(ArrayList<TravelPath> paths) {
		ArrayList<MovementSquare> destinations = new ArrayList<MovementSquare>();
		for(TravelPath onePath : paths) {
			destinations.add(onePath.getDestination());
		}
		return destinations;
	}
	
	/*
	 * Method that will find an active path by a given destination square
	 * Inputs:
	 * 	destination: square that the found path must take to
	 */
	public TravelPath findPathForDestination(MovementSquare destination) {
		for(TravelPath onePath : this.activePaths) {
			if(onePath.getDestination().equals(destination)) {
				return onePath;
			}
		}
		return null;
	}
	
	/*
	 * Method for clearing all visual ques from the movmentField
	 */
	public void clearVisuals() {
		if(hasActiveSquares()) {
			this.activeSquares = null;
		}
		if(hasActivePaths()) {
			this.activePaths = null;
		}
		this.battleField.clearColors();;
	}
	
	/*
	 * Checks if there are active squares
	 */
	public boolean hasActiveSquares() {
		return this.activeSquares != null;
	}
	
	/*
	 * Checks if there are active paths
	 */
	public boolean hasActivePaths() {
		return this.activePaths != null;
	}
	
	/*
	 * getter for active squares
	 */
	public ArrayList<MovementSquare> getActiveSquares() {
		return this.activeSquares;
	}
	
	/*
	 * Method that checks for winning situation and if the player has won, makes a popUp for it
	 */
	public void checkForWin() {
		if(!this.mainAi.hasEnemiesToDefeat()) {
			StaticFunctions.writeOutDebug(debug, "Player wins");
			this.mainFactory.getCharacterManager().flushCopyList();
			this.sendWinPopUp();
		}
	}
	
	/*
	 * Sends a victory message that will let player leave the battle view
	 */
	private void sendWinPopUp() {
		if(this.hasSpecialWin()) {
			this.mainFactory.getGElementManager().openElementOnPopupLayer(this.specialWinPopUp);
		}
		else {
			this.mainFactory.getGElementManager().openElementOnPopupLayer(makeWinPopUp());
		}
	}
	
	/*
	 * Makes the "player wins" pop up that marks the end of a battle
	 */
	public ScreenPopUp makeWinPopUp() {
		ScreenPopUp newPopUp = this.mainFactory.getElemFactory().getScreenPopUp("50red");
		newPopUp.fillTextOneLine("Player wins");
		newPopUp.addButton(makeWinButton(newPopUp));
		newPopUp.updateSizeForTime();
		return newPopUp;
	}
	
	//Checks if there has been set a custom victory message
	public boolean hasSpecialWin() {
		return this.specialWinPopUp != null;
	}
	
	//method for setting a custom victory pop up to be shown after victory
	public void insertSpecialWinPopUp(ScreenPopUp specialWinPopUp) {
		this.specialWinPopUp = specialWinPopUp;
	}
	
	/*
	 * Makes a end battle in a victory button for the end popUp
	 * Inputs:
	 * 	popUp: the popup where the button is displayed
	 */
	public ScreenButton makeWinButton(ScreenPopUp popUp) {
		ScreenButton newButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.POPUPSELECT, "Ok");
		newButton.addButtonEventListener(this.mainFactory.getElemFactory().getCloseIStackableElementListener(popUp));
		newButton.addButtonEventListener(this.mainFactory.getElemFactory().getBattleEndListener());
		return newButton;
	}

	
	//Safety method for fetching a debug map
	private void getDebugField() {
		this.battleField = this.mainFactory.getBattleMapManager().getDebugMovementField();
	}
	
	/*
	 * Adds an enemy spawnable characters for the movement field at the start of the fight
	 * Inputs:
	 * 	newEnemy: single character that will be spawned by the computer
	 */
	public void addEnemy(AICharacterSet newEnemy) {
		this.fieldEnemyInfo.add(newEnemy);
	}
	
	/*
	 * Getter for all spawnInfos
	 */
	public ArrayList<AICharacterSet> getAllSpawnInfos() {
		return this.fieldEnemyInfo;
	}
	
	/*
	 * Method for emptying the entire spawn list
	 */
	public void emptySpawnInfoList() {
		this.fieldEnemyInfo.clear();
	}
	
	/*
	 * Getter for the designated movementfield on this battle controller
	 */
	public MovementField getMovementField() {
		if(this.battleField == null) {
			this.getDebugField();
		}
		return this.battleField;
	}
	
	//Getter for spawn areas that are saved on the current map
	public ArrayList<SpawnArea> getSpawnAreas() {
		return this.battleField.getSpawnAreas();
	}
	
	//Used to safely end a battle, will delete all neccesary connections
	public void safeEndBattle() {
		this.mainFactory.getPlayerLimiting().deleteCurrentSpawnLimiter();
		this.fieldCommandControl.safeDelete();
	}
	
	/*
	 * Getter for a specific factions spawn area
	 * Inputs:
	 * 	faction: faction whos spawn area will be returned or null if not found
	 */
	public SpawnArea findSpawnArea(ControlFaction faction) {
		for(SpawnArea oneSpawnArea : this.getSpawnAreas()) {
			if(oneSpawnArea.belongsToFaction(faction)) {
				return oneSpawnArea;
			}
		}
		return null;
	}

}

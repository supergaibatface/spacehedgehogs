package newJobs;

import java.util.List;

import barChange.BarGain;
import barChange.BarLimiter;
import eventSuccessFactors.IEventSuccessFactor;
import events.EventTagName;
import gameCharacters.IGameCharacter;
import jobs.SkillGain;
import propertyBars.PropertyBarName;
import screenElements.ScreenButton;

public class SettlementWaterSearchJob extends BaseJob {

	@Override
	public void newElementAdded(IGameCharacter newChar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void oldElementRemoved(IGameCharacter oldChar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void safeDelete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void setWorkTags(List<EventTagName> tagList) {
		tagList.add(EventTagName.SearchForWaterSettlement);
		
	}

	@Override
	protected void setBarCost(List<BarLimiter> limiterList) {
		limiterList.add(this.mainFactory.getElemFactory().getPropertyBarLimiter(PropertyBarName.Stamina, 5));
		
	}

	@Override
	protected void setBarGains(List<BarGain> gainList) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void setSkillGains(List<SkillGain> gainList) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void addEventSuccessFactors(List<IEventSuccessFactor> factorList) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void socketGoingOnScreenCodeSpace() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void addButtonsToExtraCommandsList(List<ScreenButton> saveLocationList) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void doExtraCodePerWorkingCharacter(IGameCharacter target) {
		// TODO Auto-generated method stub
		
	}

}

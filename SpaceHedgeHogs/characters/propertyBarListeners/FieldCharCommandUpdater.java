package propertyBarListeners;

//Listener that calls text update on fieldCharCommandControl (meant for changes in attack or move numbers)
public class FieldCharCommandUpdater extends BaseBarChangeListener {

	@Override
	public void barChangeHappened(int newValue) {
		this.mainFactory.getFieldCharCommandControl().callTextUpdate();
		
	}

}

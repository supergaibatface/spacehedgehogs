package view;

//Data class for individual cutscene frame
public class CutsceneData {
	private String backgroundName;
	private String text;
	
	//constructor
	public CutsceneData(String backgroundName, String text) {
		this.backgroundName = backgroundName;
		this.text = text;
	}

	//getter for the cutscenes background image name
	public String getBackgroundName() {
		return backgroundName;
	}

	//setter for background image name
	public void setBackgroundName(String backgroundName) {
		this.backgroundName = backgroundName;
	}

	//getter for the scenes text
	public String getText() {
		return text;
	}

	//setter for the scenes text
	public void setText(String text) {
		this.text = text;
	}

}

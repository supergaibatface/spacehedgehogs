package questReward;

import dialogueCharacters.IDialogueCharacter;
import dialogueFlags.DialogueFlag;
import questRewardBase.BaseQuestReward;

public class GainDialogueFlagReward extends BaseQuestReward {
	private DialogueFlag gainedFlag;
	private IDialogueCharacter targetCharacter;
	
	public GainDialogueFlagReward(DialogueFlag gainedFlag, IDialogueCharacter targetCharacter) {
		this.gainedFlag = gainedFlag;
		this.targetCharacter = targetCharacter;
	}

	@Override
	public String getRewardDescription() {
		return "Will trigger dialogue flag: "+this.gainedFlag+" for character: "+this.targetCharacter.getName();
	}

	@Override
	protected void rewardScript() {
		this.targetCharacter.sendDialogueFlag(gainedFlag);
		
	}

}

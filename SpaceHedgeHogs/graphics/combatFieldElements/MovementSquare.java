package combatFieldElements;

import java.util.ArrayList;
import java.util.List;

import frameworks.IStackable;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import itemBase.IGameItem;
import screenElements.ScreenElement;
import screenElements.ScreenText;

//One square of a movementField
public class MovementSquare extends ScreenElement {
	private int locationNr;
	private FieldElement element;

	/*
	 * constructor
	 * Inputs:
	 * 	locationNr: squares number in a line
	 * 	size: visual size of a square
	 */
	public MovementSquare(int locationNr, int size) {
		super(0, 0);
		this.locationNr = locationNr;
		this.setSize(size, size);
	}
	
	public void bootUp() {
		super.bootUp();
		this.xDataStart = 0;
		this.yDataStart = 0;
	}
	
	//Method that checks if there is anything on the square
	public boolean isOccupied() {
		return this.element != null;
	}
	
	/*
	 * Sets an element on top of the square
	 * Inputs:
	 * 	element: the element that will be put on top of the square
	 */
	private void setElement(FieldElement element) {
		this.element = element;
		element.setLocation(this);
	}
	
	/*
	 * public method for moving element here, also checks that this location is free
	 * Inputs:
	 * 	character: character that will be moved here
	 */
	public boolean moveCharacterHere(FieldCharacter character) {
		if(isOccupied()) {
			return false;
		}
		else {
			setElement(character);
			return true;
		}
	}
	
	//used for moving a character or element away from this square
	public boolean moveCharacterAway() {
		if(isOccupied() ) {
			this.element = null;
			return true;
		}
		else {
			return false;
		}
	}
	
	//Forces an element to move away from here
	public void makeCharLeave() {
		this.element = null;
	}
	
	/*
	 * Spawns a character on this square
	 * Inputs:
	 * 	masterChar: game character that the spawned field character is based on
	 * 	faction: the faction that the spawned character belongs to
	 */
	public FieldCharacter SpawnCharacter(IGameCharacter masterChar, ControlFaction faction) {
		String imgName = factionImageChooser(faction);
		FieldCharacter newChar = this.mainFactory.getElemFactory().getFieldCharacter(masterChar, faction, imgName);
		moveCharacterHere(newChar);
		return newChar;
	}
	
	/*
	 * Method for deciding a fieldCharacters image based on their faction
	 * Inputs:
	 * 	faction: the faction that the spawned character belongs to
	 */
	private String factionImageChooser(ControlFaction faction) {
		if(faction == ControlFaction.PLAYER) {
			return "AquaTriangle";
		}
		else {
			return "RedTriangle";
		}
	}
	
	/*
	 * Spawns an item on the square as fieldItem
	 * Inputs:
	 * 	masterItem: trade item that the spawned item is based on
	 */
	public void SpawnItem(IGameItem masterItem) {
		String imgName = "GreenTriangle";
		FieldItem newItem = this.mainFactory.getElemFactory().getFieldItem(masterItem, imgName);
		this.setElement(newItem);
	}
	
	/*
	 * Checks if the specific fieldCharacter is on this square
	 * Inputs:
	 * 	oneCharacter: field character that is checked to be on this square
	 */
	public boolean isCharOnSquare(FieldCharacter oneCharacter) {
		if(!isOccupied()) {
			return false;
		}
		else if (oneCharacter.equals(this.element)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	//Getter for the fieldElement on this square
	public FieldElement getFieldElement() {
		return this.element;
	}
	
	//getter for the squares location number
	public int getLocationNr() {
		return this.locationNr;
	}
	
	//Getter for the square that is right from this one
	public MovementSquare getRightSquare() {
		return getSideSquare(this.locationNr + 1);
	}
	
	//Getter for the square that is left from this one
	public MovementSquare getLeftSquare() {
		return getSideSquare(this.locationNr - 1);
	}
	
	//Getter for the square that is above from this one
	public MovementSquare getTopSquare() {
		return getSquare(this.locationNr, getTopLine());
	}
	
	//Getter for the square that is below from this one
	public MovementSquare getBotSquare() {
		return getSquare(this.locationNr, getBotLine());
	}
	
	//Getter for the line that is above this squares line
	private MovementLine getTopLine() {
		return getMasterLine().getTopLine();
	}
	
	//Getter for the line that is below from this squares line
	private MovementLine getBotLine() {
		return getMasterLine().getBotLine();
	}
	
	/*
	 * Getter for any other square in this squares line
	 * Inputs:
	 * 	squareNr: searched squares number
	 */
	private MovementSquare getSideSquare(int squareNr) {
		return getSquare(squareNr, getMasterLine());
	}
	
	/*
	 * Getter for a square in a line
	 * Inputs:
	 * 	squareNr: returned squares number
	 * 	line: movementLine that the searched square is in
	 */
	private MovementSquare getSquare(int squareNr, MovementLine line) {
		if(line != null) {
			return line.getSquareNr(squareNr);
		}
		else {
			return null;
		}
		
	}
	
	//Getter for the master line that this square is on
	private MovementLine getMasterLine() {
		return (MovementLine) this.wrapper;
	}
	
	/*
	 * Distance calculator from a different square, counts in a cross-step style, 
	 * where anything location one step away in a cross patter is n+1 away, where n
	 * is the previous steps taken to get to the square
	 * Inputs:
	 * 	otherSquare: other square who's distance from this one will be calculated
	 */
	public int getDistanceFrom(MovementSquare otherSquare) {
		return calcXDistance(otherSquare) + calcYDistance(otherSquare);
	}
	
	/*
	 * Method for calculating x distance between squares, measurement unit is one square
	 * Inputs:
	 * 	otherSquare: the distance to that square
	 */
	private int calcXDistance(MovementSquare otherSquare) {
		return calcDistance(this.locationNr, otherSquare.getLocationNr());
	}
	
	/*
	 * Method for calculating y distance between squares, measurement unit is one square
	 * Inputs:
	 * 	otherSquare: the distance to that square
	 */
	private int calcYDistance(MovementSquare otherSquare) {
		return calcDistance(this.getMasterLine().getLineNr(), otherSquare.getMasterLine().getLineNr());
	}
	
	/*
	 * Calculates the distance between two points represented by integers
	 * Inputs:
	 * 	firstPoint: first point in the calculation
	 * 	secondPoint: second point in the calculation
	 * Output:
	 * 	distance: positive value distance between the two input points
	 */
	private int calcDistance(int firstPoint, int secondPoint) {
		int distance = firstPoint - secondPoint;
		if(distance < 0) {
			distance = distance * -1;
		}
		return distance;
	}
	
	//Debug method that prints out this squares location in the movement grid
	public String printOutCoordinates() {
		int xNr = this.locationNr;
		int yNr = this.getMasterLine().getLineNr();
		return "coordinates, x: " + xNr + ", y: " + yNr;
	}

	@Override
	public void fillTopElementList(List<IStackable> list) {
		if(this.isOccupied()) {
			list.add(element);
		}
		
	}

	@Override
	public void fillTopTextList(List<ScreenText> list) {
		
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOffScreenScript() {
		// TODO Auto-generated method stub
		
	}

}

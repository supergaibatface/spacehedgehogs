package screenElements;

import java.util.ArrayList;
import java.util.List;

import frameworks.IStackable;
import generalGraphics.ButtonLookType;
import generalGraphics.ClickBox;

/*
 * ScreenElement thats content can be scrolled
 */
public class ScreenScrollPanel extends ScreenElement {
	protected ArrayList<IStackable> elements;
	protected int xScroll;
	protected int yScroll;

	//constructor
	public ScreenScrollPanel(int x, int y, int length, int height) {
		super(x, y);
		this.setSize(length, height);
	}
	
	public void bootUp() {
		super.bootUp();
		this.elements = new ArrayList<IStackable>();
		this.xScroll = 0;
		this.yScroll = 0;
		this.xDataStart = 0;
		this.yDataStart = 0;
	}
	
	//Method for adding a new element to this panels content
	public boolean addNewElement(IStackable newElement) {
		this.elements.add(newElement);
		newElement.setWrapper(this);
		this.doingScrollCalc();
		return true;
	}

	//Getter for a visible area in this elements contents
	public ClickBox getLocalScrolledBox() {
		return new ClickBox( this.getXScroll(), 
				this.getYScroll(), 
				this.getLength() - this.xDataStart, 
				this.getHeight() - this.yDataStart);
	}
	
	//Getter for the length of visible area in this element
	protected int getDisplayBoxLength() {
		int length = this.getLength() - 50;
		return length;
	}
	
	//Getter for the height of visible area in this element
	protected int getDisplayBoxHeight() {
		int height = this.getHeight() - 50;
		return height;
	}
	
	//Getter for the x dimensions scroll value
	public int getXScroll() {
		return this.xScroll;
	}
	
	//Getter for the y dimension scroll value
	public int getYScroll() {
		return this.yScroll;
	}
	
	//Method for scrolling down this elements contents
	public void scrollDown(int changeAmount) {
		this.yScroll = this.scroll(this.yScroll, changeAmount);
		this.doingScrollCalc();
	}
	
	//Method for scrolling up this elements contents
	public void scrollUp(int changeAmount) {
		this.yScroll = this.scroll(this.yScroll, -changeAmount);
		this.doingScrollCalc();
	}
	
	//Method for scrolling right this elements contents
	public void scrollRight(int changeAmount) {
		this.xScroll = this.scroll(this.xScroll, changeAmount);
		this.doingScrollCalc();
	}
	
	//Method for scrolling left this elements contents
	public void scrollLeft(int changeAmount) {
		this.xScroll = this.scroll(this.xScroll, -changeAmount);
		this.doingScrollCalc();
	}

	//Method for getting new scroll value based on current value and change amount
	private int scroll(int scrollCurrentValue, int changeAmount) {
		return scrollCurrentValue + changeAmount;
	}
	
	/*
	 * TODO NB! This should be imrpoved
	 * Getter for buttons that scroll this panel
	 */
	public ScreenButton getAScrollButtonForThisPanel(boolean xAxis, boolean increaseScroll) {
		ButtonLookType lookType;
		int xCoord;
		int yCoord;
		if(xAxis) {
			yCoord = this.getY() + this.getHeight();
			if(increaseScroll) {
				xCoord = this.getX() + this.getLength() - 50;
				lookType = ButtonLookType.SCROLLRIGHT;
				
			}
			else {
				xCoord = this.getX();
				lookType = ButtonLookType.SCROLLLEFT;
			}
		}
		else {
			xCoord = this.getX() + this.getLength();
			if(increaseScroll) {
				yCoord = this.getY() + this.getHeight() - 50;
				lookType = ButtonLookType.SCROLLDOWN;
			}
			else {
				yCoord = this.getY();
				lookType = ButtonLookType.SCROLLUP;
			}
		}
		ScreenButton newScrollButton = this.mainFactory.getElemFactory().getScreenButton(xCoord, yCoord, lookType, " ");
		newScrollButton.addButtonEventListener(this.mainFactory.getElemFactory().getPanelScrollListener(this, xAxis, increaseScroll));
		return newScrollButton;
	}

	/*
	 * Calls for visiblity calculation on this element and its children
	 */
	public void doingScrollCalc() {
		ClickBox scrolledDisplayBox = this.getDisplayBox().getOverlap(this.getLocalScrolledBox());
		ClickBox overLap;
		for(IStackable oneElement : this.elements) {
			if(!oneElement.isForceNonScroll()) {
				overLap = oneElement.getClickBox().getOverlap(scrolledDisplayBox);
				oneElement.overLapMaster(overLap);
			}
			else {
				overLap = oneElement.getClickBox().getOverlap(this.getDisplayBox());
				oneElement.overLapMaster(overLap);
			}

		}
	}
	
	/*
	 * Calls for visiblity calculation on all its children
	 */
	protected void callOverlapOnChildren(ClickBox localVisibleBox) {
		ClickBox overLap;
		for(IStackable oneElement : this.getOnTopElements()) {
			if(!oneElement.isForceNonScroll()) {
				overLap = localVisibleBox.getOverlap(this.getLocalScrolledBox()).getOverlap(oneElement.getClickBox());
				oneElement.overLapMaster(overLap);
			}
			else {
				overLap = localVisibleBox.getOverlap(oneElement.getClickBox());
				oneElement.overLapMaster(overLap);
			}

		}
	}

	@Override
	public void fillTopElementList(List<IStackable> list) {
		list.addAll(this.elements);
	}

	@Override
	public void fillTopTextList(List<ScreenText> list) {
		
	}
	
	public void safeDelete() {
		for(IStackable oneElement : this.elements) {
			oneElement.safeDelete();
		}
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOffScreenScript() {
		// TODO Auto-generated method stub
		
	}

}

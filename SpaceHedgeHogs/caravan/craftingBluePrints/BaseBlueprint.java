package craftingBluePrints;

import java.util.ArrayList;
import java.util.List;

import dataValues.ItemValue;
import factories.IAutoMake;
import factories.MainFactory;
import itemBase.BaseItem;
import itemBase.IGameItem;
import itemExceptions.CantStartCraftingProjectException;

public abstract class BaseBlueprint implements IAutoMake, IBlueprint{
	protected MainFactory mainFactory;
	private String name;
	private int neccesaryWorkValue;
	private IGameItem targetValue;
	private List<IGameItem> components;
	
	public BaseBlueprint(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.neccesaryWorkValue = this.bootAssignNeccesaryWorkValue();
		this.targetValue = this.bootGiveTargetValue();
		this.targetValue.addAmount(bootGiveTargetValueAmount());
		this.components = new ArrayList<IGameItem>();
		this.bootAddCraftingComponents(components);
	}

	@Override
	public int getNeccesaryWorkValue() {
		return this.neccesaryWorkValue;
	}
	
	protected abstract int bootAssignNeccesaryWorkValue();
	
	public IGameItem getFinishedProjetValue() {
		return this.targetValue;
	}
	
	protected abstract IGameItem bootGiveTargetValue();
	
	protected abstract int bootGiveTargetValueAmount();
	
	protected abstract void bootAddCraftingComponents(List<IGameItem> saveLocation);
	
	public List<IGameItem> getComponents() {
		return new ArrayList<IGameItem>(this.components);
	}
	
	public boolean canMakeProject() {
		if(!this.hasComponentsInInventory()) {
			return false;
		}
		return true;
	}
	
	public boolean hasComponentsInInventory() {
		for(IGameItem oneItem : this.components) {
			if(!this.mainFactory.getInventory().hasItemWithEnoughSize(oneItem)) {
				return false;
			}
		}
		return true;
	}
	
	public CraftingProject makeGetNewCraftingProject() throws CantStartCraftingProjectException {
		if(!this.canMakeProject()) {
			throw new CantStartCraftingProjectException(this.targetValue.getName().toString());
		}
		this.takeAwayComponentItemsFromInventory();
		CraftingProject newProject = this.getNewCraftingProjectForBlueprint();
		return newProject;
	}
	
	private CraftingProject getNewCraftingProjectForBlueprint() {
		return this.mainFactory.getCraftingFactory().getNewCraftingProject(this);
	}
	
	private void takeAwayComponentItemsFromInventory() {
		this.mainFactory.getInventory().tryToRemoveItemWithAmount(components);
	}
		


}

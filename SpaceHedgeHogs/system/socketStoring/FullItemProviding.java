package socketStoring;

import java.util.ArrayList;

import itemBase.IGameItem;
import sreenElementsSockets.ScreenSimpleItem;

/*
 * Provides full list of screen items that is listed in inventory as trade items
 */
public class FullItemProviding extends BaseContentProviding<ScreenSimpleItem> {

	@Override
	public ArrayList<ScreenSimpleItem> getArrayOfSupplies() {
		ArrayList<ScreenSimpleItem> items = new ArrayList<ScreenSimpleItem>();
		for (IGameItem oneItem : this.mainFactory.getInventory().getCurrentInventory()) {
			items.add(this.mainFactory.getElemFactory().getScreenSimpleItem(oneItem, oneItem.getImageName()));
		}
		return items;
	}

}

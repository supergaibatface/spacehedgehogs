package commandAction;

import battle.SpawnArea;
import combatFieldElements.ControlFaction;
import combatFieldElements.FieldCharacter;
import combatFieldElements.MovementSquare;
import screenElements.ScreenElement;
import sreenElementsSockets.ScreenSimpleChar;

//Command action for spawning a ScreenCharacter on a movement field as field character
public class CACharSpawn extends CABaseClass<MovementSquare> {

	@Override
	public boolean inputIsCorrect() {
		int nr = this.hasItemTypeClicked();
		if(nr<0) {
			return false;
		}
		MovementSquare selectedSquare = this.castToClickTargetClass(conditions.getClickedItem(nr));
		if(!detailedItemCheck(selectedSquare)) {
			return false;
		}
		return true;
	}
	
	//Checks if a square is occupied already
	private boolean detailedItemCheck(MovementSquare square) {
		if(square.isOccupied()) {
			return false;
		}
		SpawnArea area = this.mainFactory.getGameStateController().getBattleController().findSpawnArea(ControlFaction.PLAYER);
		if(area == null) {
			return false;
		}
		if(!area.getSquares().contains(square)) {
			return false;
		}
		if(this.mainFactory.getPlayerLimiting().hasSpawnLimiter()) {
			if(!this.mainFactory.getPlayerLimiting().getSpawnLimiter().allowedToSpawn()) {
				return false;
			}
		}
		return true;
	}

	@Override
	public void actionImplement() {
		MovementSquare square = this.castToClickTargetClass(conditions.getClickedItem(this.hasItemTypeClicked()));
		ScreenSimpleChar character = (ScreenSimpleChar) conditions.getSelectedItem();
		FieldCharacter spawnedChar = square.SpawnCharacter(character.getMaster(), ControlFaction.PLAYER);
		spawnedChar.makeLabel();
		selection.takeCharAway();
		countDownFromSpawnLimiting();
		this.mainFactory.getGameStateController().getBattleController().clearVisuals();
	}

	@Override
	public Class<MovementSquare> setClickTargetClass() {
		return MovementSquare.class;
	}
	
	//will check if there is a spawn limit and then counts it down by one and updates its graphical writing
	private void countDownFromSpawnLimiting() {
		if(this.mainFactory.getPlayerLimiting().hasSpawnLimiter()) {
			this.mainFactory.getPlayerLimiting().getSpawnLimiter().countDownSpawn();
			this.mainFactory.getPlayerLimiting().writeNewText();
		}
	}

	@Override
	protected int giveMouseButtonNr() {
		return this.giveLeftMouseButtonValue();
	}

}

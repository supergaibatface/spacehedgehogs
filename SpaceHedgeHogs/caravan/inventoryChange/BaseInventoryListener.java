package inventoryChange;

import factories.IAutoMake;
import factories.MainFactory;

//Base class for inventory listener, listeners that deal with changes in the inventory
public abstract class BaseInventoryListener implements IAutoMake, IInventoryListener {
	protected MainFactory mainFactory;

	@Override
	public abstract void inventoryNewItemEventOccured(InventoryNewItemEvent evt);

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public abstract void bootUp();

}

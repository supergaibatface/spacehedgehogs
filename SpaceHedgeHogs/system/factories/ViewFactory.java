package factories;

import view.IGameView;
import viewClasses.BattleTutorial1View;
import viewClasses.BattleTutorial2View;
import viewClasses.BattleTutorial3View;
import viewClasses.DebugMenuView;
import viewClasses.DebugView;
import viewClasses.LoupeCampView;
import viewClasses.LoupeFortView;
import viewClasses.MainMenuView;
import viewClasses.TutorialCampView;
import viewClasses.TutorialTravelView;

public class ViewFactory extends BaseMinorFactory {
	private IGameView debugView;
	private IGameView mainMenuView;
	private IGameView debugMenuView;
	private IGameView battleTutorial1View;
	private IGameView battleTutorial2View;
	private IGameView battleTutorial3View;
	private IGameView tutorialCampView;
	private IGameView travelTutorialView;
	private IGameView loupeCampView;
	private IGameView loupeFortView;
	
	public IGameView getDebugView() {
		if(this.debugView == null) {
			this.debugView = setUpNewElement(new DebugView());
		}
		return this.debugView;
	}
	
	public IGameView getMainMenuView() {
		if(this.mainMenuView == null) {
			this.mainMenuView = setUpNewElement(new MainMenuView());
		}
		return this.mainMenuView;
	}
	
	public IGameView getDebugMenuView() {
		if(this.debugMenuView == null) {
			this.debugMenuView = setUpNewElement(new DebugMenuView());
		}
		return this.debugMenuView;
	}
	
	public IGameView getBattleTutorial1View() {
		if(this.battleTutorial1View == null) {
			this.battleTutorial1View = setUpNewElement(new BattleTutorial1View());
		}
		return this.battleTutorial1View;
	}
	
	public IGameView getBattleTutorial2View() {
		if(this.battleTutorial2View == null) {
			this.battleTutorial2View = setUpNewElement(new BattleTutorial2View());
		}
		return this.battleTutorial2View;
	}
	
	public IGameView getBattleTutorial3View() {
		if(this.battleTutorial3View == null) {
			this.battleTutorial3View = setUpNewElement(new BattleTutorial3View());
		}
		return this.battleTutorial3View;
	}
	
	public IGameView getTutorialCampView() {
		if(this.tutorialCampView == null) {
			this.tutorialCampView = setUpNewElement(new TutorialCampView());
		}
		return this.tutorialCampView;
	}
	
	public IGameView getTravelTutorialView() {
		if(this.travelTutorialView == null) {
			this.travelTutorialView = setUpNewElement(new TutorialTravelView());
		}
		return this.travelTutorialView;
	}
	
	public IGameView getLoupeCampView() {
		if(this.loupeCampView == null) {
			this.loupeCampView = setUpNewElement(new LoupeCampView());
		}
		return this.loupeCampView;
	}
	
	public IGameView getLoupeFortView() {
		if(this.loupeFortView == null) {
			this.loupeFortView = setUpNewElement(new LoupeFortView());
		}
		return this.loupeFortView;
	}

}

package cutsceneInstances;

import view.CutsceneDataCollection;

public interface ICutsceneInstance {
	
	public void startSavedCutscene();
	
	public CutsceneDataCollection getSavedCutScene();

}

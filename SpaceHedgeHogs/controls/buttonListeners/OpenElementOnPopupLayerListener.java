package buttonListeners;

import frameworks.IStackable;

/*
 * Button listener that will open a popup on click
 */
public class OpenElementOnPopupLayerListener extends BaseButtonListener {
	private IStackable newElement;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	popUp: popup that will be opened on click
	 */
	public OpenElementOnPopupLayerListener(IStackable newElement) {
		this.newElement = newElement;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.mainFactory.getGElementManager().addQuickPopUp(newElement);
		
	}

	@Override
	public String toInfo() {
		return "Opens a popup";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

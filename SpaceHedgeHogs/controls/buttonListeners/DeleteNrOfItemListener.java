package buttonListeners;

import itemBase.IGameItem;

//Will delete a certain amount of specific item upon activation
public class DeleteNrOfItemListener extends BaseButtonListener {
	private int amount;
	private IGameItem item;
	
	/*
	 * constructor
	 * Inputs:
	 * 	amount: the amount of item that will be deleted
	 * 	item: the item that will have a designated amount deleted from it
	 */
	public DeleteNrOfItemListener(int amount, IGameItem item) {
		this.amount = amount;
		this.item = item;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.item.removeAmount(amount);
		
	}

	@Override
	public String toInfo() {
		// TODO Auto-generated method stub
		return "Removes "+amount+" of "+item.getName();
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

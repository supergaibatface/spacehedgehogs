package timedSystems;

//Interface for objects that are supposed to dissapear after some time
public interface ITimedObject {
	
	//Method that counts down till dissapearing
	public void countDown();
	
	//Checks if this item is supposed to be deleted immediately
	public boolean checkIfDelete();

}

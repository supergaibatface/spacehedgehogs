package buttonListeners;

import announcements.Notice;

/*
 * Button listener that will open a notice as a pop up
 */
public class OpenNoticeListener extends BaseButtonListener {
	private Notice notice;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	notice: notice that will be opened on click
	 */
	public OpenNoticeListener(Notice notice) {
		this.notice = notice;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.notice.triggerPopUp();
		
	}

	@Override
	public String toInfo() {
		return "Opens a notice";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

package jobPanels;

import java.util.ArrayList;
import java.util.List;

import activityListeners.JobFrameVisualListener;
import frameworks.IStackable;
import frameworks.IndependentBaseFrame;
import jobs.JobTopic;
import newJobs.IJob;
import screenElements.ScreenText;
import sreenElementsSockets.ScrollCharSocket;
import timeListeners.JobFrameExpellingListener;

public abstract class BaseJobPanel extends IndependentBaseFrame implements IJobPanel {
	private JobFrameVisualListener visualActivityListener;
	private JobFrameExpellingListener jobFrameExpellingListener;
	protected JobTopic passiveJobs;
	protected List<JobTopic> jobTopics;
	
	public void bootUp() {
		super.bootUp();
		this.setLocation(50, 50);
		this.jobTopics = new ArrayList<JobTopic>();
		this.makePassiveJobsTopic();
	}
	
	private void makePassiveJobsTopic() {
		this.passiveJobs = this.mainFactory.getElemFactory().getJobTopic(this);
		this.fillInPassiveJobTopic(passiveJobs);
		this.jobTopics.add(passiveJobs);
	}
	
	protected abstract void fillInPassiveJobTopic(JobTopic saveLocation);

	/*@Override
	public void callLocationUpdate() {
		this.setLocationsForTopics();
	}*/
	
	//Default old job frame way of setting locations for topics
	/*private void setLocationsForTopics() {
		int currentY = 0;
		int currentX= 0;
		int newElementLength = 0;
		for(JobTopic oneTopic : this.getAllJobTopics()) {
			newElementLength = oneTopic.getFullLength();
			//goes to the current line
			if(currentX + newElementLength < this.getJobPanelLength()) {
				oneTopic.setLocation(currentX, currentY);
				currentX = currentX + newElementLength + getBufferSpaceSize();
			}
			//goes to the next line
			else {
				oneTopic.setLocation(0, currentY + this.getOneTopicHeight());
				currentX = newElementLength;
				currentY = currentY + this.getOneTopicHeight();
			}
			//this.debugPringForOneTopic(oneTopic);
		}
	}*/
	
	protected List<JobTopic> getAllJobTopics() {
		return this.jobTopics;
	}
	
	protected abstract int getJobPanelLength();
	
	protected abstract int getOneTopicHeight();
	
	protected abstract int getBufferSpaceSize();

	@Override
	public void callSocketHighlightUpdate() {
		this.callVisualQuesActivation();
		
	}
	
	//for calling an update for the visual ques of sockets in this frame
	private void callVisualQuesActivation() {
		for(JobTopic oneTopic : this.getAllJobTopics()) {
			oneTopic.callVisualQues();
		}
	}

	@Override
	public void callCleanHighlights() {
		this.clearVisualQues();
		
	}
	
	//For cleaging all visual ques on sockets in this frame
	private void clearVisualQues() {
		for(JobTopic oneTopic : this.getAllJobTopics()) {
			oneTopic.clearVisualQues();
		}
	}
	

	@Override
	public void expellUnfitCharacters() {
		this.expellAllCharsWhoDontFit();
		
	}
	
	//Does a rewuirement check on all characters currently assigned to sockets, and expels those that dont fit
	private void expellAllCharsWhoDontFit() {
		for(JobTopic oneTopic : this.getAllJobTopics()) {
			oneTopic.expellCharsWhoDontFit();
		}
	}
	

	@Override
	protected int getFullHeight() {
		int highestPoint = 0;
		for(JobTopic oneTopic : this.getAllJobTopics()) {
			if(oneTopic.getY() + oneTopic.getHeight() > highestPoint) {
				highestPoint = oneTopic.getY() + oneTopic.getHeight();
			}
		}
		return highestPoint;
	}

	@Override
	protected int getFullLength() {
		int highestPoint = 0;
		for(JobTopic oneTopic : this.getAllJobTopics()) {
			if(oneTopic.getX() + oneTopic.getLength() > highestPoint) {
				highestPoint = oneTopic.getX() + oneTopic.getLength();
			}
		}
		return highestPoint;
	}

	@Override
	public void fillTopElementList(List<IStackable> list) {
		list.addAll(getAllJobTopics());
		
	}

	@Override
	public void fillTopTextList(List<ScreenText> list) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOnScreenScript() {
		setUpVisualListener();
		setUpTurnChangeListeners();
	}
	
	//for setting up a listener that informs the frame when sockets need visual updates
	private void setUpVisualListener() {
		this.visualActivityListener = this.mainFactory.getElemFactory().getJobFrameVisualListener(this);
		this.mainFactory.getActivityController().addListener(visualActivityListener);
	}
	
	//sets up a listener that informs of turn changes
	private void setUpTurnChangeListeners() {
		this.jobFrameExpellingListener = this.mainFactory.getElemFactory().getJobFrameExpellingListener(this);
		this.mainFactory.getWorldTime().addTimeListener(jobFrameExpellingListener);
	}
	
	@Override
	protected void goingOffScreenScript() {
		removeTurnChangeListeners();
		removeVisualListeners();
		clearElementsFromSockets();
	}
	
	//for removing this frames turn change listener
	private void removeVisualListeners() {
		this.mainFactory.getActivityController().removeListener(visualActivityListener);
	}
	
	//for removing this frames turn change listener
	private void removeTurnChangeListeners() {
		this.mainFactory.getWorldTime().removeTimeListener(jobFrameExpellingListener);
	}
	
	private void clearElementsFromSockets() {
		for(JobTopic oneTopic : this.getAllJobTopics()) {
			oneTopic.clearElementsFromSockets();
		}
	}
	
	protected ScrollCharSocket getNewStandardSocketForJob(IJob job, String socketTitle) {
		ScrollCharSocket newSocket = this.makeBaseSocket(2, "LightGreen", socketTitle);
		newSocket.setJob(job);
		return newSocket;
	}
	
	/*
	 * Makes a socket base
	 * Inputs:
	 * 	socketSize: visual size for the socket (in how many elements will be shown)
	 * 	imgName: socket visual base
	 * 	socketName: name of the socket
	 */
	protected ScrollCharSocket makeBaseSocket(int socketSize, String imgName, String socketName) {
		ScrollCharSocket workSocket = this.mainFactory.getElemFactory().getScrollCharSocket(0, 0, 
				this.mainFactory.getLengthAssigner().getSocketLengthForSize(1), 
				this.mainFactory.getLengthAssigner().getSocketLengthForSize(socketSize), imgName);//.getScreenSocket(0, 0, socketSize, imgName);
		workSocket.setTitle(socketName);
		return workSocket;
	}

}

package factories;

/*
 * Interface for classes that would be made through a factory object
 */
public interface IAutoMake {
	
	//Method for setting the main factory of the element
	public void setMainFactory(MainFactory mainFactory);
	
	//Method for booting up a new object after using a constructor
	public void bootUp();

}

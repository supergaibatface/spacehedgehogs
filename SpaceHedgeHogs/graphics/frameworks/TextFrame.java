package frameworks;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.util.ArrayList;
import java.util.List;

import screenElements.ScreenElement;
import screenElements.ScreenText;

/*
 * Frame class for containing and managing multiple lines on a screen element
 */
public class TextFrame extends BaseFrame {
	private ArrayList<ScreenText> lines;
	private FontMetrics fontMetrics;

	//constructor
	public TextFrame(IStackable wrapper) {
		super(wrapper);
		// TODO Auto-generated constructor stub
	}
	
	//bootup method
	public void bootUp() {
		super.bootUp();
		this.lines = new ArrayList<ScreenText>();
		this.getFontMetrics();
	}
	
	//private method for retrieving font metrics used to calculate text size
	private void getFontMetrics() {
		this.fontMetrics = this.mainFactory.getScreen().getFontMetrics(new Font("Tahoma", Font.PLAIN, lenAss.getOneTextSize()));
	}
	
	//getter for the full height of the text frame
	public int getFullHeight() {
		//int height = this.lines.size() * this.lenAss.getOneTextSize();
		return this.lines.size() * this.getOneTextHeight();
	}
	
	//Returns the heigh of one line of text
	public int getOneTextHeight() {
		return this.fontMetrics.getAscent();
	}
	
	/*
	 * Used for setting the text colour for one line of text 
	 * Inputs:
	 * 	newColor: color type value that will become the new color of the designated line
	 * 	lineNr: location nr of the line in frame
	 */
	public boolean setColorForALine(Color newColor, int lineNr) {
		if(lineNr < this.lines.size()) {
			this.lines.get(lineNr).setColor(newColor);
			return true;
		}
		return false;
	}
	
	/*
	 * Method for filling the frame with lines of text, will overwrite whatever was written on it before
	 * Input:
	 * 	newLines: an array of strings that will be displayed in the text frame
	 */
	public void fillTexts(List<String> newLines) {
		this.removeExtraLines(newLines.size());
		for(String line: newLines) {
			writeLine(line, newLines.indexOf(line));
		}
	}
	
	/*
	 * Removes lines that will not be used by the new set of writing
	 * Input:
	 * 	neededAmount: the nr of lines that are needed for displaying the new set of texts
	 */
	protected void removeExtraLines(int neededAmount) {
		while (this.lines.size() > neededAmount) {
			this.lines.remove(neededAmount);
		}
	}
	
	/*
	 * Writes a line into the text frame
	 * Input:
	 * 	value: string value of the line
	 * 	lineNr: the location in the array where the line would be
	 */
	protected void writeLine(String value, int lineNr) {
		if(lineNr > this.lines.size() - 1) {
			addNewLine(value);
		}
		else {
			changeText(value, lineNr);
		}
	}
	
	
	/*
	 * adds a new line to the textframe, its used when new set of lines require more space then previous text
	 * Input:
	 * 	value: string value of the new line
	 */
	protected void addNewLine(String value) {
		ScreenText newText = this.mainFactory.getElemFactory().getText(0, this.getOneTextHeight() * (this.lines.size() + 1), value);
		newText.setWrapper(this);
		this.lines.add(newText);
	}
	
	/*
	 * Overwrites an existing line in the textframe
	 * Input:
	 * 	newValue: string value that will be written onto an existing line
	 * 	nr: location in the array of the line that will be overwritten
	 */
	protected void changeText(String newValue, int nr) {
		this.lines.get(nr).setText(newValue);
	}
	
	//Getter for all the lines in the text frame
	public ArrayList<ScreenText> getTexts() {
		return this.lines;
	}

	@Override
	public int getFullLength() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void fillTopElementList(List<IStackable> list) {
		
	}

	@Override
	public void fillTopTextList(List<ScreenText> list) {
		for(ScreenText oneText : this.lines) {
			list.add(oneText);
		}
		
	}
	
	public void debugPrintOut() {
		System.out.println("textFrame");
		System.out.println("contents: ");
		for(ScreenText oneLine : this.lines) {
			System.out.println(oneLine.getText());
			System.out.println("line is visible: "+oneLine.isVisibile());
			System.out.println("own x: "+oneLine.getX()+", full x: "+oneLine.getOffWrapperX());
			System.out.println("own y: "+oneLine.getY()+", full y: "+oneLine.getOffWrapperY());
		}
		System.out.println("-----------");
	}
	
	//Sets debug values for every line of text in this frame
	public void setDebugForAlltexts() {
		for(ScreenText oneLine : this.lines) {
			oneLine.setDebug(true);
		}
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOffScreenScript() {
		// TODO Auto-generated method stub
		
	}
	
}


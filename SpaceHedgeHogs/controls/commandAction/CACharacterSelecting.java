package commandAction;

import combatFieldElements.ControlFaction;
import sreenElementsSockets.ScreenSimpleChar;

//Command action for activating a screenCharcter
public class CACharacterSelecting extends CABaseClass<ScreenSimpleChar> {

	@Override
	public boolean inputIsCorrect() {
		return this.standardClickCheck();
	}

	@Override
	public void actionImplement() {
		ScreenSimpleChar selectedChar = this.castToClickTargetClass(conditions.getClickedItem(this.hasItemTypeClicked()));
		this.selection.makeActive(selectedChar);
		if(this.mainFactory.getGameStateController().inBattle()) {
			this.mainFactory.getGameStateController().getBattleController().setVisualSpawnSquares(ControlFaction.PLAYER);
		}
	}

	@Override
	public Class<ScreenSimpleChar> setClickTargetClass() {
		return ScreenSimpleChar.class;
	}

	@Override
	protected int giveMouseButtonNr() {
		// TODO Auto-generated method stub
		return this.giveLeftMouseButtonValue();
	}


}

package gameState;

import java.util.ArrayList;

import central.AICharacterSet;
import dataValues.CharValue;
import screenElements.ScreenButton;
import skills.SkillName;
import view.CutsceneData;
import view.CutsceneDataCollection;
import view.GameView;
import view.IGameView;
import view.ViewName;

public class BattleTutorialState2 extends BaseGameState {

	@Override
	protected void loadInPlayerCharacters(ArrayList<CharValue> listOfChars) {
		
		CharValue newCharValue = new CharValue("Bob", "charspot");
		newCharValue.addSavedSkillValue(SkillName.PhysicalForm, 20);
		newCharValue.addSavedSkillValue(SkillName.MeleeCombat, 20);;
		listOfChars.add(newCharValue);
		
		newCharValue = new CharValue("Michael", "charspot");
		newCharValue.addSavedSkillValue(SkillName.PhysicalForm, 20);
		newCharValue.addSavedSkillValue(SkillName.MeleeCombat, 20);;
		listOfChars.add(newCharValue);
		
	}

	@Override
	protected void bootLoadFillViewList() {
		/*GameView newView =  mainFactory.getViewManager().makeAndGetDebugView();
		this.travelView = newView.getName();*/
		this.travelView = ViewName.Debug;
	}

	@Override
	protected void loadInBattleViewBase(IGameView viewSaveLocation) {
		viewSaveLocation = this.mainFactory.getViewFactory().getBattleTutorial2View();
		this.mainFactory.getViewManager().addNewView(viewSaveLocation);
		
	}
	
	/*@Override
	protected void loadInOtherViews() {
		// TODO Auto-generated method stub
		
	}*/

	@Override
	protected void loadInInventory() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void loadInTravelOptions() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void loadInCaravanJobs() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startGameScript() {
		ArrayList<AICharacterSet> enemies = new ArrayList<AICharacterSet>();
		enemies.add(this.mainFactory.getCharacterManager().getEnemyCharacterSets().get(1));
		enemies.add(this.mainFactory.getCharacterManager().getEnemyCharacterSets().get(1));
		this.mainFactory.getGameStateController().addEnemysForAFight(enemies);
		
		this.mainFactory.getGameStateController().setBattleTutorialFlag(true);
		this.mainFactory.getGameStateController().startBattle(this.mainFactory.getBattleMapManager().getTutorialMovementField2(), 2, this.mainFactory.getElemFactory().getTimeSpawnEveryTurnOne());
		this.mainFactory.getGameStateController().getBattleController().insertSpecialWinPopUp(makeNextModulePopUp());
		
		this.mainFactory.getViewManager().changeToView(ViewName.Battle);
		
		this.mainFactory.getCutsceneBuilder().startACutscene(this.makeStartCutscene());
		
	}

	@Override
	protected void addNextPartButtonListeners(ScreenButton nextPartButton) {
		nextPartButton.addButtonEventListener(this.mainFactory.getElemFactory().getImportAllCharsToModuleListener(this.mainFactory.getBattleTutorialState3()));
		nextPartButton.addButtonEventListener(this.mainFactory.getElemFactory().getLoadInGameStateListener(this.mainFactory.getBattleTutorialState3()));
		
	}
	
	//Makes tutorial cutscene for the game
	private CutsceneDataCollection makeStartCutscene() {
		CutsceneDataCollection tutorial = this.mainFactory.getElemFactory().getCutsceneDataCollection("Intro2");
		CutsceneData newFrame = new CutsceneData("travelBG", "The monster.... its down.");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "You can see it in the distance, the palace.... in flames.");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "There is still hope. You rush through the streets.");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "A voice calls out to you, its Bob. Michael is also there.");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "They too got seperated in the fighting and even now there are enemies blocking the way.");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "But now theres 3 of you. And you got the element of surprise.");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "They wont stand a chance!");
		tutorial.addNewCutScene(newFrame);
		//tutorial.addFinalButtonEventListener(this.mainFactory.getElemFactory().getViewChangeListener(ViewName.Battle));
		return tutorial;
	}

}

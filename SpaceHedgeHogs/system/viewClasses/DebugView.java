package viewClasses;

import generalGraphics.ButtonLookType;
import screenElements.ScreenBackground;
import screenElements.ScreenButton;
import screenElements.ScreenDataBox;
import screenElements.ScreenPopUp;
import view.BaseGameView;
import view.GameView;
import view.ViewName;

public class DebugView extends BaseGameView {

	public DebugView() {
		super(ViewName.Debug);
	}

	@Override
	protected String bootGiveBackgroundImageName() {
		return "TestBG";
	}

	@Override
	protected void bootElements() {
		this.addDataBox((ScreenDataBox) makeAndGetEndGamePopUp("Debug view", "back to menu", "100X100Mark"), 100, 100);
	}
	
	//Makes an end game popup that will direct player back to main menu
	public ScreenPopUp makeAndGetEndGamePopUp(String popUpMessage, String buttonMessage, String popUpImage) {
		ScreenPopUp endPopUp = this.mainFactory.getElemFactory().getScreenPopUp("50red");
		endPopUp.setEventImage(popUpImage);
		endPopUp.fillTextOneLine(popUpMessage);
		endPopUp.addButton(this.makeEndGameButton(buttonMessage));
		endPopUp.autoSize();
		return endPopUp;
	}
	
	//makes an end game button that will be placed on an end game pop up and will lead player back to main menu
	private ScreenButton makeEndGameButton(String message) {
		ScreenButton endButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.POPUPSELECT, message);
		endButton.addButtonEventListener(this.mainFactory.getElemFactory().getClearTravelHistoryListener());
		endButton.addButtonEventListener(this.mainFactory.getElemFactory().getViewChangeListener(ViewName.MainMenu));
		return endButton;
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

}

package time;

//enum that represents parts of the day
public enum TimeOfDay {
	NIGHT, DAWN, NOON, DUSK

}

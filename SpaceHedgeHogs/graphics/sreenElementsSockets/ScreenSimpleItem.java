package sreenElementsSockets;

import java.util.ArrayList;
import java.util.List;

import combatFieldElements.MovementSquare;
import fieldPainters.IFieldPainter;
import frameworks.IStackable;
import frameworks.SimpleCharacterFrame;
import frameworks.SimpleItemFrame;
import frameworks.TextFrame;
import generalGraphics.ButtonLookType;
import itemBase.IGameItem;
import itemBase.ItemTagName;
import itemChange.ScreenInventoryItemUpdateListener;
import other.StaticFunctions;
import screenElements.ScreenButton;
import screenElements.ScreenElement;
import screenElements.ScreenText;

//ScreenElement that represents items in sockets
public class ScreenSimpleItem extends ScreenElement {
	private IFieldPainter fieldPainter;
	private ScreenInventoryItemUpdateListener listener;
	private IGameItem master;
	private ScreenButton deleteButton;
	private TextFrame labelFrame;

	//constructor
	public ScreenSimpleItem(IGameItem master) {
		super(0, 0);
		this.master = master;
		this.master.setSocketAvatar(this);
	}
	
	public void bootUp() {
		super.bootUp();
		this.makeLabel();
		this.makeSize();
		tagWorker();
		this.listener = this.mainFactory.getElemFactory().getScreenInventoryItemUpdateListener(this);
		this.master.addItemChangeEventListener(this.listener);
		this.makeDeleteButton();
		this.makeExtraCommands();
	}
	
	//Makes label for this element
	private void makeLabel() {
		this.labelFrame = this.mainFactory.getElemFactory().getTextFrame(this);
		this.labelFrame.setLocation(0, 0);
		this.updateLabelValue();
	}
	
	//makes a delete button for this item, it can delete one of the element each time when clicked
	private void makeDeleteButton() {
		this.deleteButton = this.mainFactory.getElemFactory().getScreenButton(140, 0, ButtonLookType.DELETEITEM, "del");
		this.deleteButton.addButtonEventListener(this.mainFactory.getElemFactory().getDeleteNrOfItemListener(1, this.master));
		this.deleteButton.setWrapper(this);
	}
	
	//checks if the item has a delete button
	public boolean hasDeleteButton() {
		return this.deleteButton != null;
	}
	
	//makes all the extra commands of this element
	private void makeExtraCommands() {
		this.addExtraCommand(makeInfoButton());
	}
	
	//makes button that opens the info page of this element
	private ScreenButton makeInfoButton() {
		ScreenButton infoButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.POPUPSELECT, "item Info");
		infoButton.addButtonEventListener(this.mainFactory.getElemFactory().getOpenElementOnTopLayerListener(this.master.getInfoPage()));
		return infoButton;
	}
	
	//Updates label value on this element
	public void updateLabelValue() {
		this.labelFrame.fillTexts(this.master.getFrontText());
	}
	
	/*
	 * Builds a label value from values gotten from master element
	 */
	private String getItemLabelValue() {
		StringBuilder newStringBuilder = new StringBuilder();
		newStringBuilder.append(master.getName().toString());
		newStringBuilder.append(": ");
		newStringBuilder.append(master.getSupply());
		return newStringBuilder.toString();
	}
	
	/*
	 * Sets proper sizes for this element
	 */
	void makeSize() {
		this.setSize(this.lenAss.getCharAvatarLength(), this.lenAss.getCharAvatarLength());	
	}
	
	//Method that paints squares that an item can be placed on by their tags
	private void tagWorker() {
		for(ItemTagName oneTag : this.master.getTags()) {
			if(oneTag.equals(ItemTagName.FieldPlacement)) {
				this.setPainter(this.mainFactory.getElemFactory().getFromAlliedCharacterPainter(2));
			}
		}
		if(this.fieldPainter == null) {
			this.setPainter(this.mainFactory.getElemFactory().getOnAlliedCharactersPainter());
		}
	}
	
	/*
	 * Removes item change listener from this element
	 */
	public void removeListener() {
		this.master.removeItemChangeEventListener(this.listener);
	}
	
	//Sets a painter method for this, a painter paints squares that this item can be used on
	public void setPainter(IFieldPainter painter) {
		this.fieldPainter = painter;
	}
	
	//Ggetter for squares this item can be used on when active
	public ArrayList<MovementSquare> getActivationSquares() {
		if(this.fieldPainter == null) {
			return null;
		}
		else {
			return this.fieldPainter.getActiveSquares();
		}
	}
	
	//Method that will remove this element from its current socket
	public boolean removeThisElementFromCurrentLocation() {
		if(!this.checkForItemFrameAsWrapper()) {
			return false;
		}
		ScrollItemSocket socket = this.getWrapperAsItemFrame().getWrapperAsScrollSocket();
		if(socket == null) {
			return false;
		}
		socket.removeElement(this);
		this.wrapper = null;
		return true;
	}
	
	//Checks if this element is currently stored in a char framework
	private boolean checkForItemFrameAsWrapper() {
		if(this.wrapper == null) {
			return false;
		}
		if(!StaticFunctions.isElementOfClass(this.wrapper, SimpleItemFrame.class)) {
			return false;
		}
		return true;
	}
	
	/*
	 * Getter for this elements wrapper as item frame
	 */
	public SimpleItemFrame getWrapperAsItemFrame() {
		if(this.wrapper.getClass().equals(SimpleCharacterFrame.class)) {
			return (SimpleItemFrame) this.wrapper;
		}
		return null;
	}

	@Override
	public void fillTopElementList(List<IStackable> list) {
		list.add(this.deleteButton);
		list.add(this.labelFrame);
	}

	@Override
	public void fillTopTextList(List<ScreenText> list) {
		
	}
	
	//Getter for master element
	public IGameItem getMaster() {
		return this.master;
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOffScreenScript() {
		// TODO Auto-generated method stub
		
	}

}

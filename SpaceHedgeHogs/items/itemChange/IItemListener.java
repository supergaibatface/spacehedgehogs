package itemChange;

import java.util.EventListener;

//Interface for item listeners
public interface IItemListener extends EventListener {
	
	//method for item change events events
	public void itemChangeEventOccured(ItemChangeEvent evt);
	

}

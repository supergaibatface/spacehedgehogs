package screenManagement;

import java.util.List;

import frameworks.IStackable;

public interface IScreenLayer {
	
	public List<IStackable> retrieveElementsIntoInputList(List<IStackable> fillList);
	
	public boolean hasElement(IStackable checkedElement);
	
	public boolean addElementToLayer(IStackable newElement);
	
	public boolean addElementToTop(IStackable newElement);
	
	public boolean removeElement(IStackable oldElement);
	
	public void removeAllElements();
	
	public boolean isEmpty();

}

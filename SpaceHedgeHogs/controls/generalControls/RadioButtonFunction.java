package generalControls;

import java.awt.Image;

import javax.swing.event.EventListenerList;

import factories.IAutoMake;
import factories.MainFactory;
import frameworks.ButtonFrame;
import screenElements.ScreenButton;

//Functionality that can be added to a button frame that makes those buttons graphically into selectable radiobuttons
public class RadioButtonFunction implements IAutoMake{
	private MainFactory mainFactory;
	private String activeImgName;
	private String inactiveImgName;
	private int defaultLength;
	private int defaultHeight;
	private Image activeImg;
	private Image inactiveImg;
	private ButtonFrame buttonFrame;
	private ScreenButton selectedButton;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	buttonFrame: the button frame that this functionality is added to
	 * 	activeImg: string name of the image that will be used for active buttons
	 * 	inactiveImg: string name of the image that will be used for inactive buttons
	 */
	public RadioButtonFunction(ButtonFrame buttonFrame, String activeImg, String inactiveImg) {
		this.buttonFrame = buttonFrame;
		this.activeImgName = activeImg;
		this.inactiveImgName = inactiveImg;
	}
	
	/*
	 * Method for gettign default sizes for buttons based on a default button in the buttonframe
	 */
	public void getDefaultSizes(ScreenButton defaultButton) {
		this.defaultHeight = defaultButton.getHeight();
		this.defaultLength = defaultButton.getLength();
		loadImages();
	}
	
	/*
	 * Gets and scales active and inactive images for this functionality
	 * Meant to be used once it has been assigned default button sizes
	 */
	private void loadImages() {
		this.activeImg = this.mainFactory.getImageLoader().loadImage(activeImgName);
		this.inactiveImg = this.mainFactory.getImageLoader().loadImage(inactiveImgName);
		scaleImages();
	}
	
	/*
	 * Scales loaded images to default sizes
	 */
	private void scaleImages() {
		this.activeImg = this.mainFactory.getImageLoader().scaleImage(activeImg, defaultLength, defaultHeight);
		this.inactiveImg = this.mainFactory.getImageLoader().scaleImage(inactiveImg, defaultLength, defaultHeight);
	}
	
	/*
	 * Checks if actual images have been loaded into the functionality
	 */
	public boolean hasImg() {
		return this.activeImg != null;
	}
	
	/*
	 * Checks if the functionality has default sizes assigned
	 */
	public boolean hasDefaultSizes() {
		return this.defaultHeight != 0;
	}
	
	/*
	 * Used when changing activity on buttons
	 */
	public void switchActivity(ScreenButton button) {
		if(button.equals(selectedButton)) {
			return;
		}
		setButtonImgInactive(this.selectedButton);
		setButtonImgActive(button);
		this.selectedButton = button;
	}
	
	/*
	 * Sets active image to a button
	 * Inputs:
	 * 	button: the button that will be turned active
	 */
	private void setButtonImgActive(ScreenButton button) {
		setButtonImage(button, activeImg);
	}
	
	/*
	 * Sets inactive image to a button
	 * Inputs:
	 * 	button: the button that will be made inactive
	 */
	private void setButtonImgInactive(ScreenButton button) {
		setButtonImage(button, inactiveImg);
	}
	
	/*
	 * Method for changing the image of a button
	 * Inputs:
	 * 	button: the button that will have its image changed
	 * 	img: the image that will be given to the selected button
	 */
	private void setButtonImage(ScreenButton button, Image img) {
		button.switchImage(img);
	}

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
	}

	@Override
	public void bootUp() {
		this.defaultHeight = 0;
		this.defaultLength = 0;
		this.buttonFrame.addRadioButtonFunction(this);
	}
	
	/*
	 * For expanding the functionality to new buttons
	 * Inputs:
	 * 	button: the new button that needs to be added to radio selection
	 */
	public void newButtonAdded(ScreenButton button) {
		if(this.buttonFrame.getButtons().indexOf(button) == 0) {
			setActive(button);
		}
		else {
			setButtonImgInactive(button);
		}
		button.addButtonEventListener(this.mainFactory.getElemFactory().getRadioButtonListener(this));
	}
	
	/*
	 * Method for setting activity, will only change the button given
	 * Inputs:
	 * 	button: the button that will be made active
	 */
	private void setActive(ScreenButton button) {
		setButtonImgActive(button);
		this.selectedButton = button;
	}
	
	//Getter for current activeButton
	public ScreenButton getActiveButton() {
		return this.selectedButton;
	}

}

package buttonListeners;

import questStepBase.IQuestStep;
import screenInfoPage.QuestInfoPage;

public class SelectInfoQuestStepListener extends BaseButtonListener {
	private QuestInfoPage masterPage;
	private IQuestStep designatedStep;
	
	public SelectInfoQuestStepListener(QuestInfoPage page, IQuestStep step) {
		this.masterPage = page;
		this.designatedStep = step;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.masterPage.selectQuestStep(designatedStep);
		
	}

	@Override
	public String toInfo() {
		return "Selects "+this.designatedStep.getQuestStepName()+" as visible step for "+masterPage.getMasterQuest().getName()+ " quest info page.";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

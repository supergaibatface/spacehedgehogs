package inventoryException;

import itemBase.IGameItem;

public class ItemNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ItemNotFoundException(IGameItem notFoundItem) {
		super("Cant find item " + notFoundItem.getName() + " from inventory");
	}

}

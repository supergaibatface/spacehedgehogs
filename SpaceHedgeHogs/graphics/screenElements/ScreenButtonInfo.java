package screenElements;

import java.util.ArrayList;

import buttonListeners.CloseIStackableElementListener;
import buttonListeners.ISButtonListener;

/*
 * Generates, manages and displays info about a specific buttons functions
 */
public class ScreenButtonInfo extends ScreenDataBox{
	private ScreenButton master;
	private boolean visible;
	private ArrayList<String> info;

	//constructor
	public ScreenButtonInfo(ScreenButton button) {
		super(button.getX() + button.getLength(), button.getY());
		this.master = button;
	}
	
	//boot up method
	public void bootUp() {
		super.bootUp();
		this.visible = false;
		this.info = new ArrayList<String>();
		this.wrapper = this.master.getWrapper();
	}
	
	/*
	 * Method for updating the location of this element
	 */
	public void updateLocation() {
		this.setCoordinates(this.master.getX() + this.master.getLength(), this.master.getY());
	}
	
	/*
	 * Takes a list of listeners from a button and builds a text about them
	 * Inputs:
	 * 	buttonListeners: array of the listeners
	 */
	public void feedListeners(ArrayList<ISButtonListener> buttonListeners) {
		for(ISButtonListener oneListener : buttonListeners) {
			if(oneListener.getClass() != CloseIStackableElementListener.class) {
				this.info.add(oneListener.toInfo());
			}
		}
		fillWithText();
	}
	
	/*
	 * Sets the info strings up for display
	 */
	private void fillWithText() {
		this.fillDataBoxWithTexts(info);
		this.updateSizeForTime();
	}
	
	/*
	 * Method for displaying buttons info in the console
	 */
	public void printOutInfo() {
		System.out.println("Start of the info");
		for(String oneLine : this.info) {
			System.out.println(oneLine);
		}
	}

}

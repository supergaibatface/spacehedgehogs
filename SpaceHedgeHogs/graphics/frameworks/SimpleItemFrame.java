package frameworks;

import java.util.ArrayList;
import java.util.List;

import screenElements.ScreenElement;
import screenElements.ScreenText;
import sreenElementsSockets.ScreenSimpleItem;
import sreenElementsSockets.ScrollCharSocket;
import sreenElementsSockets.ScrollItemSocket;

/*
 * Framework for storing ScreenSimpleItem type of elements
 */
public class SimpleItemFrame extends BaseFrame {
	private ArrayList<ScreenSimpleItem> elements;
	private int extraRoomLen;

	//constructor
	public SimpleItemFrame(IStackable wrapper) {
		super(wrapper);
		// TODO Auto-generated constructor stub
	}
	
	public void bootUp() {
		super.bootUp();
		this.elements = new ArrayList<ScreenSimpleItem>();
		this.extraRoomLen = 10;
	}
	
	/*
	 * Method for adding new elements to the frame
	 */
	public boolean addNewElement(ScreenSimpleItem newItem) {
		if(!canAddNewElement()) {
			return false;
		}
		newItem.setCoordinates(getNewElementX(), getNewElementY());
		this.elements.add(newItem);
		newItem.setWrapper(this);
		return true;
	}
	
	//Method that shows if its allowed to add new element here
	public boolean canAddNewElement() {
		return true;
	}
	
	//Method that provides a x coordinate for a new element
	public int getNewElementX() {
		return 0;
	}
	
	//Method that provides a y coordinate for a new element
	public int getNewElementY() {
		if(this.elements.isEmpty()) {
			return 0;
		}
		return this.getFullHeight() + this.extraRoomLen;
	}
	
	//Method for removing elements from the frame
	public boolean removeElement(ScreenSimpleItem oldItem) {
		if(!elements.contains(oldItem)) {
			return false;
		}
		int location = this.elements.indexOf(oldItem);
		this.elements.remove(oldItem);
		this.moveLocationsUpByOne(location);
		return true;
	}
	
	//Removes all elements from the frame
	public void removeAllElements() {
		this.elements.clear();
	}
	
	/*
	 * From a point in the array moves all elements graphical locations up by one, 
	 * used for fixing locations when an element is removed from the socket
	 */
	protected void moveLocationsUpByOne(int startFrom) {
		for(int i = startFrom; i < this.elements.size(); i++) {
			ScreenSimpleItem temp = this.elements.get(i);
			temp.setCoordinates(xForElement(), yForElement(i, temp.getHeight()));
		}
	}
	
	//Returns the wrapper as scrollSocket, if wrapper is not scroll socket returns null
	public ScrollItemSocket getWrapperAsScrollSocket() {
		if(this.wrapper.getClass().equals(ScrollItemSocket.class)) {
			return (ScrollItemSocket) this.wrapper;
		}
		return null;
	}
	
	/*
	 * gives x for case of recalculating element coordinates
	 */
	public int xForElement() {
		return 0;
	}
	
	/*
	 * Calculates the y coordinate for the element that is in the socket
	 * Inputs
	 * 	nr: the elements placement number in the socket
	 * 	elementHeight: the height of the element that will be fitted into the socket
	 * Output
	 * 	y coordinate as an integer
	 */
	private int yForElement(int nr, int elementHeight) {
		return (elementHeight + this.extraRoomLen) * nr;
	}

	@Override
	public int getFullHeight() {
		int height = 0;
		for(ScreenSimpleItem oneItem : this.elements) {
			height = height + oneItem.getHeight() + this.extraRoomLen;
		}
		if(height > 0) {
			height = height - this.extraRoomLen;
		}
		return height;
	}

	@Override
	public int getFullLength() {
		if(this.elements.isEmpty()) {
			return 0;
		}
		else {
			return this.elements.get(0).getLength();
		}
	}

	@Override
	public void fillTopElementList(List<IStackable> list) {
		if(!this.elements.isEmpty()) {
			list.addAll(elements);
		}
		
	}

	@Override
	public void fillTopTextList(List<ScreenText> list) {
		
	}
	
	//getter for all the elements in this itemFrame
	public ArrayList<ScreenSimpleItem> getAllElements() {
		return this.elements;
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOffScreenScript() {
		// TODO Auto-generated method stub
		
	}

}

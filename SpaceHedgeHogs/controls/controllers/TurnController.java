package controllers;

import java.util.ArrayList;

import factories.IAutoMake;
import factories.MainFactory;
import screenElements.ScreenDataBox;
import time.BattleTime;
import time.WorldTime;

/*
 * Controller for the games turn counter
 */
public class TurnController implements IAutoMake {
	private MainFactory mainFactory;
	private boolean battleTimeSelected;
	private WorldTime worldTime;
	private BattleTime battleTime;
	private ScreenDataBox dataBox;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.battleTimeSelected = false;
		this.worldTime = this.mainFactory.getWorldTime();
		this.battleTime = this.mainFactory.getBattleTime();
		this.dataBox = mainFactory.getElemFactory().getScreenDataBox(0, 0, "LightBlue");
		this.updateDataBox();
		this.dataBox.updateSizeForTime();
	}
	
	/*
	 * Advances the turn by 1
	 */
	public void nextTurn() {
		if(battleTimeSelected) {
			this.battleTime.nextTurn();
			this.mainFactory.getMouseCommands().turnChangeResets();
			this.mainFactory.getActivityController().makeInactive();
		}
		else {
			this.worldTime.nextTurn();
		}
		updateDataBox();
	}
	
	/*
	 * Method for updating the databox that displays the turn
	 */
	public void updateDataBox() {
		ArrayList<String> lines = new ArrayList<String>();
		if(battleTimeSelected) {
			lines = battleTime.getLines();
		}
		else {
			lines = worldTime.getLines();
		}
		this.dataBox.fillDataBoxWithTexts(lines);
	}
	
	/*
	 * Getter for the databox that displays the turn
	 */
	public ScreenDataBox getDataBox() {
		return this.dataBox;
	}
	
	/*
	 * Changes the turn counter to use battle turn system
	 */
	public void changeToBattleTime() {
		this.battleTimeSelected = true;
		this.battleTime.resetTurnCounter();
		updateDataBox();
		this.dataBox.updateSizeForTime();
	}
	
	/*
	 * Changes turn counter to use travel turn system
	 */
	public void changeToTravelTime() {
		this.battleTimeSelected = false;
		updateDataBox();
		this.dataBox.updateSizeForTime();
	}
	
	

}

package jobChoiceController;

import java.util.ArrayList;

import jobs.JobName;
import jobs.JobTopic;
import party.PartyStance;

//JobTopicController that adds jobs depending on current party stance
public class BasicPartyStanceController extends BaseJobTopicController {

	//constructor
	public BasicPartyStanceController(JobTopic newTopic) {
		super(newTopic);
	}

	@Override
	protected ArrayList<Object> makeModifierValuePackage() {
		ArrayList<Object> modifierPackage = new ArrayList<Object>();
		modifierPackage.add(findCurrentPartyStance());
		return modifierPackage;
	}
	
	/*
	 * Finds current party stance
	 */
	private PartyStance findCurrentPartyStance() {
		return this.mainFactory.getParty().getPartyStance();
	}

	@Override
	protected void addSockets() {
		PartyStance partyStance = this.findCurrentPartyStance();
		if(partyStance == PartyStance.Camp) {
			prepareANewSocket(2, "LightGreen", "Resting", this.mainFactory.getElemFactory().getRestJob());
		}
		if(partyStance == PartyStance.Travel) {
			prepareANewSocket(2, "LightGreen", "Baggage management", this.mainFactory.getElemFactory().getBaggageJob());
		}
		
	}

}

package dataValues;

import itemBase.TradeItemName;

//itemValue class for food items
public class FoodValue extends ItemValue {
	protected int expiration;

	/*
	 * constructor
	 * Inputs:
	 * 	name: item name
	 * 	amount: item amount
	 * 	expiration: nr of turns till this amount of food expires
	 */
	public FoodValue(TradeItemName name, int amount, int expiration) {
		super(name, amount);
		this.expiration = expiration;
	}
	
	//getter for the expiration value
	public int getExpiration() {
		return this.expiration;
	}


}

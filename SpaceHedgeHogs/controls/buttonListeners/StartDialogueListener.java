package buttonListeners;

import dialogueCharacters.IDialogueCharacter;

public class StartDialogueListener extends BaseButtonListener {
	private IDialogueCharacter target;
	
	public StartDialogueListener(IDialogueCharacter target) {
		this.target = target;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		if(target.canStartDialogue()) {
			target.startDialogue();
		}
		
	}

	@Override
	public String toInfo() {
		return "Starts dialogue with "+target.getName();
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

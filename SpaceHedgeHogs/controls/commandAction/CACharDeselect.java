package commandAction;

import jobChoiceController.JobChoiceController;
import screenElements.ScreenElement;

//Command action for deslecting character or item
public class CACharDeselect extends CABaseClass<ScreenElement> {

	@Override
	public boolean inputIsCorrect() {
		return true;
	}

	@Override
	public void actionImplement() {
		this.selection.makeInactive();
		if(this.mainFactory.getGameStateController().inBattle()) {
			this.mainFactory.getGameStateController().getBattleController().clearVisuals();
		}
	}

	@Override
	public Class<ScreenElement> setClickTargetClass() {
		return null;
	}

	@Override
	protected int giveMouseButtonNr() {
		return this.giveLeftMouseButtonValue();
	}

}

package generalGraphics;

/*
 * Enum that decides the graphical look of a button
 */
public enum ButtonLookType {
	SCROLLUP("arrowUp", 2, 2), 
	SCROLLDOWN("arrowDown", 2, 2),
	SCROLLLEFT("arrowLeft", 2, 2),
	SCROLLRIGHT("arrowRight", 2, 2),
	POPUPSELECT("LightBlue", 10, 3),
	QUESTSSTEPSELECT("LightBlue", 12, 2),
	NEXT("LightGreen", 6, 3), 
	CUSTOM("arrowLeft", 6, 4), 
	TRAVELSELECT("50yellow", 10, 2),
	NOTICE("LightGreen", 2, 2),
	GIANTTEST("LightGreen", 20, 20),
	DELETEITEM("grayOut", 2, 2),
	CLOSE("grayOut", 2, 2);
	
	public final String image;
	public final int lengthMod;
	public final int heightMod;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	imageName: name of the button graphical image
	 * 	length: length discreet value, this shows button sizes compared to other buttons
	 * 	height: height discreet value, this shows button sizes compared to other buttons
	 */
	private ButtonLookType(String imageName, int length, int height) {
		this.lengthMod = length;
		this.heightMod = height;
		this.image = imageName;
	}

}

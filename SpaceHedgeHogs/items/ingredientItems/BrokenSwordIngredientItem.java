package ingredientItems;

import ingredientItemBase.IngredientItemBase;
import itemBase.IGameItem;
import itemBase.TradeItemName;

public class BrokenSwordIngredientItem extends IngredientItemBase {

	public BrokenSwordIngredientItem() {
		super(TradeItemName.BrokenSword);
	}

	@Override
	protected IGameItem giveBaseClass() {
		return this.mainFactory.setUpNewNonPerservativeValue(new BrokenSwordIngredientItem());
	}

	@Override
	protected String bootGiveItemImageName() {
		return this.bootGivePlaceholderImage();
	}

	@Override
	protected int bootGiveOneUnitWeight() {
		return 5;
	}

}

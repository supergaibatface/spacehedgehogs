package craftingCenter;

import java.util.ArrayList;
import java.util.List;

import craftingBluePrints.CraftingProject;
import craftingBluePrints.IBlueprint;
import craftingManagerListeners.ICraftingManagerListener;
import frameworks.ButtonFrame;
import frameworks.TextFrame;
import generalGraphics.ButtonLookType;
import screenElements.ScreenButton;
import screenInfoPage.BaseInfoPage;

public class CraftingInfoPage extends BaseInfoPage {
	private CraftingManager masterElement;
	private ICraftingManagerListener masterElementListener;
	private ButtonFrame craftingBlueprints;
	private TextFrame selectedBlueprint;
	private ScreenButton addSelectedBlueprintToCraftingList;
	private TextFrame workListHeader;
	private TextFrame workList;
	private IBlueprint activeBlueprint;

	public CraftingInfoPage(CraftingManager masterManager) {
		super(1000, 900);
		this.masterElement = masterManager;
	}
	
	public void bootUp() {
		super.bootUp();
		this.setCoordinates(200, 50);
		this.masterElementListener = this.makeListenerToMasterElement();
		this.craftingBlueprints = this.makeButtonFrameForCratingBluePrints();
		this.selectedBlueprint = this.makeNewTextFrameForActiveBluePrint();
		this.addSelectedBlueprintToCraftingList = this.makeScreenButtonForAddingNewProjects();
		this.workListHeader = makeHeaderForWorkList();
		this.workList = makeWorkList();
	}
	
	private ICraftingManagerListener makeListenerToMasterElement() {
		ICraftingManagerListener newListener = this.mainFactory.getCraftingFactory().getCraftingInfoPageListUpdater(this);
		this.masterElement.addNewListener(newListener);
		return newListener;
	}

	@Override
	protected void setLocationForCloseButton(ScreenButton closeButton) {
		closeButton.setCoordinates(950, 20);
	}

	@Override
	protected void setLocationForNameField(TextFrame nameField) {
		nameField.setLocation(50, 80);
	}

	@Override
	protected void writeNameFieldLines(List<String> lines) {
		lines.add("Crafting manager");	
	}
	
	private ButtonFrame makeButtonFrameForCratingBluePrints() {
		ButtonFrame newFrame = this.mainFactory.getElemFactory().getButtonFrame(this);
		this.addNewElement(newFrame);
		newFrame.setLocation(50, 120);
		this.refreshBluePrintList(newFrame);
		return newFrame;
	}
	
	private void refreshBluePrintList(ButtonFrame targetFrame) {
		targetFrame.removeAllButtons();
		for(IBlueprint onePrint : this.masterElement.getAllBluePrints()) {
			targetFrame.addElement(this.makeNewButtonForBluePrint(onePrint));
		}
	}
	
	private ScreenButton makeNewButtonForBluePrint(IBlueprint targetPrint) {
		ScreenButton newButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.POPUPSELECT, targetPrint.getName());
		newButton.addButtonEventListener(this.mainFactory.getElemFactory().getActivateBluePrintListener(this, targetPrint));
		return newButton;
	}
	
	public boolean activateBlueprint(IBlueprint oneBlueprint) {
		System.out.println("Tried to active blueprint "+oneBlueprint.getName());
		if(this.activeBlueprint == oneBlueprint) {
			return false;
		}
		this.activeBlueprint = oneBlueprint;
		refreshSelectedBluePrintDependencies();
		return true;
	}
	
	private TextFrame makeNewTextFrameForActiveBluePrint() {
		TextFrame newTextFrame = this.mainFactory.getElemFactory().getTextFrame(this);
		this.addNewElement(newTextFrame);
		newTextFrame.setLocation(350, 120);
		return newTextFrame;
	}
	
	private void refreshSelectedBluePrintDependencies() {
		writeActiveBlueprintToSelectedBlueprintTextFrame();
	}
	
	private void writeActiveBlueprintToSelectedBlueprintTextFrame() {
		if(this.activeBlueprint == null) {
			this.selectedBlueprint.fillTexts(new ArrayList<String>());
			return;
		}
		this.selectedBlueprint.fillTexts(this.makeSelectedBlueprintTextLines(this.activeBlueprint));
	}
	
	private List<String> makeSelectedBlueprintTextLines(IBlueprint targetPrint) {
		List<String> newLines = new ArrayList<String>();
		newLines.add("Selected blueprint:");
		newLines.add(targetPrint.getName());
		newLines.add("Have required components:" +targetPrint.canMakeProject());
		return newLines;
	}
	
	public boolean addSelectedBlueprintToProjects() {
		if(this.activeBlueprint == null) {
			return false;
		}
		return this.masterElement.addNewCraftingProject(activeBlueprint);
	}
	
	private ScreenButton makeScreenButtonForAddingNewProjects() {
		ScreenButton newButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.POPUPSELECT, "Add selected project");
		newButton.addButtonEventListener(this.mainFactory.getElemFactory().getAddSelectedBlueprintAsCraftingProjectListener(this));
		newButton.setWrapper(this);
		this.addNewElement(newButton);
		newButton.setCoordinates(350, 200);
		return newButton;
	}
	
	private TextFrame makeHeaderForWorkList() {
		TextFrame newTextFrame = this.mainFactory.getElemFactory().getTextFrame(this);
		this.addNewElement(newTextFrame);
		newTextFrame.setLocation(650, 120);
		newTextFrame.fillTexts(getTextContextForWorkListHeader());
		return newTextFrame;
	}
	
	private List<String> getTextContextForWorkListHeader() {
		List<String> newLines = new ArrayList<String>();
		newLines.add("Project list:");
		return newLines;
	}
	
	private TextFrame makeWorkList() {
		TextFrame newTextFrame = this.mainFactory.getElemFactory().getTextFrame(this);
		this.addNewElement(newTextFrame);
		newTextFrame.setLocation(650, 200);
		newTextFrame.fillTexts(getTextContextForWorkList());
		return newTextFrame;
	}
	
	private List<String> getTextContextForWorkList() {
		List<String> newLines = new ArrayList<String>();
		int nr = 0;
		for(CraftingProject oneProject : this.masterElement.getCopyOfProjects()) {
			nr++;
			newLines.add(giveOneTextLineAboutCraftingProject(oneProject, nr));
		}
		return newLines;
	}
	
	private String giveOneTextLineAboutCraftingProject(CraftingProject oneProject, int projectNr) {
		StringBuilder newBuilder = new StringBuilder();
		newBuilder.append(projectNr);
		newBuilder.append(". ");
		newBuilder.append(oneProject.getMasterBlueprint().getName());
		newBuilder.append(", ");
		newBuilder.append(oneProject.getInvestedWork());
		newBuilder.append("/");
		newBuilder.append(oneProject.getMasterBlueprint().getNeccesaryWorkValue());
		return newBuilder.toString();
		//return projectNr+". "+oneProject.getMasterBlueprint().getName()+", "+oneProject.getInvestedWork()+"/"+oneProject.getMasterBlueprint().getNeccesaryWorkValue();
	}
	
	public void callUpdateOnWorkList() {
		this.workList.fillTexts(this.getTextContextForWorkList());
	}
	
	protected void goingOnScreenScript() {
		super.goingOnScreenScript();
		refreshSelectedBluePrintDependencies();
	}

}

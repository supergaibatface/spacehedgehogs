package land;

//Possible terrain names
public enum TerrainName {
	Forest(0.6), 
	Field(0.8), 
	Swamp(0.4), 
	Hills(0.4);
	
	private double travelSpeed;
	
	//constructor for the enum type
	TerrainName(double travelSpeed) {
		this.travelSpeed = travelSpeed;
	}
	
	/*
	 * getter for the assigned travel speed for the terrain
	 * Travel speeds are depicted as multipliers
	 */
	public double getTravelSpeedMultiplier() {
		return this.travelSpeed;
	}

}

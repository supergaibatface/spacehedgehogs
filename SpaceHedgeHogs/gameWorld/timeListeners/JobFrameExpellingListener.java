package timeListeners;

import jobPanels.IJobPanel;

//Listener that will inform job frame on turn change in order to expell chars who no longer meet requirements to work their assigned stations
public class JobFrameExpellingListener extends BaseTimeListener {
	private IJobPanel jobFrame;
	
	//constructor, input is the job frame it informs
	public JobFrameExpellingListener(IJobPanel jobFrame) {
		this.jobFrame = jobFrame;
	}

	@Override
	public void timeAdvanced(boolean inBattle) {
		if(!inBattle) {
			this.correctCharsInSockets();
		}
		
	}
	
	//command that calls for expelling check on character
	private void correctCharsInSockets() {
		this.jobFrame.expellUnfitCharacters();
	}

}

package mainPartyCharacters;

import dialogueCharacters.IDialogueCharacter;
import gameCharacters.BaseGameCharacter;

public class CharacterBob extends BaseGameCharacter {

	public CharacterBob() {
		super("Bob", "charspot");
	}

	@Override
	protected IDialogueCharacter bootDialogueElement() {
		return this.mainFactory.getCharacterFactory().getDialogueBob();
		
	}

}

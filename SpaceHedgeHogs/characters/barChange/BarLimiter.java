package barChange;

import factories.IAutoMake;
import factories.MainFactory;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import propertyBars.PropertyBar;
import propertyBars.PropertyBarName;

//Limiter that adds a bar value limit to a job
public class BarLimiter implements IAutoMake {
	private MainFactory mainFactory;
	private PropertyBarName propertyName;
	private int amount;
	
	/*
	 * constructor
	 * Inputs:
	 * 	propertyBarName: name of the property bar that will be limiting
	 * 	amount: the amount of the property bar value that will be limited by
	 */
	public BarLimiter(PropertyBarName propertyName, int amount) {
		this.propertyName = propertyName;
		this.amount = amount;
	}

	//setter for main factory
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	//bootup method
	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	 //Checks if a char has enough of the bar value to pass the limit
	public boolean checkChar(IGameCharacter oneChar) {
		PropertyBar checkBar = oneChar.getPropertyBar(propertyName);
		if(checkBar == null) {
			return false;
		}
		else {
			int remainingValue = checkBar.getCurrentValue() - checkBar.getMinValue();
			return remainingValue >= this.amount;
		}
	}
	
	//Method for triggering the limiters cost on a char
	public void doCost(IGameCharacter oneChar) {
		oneChar.getPropertyBar(propertyName).decreaseByValue(amount);
	}
	
	//getter for the limits bars amount
	public int getAmount() {
		return this.amount;
	}
	
	//returns the name of the barname that this limiter interacts with
	public PropertyBarName getLimitedBarName() {
		return this.propertyName;
	}

}

package buttonListeners;

import java.util.ArrayList;
import java.util.List;

import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import other.StaticFunctions;
import propertyBars.PropertyBarName;

/*
 * Button listener that can trigger the refilling of a property bar on all characters
 */
public class CombatBarValueRefresh extends BaseButtonListener {
	private PropertyBarName propertyName;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	propertyName: name of the property bar that will be refilled
	 */
	public CombatBarValueRefresh(PropertyBarName propertyName) {
		this.propertyName = propertyName;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		for(IGameCharacter oneChar : this.getCurrentCharacters()) {
			oneChar.getPropertyBar(propertyName).fillUp();
		}
		
	}

	@Override
	public String toInfo() {
		return "Fills up the " + this.propertyName + " property value on characters";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	/*
	 * Method for fetching all the current characters in game
	 */
	private List<IGameCharacter> getCurrentCharacters() {
		List<IGameCharacter> fullList = this.mainFactory.getCharacterManager().getCharacters();
		fullList.addAll(this.mainFactory.getCharacterManager().getTempChars());
		fullList.addAll(this.mainFactory.getCharacterManager().getTempCharSetChars());
		return fullList;
	}

}

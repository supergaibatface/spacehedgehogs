package gameCharacters;

import java.util.ArrayList;

import dialogueCharacters.IDialogueCharacter;
import propertyBars.PropertyBar;
import propertyBars.PropertyBarName;
import screenInfoPage.ScreenCharInfoPage;
import skills.Skill;
import skills.SkillName;
import sreenElementsSockets.ScreenSimpleChar;

public interface IGameCharacter {
	
	//getter for the characters name
	public String getName();
	
	//getter for the character image name
	public String getImgName();
	
	//getter for this chatacters info page
	public ScreenCharInfoPage getInfoPage();
	
	/*
	 * method for adding a new skill to the character
	 * the skills should be gotten from skill manager
	 */
	public void addSkill(Skill newSkill);
	
	/*
	 * Method for addiong a property bar to the character
	 * Inputs:
	 * 	newBar: the property bar that will be added to the char
	 */
	public void addPropertyBar(PropertyBar newBar);
	
	//getter for a list of all the skills on this character
	public ArrayList<Skill> getAllSkill();
	
	//getter for the property bars assigned to this char
	public ArrayList<PropertyBar> getAllPropertyBars();
	
	//getter for a specific porperty bar, searches by name
	public PropertyBar getPropertyBar(PropertyBarName name);
	
	//method for adding experience to a skill
	public void improveSkill(SkillName skillname, int experience);
	
	//Method for finding a specific skill on a character
	public Skill findSkill(SkillName name);
	
	//Checks if this gameCharacter already has a screen version of it made
	public boolean hasSocketAvatar();
	
	//Getter for GameCharacters screen avatar version
	public ScreenSimpleChar getSocketAvatar();
	
	//stter for the character screen avatar version
	public void setSocketAvatar(ScreenSimpleChar avatar);
	
	//For safely deleting this element
	public void safeDelete();
	
	public boolean hasDialogueElement();
	
	public void addDialogueElement(IDialogueCharacter dialogueElement);
	
	public IDialogueCharacter getDialogueCharacter();

}

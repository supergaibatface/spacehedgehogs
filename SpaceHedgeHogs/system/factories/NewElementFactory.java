package factories;

import java.util.ArrayList;
import java.util.List;

import activityListeners.FieldCommandControlVisualListener;
import activityListeners.JobFrameVisualListener;
import announcements.Notice;
import baggageCarryItemBase.BaggageAnimalItemBase;
import barChange.BarGain;
import barChange.BarLimiter;
import battle.SpawnArea;
import battleManagement.BattleController;
import battleManagement.SpawnLimiting;
import buttonLimiters.DialogueElementLimiter;
import buttonLimiters.EnemyCharLimiter;
import buttonLimiters.EventLimiter;
import buttonLimiters.FoodLimiter;
import buttonLimiters.IButtonLimiter;
import buttonLimiters.ItemLimiter;
import buttonLimiters.NotEnoughCarryLimiter;
import buttonLimiters.OverTurnLimiter;
import buttonLimiters.QuestLimiter;
import buttonLimiters.SettlementLocationLimiter;
import buttonLimiters.TerrainLimiter;
import buttonListeners.ActivateAnotherButtonListener;
import buttonListeners.ActivateBlueprintListener;
import buttonListeners.AddEnemiesListener;
import buttonListeners.AddLandAreaListener;
import buttonListeners.AddNewQuestListener;
import buttonListeners.AddSelectedBlueprintAsCraftingProjectListener;
import buttonListeners.AddCharToGameStateListener;
import buttonListeners.AddValueToBarListener;
import buttonListeners.AdvanceCutsceneListener;
import buttonListeners.BarValueRefresher;
import buttonListeners.BattleEndListener;
import buttonListeners.BattleStartListener;
import buttonListeners.ChangeFCCCSelectionListener;
import buttonListeners.ChangeTabListener;
import buttonListeners.ClearAllNoticesListener;
import buttonListeners.ClearTravelHistoryListener;
import buttonListeners.CloseIStackableElementListener;
import buttonListeners.CombatBarValueRefresh;
import buttonListeners.DecreaseWaterSupplyByTurnListener;
import buttonListeners.DeleteNrOfItemListener;
import buttonListeners.DialogueFinishListener;
import buttonListeners.GoSituationChangeListener;
import buttonListeners.ISButtonListener;
import buttonListeners.ImportAllCharsToModuleListener;
import buttonListeners.ItemAddListener;
import buttonListeners.LandAreaChangeListener;
import buttonListeners.LoadInDefaultViewListener;
import buttonListeners.LoadInGameStateListener;
import buttonListeners.OpenElementOnTopLayerListener;
import buttonListeners.OpenInfoSelectionListener;
import buttonListeners.OpenNoticeListener;
import buttonListeners.OpenElementOnPopupLayerListener;
import buttonListeners.PanelScrollListener;
import buttonListeners.PartyStanceChangeListener;
import buttonListeners.RadioButtonListener;
import buttonListeners.RemoveNoticeListener;
import buttonListeners.ResultEventListener;
import buttonListeners.SelectInfoQuestStepListener;
import buttonListeners.SettlementLocationListener;
import buttonListeners.ShowAttackSquaresListener;
import buttonListeners.ShowMovementPathsListener;
import buttonListeners.SkipCutsceneListener;
import buttonListeners.StartDialogueListener;
import buttonListeners.TravelListener;
import buttonListeners.TurnChangeListener;
import buttonListeners.UpdateJobFrameListener;
import buttonListeners.ViewChangeListener;
import central.AICharPatterns;
import central.AICharacterSet;
import combatFieldElements.ControlFaction;
import combatFieldElements.FieldCharacter;
import combatFieldElements.FieldItem;
import combatFieldElements.MovementField;
import combatFieldElements.MovementLine;
import combatFieldElements.MovementSquare;
import craftingBluePrints.IBlueprint;
import craftingCenter.CraftingInfoPage;
import craftingCenter.CraftingManager;
import dataValues.CharValue;
import dialogueCharacters.IDialogueCharacter;
import eventModules.FoodSearchEvents1;
import eventModules.IEventModule;
import eventModules.SettlementWaterSearchEvents;
import eventModules.SupplySearchEvents1;
import eventSuccessFactors.SimpleSkillFactor;
import events.EventChoice;
import events.GameEvent;
import fieldCharCommand.FieldCharCommandControl;
import fieldCharCommand.FieldCharCommandStateName;
import fieldPainters.FromAlliedCharactersPainter;
import fieldPainters.OnAlliedCharactersPainter;
import foodBase.FoodItemBase;
import frameworks.ButtonFrame;
import frameworks.FieldAndValue;
import frameworks.IStackable;
import frameworks.TextFrame;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import gameState.IGameState;
import frameworks.SimpleCharacterFrame;
import frameworks.SimpleItemFrame;
import generalControls.RadioButtonFunction;
import generalGraphics.ButtonLookType;
import generalRNG.LotteryBox;
import graphicalWrapping.CharSpawnInfo;
import graphicalWrapping.DataBoxWrapper;
import inventory.ItemBag;
import inventory.WeightCarryingAbility;
import inventoryChange.ItemSocketUpdateListener;
import inventoryFilters.AllItemsFilter;
import inventoryFilters.TagFilter;
import itemBase.IGameItem;
import itemBase.ItemTagName;
import itemBase.TradeItemName;
import itemChange.FoodInfoExpirationListener;
import itemChange.ItemInfoPageAmountListener;
import itemChange.ScreenInventoryItemUpdateListener;
import jobChoiceController.BasicPartyStanceController;
import jobChoiceController.BasicSurvivalTopicController;
import jobChoiceController.BattleTopicController;
import jobChoiceController.DefaultTopicController;
import jobChoiceController.JobChoiceController;
import jobChoiceController.PartyStanceTopicController;
import jobChoiceController.ProtoShowDefaultController;
import jobChoiceController.ProtoShowPartyStanceController;
import jobChoiceController.QuestTopicController;
import jobChoiceController.TerrainTopicController;
import jobPanels.IJobPanel;
import jobs.Job;
import jobs.JobName;
import jobs.JobTopic;
import jobs.SkillGain;
import land.LandArea;
import land.TerrainName;
import newJobs.BaggageJob;
import newJobs.CraftingJob;
import newJobs.FoodSearchJob;
import newJobs.IJob;
import newJobs.RestJob;
import newJobs.SettlementWaterSearchJob;
import newJobs.WaterSearchJob;
import party.PartyStance;
import playerActionListener.RightMouseInfoCloser;
import propertyBarListeners.FieldCharCommandUpdater;
import propertyBarListeners.ScreenBarValueUpdater;
import propertyBarListeners.TutorialMakeElementsVisibleOnBarValue;
import propertyBarListeners.UpdateInfoPageHp;
import propertyBarListeners.UpdateInfoPageStamina;
import propertyBarListeners.WaterBarListener;
import propertyBars.PropertyBar;
import propertyBars.PropertyBarName;
import questBase.IQuest;
import questStepBase.IQuestStep;
import screenElements.ScreenBackground;
import screenElements.ScreenBar;
import screenElements.ScreenButton;
import screenElements.ScreenButtonInfo;
import screenElements.ScreenDataBox;
import screenElements.ScreenElement;
import screenElements.ScreenPopUp;
import screenElements.ScreenScrollPanel;
import screenElements.ScreenSoloImage;
import screenElements.ScreenText;
import screenElements.ScreenTimedDataBox;
import screenInfoPage.BaseInfoPage;
import screenInfoPage.FoodInfoPage;
import screenInfoPage.ItemBasePage;
import screenInfoPage.ItemInfoPage;
import screenInfoPage.JobInfoPage;
import screenInfoPage.QuestInfoPage;
import screenInfoPage.ScreenCharInfoPage;
import screenManagement.PopupGraphicLayer;
import screenManagement.StandardGraphicLayer;
import settlementLocation.ISettlementLocation;
import situationChange.ISituationChange;
import skillListeners.UpdateInfoPageSkills;
import skills.Skill;
import skills.SkillName;
import socketStoring.CharSocketBuilder;
import socketStoring.FullCharacterListProviding;
import socketStoring.FullItemProviding;
import socketStoring.ISocketContentProviding;
import socketStoring.NewItemSocketBuilder;
import socketStoring.TagItemProviding;
import spawning.TimeSpawnEveryTurnOne;
import spawning.TimeSpawnKeepNrOfCharsOnField;
import sreenElementsSockets.ScreenSimpleChar;
import sreenElementsSockets.ScreenSimpleItem;
import sreenElementsSockets.ScrollCharSocket;
import sreenElementsSockets.ScrollItemSocket;
import tabInterface.TabDirectory;
import tabInterface.TabWindow;
import timeListeners.InventoryTurnChangeListener;
import timeListeners.JobFrameExpellingListener;
import timeListeners.PlayerLimiterRefreshListener;
import travelGoals.GoalMarker;
import view.CutsceneDataCollection;
import view.GameView;
import view.IGameView;
import view.ViewName;
import viewClasses.LoupeFortView;

/*
 * Factory class that deals with new elements that are not singular and need to be made every time when asked for
 * new objects are made through get functions
 */
public class NewElementFactory extends BaseMinorFactory {
	
	NewElementFactory() {	
	}
	
	/*
	 * getter for a new screen button
	 * Inputs:
	 * 	x: x-coordinate of the element
	 * 	y: y-coordinate of the element
	 * 	functionType: enum of ButtonFunctionType that decides what type of function the button performs
	 * 	lookType: enum of ButtonLookType that decides the button graphical look
	 * 	label: String text that will be displayed on the button
	 */
	public ScreenButton getScreenButton(int x, int y, ButtonLookType lookType, String label) {
		ScreenButton newButton = new ScreenButton(x, y, lookType, label);
		setUpNewGraphicalElement(newButton, lookType.image);
		return newButton;
	}
	
	
	/*
	 * getter for a new game view
	 * Inputs:
	 * 	name: string type name for the view, for debugging purposes only
	 * 	type: view type that decides what elements the view will have
	 */
	public GameView getGameView(ViewName name) {
		GameView newView = new GameView(name);
		setUpNewElement(newView);
		return newView;
	}
	
	/*
	 * getter for a new game character
	 * Inputs:
	 * 	name: string type name of the character
	 * 	imgName: string name for the portrait of the char
	 */
	public GameCharacter getGameCharacter(String name, String imgName) {
		GameCharacter newChar = new GameCharacter(name, imgName);
		setUpNewElement(newChar);
		return newChar;
	}
	
	/*
	 * getter for a new skill
	 * Inputs:
	 * 	name: skills name
	 */
	public Skill getSkill(SkillName name) {
		Skill newSkill = new Skill(name);
		setUpNewElement(newSkill);
		return newSkill;
	}
	
	/*
	 * getter for a skill from a skill template
	 * Inputs:
	 * 	oldSkill: skill template that will be used for making a same type skill
	 */
	public Skill copySkill(Skill oldSkill) {
		Skill newSkill = getSkill(oldSkill.getName());
		return newSkill;
	}
	
	/*
	 * getter for a screen data box
	 * Inputs:
	 * 	x: x-coordinate of the element
	 * 	y: y-coordinate of the element
	 * 	image: name of the image file that will be used for the data box
	 */
	public ScreenDataBox getScreenDataBox(int x, int y, String image) {
		ScreenDataBox newDataBox = new ScreenDataBox(x, y);
		setUpNewGraphicalElement(newDataBox, image);
		return newDataBox;
	}
	
	/*
	 * getter for a new pop up window
	 * Inputs:
	 * 	image: string name of the image that will be used for the popup
	 */
	public ScreenPopUp getScreenPopUp(String image) {
		ScreenPopUp newPopUp = new ScreenPopUp(500, 300);
		setUpNewGraphicalElement(newPopUp, image);
		return newPopUp;
	}
	
	/*
	 * getter for a new background element
	 * Inputs:
	 * 	image: string name of the image that will be used for the background
	 */
	public ScreenBackground getBackground(String image) {
		ScreenBackground newBackground = new ScreenBackground();
		setUpNewGraphicalElement(newBackground, image);
		return newBackground;
	}
	
	/*
	 * getter for a game event
	 * Inputs:
	 * 	-----------------
	 */
	public GameEvent getGameEvent(int nrOfChars) {
		GameEvent newEvent = new GameEvent(nrOfChars);
		setUpNewElement(newEvent);
		return newEvent;
	}
	
	/*
	 * getter for a new job that will be tied to sockets
	 * Inputs:
	 * 	assignSkill: skill that the job uses
	 * 	expValue: exp that the job gives for a one turn of work
	 */
	public Job getJob(JobName jobName) {
		Job newJob = new Job(jobName);
		setUpNewElement(newJob);
		return newJob;
	}
	
	/*
	 * Getter for a new tradeItem
	 * Inputs:
	 * 	name: name of the trade item
	 */
	/*public TradeItem getTradeItem(TradeItemName name) {
		TradeItem newItem = new TradeItem(name);
		setUpNewElement(newItem);
		return newItem;
	}*/
	
	/*
	 * Getter for button listener that adds items to the invetory
	 * Inputs:
	 * 	item: trade item that will be added to the invetory
	 */
	public ItemAddListener getItemAddListener(IGameItem item) {
		return setUpNewElement(new ItemAddListener(item));
	}
	
	/*
	 * Getter for a new event choice
	 * Inputs:
	 * 	masterEvent: event that this choice will be attached to
	 * 	label: string that will be displayed on the button that represents the choice
	 */
	public EventChoice getEventChoice(GameEvent masterEvent, String label) {
		EventChoice newChoice = new EventChoice(masterEvent, label);
		setUpNewElement(newChoice);
		return newChoice;
	}
	
	/*
	 * Gives a button listener that changes the view
	 * Inputs:
	 *  viewName: name of the view that the listener will change the view to
	 */
	public ViewChangeListener getViewChangeListener(ViewName viewName) {
		ViewChangeListener newListener = new ViewChangeListener(viewName);
		setUpNewElement(newListener);
		return newListener;
	}
	
	/*
	 * Gives a listener that changes the turn
	 * NB! (might have to move to mainfactory instead)
	 */
	public TurnChangeListener getTurnChangeListener() {
		TurnChangeListener newListener = new TurnChangeListener();
		setUpNewElement(newListener);
		return newListener;
	}
	
	/*
	 * Button listener that will close a popup
	 * Inputs:
	 * 	target: the pop up that will be closed if this listener is triggered
	 */
	/*public ClosePopUpListener getClosePopUpListener(ScreenPopUp target) {
		ClosePopUpListener newListener = new ClosePopUpListener(target);
		setUpNewElement(newListener);
		return newListener;
	}*/
	
	/*
	 * Getter for a soloImage element 
	 * Inputs: 
	 * 	lenght: length of the solo image
	 * 	height: height of the solo image
	 * 	imageName: string name of the image that will be displayed
	 */
	public ScreenSoloImage getSoloImage(int length, int height, String imageName) {
		ScreenSoloImage newElement = new ScreenSoloImage(length, height);
		setUpNewGraphicalElement(newElement, imageName);
		return newElement;
	}
	
	/*
	 * Getter for a result event listener
	 * Inputs:
	 * 	resultEvent: the event that will be triggered by the listener
	 * 	characters: characters that the new event will be triggered for
	 */
	public ResultEventListener getResultEventListener(GameEvent resultEvent, List<IGameCharacter> characters) {
		ResultEventListener newListener = new ResultEventListener(resultEvent, characters);
		setUpNewElement(newListener);
		return newListener;
	}
	
	/*
	 * Getter for a result event listener
	 * Inputs:
	 * 	resultEvent: the event that will be triggered by the listener
	 */
	public ResultEventListener getResultEventListener(GameEvent resultEvent) {
		ResultEventListener newListener = new ResultEventListener(resultEvent);
		setUpNewElement(newListener);
		return newListener;
	}
	
	/*
	 * Getter for a property bar
	 * Inputs:
	 * 	name: name of the property bad
	 */
	public PropertyBar getPropertyBar(PropertyBarName name) {
		PropertyBar newElement = new PropertyBar(name);
		setUpNewElement(newElement);
		return newElement;
	}
	
	/*
	 * Getter for a property bar limiter
	 * Inputs:
	 * 	name: name of the bar that will be limited
	 * 	amount: the amount value that the limiter is limiting
	 */
	public BarLimiter getPropertyBarLimiter(PropertyBarName name, int amount) {
		BarLimiter newElement = new BarLimiter(name, amount);
		setUpNewElement(newElement);
		return newElement;
	}
	
	/*
	 * Getter for a land area
	 * Inputs:
	 * 	terrain: the terrain on this land area
	 * 	totalDistance: how much distance can be traveled on this land area
	 */
	public LandArea getLandArea(TerrainName terrain, double totalDistance) {
		LandArea newElement = new LandArea(terrain, totalDistance);
		setUpNewElement(newElement);
		return newElement;
	}
	
	/*
	 * Getter for a travel listener (might have to move to
	 */
	public TravelListener getTravelListener() {
		TravelListener newListener = new TravelListener();
		setUpNewElement(newListener);
		return newListener;
	}
	
	/*
	 * Getter for a button frame
	 * Inputs:
	 * 	wrapper: the element this frame will be added to
	 */
	public ButtonFrame getButtonFrame(IStackable wrapper) {
		ButtonFrame newFrame = new ButtonFrame(wrapper);
		setUpNewElement(newFrame);
		return newFrame;
	}
	
	/*
	 * Getter for a button info element
	 * Inputs:
	 * 	button: the button this info element will explain
	 * 	img: background image of this info element
	 */
	public ScreenButtonInfo getButtonInfo(ScreenButton button, String img) {
		ScreenButtonInfo newElement = new ScreenButtonInfo(button);
		setUpNewGraphicalElement(newElement, img);
		return newElement;
	}
	
	/*
	 * Getter for a screenText
	 * Input:
	 * 	x: x coordinate for the text
	 * 	y: y coordinate for the text
	 * 	text: string representation of the text
	 */
	public ScreenText getText(int x, int y, String text) {
		ScreenText newElement = new ScreenText(x, y, text);
		setUpNewElement(newElement);
		return newElement;
	}
	
	/*
	 * Getter for a screenText
	 * Input:
	 * 	text: string representation of the text
	 */
	public ScreenText getText(String text) {
		ScreenText newElement = new ScreenText(text);
		setUpNewElement(newElement);
		return newElement;
	}
	
	/*
	 * Getter for a databox wrapper
	 * Input:
	 * 	x: x coordinate for the wrapped databox
	 * 	y: y coordinate for the wrapped databox
	 * 	dataBox: the databox that will be wrapped
	 */
	public DataBoxWrapper getWrapperForDataBox(int x, int y, ScreenDataBox dataBox) {
		DataBoxWrapper newElement = new DataBoxWrapper(x, y, dataBox);
		setUpNewElement(newElement);
		return newElement;
	}
	
	/*
	 * Getter for radioButtonFunction
	 * Input:
	 * 	buttonFrame: the frame that holds the radio buttons
	 * 	activeImg: string name of the image used for active radio choice
	 * 	inactiveImg: string name of the image used for inactive radio choice
	 */
	public RadioButtonFunction getRadioButtonFunction(ButtonFrame buttonFrame, String activeImg, String inactiveImg) {
		RadioButtonFunction newFunction = new RadioButtonFunction(buttonFrame, activeImg, inactiveImg);
		setUpNewElement(newFunction);
		return newFunction;
	}
	
	/*
	 * Getter for radiobuttonlistener, this is a listener that is not used
	 */
	public RadioButtonListener getRadioButtonListener(RadioButtonFunction function) {
		RadioButtonListener newListener = new RadioButtonListener(function);
		setUpNewElement(newListener);
		return newListener;
	}
	
	/*
	 * Button listener that will change selected land area when triggered
	 * Input:
	 * 	landArea: the land area that will be selected when triggered
	 */
	public LandAreaChangeListener getLandAreaChangeListener(LandArea landArea) {
		LandAreaChangeListener newListener = new LandAreaChangeListener(landArea);
		setUpNewElement(newListener);
		return newListener;
	}
	
	/*
	 * Button listener that will add a land area to selectable land areas when triggere
	 * Input:
	 * 	landArea: the land are that will be added to the selection
	 */
	public AddLandAreaListener getAddLandAreaListener(LandArea landArea) {
		AddLandAreaListener newListener = new AddLandAreaListener(landArea);
		setUpNewElement(newListener);
		return newListener;
	}
	
	/*
	 * Getter for a text frame element
	 * Input:
	 * 	wrapper: the element that the text frame will be on
	 */
	public TextFrame getTextFrame(IStackable wrapper) {
		TextFrame newFrame = new TextFrame(wrapper);
		setUpNewElement(newFrame);
		return newFrame;
	}
	
	/*
	 * Getter for a field and value element that represents a string
	 * that has a specific field and a possible value for that field
	 */
	public FieldAndValue getFieldAndValue() {
		FieldAndValue newElement = new FieldAndValue();
		setUpNewElement(newElement);
		return newElement;
	}
	
	/*
	 * Getter for a field and value element that represents a string
	 * that has a specific field and a possible value for that field
	 * Input:
	 * 	fieldName: name of the field that is represented on the string
	 */
	public FieldAndValue getFieldAndValue(String fieldName) {
		FieldAndValue newElement = new FieldAndValue(fieldName);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public BattleStartListener getBattleStartListener() {
		BattleStartListener newListener = new BattleStartListener();
		setUpNewElement(newListener);
		return newListener;
	}
	
	public BattleEndListener getBattleEndListener() {
		BattleEndListener newListener = new BattleEndListener();
		setUpNewElement(newListener);
		return newListener;
	}
	
	public MovementField getMovementField() {
		MovementField newElement = new MovementField();
		setUpNewElement(newElement);
		return newElement;
	}
	
	public MovementLine getMovementLine(IStackable wrapper, int lineNr) {
		MovementLine newElement = new MovementLine(wrapper, lineNr);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public MovementSquare getMovementSquare(int locationNr, int size, String image) {
		MovementSquare newElement = new MovementSquare(locationNr, size);
		setUpNewGraphicalElement(newElement, image);
		return newElement;
	}
	
	public FieldCharacter getFieldCharacter(IGameCharacter masterChar, ControlFaction faction, String image) {
		FieldCharacter newElement = new FieldCharacter(masterChar, faction);
		setUpNewGraphicalElement(newElement, image);
		return newElement;
	}
	
	public FieldItem getFieldItem(IGameItem masterItem, String image) {
		FieldItem newElement = new FieldItem(masterItem);
		setUpNewGraphicalElement(newElement, image);
		return newElement;
	}
	
	public BarValueRefresher getBarValueRefresher(PropertyBarName propertyName, ArrayList<GameCharacter> characters) {
		BarValueRefresher newElement = new BarValueRefresher(propertyName, characters);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public CombatBarValueRefresh getCombatBarValueRefresh(PropertyBarName propertyName) {
		CombatBarValueRefresh newElement = new CombatBarValueRefresh(propertyName);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public ScreenSimpleItem getScreenSimpleItem(IGameItem masterItem, String imgName) {
		if(masterItem.hasSocketAvatar()) {
			return masterItem.getSocketAvatar();
		}
		else {
			return setUpNewGraphicalElement(new ScreenSimpleItem(masterItem), imgName);
		}
	}
	
	public ItemBag getItemBag() {
		ItemBag newElement = new ItemBag();
		setUpNewElement(newElement);
		return newElement;
	}
	
	/*public CharSpawnInfo getCharSpawnInfo(GameCharacter oneChar, int boardX, int boardY, AICharPatterns aiCharPattern) {
		return setUpNewElement(new CharSpawnInfo(oneChar, boardX, boardY, aiCharPattern));
	}*/
	
	public ShowMovementPathsListener getShowMovementPathsListener() {
		ShowMovementPathsListener newElement = new ShowMovementPathsListener();
		setUpNewElement(newElement);
		return newElement;
	}
	
	public ShowAttackSquaresListener getShowAttackSquaresListener() {
		ShowAttackSquaresListener newElement = new ShowAttackSquaresListener();
		setUpNewElement(newElement);
		return newElement;
	}
	
	public ChangeFCCCSelectionListener getChangeFCCCSelectionListener(FieldCharCommandControl controls, FieldCharCommandStateName stateName) {
		ChangeFCCCSelectionListener newElement = new ChangeFCCCSelectionListener(controls, stateName);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public ScreenBar getScreenBar(int length, int height, PropertyBar masterBar, String barVisual, String imgName) {
		ScreenBar newElement = new ScreenBar(length, height, masterBar, barVisual);
		setUpNewGraphicalElement(newElement, imgName);
		return newElement;
	}
	
	public FromAlliedCharactersPainter getFromAlliedCharacterPainter(int range) {
		FromAlliedCharactersPainter newElement = new FromAlliedCharactersPainter(range);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public OnAlliedCharactersPainter getOnAlliedCharactersPainter() {
		OnAlliedCharactersPainter newElement = new OnAlliedCharactersPainter();
		setUpNewElement(newElement);
		return newElement;
	}
	
	public BattleController getBattleController() {
		BattleController newElement = new BattleController();
		setUpNewElement(newElement);
		return newElement;
	}
	
	public TerrainLimiter getTerrainLimiter(boolean notifyOnFailure, boolean instantPopUp) {
		TerrainLimiter newElement = new TerrainLimiter(notifyOnFailure, instantPopUp);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public ItemLimiter getItemLimiter(boolean notifyOnFailure, boolean instantPopUp, IGameItem item) {
		ItemLimiter newElement = new ItemLimiter(notifyOnFailure, instantPopUp, item);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public OverTurnLimiter getOverTurnLimiter(boolean notifyOnFailure, boolean instantPopUp, int nrOfTurns) {
		OverTurnLimiter newElement = new OverTurnLimiter(notifyOnFailure, instantPopUp, nrOfTurns);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public LoadInGameStateListener getLoadInGameStateListener(IGameState gameState) {
		LoadInGameStateListener newElement = new LoadInGameStateListener(gameState);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public TabWindow getTabWindow(String backgroundImg) {
		TabWindow newElement = new TabWindow();
		setUpNewGraphicalElement(newElement, backgroundImg);
		return newElement;
	}
	
	public TabDirectory getTabDirectory(int x, int y, String backgroundImg) {
		TabDirectory newElement = new TabDirectory(x, y);
		setUpNewGraphicalElement(newElement, backgroundImg);
		return newElement;
	}
	
	public ChangeTabListener getChangeTabListener(TabDirectory masterDirectory, TabWindow tab) {
		ChangeTabListener newElement = new ChangeTabListener(masterDirectory, tab);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public CharSocketBuilder getCharSocketBuilder(ScrollCharSocket socket, ISocketContentProviding<ScreenSimpleChar> supplier) {
		CharSocketBuilder newElement = new CharSocketBuilder(socket, supplier);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public FullCharacterListProviding getFullCharacterListProvding() {
		FullCharacterListProviding newElement = new FullCharacterListProviding();
		setUpNewElement(newElement);
		return newElement;
	}
	
	public NewItemSocketBuilder getNewItemSocketBuilder(ScrollItemSocket socket, ISocketContentProviding<ScreenSimpleItem> supplier) {
		return setUpNewElement(new NewItemSocketBuilder(socket, supplier));
	}
	
	public TagItemProviding getTagItemProviding(ItemTagName tagName) {
		TagItemProviding newElement = new TagItemProviding(tagName);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public AddEnemiesListener getAddEnemiesListener(AICharacterSet newEnemy) {
		return setUpNewElement(new AddEnemiesListener(newEnemy));
	}
	
	public AddEnemiesListener getAddEnemiesListener(ArrayList<AICharacterSet> newEnemies) {
		return setUpNewElement(new AddEnemiesListener(newEnemies));
	}
	
	public JobTopic getJobTopic(IStackable wrapper) {
		JobTopic newElement = new JobTopic(wrapper);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public DefaultTopicController getDefaultTopicController(JobTopic jobTopic) {
		DefaultTopicController newElement = new DefaultTopicController(jobTopic);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public TerrainTopicController getTerrainTopicController(JobTopic jobTopic) {
		TerrainTopicController newElement = new TerrainTopicController(jobTopic);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public PartyStanceTopicController getPartyStanceTopicController(JobTopic jobTopic) {
		PartyStanceTopicController newElement = new PartyStanceTopicController(jobTopic);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public BattleTopicController getBattleTopicController(JobTopic jobTopic) {
		BattleTopicController newElement = new BattleTopicController(jobTopic);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public QuestTopicController getQuestTopicController(JobTopic jobTopic) {
		QuestTopicController newElement = new QuestTopicController(jobTopic);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public UpdateJobFrameListener getUpdateJobFrameListener() {
		UpdateJobFrameListener newElement = new UpdateJobFrameListener();
		setUpNewElement(newElement);
		return newElement;
	}
	
	public PartyStanceChangeListener getPartyStanceChangeListener(PartyStance partyStanceValue) {
		PartyStanceChangeListener newElement = new PartyStanceChangeListener(partyStanceValue);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public EnemyCharLimiter getEnemyCharLimiter(boolean notifyOnFailure, boolean instantPopUp, boolean limitWhenExist) {
		EnemyCharLimiter newElement = new EnemyCharLimiter(notifyOnFailure, instantPopUp, limitWhenExist);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public EventLimiter getEventLimiter(boolean notifyOnFailure, boolean instantPopUp) {
		EventLimiter newElement = new EventLimiter(notifyOnFailure, instantPopUp);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public CutsceneDataCollection getCutsceneDataCollection(String name) {
		CutsceneDataCollection newElement = new CutsceneDataCollection(name);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public AdvanceCutsceneListener getAdvanceCutsceneListener() {
		AdvanceCutsceneListener newElement = new AdvanceCutsceneListener();
		setUpNewElement(newElement);
		return newElement;
	}
	
	public SpawnArea getSpawnArea(ControlFaction faction) {
		SpawnArea newElement = new SpawnArea(faction);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public LoadInDefaultViewListener getLoadInDefaultViewListener() {
		LoadInDefaultViewListener newElement = new LoadInDefaultViewListener();
		setUpNewElement(newElement);
		return newElement;
	}
	
	public FullItemProviding getFullItemProviding() {
		FullItemProviding newElement = new FullItemProviding();
		setUpNewElement(newElement);
		return newElement;
	}
	
	public ProtoShowDefaultController getDefaultJobTopicControllerForProtoShow(JobTopic topic) {
		ProtoShowDefaultController newElement = new ProtoShowDefaultController(topic);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public ProtoShowPartyStanceController getPartyStanceJobTopicControllerForProtoShow(JobTopic topic) {
		ProtoShowPartyStanceController newElement = new ProtoShowPartyStanceController(topic);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public SkillGain getSkillGain(SkillName skillName, int expGain) {
		SkillGain newElement = new SkillGain(skillName, expGain);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public SkipCutsceneListener getSkipCutseneListener() {
		SkipCutsceneListener newElement = new SkipCutsceneListener();
		setUpNewElement(newElement);
		return newElement;
	}
	
	public FoodSearchEvents1 getFoodSearchEvents1() {
		FoodSearchEvents1 newElement = new FoodSearchEvents1();
		setUpNewElement(newElement);
		return newElement;
	}
	
	public SupplySearchEvents1 getSupplySearchEvents1() {
		SupplySearchEvents1 newElement = new SupplySearchEvents1();
		setUpNewElement(newElement);
		return newElement;
	}
	
	public ScreenTimedDataBox getScreenTimedDataBox(String textValue, IStackable bottomElem, int timeValue) {
		ScreenTimedDataBox newElement = new ScreenTimedDataBox(textValue, bottomElem, timeValue);
		setUpNewGraphicalElement(newElement, "50red");
		return newElement;
	}
	
	public Notice getNotice(String text) {
		Notice newElement = new Notice(text);
		setUpNewElement(newElement);
		return newElement;
	}
	
	public ScreenInventoryItemUpdateListener getScreenInventoryItemUpdateListener(ScreenSimpleItem item) {
		return setUpNewElement(new ScreenInventoryItemUpdateListener(item));
	}
	
	public OpenNoticeListener getOpenNoticeButtonListener(Notice notice) {
		return setUpNewElement(new OpenNoticeListener(notice));
	}
	
	public RemoveNoticeListener getRemoveNoticeListener(Notice notice) {
		return setUpNewElement(new RemoveNoticeListener(notice));
	}
	
	public ItemSocketUpdateListener getItemSocketUpdateListener(ScrollItemSocket socket) {
		return setUpNewElement(new ItemSocketUpdateListener(socket));
	}
	
	public ScreenScrollPanel getNewScrollPanel(int x, int y, int length, int height, String image) {
		return setUpNewGraphicalElement(new ScreenScrollPanel(x, y, length, height), image);
	}
	
	public PanelScrollListener getPanelScrollListener(ScreenScrollPanel target, boolean xAxis, boolean increaseScroll) {
		return setUpNewElement(new PanelScrollListener(target, xAxis, increaseScroll));
	}
	
	public OpenElementOnPopupLayerListener getOpenElementOnPopupLayerListener(IStackable target) {
		return setUpNewElement(new OpenElementOnPopupLayerListener(target));
	}
	
	public SimpleCharacterFrame getSimpleCharacterFrame(IStackable wrapper) {
		return setUpNewElement(new SimpleCharacterFrame(wrapper));
	}
	
	public ScrollCharSocket getScrollCharSocket(int x, int y, int length, int height, String imageName) {
		return setUpNewGraphicalElement(new ScrollCharSocket(x, y, length, height), imageName);
	}
	
	public ScreenSimpleChar getScreenSimpleChar(IGameCharacter master) {
		if(master.hasSocketAvatar()) {
			return master.getSocketAvatar();
		}
		else {
			return setUpNewGraphicalElement(new ScreenSimpleChar(master), master.getImgName());
		}
	}
	
	public SimpleItemFrame getSimpleItemFrame(IStackable wrapper) {
		return setUpNewElement(new SimpleItemFrame(wrapper));
	}
	
	public ScrollItemSocket getScrollItemSocket(int x, int y, int length, int height, String imageName) {
		return setUpNewGraphicalElement(new ScrollItemSocket(x, y, length, height), imageName);
	}
	
	public BasicSurvivalTopicController getBasicSurvivalTopicController(JobTopic newTopic) {
		return setUpNewElement(new BasicSurvivalTopicController(newTopic));
	}
	
	public BasicPartyStanceController getBasicPartyStanceController(JobTopic newTopic) {
		return setUpNewElement(new BasicPartyStanceController(newTopic));
	}
	
	public GoalMarker getGoalMarker(double distance, GameEvent resultEvent) {
		return setUpNewElement(new GoalMarker(distance, resultEvent));
	}
	
	public ClearTravelHistoryListener getClearTravelHistoryListener() {
		return setUpNewElement(new ClearTravelHistoryListener());
	}
	
	public DecreaseWaterSupplyByTurnListener getDecreaseWaterSupplyByTurnListener() {
		return setUpNewElement(new DecreaseWaterSupplyByTurnListener());
	}
	
	public AddValueToBarListener getAddValueToBarListener(PropertyBar bar, int value) {
		return setUpNewElement(new AddValueToBarListener(bar, value));
	}
	
	public AddValueToBarListener getAddValueToBarListener(PropertyBar bar) {
		return setUpNewElement(new AddValueToBarListener(bar));
	}
	
	public WaterBarListener getWaterBarListener() {
		return setUpNewElement(new WaterBarListener());
	}
	
	public FieldCharCommandUpdater getFieldCharCommandUpdater() {
		return setUpNewElement(new FieldCharCommandUpdater());
	}
	
	public TutorialMakeElementsVisibleOnBarValue getTutorialMakeElementsVisibleOnBarValue(int onValue, ArrayList<ScreenElement> elements, PropertyBar targetBar) {
		return setUpNewElement(new TutorialMakeElementsVisibleOnBarValue(onValue, elements, targetBar));
	}
	
	public SpawnLimiting getSpawnLimiting(int oneTurnSpawnAmount) {
		return setUpNewElement(new SpawnLimiting(oneTurnSpawnAmount));
	}
	
	public PlayerLimiterRefreshListener getPlayerLimiterRefreshListener() {
		return setUpNewElement(new PlayerLimiterRefreshListener());
	}
	
	public TimeSpawnEveryTurnOne getTimeSpawnEveryTurnOne() {
		return setUpNewElement(new TimeSpawnEveryTurnOne());
	}
	
	public TimeSpawnKeepNrOfCharsOnField getTimeSpawnKeepNrOfCharsOnField(int amountOfChars) {
		return setUpNewElement(new TimeSpawnKeepNrOfCharsOnField(amountOfChars));
	}
	
	public ActivateAnotherButtonListener getActivateAnotherButtonListener(ScreenButton secondButton) {
		return setUpNewElement(new ActivateAnotherButtonListener(secondButton));
	}
	
	public ImportAllCharsToModuleListener getImportAllCharsToModuleListener(IGameState gameState) {
		return setUpNewElement(new ImportAllCharsToModuleListener(gameState));
	}
	
	public AddCharToGameStateListener getAddCharToGameStateListener(IGameState gameState, CharValue oneCharValues) {
		return setUpNewElement(new AddCharToGameStateListener(gameState, oneCharValues));
	}
	
	public AddCharToGameStateListener getAddCharToGameStateListener(IGameState gameState, ArrayList<CharValue> charValues) {
		return setUpNewElement(new AddCharToGameStateListener(gameState, charValues));
	}
	
	public LotteryBox getLotteryBox() {
		return setUpNewElement(new LotteryBox());
	}
	
	public FoodSearchJob getFoodSearchJob() {
		return setUpNewElement(new FoodSearchJob());
	}
	
	public RestJob getRestJob() {
		return setUpNewElement(new RestJob());
	}
	
	public BaggageJob getBaggageJob() {
		return setUpNewElement(new BaggageJob());
	}
	
	public WeightCarryingAbility getWeightCarryingAbility() {
		return setUpNewElement(new WeightCarryingAbility());
	}
	
	public NotEnoughCarryLimiter getNotEnoughCarrylimiter(boolean notifyOnFailure, boolean instantPopUp) {
		return setUpNewElement(new NotEnoughCarryLimiter(notifyOnFailure, instantPopUp));
	}
	
	public DeleteNrOfItemListener getDeleteNrOfItemListener(int amount, IGameItem item) {
		return setUpNewElement(new DeleteNrOfItemListener(amount, item));
	}
	
	public BarGain getBarGain(PropertyBarName name, int amount) {
		return setUpNewElement(new BarGain(name, amount));
	}
	
	public JobFrameVisualListener getJobFrameVisualListener(IJobPanel frame) {
		return setUpNewElement(new JobFrameVisualListener(frame));
	}
	
	public ClearAllNoticesListener getClearAllNoticesListener() {
		return setUpNewElement(new ClearAllNoticesListener());
	}
	
	public FieldCommandControlVisualListener getFieldCommandControlVisualListener(FieldCharCommandControl controller) {
		return setUpNewElement(new FieldCommandControlVisualListener(controller));
	}
	
	/*public FoodItem getFoodItem(TradeItemName itemName) {
		return setUpNewElement(new FoodItem(itemName));
	}*/
	
	public InventoryTurnChangeListener getInventoryTurnChangeListener() {
		return setUpNewElement(new InventoryTurnChangeListener());
	}
	
	public JobFrameExpellingListener getJobFrameExpellingListener(IJobPanel jobFrame) {
		return setUpNewElement(new JobFrameExpellingListener(jobFrame));
	}
	
	public ScreenBarValueUpdater getScreenBarValueUpdater(ScreenBar barToUpdate) {
		return setUpNewElement(new ScreenBarValueUpdater(barToUpdate));
	}
	
	public AllItemsFilter getAllItemsFilter() {
		return setUpNewElement(new AllItemsFilter());
	}
	
	public TagFilter getTagFilter(ItemTagName filterTag) {
		return setUpNewElement(new TagFilter(filterTag));
	}
	
	public FoodLimiter getFoodLimiter(boolean notifyOnFailure, boolean instantPopUp) {
		return setUpNewElement(new FoodLimiter(notifyOnFailure, instantPopUp));
	}
	
	public WaterSearchJob getWaterSearchJob() {
		return setUpNewElement(new WaterSearchJob());
	}
	
	public SimpleSkillFactor getSimpleSkillFactor(SkillName skillName) {
		return setUpNewElement(new SimpleSkillFactor(skillName));
	}
	
	public RightMouseInfoCloser getRightMouseInfoCloser() {
		return setUpNewElement(new RightMouseInfoCloser());
	}
	
	public ScreenCharInfoPage getScreenCharInfoPage(IGameCharacter masterChar, String imageName) {
		return setUpNewGraphicalElement(new ScreenCharInfoPage(masterChar), imageName);
	}
	
	public OpenElementOnTopLayerListener getOpenElementOnTopLayerListener(IStackable newElement) {
		return setUpNewElement(new OpenElementOnTopLayerListener(newElement));
	}
	
	/*public CloseInfoPageListener getCloseInfoPageListener(BaseInfoPage page) {
		return setUpNewElement(new CloseInfoPageListener(page));
	}*/
	
	public UpdateInfoPageSkills getUpdateInfoPageSkills(ScreenCharInfoPage infoPage) {
		return setUpNewElement(new UpdateInfoPageSkills(infoPage));
	}
	
	public UpdateInfoPageHp getUpdateInfoPageHp(ScreenCharInfoPage infoPage) {
		return setUpNewElement(new UpdateInfoPageHp(infoPage));
	}
	
	public UpdateInfoPageStamina getUpdateInfoPageStamina(ScreenCharInfoPage infoPage) {
		return setUpNewElement(new UpdateInfoPageStamina(infoPage));
	}
	
	public ItemInfoPage getItemInfoPage(IGameItem masterItem, String imageName) {
		return setUpNewGraphicalElement(new ItemInfoPage(masterItem), imageName);
	}
	
	public ItemInfoPageAmountListener getItemInfoPageAmountListener(ItemBasePage infoPage) {
		return setUpNewElement(new ItemInfoPageAmountListener(infoPage));
	}
	
	public FoodInfoPage getFoodInfoPage(FoodItemBase masterItem, String imageName) {
		return setUpNewGraphicalElement(new FoodInfoPage(masterItem), imageName);
	}
	
	public FoodInfoExpirationListener getFoodInfoExpirationListener(FoodInfoPage infoPage) {
		return setUpNewElement(new FoodInfoExpirationListener(infoPage));
	}
	
	public GoSituationChangeListener getGoSituationChangeListener(ISituationChange situationChange) {
		return setUpNewElement(new GoSituationChangeListener(situationChange));
	}
	
	public SettlementLocationListener getSettlementLocationListener(ISettlementLocation location) {
		return setUpNewElement(new SettlementLocationListener(location));
	}
	
	public StandardGraphicLayer getStandardGraphicLayer() {
		return setUpNewElement(new StandardGraphicLayer());
	}
	
	public PopupGraphicLayer getPopupGraphicsLayer() {
		return setUpNewElement(new PopupGraphicLayer());
	}
	
	public CloseIStackableElementListener getCloseIStackableElementListener(IStackable target) {
		return setUpNewElement(new CloseIStackableElementListener(target));
	}
	
	public StartDialogueListener getStartDialogueListener(IDialogueCharacter target) {
		return setUpNewElement(new StartDialogueListener(target));
	}
	
	public AddNewQuestListener getAddNewQuestListener(IQuest newQuest) {
		return setUpNewElement(new AddNewQuestListener(newQuest));
	}
	
	public QuestInfoPage getQuestInfoPage(IQuest targetQuest, String imageName) {
		return setUpNewGraphicalElement(new QuestInfoPage(targetQuest), imageName);
	}
	
	public DialogueFinishListener getDialogueFinishListener(CutsceneDataCollection targetDialogue, IDialogueCharacter targetNPC) {
		return setUpNewElement(new DialogueFinishListener(targetDialogue, targetNPC));
	}
	
	public QuestLimiter getQuestLimiter(IQuest limitingQuest) {
		return setUpNewElement(new QuestLimiter(limitingQuest));
	}
	
	public SelectInfoQuestStepListener getSelectInfoQuestStepListener(QuestInfoPage page, IQuestStep step) {
		return setUpNewElement(new SelectInfoQuestStepListener(page, step));
	}
	
	public IButtonLimiter getSettLemenLocationLimiter(ISettlementLocation masterLocation) {
		return setUpNewElement(new SettlementLocationLimiter(masterLocation));
	}
	
	public IButtonLimiter getDialogueElementLimiter(IDialogueCharacter limitingDialogueElement) {
		return setUpNewElement(new DialogueElementLimiter(limitingDialogueElement));
	}
	
	public IEventModule getSettlementWaterSearchEvents() {
		return setUpNewElement(new SettlementWaterSearchEvents());
	}
	
	public IJob getSettlementWaterSearchJob() {
		return setUpNewElement(new SettlementWaterSearchJob());
	}
	
	public IJob getCraftingJob(CraftingManager connectedManager) {
		return setUpNewElement(new CraftingJob(connectedManager));
	}
	
	public BaseInfoPage getJobInfoPage(IJob masterJob) {
		return setUpNewGraphicalElement(new JobInfoPage(masterJob), this.getBackgroundNameForInfoPages());
	}
	
	public OpenInfoSelectionListener getOpenInfoSelectionListener(ScreenElement masterElement, String labelText) {
		return setUpNewElement(new OpenInfoSelectionListener(masterElement, labelText));
	}
	
	public ISButtonListener getActivateBluePrintListener(CraftingInfoPage masterPage, IBlueprint blueprintToActivate) {
		return setUpNewElement(new ActivateBlueprintListener(masterPage, blueprintToActivate));
	}
	
	public ISButtonListener getAddSelectedBlueprintAsCraftingProjectListener(CraftingInfoPage masterPage) {
		return setUpNewElement(new AddSelectedBlueprintAsCraftingProjectListener(masterPage));
	}
	
	/*
	 * method for setting main factory, booting up and loading an image to a graphical element
	 * Inputs:
	 * 	element: screen element that will be set up
	 * 	image: string name of the elements skin
	 */
	/*private <T extends ScreenElement> T setUpNewGraphicalElement(T element, String image) {
		this.setUpNewElement(element);
		element.setImage(image);
		return element;
	}*/
	
	/*
	 * method for setting main factory and booting up a object before returning it
	 * Input:
	 * 	element: the object that will be set up
	 */
	/*private <T extends IAutoMake> T setUpNewElement(T element) {
		element.setMainFactory(this.mainFactory);
		element.bootUp();
		return element;
	}*/
	
	private String getBackgroundNameForInfoPages() {
		return "Orange";
	}

}

package factories;

import cutsceneInstances.ReachingLoupeCutscene;

public class CutsceneFactory extends BaseMinorFactory {
	//private MainFactory mainFactory;

	/*@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}*/

	/*@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}*/
	
	/*
	 * method for setting main factory and booting up a object before returning it
	 * Input:
	 * 	element: the object that will be set up
	 */
	/*private <T extends IAutoMake> T setUpNewElement(T element) {
		element.setMainFactory(this.mainFactory);
		element.bootUp();
		return element;
	}*/

	public ReachingLoupeCutscene getReachingLoupeCutscene() {
		return setUpNewElement(new ReachingLoupeCutscene());
	}

}

package skillListeners;

import screenInfoPage.ScreenCharInfoPage;

//Listener that updates the skill values on a info page
public class UpdateInfoPageSkills extends BaseSkillChangeListener {
	private ScreenCharInfoPage infoPage;
	
	//constructor, input is the info page that will be updated
	public UpdateInfoPageSkills(ScreenCharInfoPage page) {
		this.infoPage = page;
	}

	@Override
	public void skillChangeEvent() {
		this.infoPage.callUpdateForSkills();
		
	}

}

package inventoryChange;

import sreenElementsSockets.ScrollItemSocket;

//Inventory lister that listens for new item events, is place on sockets that represent inventory
public class ItemSocketUpdateListener extends BaseInventoryListener {
	protected ScrollItemSocket socket;
	
	//constructor
	public ItemSocketUpdateListener(ScrollItemSocket socket) {
		this.socket = socket;
	}

	@Override
	public void inventoryNewItemEventOccured(InventoryNewItemEvent evt) {
		this.socket.addNewElement(this.mainFactory.getElemFactory().getScreenSimpleItem(evt.getNewItem(), evt.getNewItem().getImageName()));
		/*if(socket.hasFilter()) {
			filterSituation();
		}
		else {
			nonFilterSituation(evt);
		}*/
	}
	
	//does an socket update in a situation where socket has a filter
	private void filterSituation() {
		this.socket.callFilterUpdate();
	}
	
	//does a socket update that does not use a filter
	private void nonFilterSituation(InventoryNewItemEvent evt) {
		this.socket.addNewElement(this.mainFactory.getElemFactory().getScreenSimpleItem(evt.getNewItem(), evt.getNewItem().getImageName()));
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

package loupeNpcs;

import dialogueCharacters.BaseDialogueCharacter;
import view.CutsceneData;
import view.CutsceneDataCollection;
import view.ViewName;

public class LoupeDocilePrisoner extends BaseDialogueCharacter {
	private CutsceneDataCollection dialogue;
	
	public void bootUp() {
		super.bootUp();
		this.dialogue = this.makePlaceholderCutscene();
	}

	@Override
	protected void getCurrentDialogueInstance() {
		this.mainFactory.getCutsceneBuilder().startACutscene(dialogue);
		
	}

	@Override
	protected boolean hasAllowedDialogue() {
		return true;
	}

	@Override
	protected String giveNameToChar() {
		return "Docile prisoners";
	}
	
	private CutsceneDataCollection makePlaceholderCutscene() {
		CutsceneDataCollection intoduction = this.getNewDialogueBaseCutscene("Loupe docile prisoner dialogue");
		CutsceneData newFrame = new CutsceneData("travelBG", "Loupe docile prisoner dialogue.");
		intoduction.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "test frame");
		intoduction.addNewCutScene(newFrame);
		//intoduction.addFinalButtonEventListener(this.mainFactory.getElemFactory().getViewChangeListener(ViewName.Loupe));
		return intoduction;
	}

}

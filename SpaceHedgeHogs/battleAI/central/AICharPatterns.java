package central;

import attacking.AIAttackPatterns;
import moving.AIMovementPatterns;
import targetChoosing.AITargetingPatterns;

//Full behaviour pattern set for an AI character, this is for individual character behaviour
public class AICharPatterns {
	private AIAttackPatterns attackPatern;
	private AIMovementPatterns movementPattern;
	private AITargetingPatterns targetingPattern;
	
	//constructor
	public AICharPatterns(AIAttackPatterns attackPattern, AIMovementPatterns movementPattern, AITargetingPatterns targetingPattern) {
		this.attackPatern = attackPattern;
		this.movementPattern = movementPattern;
		this.targetingPattern = targetingPattern;
	}

	//getter for attack logic value
	public AIAttackPatterns getAttackPatern() {
		return attackPatern;
	}

	//getter for movement logic value
	public AIMovementPatterns getMovementPattern() {
		return movementPattern;
	}

	//getter for targeting pattern logic value
	public AITargetingPatterns getTargetingPattern() {
		return targetingPattern;
	}

	//setter for attack logic value
	public void setAttackPatern(AIAttackPatterns attackPatern) {
		this.attackPatern = attackPatern;
	}

	//setter for movement logic value
	public void setMovementPattern(AIMovementPatterns movementPattern) {
		this.movementPattern = movementPattern;
	}

	//setter for targeting logic value
	public void setTargetingPattern(AITargetingPatterns targetingPattern) {
		this.targetingPattern = targetingPattern;
	}
	
	
	

}

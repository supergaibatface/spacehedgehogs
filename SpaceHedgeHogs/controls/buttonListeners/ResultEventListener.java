package buttonListeners;

import java.util.ArrayList;
import java.util.List;

import events.GameEvent;
import factories.IAutoMake;
import factories.MainFactory;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;

//Listener for triggering another event after this is triggered
public class ResultEventListener extends BaseButtonListener {
	private GameEvent resultEvent;
	private List<IGameCharacter> characters;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	resultEvent: the event that will be triggered with this listener
	 * 	characters: the list of characters that the new event will be triggered for
	 */
	public ResultEventListener(GameEvent resultEvent, List<IGameCharacter> characters) {
		this.resultEvent = resultEvent;
		this.characters = characters;
	}
	
	/*
	 * Constructor
	 * Inputs:
	 * 	resultEvent: the event that will be triggered with this listener
	 * 	characters: the list of characters that the new event will be triggered for
	 */
	public ResultEventListener(GameEvent resultEvent) {
		this.resultEvent = resultEvent;
		this.characters = new ArrayList<IGameCharacter>();
	}


	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		if(this.characters.isEmpty()) {
			this.mainFactory.getGElementManager().addQuickPopUp(this.resultEvent.triggerPopUp());
		}
		else {
			this.mainFactory.getGElementManager().addQuickPopUp(this.resultEvent.triggerPopUp(characters));
		}
		//this.mainFactory.getGElementManager().addQuickPopUp(this.resultEvent.triggerPopUp(characters));
		
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String toInfo() {
		return "Calls another event";
	}

}

package propertyBarListeners;

//Listener that updates water bars value on activation
public class WaterBarListener extends BaseBarChangeListener {

	@Override
	public void barChangeHappened(int newValue) {
		this.mainFactory.getWaterSupplyController().updateData();
		
	}

}

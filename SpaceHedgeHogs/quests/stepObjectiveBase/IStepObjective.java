package stepObjectiveBase;

public interface IStepObjective {
	
	public String getObjectiveDescription();
	
	public boolean isCompleted();
	
	public void announceObjectiveCompletion();

}

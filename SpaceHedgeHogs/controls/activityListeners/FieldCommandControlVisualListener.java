package activityListeners;

import combatFieldElements.FieldCharacter;
import fieldCharCommand.FieldCharCommandControl;
import sreenElementsSockets.ScreenSimpleChar;
import sreenElementsSockets.ScreenSimpleItem;

//Listener that reveals field command control when a field character is activated
public class FieldCommandControlVisualListener extends BaseActivityListener {
	private FieldCharCommandControl fieldCCC;
	
	//constructor, input is fieldCharCommandControl that is used
	public FieldCommandControlVisualListener(FieldCharCommandControl fieldCCC) {
		this.fieldCCC = fieldCCC;
	}

	@Override
	protected void deselected() {
		if(this.fieldCCC.getDataBox().isVisible()) {
			this.fieldCCC.deactivateCommandSelection();
		}
		
	}

	@Override
	protected void screenSimpleCharActivated(ScreenSimpleChar activatedChar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void screenSimpleItemActivated(ScreenSimpleItem activateditem) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void fieldCharActivated(FieldCharacter activatedChar) {
		this.fieldCCC.activateCommandSelection();
		
	}

}

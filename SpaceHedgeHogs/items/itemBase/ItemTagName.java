package itemBase;

//Item tags used for distinguishing between different items by use
public enum ItemTagName {
	Battle, FieldPlacement, Food, BaggageAnimal, Ingredient

}

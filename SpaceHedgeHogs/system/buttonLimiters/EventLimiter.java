package buttonLimiters;

public class EventLimiter extends BaseButtonLimiter {

	//Limiter that stops a button if there are active popups
	public EventLimiter(boolean notifyOnFailure, boolean instantPopUp) {
		super(notifyOnFailure, instantPopUp);
	}

	@Override
	public boolean check() {
		if(this.mainFactory.getGElementManager().hasPopUps()) {
			return false;
		}
		return true;
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doCost() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String reasonForFail() {
		return "there are popups";
	}

}

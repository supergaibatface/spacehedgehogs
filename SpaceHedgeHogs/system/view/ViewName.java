package view;

//Represent all possible view names
public enum ViewName {
	MainMenu, 
	DebugMenu, 
	TestF, 
	TestT, 
	ShowT, 
	ShowC, 
	Debug,
	Battle, 
	Cutscene, 
	TravelTutorial, 
	CampTutorial, 
	SettlementTutorial,
	Loupe,
	LoupeCamp;

}

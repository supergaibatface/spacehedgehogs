package characterExceptions;

public class NoCustomCharacterClassException extends Exception {
	
	public NoCustomCharacterClassException(String nameOfTheNotFoundCharacter) {
		super("Couldn't find a character class for name: "+nameOfTheNotFoundCharacter);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}

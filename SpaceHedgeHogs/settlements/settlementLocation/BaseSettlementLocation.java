package settlementLocation;

import java.util.ArrayList;
import java.util.List;

import dialogueCharacters.IDialogueCharacter;
import factories.IAutoMake;
import factories.MainFactory;
import generalGraphics.ButtonLookType;
import screenElements.ScreenButton;
import screenElements.ScreenDataBox;
import settlementLocationListeners.ISettlementLocationListener;

public abstract class BaseSettlementLocation implements IAutoMake, ISettlementLocation {
	protected MainFactory mainFactory;
	protected boolean accessable;
	protected ScreenButton locationButton;
	protected ScreenDataBox actionDisplayList;
	protected ScreenButton closeActionDisplayListButton;
	protected List<IDialogueCharacter> locationCharacters;
	protected String locationName;
	protected List<ISettlementLocationListener> listeners;
	
	public BaseSettlementLocation(String locationName) {
		this.locationName = locationName;
	}

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.accessable = giveStartAccessabilityValue();
		this.makeLocationButton();
		this.makeCharacterList();
		this.makeActionDisplayList();
		this.listeners = new ArrayList<ISettlementLocationListener>();
	}
	
	public boolean isAccessable() {
		return this.accessable;
	}
	
	public boolean changeAccessabilityTo(boolean newValue) {
		this.accessable = newValue;
		return newValue;
	}
	
	public void reactToClick() {
		this.announceVisitingToListeners();
		this.clickScript();
	}
	
	protected abstract void clickScript();
	
	protected void makeLocationButton() {
		this.locationButton = this.mainFactory.getElemFactory().getScreenButton(this.giveButtonX(), this.giveButtonY(), ButtonLookType.CUSTOM, this.giveButtonLabel());
		this.locationButton.setSize(giveButtonLength(), giveButtonHeight());
		this.locationButton.setImage(this.giveButtonImgName());
		this.locationButton.addButtonEventListener(this.mainFactory.getElemFactory().getSettlementLocationListener(this));
		this.locationButton.addActionLimiter(this.mainFactory.getElemFactory().getSettLemenLocationLimiter(this));
	}
	
	protected abstract int giveButtonX();
	
	protected abstract int giveButtonY();
	
	protected abstract int giveButtonLength();
	
	protected abstract int giveButtonHeight();
	
	protected abstract String giveButtonImgName();
	
	protected abstract String giveButtonLabel();
	
	public ScreenButton getLocationButton() {
		return this.locationButton;
	}
	
	protected void makeActionDisplayList() {
		this.actionDisplayList = this.mainFactory.getElemFactory().getScreenDataBox(0, 0, "Orange");
		this.assignCoordinatesForActionDisplayList();
		this.setLabelForActionDisplayList();
		this.makeCloseActionDisplayListButton();
		this.refreshActionDisplayListButtonContent();
		this.actionDisplayList.autoSize();
	}
	
	protected void makeCloseActionDisplayListButton() {
		this.closeActionDisplayListButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.POPUPSELECT, "Close selection");
		this.closeActionDisplayListButton.addButtonEventListener(this.mainFactory.getElemFactory().getCloseIStackableElementListener(actionDisplayList));
	}
	
	protected void refreshActionDisplayListButtonContent() {
		this.actionDisplayList.removeAllButtons();
		this.addDialogueCharcterOptions(actionDisplayList);
		this.actionDisplayList.addButton(closeActionDisplayListButton);
	}
	
	protected void assignCoordinatesForActionDisplayList() {
		this.actionDisplayList.setCoordinates(this.giveButtonX()+40, this.giveButtonY()+40);
	}
	
	protected void setLabelForActionDisplayList() {
		this.actionDisplayList.fillTextOneLine(this.giveButtonLabel());
	}
	
	protected void makeCharacterList() {
		this.locationCharacters = new ArrayList<IDialogueCharacter>();
		this.setUpLocationCharacters(locationCharacters);
	}
	
	protected abstract void setUpLocationCharacters(List<IDialogueCharacter> addLocation);
	
	protected void addDialogueCharcterOptions(ScreenDataBox location) {
		for(IDialogueCharacter oneCharacter : this.locationCharacters) {
			if(oneCharacter.canStartDialogue()) {
				ScreenButton newButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.POPUPSELECT, "Talk to: "+oneCharacter.getName());
				newButton.addButtonEventListener(this.mainFactory.getElemFactory().getStartDialogueListener(oneCharacter));
				location.addButton(newButton);
			}
		}
	}
	
	@Override
	public boolean addListener(ISettlementLocationListener newListener) {
		if(!this.listeners.contains(newListener)) {
			this.listeners.add(newListener);
			return true;
		}
		return false;
	}
	
	@Override
	public boolean removeListener(ISettlementLocationListener oldListener) {
		if(this.listeners.contains(oldListener)) {
			this.listeners.remove(oldListener);
			return true;
		}
		return false;
	}
	
	protected void announceVisitingToListeners() {
		for(ISettlementLocationListener oneListener : this.listeners) {
			oneListener.callVistingEvent();
		}
	}
	
	public String getName() {
		return this.locationName;
	}
	
	protected abstract boolean giveStartAccessabilityValue();

}

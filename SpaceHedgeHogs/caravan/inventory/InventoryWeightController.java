package inventory;

import java.util.ArrayList;
import java.util.List;

import factories.IAutoMake;
import factories.MainFactory;

//Controller that catches and reflects weight changes in the inventory, its used for itneracting with the total weight of the inventory
public class InventoryWeightController implements IAutoMake {
	private MainFactory mainFactory;
	private List<WeightCarryingAbility> weightCarrying;
	

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.weightCarrying = new ArrayList<WeightCarryingAbility>();
		
	}
	
	//getter for the full weight of the inventory
	public int getFullInventoryWeight() {
		return this.mainFactory.getInventory().getInventoryFullWeight();
	}
	
	//getter for the full current designated carrying ability
	public int getFullCarryingAbility() {
		int fullValue = 0;
		for(WeightCarryingAbility oneAbility : this.weightCarrying) {
			fullValue = fullValue + oneAbility.getCapability();
		}
		return fullValue;
	}
	
	//checks if the current carry ability is enough to carry current inventory weight
	public boolean hasEnoughCarryingAbility() {
		return this.getFullCarryingAbility() >= this.getFullInventoryWeight();
	}
	
	/*
	 * method for updating weight values in water supply controller 
	 * NB! Temp solution for informing the player
	 */
	public void visualUpdate() {
		this.mainFactory.getWaterSupplyController().updateData();
	}
	
	//Method for registering a new source of weight carrying ability, input is the new weight carrying ability
	public boolean registerWeightCarry(WeightCarryingAbility oneAbility) {
		if(!this.weightCarrying.contains(oneAbility)) {
			this.weightCarrying.add(oneAbility);
			return true;
		}
		System.out.println("Bug: Weight carrying ability was already registered");
		return false;
		
	}
	
	//Method for removing a weight carry ability from the active registry, input is the old weight carrying ability
	public boolean unRegisterWeightCarry(WeightCarryingAbility oneAbility) {
		if(this.weightCarrying.contains(oneAbility)) {
			this.weightCarrying.remove(oneAbility);
			return true;
		}
		System.out.println("Bug: Cant remove, Weight carrying ability wasn't registered");
		return false;
	}

}

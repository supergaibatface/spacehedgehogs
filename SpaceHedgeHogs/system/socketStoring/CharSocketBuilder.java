package socketStoring;

import sreenElementsSockets.ScreenSimpleChar;
import sreenElementsSockets.ScrollCharSocket;

//Character Socket builder class that takes a content provider and fills a provided socket with said content
public class CharSocketBuilder extends NewBaseSocketBuilder<ScrollCharSocket> {
	private ScrollCharSocket socket;
	private ISocketContentProviding<ScreenSimpleChar> supplier;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	socket: socket assigned to this builder
	 * 	supplier: socket content provider that provides content for the socket
	 */
	public CharSocketBuilder(ScrollCharSocket socket, ISocketContentProviding<ScreenSimpleChar> supplier) {
		this.socket = socket;
		this.supplier = supplier;
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	//Getter method that returns a socket that is filled with providers content
	public ScrollCharSocket getFullSocket() {
		socket.checkIdleRegistering();
		for(ScreenSimpleChar oneChar : this.supplier.getArrayOfSupplies()) {
			this.socket.addNewElement(oneChar);
		}
		return this.socket;
	}

}

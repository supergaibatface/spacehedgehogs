package activityListeners;

import combatFieldElements.FieldCharacter;
import controllers.ActivityController;
import factories.IAutoMake;
import factories.MainFactory;
import screenElements.ScreenElement;
import sreenElementsSockets.ScreenSimpleChar;
import sreenElementsSockets.ScreenSimpleItem;

//Base class for activity listeners
public abstract class BaseActivityListener implements IAutoMake, IActivityListener {
	protected MainFactory mainFactory;
	protected ActivityController activity;

	@Override
	public void activityChange(ScreenElement activatedElement) {
		if(activatedElement == null) {
			this.deselected();
		}
		else if(this.activity.isActiveSimpleChar()) {
			this.screenSimpleCharActivated((ScreenSimpleChar) activatedElement);
		}
		else if(this.activity.isActivitySimpleItem()) {
			this.screenSimpleItemActivated((ScreenSimpleItem) activatedElement);
		}
		else if(this.activity.isActivityFieldCharacter()) {
			this.fieldCharActivated((FieldCharacter) activatedElement);
		}
		else {
			System.out.println("un expected type of activation happened");
		}
		
		
	}
	
	//method that is called when deselect happens and there is no new activated element
	protected abstract void deselected();
	
	//meethod that is called when new activated element is screenSimpleCharacter
	protected abstract void screenSimpleCharActivated(ScreenSimpleChar activatedChar);
	
	//Is called when new activated element is simpleItem
	protected abstract void screenSimpleItemActivated(ScreenSimpleItem activateditem);
	
	//Called when field character was activated
	protected abstract void fieldCharActivated(FieldCharacter activatedChar);

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}
	
	@Override
	public void bootUp() {
		this.activity = this.mainFactory.getActivityController();
	}

}

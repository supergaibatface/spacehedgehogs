package screenElements;

//Class that represents the background of a view
public class ScreenBackground extends ScreenSoloImage{
	private int height;
	private int length;

	//constructor
	public ScreenBackground() {
		super(0, 0);
	}
	
	//method for booting up an object that has been constructed
	public void bootUp() {
		super.bootUp();
		this.updateSize();
	}
	
	//Method for updating the size of the background image
	public void updateSize() {
		this.height = this.mainFactory.getScreen().getHeight();
		this.length = this.mainFactory.getScreen().getWidth();
		this.setSize(this.length, this.height);
	}

}

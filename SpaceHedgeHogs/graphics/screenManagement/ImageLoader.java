package screenManagement;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import factories.IAutoMake;
import factories.MainFactory;
import screenElements.ScreenElement;

//method for loading images into the game, currently accepts only png type images
public class ImageLoader implements IAutoMake{
	private MainFactory mainFactory;
	private String path;
	private String imgType = ".png";
	
	//constructor
	public ImageLoader() {
		
	}
	
	//setter for main factory object
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
	}
	
	//Boot up method, meant for starting up the object after creation
	public void bootUp() {
		makePath();
	}
	
	/*
	 * Method for producing a image and scaling it
	 * Inputs:
	 * 	name: string version of the picture name without the file extension
	 * 	element: ScreenElement type of item, the pciture will be scaled to its format
	 * Ouput:
	 * 	newImage: Image type picture
	 */
	public Image getAndScale(String name, ScreenElement element) {
		Image newImage = loadImage(name);
		newImage = scaleImage(newImage, element);
		return newImage;
	}
	
	/*
	 * Method for loading image into the program, can only load images from
	 * Images foulder in the main app folder
	 * Inputs:
	 * 	name: string version of the picture name without the file extension
	 * Ouput:
	 * 	newImage: Image type picture
	 */
	public Image loadImage(String fileName) {
		Image img = null;
		try {
		    img = ImageIO.read(new File(this.path + fileName + this.imgType));
		} 
		catch (IOException e) {
			System.out.println("There was img loading problem");
		}
		return img;
	}
	
	/*
	 * Method for scaling images
	 * Inputs:
	 * 	img: Image type of a picture that will be scaled
	 * 	element: ScreenElement for what the image will be used, scales to its dimensions
	 * Output:
	 * 	newImg: version of the input image scaled for the element
	 */
	public Image scaleImage(Image img, ScreenElement element) {
		Image newImg = scaleImage(img, element.getLength(), element.getHeight());
		return newImg;
	}
	
	/*
	 * Method for scaling images
	 * Inputs
	 * 	img: Imgage type of a pciture that will be scaled
	 * 	length: length value that picture will be scaled to (in pixels)
	 * 	height: height value that the picture will be scale to (in pixels)
	 */
	public Image scaleImage(Image img, int length, int height) {
		Image newImg = img.getScaledInstance(length, height, Image.SCALE_DEFAULT);
		return newImg;
	}
	
	/*
	 * Private method for making a path that will be used for loading images
	 * will make a path into the images foulder that is in the main project foulder
	 */
	private void makePath() {
		this.path = System.getProperty("user.dir");
		this.path = this.path + "\\Images\\";
	}

}

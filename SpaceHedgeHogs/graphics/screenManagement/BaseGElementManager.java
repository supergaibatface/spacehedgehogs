package screenManagement;

import java.util.ArrayList;
import java.util.List;

import factories.IAutoMake;
import factories.MainFactory;
import frameworks.IStackable;
import frameworks.TextFrame;
import screenElements.ScreenButton;
import screenElements.ScreenDataBox;
import tabInterface.TabDirectory;
import view.IGameView;

public abstract class BaseGElementManager implements IAutoMake, IGElementManager {
	protected MainFactory mainFactory;
	protected List<IScreenLayer> layers;
	protected ScreenDataBox debugMessages;
	protected boolean showDebug = true;

	@Override
	public void changeToView(IGameView newView) {
		this.removeAllElements();
		this.viewSwitch(newView);
	}
	
	//Method for removing all the elements that would be displayed on the screen
	private void removeAllElements() {
		this.mainFactory.getActivityController().makeInactive();
		this.removeEverythingOnLayers();
	}
	
	private void removeEverythingOnLayers() {
		for(IScreenLayer oneLayer : this.layers) {
			oneLayer.removeAllElements();
		}
	}
	
	public boolean removeElementFromScreen(IStackable oldElement) {
		for(IScreenLayer oneLayer : this.layers) {
			if(oneLayer.removeElement(oldElement)) {
				return true;
			}
		}
		return false;
	}
	
	//branch method for deciding what type of view to load in, uses view type for that
	protected void viewSwitch(IGameView newView) {
		newView.goingOnScreenAlert();
		viewSwitchWithLayers(newView);
	}
	
	protected void viewSwitchWithLayers(IGameView newView) {
		this.addBackground(newView);
		this.addButtons(newView);
		this.addDataBoxes(newView);
		this.addScreenScrollPanels(newView);
		this.addTabDirectory(newView);
	}
	
	protected void addBackground(IGameView newView) {
		if(newView.hasBackground()) {
			this.giveBackgroundDestination().addElementToLayer(newView.getBackground());
		}
	}
	
	protected abstract IScreenLayer giveBackgroundDestination();
	
	protected void addButtons(IGameView newView) {
		for (ScreenButton tempButton : newView.getButtons()) {
			this.giveButtonDestination().addElementToLayer(tempButton);
		}
	}
	
	protected abstract IScreenLayer giveButtonDestination();
	
	protected void addDataBoxes(IGameView newView) {
		for (ScreenDataBox tempBox : newView.getDataBoxes()) {
			this.giveDataBoxDestination().addElementToLayer(tempBox);
		}
	}
	
	protected abstract IScreenLayer giveDataBoxDestination();
	
	protected void addScreenScrollPanels(IGameView newView) {
		if(newView.hasScrollableMainScreen()) {
			this.giveScrollPanelDestination().addElementToLayer(newView.getScrollableMainScreen());
		}
	}
	
	protected abstract IScreenLayer giveScrollPanelDestination();
	
	private void addTabDirectory(IGameView newView) {
		TabDirectory tabDirectory = newView.getMainTabDirectory();
		if(tabDirectory != null) {
			this.giveTabDirectoryDestination().addElementToLayer(tabDirectory);
		}
	}
	
	protected abstract IScreenLayer giveTabDirectoryDestination();
	
	public void addQuickPopUp(IStackable onePopUp) {
		this.givePopUpDestination().addElementToTop(onePopUp);
	}
	
	public void addPopUp(IStackable onePopUp) {
		this.givePopUpDestination().addElementToLayer(onePopUp);
	}
	
	protected abstract IScreenLayer givePopUpDestination();

	@Override
	public List<IStackable> getAllElementsFromLayers() {
		List<IStackable> fullList = new ArrayList<IStackable>();
		for(IScreenLayer oneLayer : layers) {
			this.getElementsFromALayer(oneLayer, fullList);
		}
		this.checkAddDebug(fullList);
		return fullList;
	}
	
	protected void getElementsFromALayer(IScreenLayer layer, List<IStackable> fillList) {
		layer.retrieveElementsIntoInputList(fillList);
	}
	
	protected void checkAddDebug(List<IStackable> fillList) {
		if(this.showDebug) {
			fillList.add(debugMessages);
		}
	}
	
	public boolean hasIndepenendentElementOnScreen(IStackable checkedElement) {
		for(IScreenLayer oneLayer : layers) {
			if(oneLayer.hasElement(checkedElement)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.layers = new ArrayList<IScreenLayer>();
		this.addLayersToList(this.layers);
		makeDebugMessage();
	}
	
	protected abstract void addLayersToList(List<IScreenLayer> layerList);
	
	protected void makeDebugMessage() {
		this.debugMessages = this.mainFactory.getElemFactory().getScreenDataBox(0, 0, "20Opacity");
		this.debugMessages.fillTextOneLine("Source: "+this.giveManagerName());
		this.debugMessages.autoSize();
	}
	
	protected abstract String giveManagerName();

}

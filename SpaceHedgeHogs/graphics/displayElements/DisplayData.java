package displayElements;

import generalGraphics.ClickBox;
import screenElements.ScreenElement;

/*
 * Add on class to screenElements that provides data for drawing half visible elements
 */
public class DisplayData {
	private boolean usesData;
	private int dstx1;
	private int dstx2;
	private int dsty1;
	private int dsty2;
	private int srcx1;
	private int srcx2;
	private int srcy1;
	private int srcy2;
	private final ScreenElement masterElement;
	
	/*
	 * Constructor
	 * Input:
	 * 	master: screenElement this data set applies to
	 */
	public DisplayData(ScreenElement master) {
		this.masterElement = master;
		this.usesData = false;
		this.dstx1 = 0;
		this.dstx2 = 0;
		this.dsty1 = 0;
		this.dsty2 = 0;
		this.srcx1 = 0;
		this.srcx2 = 0;
		this.srcy1 = 0;
		this.srcy2 = 0;
	}
	
	/*
	 * Method for calculating visibility data through overlap ClickBox
	 * Inputs:
	 * 	overlapBox: ClickBox that represents the visible area on master elements, is located in the same
	 * 		x-y coordinate system as the master element
	 */
	public void fillDataWithOverlap(ClickBox overlapBox) {
		this.calculateDestinationCoords(overlapBox);
		this.calculateSourceCoords(overlapBox, this.masterElement.getClickBox());
	}
	
	/*
	 * Method that bind together destination coordinate calculations
	 */
	private void calculateDestinationCoords(ClickBox overlapBox) {
		this.calculateDestinationX1(overlapBox);
		this.calculateDestinationX2(overlapBox);
		this.calculateDestinationY1(overlapBox);
		this.calculateDestinationY2(overlapBox);
	}
	
	/*
	 * Calculates destination x1 coordinate (smallest x point)
	 */
	private void calculateDestinationX1(ClickBox overlapBox) {
		this.dstx1 = overlapBox.getLeftX();
	}
	
	/*
	 * Calculates destination x2 coordinate (largest x point)
	 */
	private void calculateDestinationX2(ClickBox overlapBox) {
		this.dstx2 = overlapBox.getRightX();
	}
	
	/*
	 * calculates destination y1 coordinate (smallest y point)
	 */
	private void calculateDestinationY1(ClickBox overlapBox) {
		this.dsty1 = overlapBox.getTopY();
	}
	
	/*
	 * Calculates destination y2 coordinate (largest y point)
	 */
	private void calculateDestinationY2(ClickBox overlapBox) {
		this.dsty2 = overlapBox.getBottomY();
	}
	
	/*
	 * Method that bind together source coordinate calculations
	 */
	private void calculateSourceCoords(ClickBox overlap, ClickBox masterBox) {
		this.calculateSourceX1(overlap, masterBox);
		this.calculateSourceX2(overlap, masterBox);
		this.calculateSourceY1(overlap, masterBox);
		this.calculateSourceY2(overlap, masterBox);
	}
	
	/*
	 * Calculates source x1
	 */
	private void calculateSourceX1(ClickBox overlap, ClickBox masterBox) {
		this.srcx1 = overlap.getLeftX() - masterBox.getLeftX();
	}
	
	/*
	 * Calculates source x2
	 */
	private void calculateSourceX2(ClickBox overlap, ClickBox masterBox) {
		this.srcx2 = overlap.getRightX() - masterBox.getLeftX();
	}
	
	/*
	 * Calculates source y1
	 */
	private void calculateSourceY1(ClickBox overlap, ClickBox masterBox) {
		this.srcy1 = overlap.getTopY() - masterBox.getTopY();
	}
	
	/*
	 * Calculates source y2
	 */
	private void calculateSourceY2(ClickBox overlap, ClickBox masterBox) {
		this.srcy2 = overlap.getBottomY() - masterBox.getTopY();
	}

	public int getDstx1() {
		return dstx1;
	}

	public int getDstx2() {
		return dstx2;
	}

	public int getDsty1() {
		return dsty1;
	}

	public int getDsty2() {
		return dsty2;
	}

	public int getSrcx1() {
		return srcx1;
	}

	public int getSrcx2() {
		return srcx2;
	}

	public int getSrcy1() {
		return srcy1;
	}

	public int getSrcy2() {
		return srcy2;
	}

	//returns master element of this displaydata
	public ScreenElement getMasterElement() {
		return masterElement;
	}

	/*
	 * Shows if master elements needs to be drawn with displaydata
	 */
	public boolean isUsesData() {
		return usesData;
	}

	//sets value for if this display data is atm in use or not
	public void setUsesData(boolean usesData) {
		this.usesData = usesData;
	}
	
	/*
	 * Makes a clickbox based on DisplayDatas described visible area
	 */
	public ClickBox getVisibileArea() {
		ClickBox visibleArea = new ClickBox(this.srcx1, this.srcy1, this.srcx2 - this.srcx1, this.srcy2 - this.srcy1);
		return visibleArea;
	}
	
	/*
	 * Calculates if saved data represents a visible element
	 */
	public boolean isVisible() {
		if(this.dstx1 == this.dstx2) {
			return false;
		}
		if(this.dsty1 == this.dsty2) {
			return false;
		}
		return true;
	}
	

	/*
	 * Debug method for printing out the values of this set of displayData
	 * Input:
	 * 	distinctionName: string that will be written out when the text is pushed to the console
	 */
	public void printOutData(String distictionName) {
		System.out.println("Starting with " + distictionName);
		System.out.println("saved values: ");
		System.out.println("srcx1: "+ this.srcx1 + " to srcx2: " + this.srcx2);
		System.out.println("srcy1: "+ this.srcy1 + " to srcy2: " + this.srcy2);
		System.out.println("dstx1: "+ this.dstx1 + " to dstx2: " + this.dstx2);
		System.out.println("dsty1: "+ this.dsty1 + " to dsty2: " + this.dsty2);
		System.out.println("from wrapper values: ");
		System.out.println("dstx1: "+ masterElement.getOffWrapperPointX(this.dstx1) + " to dstx2: " + masterElement.getOffWrapperPointX(this.dstx2));
		System.out.println("dsty1: "+ masterElement.getOffWrapperPointY(this.dsty1) + " to dsty2: " + masterElement.getOffWrapperPointY(this.dsty2));
	}

}

package screenManagement;

import java.util.List;

import frameworks.IStackable;

public class PopupGraphicLayer extends BaseScreenLayer {
	
	@Override
	public List<IStackable> retrieveElementsIntoInputList(List<IStackable> fillList) {
		if(!elements.isEmpty()) {
			fillList.add(elements.get(0));
		}
		//fillList.addAll(elements);
		return fillList;
	}

}

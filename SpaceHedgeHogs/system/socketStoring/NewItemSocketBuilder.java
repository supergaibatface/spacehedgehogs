package socketStoring;

import sreenElementsSockets.ScreenSimpleItem;
import sreenElementsSockets.ScrollItemSocket;

/*
 * Socket builder for item sockets
 */
public class NewItemSocketBuilder extends NewBaseSocketBuilder<ScrollItemSocket> {
	private ScrollItemSocket socket;
	private ISocketContentProviding<ScreenSimpleItem> supplier;
	private boolean updateValue;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	socket: socket that will be filled with content
	 * 	supplier: socket content provider that will provide with the content for input socket
	 */
	public NewItemSocketBuilder(ScrollItemSocket socket, ISocketContentProviding<ScreenSimpleItem> supplier) {
		this.socket = socket;
		this.supplier = supplier;
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ScrollItemSocket getFullSocket() {
		for(ScreenSimpleItem oneItem : this.supplier.getArrayOfSupplies()) {
			this.socket.addNewElement(oneItem);
		}
		if(this.updateValue) {
			this.mainFactory.getInventory().addInventoryChangeEventListener(this.mainFactory.getElemFactory().getItemSocketUpdateListener(socket));
		}
		return this.socket;
	}
	
	//setter for the updateValue flag
	public void setUpdateValue(boolean updateValue) {
		this.updateValue = updateValue;
	}

}

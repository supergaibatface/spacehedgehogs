package frameworks;

import java.util.ArrayList;
import java.util.List;

import factories.IAutoMake;
import factories.MainFactory;
import generalGraphics.ClickBox;
import screenElements.ScreenElement;
import screenElements.ScreenText;
import screenManagement.GraphicalLengthAssigner;

/*
 * Base frame that all other frames will inherit
 */
public abstract class IndependentBaseFrame implements IAutoMake, IStackable {
	protected MainFactory mainFactory;
	protected int x;
	protected int y;
	protected IStackable wrapper;
	protected GraphicalLengthAssigner lenAss;
	protected boolean forceNonScroll;
	
	/*
	 * Checks if frame has a wrapper
	 */
	public boolean hasWrapper() {
		return this.wrapper != null;
	}
	
	/*
	 * Recursive method for getting the X coordinates of base coordinate system
	 */
	@Override
	public int getOffWrapperX() {
		return this.getOffWrapperPointX(this.getX());
	}
	
	//Wrapper system for getting absolute X coordinates on the screen
	public int getOffWrapperPointX(int xPoint) {
		if(wrapper != null) {
			return forceScrollCheck(wrapper.getOffWrapperX() + wrapper.getXDataStart() + xPoint, wrapper.getXScroll());
		}
		else {
			return xPoint;
		}
	}

	/*
	 * Recursive method for getting the Y coordinates of base coordinate system
	 */
	@Override
	public int getOffWrapperY() {
		return this.getOffWrapperPointY(this.getY());
	}
	
	//Wrapper system for getting absolute X coordinates on the screen
	public int getOffWrapperPointY(int yPoint) {
		if(wrapper != null) {
			return forceScrollCheck(wrapper.getOffWrapperY() + wrapper.getYDataStart() + yPoint, wrapper.getYScroll());
		}
		else {
			return yPoint;
		}
	}
	
	/*
	 * Method that gives getOffWrapper result depentind on if the element has forced nonscroll
	 * if element is forced non scroll it wont have scroll values applied to its location
	 * Inputs:
	 * 	coordinate: a fully calculated coordinate value of the element (can be either dimension)
	 * 	wrapperScrollValue: master elements scrolled value
	 * returns:
	 * 	coordinate value depending on if the element is forced non scroll or not
	 */
	private int forceScrollCheck(int coordinate, int wrapperScrollValue) {
		if(!this.forceNonScroll) {
			return coordinate - wrapperScrollValue;
		}
		else {
			return coordinate;
		}
	}
	
	/*
	 * X coordinate of where first element will be placed on this frame
	 */
	@Override
	public int getXDataStart() {
		return 0;
	}

	/*
	 * Y coordinate of where first element will be placed on this frame
	 */
	@Override
	public int getYDataStart() {
		return 0;
	}
	
	//Method for getting a X coordinate of a click on this elements coordinate system
	public int getXOnElem(int clickX) {
		return clickX - this.x;
	}
		
	//Method for Getting a Y coordinate of a click on this elements coordinate system
	public int getYOnElem(int clickY) {
		return clickY - this.y;
	}
	
	/*
	 * Method for setting the X coordinate of this frames location
	 */
	public void setX(int x) {
		this.x = x;
	}
	
	/*
	 * Method for setting the Y coordinate of this frames location
	 */
	public void setY(int y) {
		this.y = y;
	}
	
	//getter for x coordinate
	public int getX() {
		return this.x;
	}
	
	//getter for y coordinate
	public int getY() {
		return this.y;
	}
	
	/*
	 * Method for setting both coordinates of the frames location
	 */
	public void setLocation(int x, int y) {
		this.setX(x);
		this.setY(y);
	}

	//Setter for main factory
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	//Boot up method
	@Override
	public void bootUp() {
		this.lenAss = mainFactory.getLengthAssigner();
		this.forceNonScroll = false;
	}
	
	//Method for changing the items wrapper element
	public void setWrapper(IStackable wrapper) {
		this.wrapper = wrapper;
	}
	
	//Getter for this frames full height
	protected abstract int getFullHeight();
	
	//Getter for this frames full length
	protected abstract int getFullLength();
	
	/*
	 * getter for clickbox based on this frame
	 */
	public ClickBox getClickBox() {
		return this.makeClickBox();
	}
	
	/*
	 * Makes a clickbox based off this frame
	 */
	private ClickBox makeClickBox() {
		return new ClickBox(this.getX(), this.getY(), this.getFullLength(), this.getFullHeight());
	}
	
	/*
	 * Method for dealing with overlap calculations
	 * Input:
	 * 	visibleBox: ClickBox that represents the visible area on the same coordination system as this element is on
	 */
	public void overLapMaster(ClickBox visibleBox) {
		if(!visibleBox.sameSquare(this.getClickBox())) {
			incompleteOverlapOnChildren(visibleBox.localiseClickBox(this));
		}
		else {
			fullOverlapOnChildren(visibleBox.localiseClickBox(this));
		}
	}
	
	/*
	 * Method called when not every part of this element is actually visible
	 * Input:
	 * 	localVisibleBox: visible area localised to the coordination set ON THIS ELEMENT
	 */
	protected void incompleteOverlapOnChildren(ClickBox localVisibleBox) {
		callOverlapOnChildren(localVisibleBox);
		callOverlapOnText(localVisibleBox);
	}
	
	/*
	 * Method called when this element is fully visible
	 * Input:
	 * 	localVisibleBox: visible area localised to the coordination set ON THIS ELEMENT
	 */
	protected void fullOverlapOnChildren(ClickBox localVisibleBox) {
		callOverlapOnChildren(localVisibleBox);
		callFullOverLapOnText();
	}
	
	/*
	 * Method that triggers overlap calculations on the child elements of this framework
	 * Input:
	 * 	localVisibleBox: visible area localised to the coordination set ON THIS ELEMENT
	 */
	protected void callOverlapOnChildren(ClickBox localVisibleBox) {
		ClickBox overLap;
		for(IStackable oneElement : this.getOnTopElements()) {
			overLap = localVisibleBox.getOverlap(oneElement.getClickBox());
			oneElement.overLapMaster(overLap);
		}
	}
	
	/*
	 * Method for calculating the visiblity of text on this element
	 * Input:
	 * 	localVisibleBox: visible area localised to the coordination set ON THIS ELEMENT
	 */
	protected void callOverlapOnText(ClickBox localVisibleBox) {
		for(ScreenText oneText : this.getOnTopTexts()) {
			oneText.checkVisibility(localVisibleBox);
		}
	}
	
	/*
	 * Called on child texts when this element is fully visible itself
	 */
	protected void callFullOverLapOnText() {
		for(ScreenText oneText : this.getOnTopTexts()) {
			oneText.setVisibile(true);
		}
	}
	
	/*
	 * Way to call this elements DisplayBox that still considers wrapper visiblity areas
	 */
	public ClickBox getDisplayBox() {
		if(this.wrapper == null) {
			return new ClickBox(this.getXScroll() + this.getXDataStart(), this.getYScroll() + this.getYDataStart(), this.getLength(), this.getHeight());
		}
		else {
			return this.wrapper.getDisplayBox().getOverlap(this.getClickBox()).localiseClickBox(this);
		}
	}
	
	/*
	 * Self click search for finding elements that might have been clicked on
	 * Inputs:
	 * 	clickX: x coordinate of the mouse click
	 * 	clickY: y coordinate of the mouse click
	 * 	searchClasses: list of classes that are allowed to be click on
	 * 	fillList: list where the clicked elements will be added to
	 */
	public List<ScreenElement> selfSearch(int clickX, int clickY, List<Class> searchClasses, List<ScreenElement> fillList) {
		List<IStackable> childList = new ArrayList<IStackable>();
		this.fillTopElementList(childList);
		for(IStackable oneElement : childList) {
			oneElement.selfSearch(this.getXOnElem(clickX), this.getYOnElem(clickY), searchClasses, fillList);
		}
		
		return fillList;
	}
	
	//Getter for possible visible clickable elements on this frame
	//protected abstract void getPossibleClickedElements(ArrayList<IStackable> fillList);
	
	@Override
	public ArrayList<IStackable> getOnTopElements() {
		ArrayList<IStackable> topElements = new ArrayList<IStackable>();
		fillTopElementList(topElements);
		return topElements;
	}
	
	/*
	 * abstract method that adds all stackable elements ontop of this
	 * element to the input list, is used for graphical displaying
	 */
	public abstract void fillTopElementList(List<IStackable> list);
	
	
	@Override
	public int getXScroll() {
		return 0;
	}

	@Override
	public int getYScroll() {
		return 0;
	}
	
	public boolean isScreenElement() {
		return false;
	}
	
	@Override
	public int getLength() {
		return this.getFullLength();
	}

	@Override
	public int getHeight() {
		return this.getFullHeight();
	}
	
	//Getter for texts on top of this element
	public List<ScreenText> getOnTopTexts() {
		List<ScreenText> topTexts = new ArrayList<ScreenText>();
		fillTopTextList(topTexts);
		return topTexts;
	}
	
	//Abstract method for filling the list of top text elements, input is the list to fill
	public abstract void fillTopTextList(List<ScreenText> list);
	
	public void printOutStackingHierarchy() {
		System.out.println(this.getClass());
		if(this.hasWrapper()) {
			this.wrapper.printOutStackingHierarchy();
		}
	}
	
	public IStackable getWrapper() {
		return this.wrapper;
	}
	
	public void safeDelete() {
		
	}
	
	public void goingOnScreenAlert() {
		this.informChildrenGoingOnScreen();
		this.goingOnScreenScript();
	}
	
	private void informChildrenGoingOnScreen() {
		for(IStackable oneElement : this.getOnTopElements()) {
			oneElement.goingOnScreenAlert();
		}
	}
	
	protected abstract void goingOnScreenScript();
	
	public void goingOffScreenAlert() {
		this.informChildrenGoingOffScreen();
		this.goingOffScreenScript();
	}
	
	private void informChildrenGoingOffScreen() {
		for(IStackable oneElement : this.getOnTopElements()) {
			oneElement.goingOffScreenAlert();
		}
	}
	
	protected abstract void goingOffScreenScript();

	
	public boolean isForceNonScroll() {
		return forceNonScroll;
	}

	//Set method for non scroll value, if true, element will not have wrapper scroll value applied to its location
	public void setForceNonScroll(boolean forceNonScroll) {
		this.forceNonScroll = forceNonScroll;
	}

}

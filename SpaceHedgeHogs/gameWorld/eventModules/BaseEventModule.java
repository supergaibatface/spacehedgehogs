package eventModules;

import java.util.ArrayList;

import events.GameEvent;
import factories.IAutoMake;
import factories.MainFactory;
import foodBase.FoodItemBase;

//Base class for event modules that combine same type of events that can be loaded into the game
public abstract class BaseEventModule implements IEventModule, IAutoMake{
	protected MainFactory mainFactory;
	protected ArrayList<GameEvent> events;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.events = new ArrayList<GameEvent>();
		
	}

	@Override
	public ArrayList<GameEvent> getEvents() {
		if(this.events.isEmpty()) {
			this.makeEvents();
		}
		return (ArrayList<GameEvent>) this.events.clone();
	}
	

	/*
	 * gets a food item object with the specified amount, used as rewards for event choices
	 * Inputs:
	 * 	ItemName: name of the item
	 * 	amount: the amount of given item
	 * Output:
	 * 	FoodItem of the given name and amount
	 */
	/*protected FoodItemBase getFoodItemGain(TradeItemName itemName, int amount) {
		FoodItem newItem = this.mainFactory.getElemFactory().getFoodItem(itemName);
		newItem.addAmount(amount);
		return newItem;
	}*/
	
	protected FoodItemBase getFoodItemWithAmountValue(FoodItemBase zeroAmountItem, int newAmount) {
		zeroAmountItem.addAmount(newAmount);
		return zeroAmountItem;
	}
 	
	/*
	 * gets a food item object with the specified amount and specified expiration time, used as rewards for event choices
	 * Inputs:
	 * 	ItemName: name of the item
	 * 	amount: the amount of given item
	 * 	expiration: in how many turns does the food expire
	 * Output:
	 * 	FoodItem of the given name and amount
	 */
	/*protected FoodItem getFoodItemGain(TradeItemName itemName, int amount, int expiration) {
		FoodItem newItem = this.mainFactory.getElemFactory().getFoodItem(itemName);
		newItem.addAmount(amount, expiration);
		return newItem;
	}*/
	
	//Method for making events for the module
	protected abstract void makeEvents();
	

}

package commandActionRightButton;

import commandAction.CABaseClass;
import screenElements.ScreenButton;
import sreenElementsSockets.ScreenSimpleChar;

//Right mouse button command action for opening extra commands on screenSimpleChar
public class CARScreenCharacter extends CABaseClass<ScreenSimpleChar>{

	@Override
	public boolean inputIsCorrect() {
		return this.standardClickCheck();
	}

	@Override
	public void actionImplement() {
		ScreenSimpleChar character = (ScreenSimpleChar) conditions.getClickedItem(this.hasItemTypeClicked());
		//System.out.println("clicked on char: "+character.getMaster().getName());
		this.mainFactory.getRightMouseInfoController().showBoxOnElement(this.mainFactory.getMouseConditionSet().getClickX(), this.mainFactory.getMouseConditionSet().getClickY(), character);
		
	}

	@Override
	public Class<ScreenSimpleChar> setClickTargetClass() {
		return ScreenSimpleChar.class;
	}

	@Override
	protected int giveMouseButtonNr() {
		return this.giveRightMouseButtonValue();
	}

}

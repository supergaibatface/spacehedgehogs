package baggageCarryItems;

import baggageCarryItemBase.BaggageAnimalItemBase;
import itemBase.IGameItem;
import itemBase.TradeItemName;

public class HorseCarryItem extends BaggageAnimalItemBase {

	public HorseCarryItem() {
		super(TradeItemName.Horse);
	}

	@Override
	protected IGameItem giveBaseClass() {
		return this.mainFactory.setUpNewNonPerservativeValue(new HorseCarryItem());
	}

	@Override
	protected String bootGiveItemImageName() {
		return this.bootGivePlaceholderImage();
	}

	@Override
	protected int bootGiveOneUnitWeight() {
		return 1;
	}

}

package screenManagement;

import factories.IAutoMake;
import factories.MainFactory;

//Method for all the graphical lengths that will be used to make graphical elemenets
public class GraphicalLengthAssigner implements IAutoMake{
	private MainFactory mainFactory;
	//master nr, in the future the other numbers will come from this
	private int masterNr;
	//side length of one characters avatar
	private int charAvatarLength;
	//frame size
	private int extraRoom;
	//one unit length of a socket avatar
	private int socketAvatarLength;
	//length for a button
	private int buttonLength;
	//height for a button
	private int buttonHeight;
	//labels y coordinate difference
	private int labelYDif;
	//labels x coordinate difference
	private int labelXDif;
	//button size for a scroll button
	private int ScrollButtonSize;
	//height for the char info box
	private int charInfoHeight;
	//length for the char info box
	private int charInfoLength;
	//size for text
	private int oneTextSize;
	//one event choice button height
	private int oneChoiceButtonHeight;
	//one event choice button length
	private int oneChoiceButtonLength;
	//height of the entire game screen
	private int screenHeight;
	//lenght for the entire game screen
	private int screenLength;
	//title room height for a socket
	private int titleHeight;
	//length, height of one square
	private int fieldSquareSize;
	//size for the space between two squares
	private int fieldSquareBetween;
	private int buttonModSize;
	
	//Constructor, input: masterNr, this is one number that all other graphical lengths are relative to.
	public GraphicalLengthAssigner() {
		
	}
	
	//setter for main factory object
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
	}
	
	//Boot up method, meant for starting up the object after creation
	public void bootUp() {
		this.masterNr = this.mainFactory.getMasterLength();
		this.charAvatarLength = masterNr;
		this.extraRoom = masterNr / 20;
		this.socketAvatarLength = masterNr + extraRoom;
		this.buttonLength = (masterNr * 2) / 3;
		this.buttonHeight = (buttonLength * 2) / 3;
		this.labelXDif = 20;
		this.labelYDif = 20;
		this.ScrollButtonSize = 50;
		this.charInfoHeight = 100;
		this.charInfoLength = 200;
		this.oneTextSize = 20;
		this.oneChoiceButtonHeight = 50;
		this.oneChoiceButtonLength = 200;
		this.screenHeight = 1000;
		this.screenLength = 1900;
		this.titleHeight = 30;
		this.fieldSquareSize = 80;
		this.fieldSquareBetween = 20;
		this.buttonModSize = 20;
		
		
	}
	
	//getter for a characters avatar length
	public int getCharAvatarLength() {
		return this.charAvatarLength;
	}
	
	/*
	 * getter for a sockets avatar length, input is the size of a socket, 
	 * that determines how many chars can be put into the socket
	 */
	public int getSocketLengthForSize(int size) {
		return (this.socketAvatarLength*size) + this.extraRoom;
	}
	
	//getter for extra room, this is the little space between a sockets avatars side to the chars avatar
	public int getExtraRoomLength() {
		return extraRoom;
	}
	
	//getter for default button height
	public int getButtonHeight() {
		return this.buttonHeight;
	}
	
	//getter for default button length
	public int getButtonLength() {
		return this.buttonLength;
	}
	
	//getter for label x coordinate offset compared to the corner of the element it goes with
	public int getLabelXDif() {
		return this.labelXDif;
	}
	
	//getter for label y coordinate offset compared to the corner of the element it goes with
	public int getLabelYDif() {
		return this.labelYDif;
	}

	//getter for scroll button size
	public int getScrollButtonSize() {
		return ScrollButtonSize;
	}

	//getter for char info height
	public int getCharInfoHeight() {
		return charInfoHeight;
	}

	//getter for char into length
	public int getCharInfoLength() {
		return charInfoLength;
	}

	//getter for one text size
	public int getOneTextSize() {
		return oneTextSize;
	}

	//getter for one choice button height
	public int getOneChoiceButtonHeight() {
		return oneChoiceButtonHeight;
	}

	//getter for one choice button length
	public int getOneChoiceButtonLength() {
		return oneChoiceButtonLength;
	}

	//getter for screen first height
	public int getScreenHeight() {
		return screenHeight;
	}

	//getter for screen first length
	public int getScreenLength() {
		return screenLength;
	}

	//getter for title height
	public int getTitleHeight() {
		return titleHeight;
	}

	//getter for field square size
	public int getFieldSquareSize() {
		return fieldSquareSize;
	}

	//getter for field square space size
	public int getFieldSquareBetween() {
		return fieldSquareBetween;
	}
	
	public int getButtonModSize() {
		return this.buttonModSize;
	}
	
}

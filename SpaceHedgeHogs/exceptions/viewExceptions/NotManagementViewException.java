package viewExceptions;

import view.ViewName;

public class NotManagementViewException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -528019347403169136L;

	public NotManagementViewException(ViewName searchedView) {
		super("View named: "+searchedView+" was not matched as management view");
	}

}

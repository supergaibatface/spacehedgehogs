package sreenElementsSockets;

import java.util.ArrayList;
import java.util.List;

import frameworks.IStackable;
import frameworks.SimpleCharacterFrame;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import generalGraphics.ButtonLookType;
import other.StaticFunctions;
import propertyBars.PropertyBarName;
import screenElements.ScreenBar;
import screenElements.ScreenButton;
import screenElements.ScreenElement;
import screenElements.ScreenPopUp;
import screenElements.ScreenText;

//Class for represeing a character on the screen
public class ScreenSimpleChar extends ScreenElement {
	private ScreenText label;
	private IGameCharacter master;
	private ScreenBar staminaBar;

	//constructor
	public ScreenSimpleChar(IGameCharacter master) {
		super(0, 0);
		this.master = master;
		this.master.setSocketAvatar(this);
	}
	
	public void bootUp() {
		super.bootUp();
		this.makeLabel();
		this.makeSize();
		this.makeStaminaBar();
		this.makeExtraCommands();
	}
	
	//Makes a label for this character
	private void makeLabel() {
		this.label = mainFactory.getElemFactory().getText(master.getName());
		this.label.setCoordinates(0, this.lenAss.getOneTextSize());
		this.label.setWrapper(this);
	}
	
	//Sets size to this element
	void makeSize() {
		this.setSize(this.lenAss.getCharAvatarLength(), this.lenAss.getCharAvatarLength());
	}
	
	//makes a stamin bar that will be displayed on the element
	private void makeStaminaBar() {
		this.staminaBar = master.getPropertyBar(PropertyBarName.Stamina).generateVisualBar(this.lenAss.getCharAvatarLength()-this.getXDataStart()*2, this.lenAss.getCharAvatarLength() / 6);
		this.staminaBar.setWrapper(this);
		this.staminaBar.setCoordinates(0, lenAss.getCharAvatarLength() / 8 * 6);
	}
	
	//makes extra commands of this element
	private void makeExtraCommands() {
		this.updateExtraCommands();
	}
	
	private void updateExtraCommands() {
		this.deleteExtraCommands();
		this.addFullListOfExtraCommands();
		
	}
	
	private void addFullListOfExtraCommands() {
		this.addExtraCommand(makeInfoButton());
		this.dealWithDialogueButton();
	}
	
	//makes a info button that will open info page for this element
	private ScreenButton makeInfoButton() {
		ScreenButton infoButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.POPUPSELECT, "Character Info");
		infoButton.addButtonEventListener(this.mainFactory.getElemFactory().getOpenElementOnTopLayerListener(this.master.getInfoPage()));
		return infoButton;
	}
	
	private void dealWithDialogueButton() {
		//System.out.println("Dealing with dialogue button, for char: "+this.master.getName()+", who dialogue element value is: "+this.master.hasDialogueElement());
		if(this.master.hasDialogueElement()) {
			this.addExtraCommand(this.makeDialogueButton());
		}
	}
	
	private ScreenButton makeDialogueButton() {
		ScreenButton dialogueButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.POPUPSELECT, "Talk to "+this.master.getName());
		dialogueButton.addButtonEventListener(this.mainFactory.getElemFactory().getStartDialogueListener(this.master.getDialogueCharacter()));
		dialogueButton.addActionLimiter(this.mainFactory.getElemFactory().getDialogueElementLimiter(this.master.getDialogueCharacter()));
		return dialogueButton;
	}
	
	//getter for master character
	public IGameCharacter getMaster() {
		return this.master;
	}
	
	/*
	 * Getter for this elements wrapper as character frame
	 */
	public SimpleCharacterFrame getWrapperAsCharFrame() {
		if(this.wrapper.getClass().equals(SimpleCharacterFrame.class)) {
			return (SimpleCharacterFrame) this.wrapper;
		}
		return null;
	}

	@Override
	public void fillTopElementList(List<IStackable> list) {
		list.add(this.staminaBar);
	}

	@Override
	public void fillTopTextList(List<ScreenText> list) {
		list.add(label);
		
	}
	
	//Method that will remove this element from its current socket
	public boolean removeThisElementFromCurrentLocation() {
		if(!this.checkForCharFrameAsWrapper()) {
			return false;
		}
		ScrollCharSocket socket = this.getWrapperAsCharFrame().getWrapperAsScrollSocket();
		if(socket == null) {
			return false;
		}
		socket.removeElement(this);
		this.wrapper = null;
		return true;
	}
	
	//Checks if this element is currently stored in a char framework
	private boolean checkForCharFrameAsWrapper() {
		if(this.wrapper == null) {
			return false;
		}
		if(!StaticFunctions.isElementOfClass(this.wrapper, SimpleCharacterFrame.class)) {
			return false;
		}
		return true;
	}

	
	public void safeDelete() {
		this.safeFrameworkRemove();
	}
	
	//method that extends safe delete to safely remove it from locations
	private void safeFrameworkRemove() {
		this.removeThisElementFromCurrentLocation();
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOffScreenScript() {
		// TODO Auto-generated method stub
		
	}

}

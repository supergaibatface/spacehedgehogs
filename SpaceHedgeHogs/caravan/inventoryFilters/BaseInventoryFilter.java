package inventoryFilters;

import java.util.ArrayList;
import java.util.List;

import factories.IAutoMake;
import factories.MainFactory;
import itemBase.IGameItem;
import sreenElementsSockets.ScreenSimpleItem;
import sreenElementsSockets.ScrollItemSocket;

//Base class for all inventory filters
public abstract class BaseInventoryFilter implements IAutoMake, IInventoryFilter {
	protected MainFactory mainFactory;
	
	//getter for all the new items that are not yet listed in the socket, input is the socket
	public List<IGameItem> getNewItems(ScrollItemSocket socket) {
		List<IGameItem> resultList = this.getFilteredItems();
		resultList.removeAll(this.extractMasterItemsFromSocket(socket));
		return resultList;
	}
	
	//Method that returns a list of IGameItem type of master items that are in a socket, input is the socket
	private List<IGameItem> extractMasterItemsFromSocket(ScrollItemSocket socket) {
		List<IGameItem> socketItems = new ArrayList<IGameItem>();
		for(ScreenSimpleItem oneItem : socket.getElements()) {
			socketItems.add(oneItem.getMaster());
		}
		return socketItems;
	}


	@Override
	public abstract List<IGameItem> getFilteredItems();

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	public boolean itemPassesFilter(IGameItem checkItem) {
		return this.itemFilterScript(checkItem);
	}
	
	protected abstract boolean itemFilterScript(IGameItem checkItem);
	
}

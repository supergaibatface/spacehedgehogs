package propertyBars;

//Represents possible names for property bars
public enum PropertyBarName {
	Stamina("50Yellow"), 
	Health("100Lime"), 
	Movement("100Lime"), 
	Attacks("100Lime"), 
	WaterSupply("100Lime");
	
	private String barVisual;
	
	//constructor, input is the visual look of the bars changing part
	private PropertyBarName(String barVisual) {
		this.barVisual = barVisual;
	}
	
	//returns the visual parts image name
	public String getBarVisual() {
		return this.barVisual;
	}

}

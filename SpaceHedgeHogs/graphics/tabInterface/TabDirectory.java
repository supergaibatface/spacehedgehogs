package tabInterface;

import java.util.ArrayList;
import java.util.List;

import frameworks.ButtonFrame;
import frameworks.IStackable;
import generalGraphics.ButtonLookType;
import screenElements.ScreenButton;
import screenElements.ScreenElement;
import screenElements.ScreenText;
import sreenElementsSockets.BaseScrollSocket;
import sreenElementsSockets.ScreenSimpleChar;
import sreenElementsSockets.ScreenSimpleItem;
import sreenElementsSockets.ScrollCharSocket;

//Directory that acts as a hub for tabwindows, only one tabwindow can be displayed on it at once
public class TabDirectory extends ScreenElement {
	private ArrayList<TabWindow> tabs;
	private TabWindow activeTab;
	private ButtonFrame tabSelection;

	//constructor
	public TabDirectory(int x, int y) {
		super(x, y);
		// TODO Auto-generated constructor stub
	}
	
	//Getter for all tabs saved to this directory
	public ArrayList<TabWindow> getAllTabs() {
		return this.tabs;
	}
	
	//Method for adding a new tab to this directory
	public void addNewTab(TabWindow newTab) {
		this.tabs.add(newTab);
		newTab.setWrapper(this);
		this.tabSelection.addElement(makeNewButtonForSelectingTab(newTab));
		if(this.tabs.size()== 1) {
			setActiveTab(this.tabs.get(0));
		}
	}
	
	//Makes a new TabWindow on this tabDirectory
	public TabWindow makeNewTabHere(String imageName) {
		TabWindow newTab = this.mainFactory.getElemFactory().getTabWindow(imageName);
		this.addNewTab(newTab);
		return newTab;
	}
	
	//Method for making a new selecting button for a tab
	public ScreenButton makeNewButtonForSelectingTab(TabWindow newTab) {
		ScreenButton newButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.SCROLLUP, ""+tabs.size());
		newButton.addButtonEventListener(this.mainFactory.getElemFactory().getChangeTabListener(this, newTab));
		return newButton;
		
	}
	
	//Sets a new active tab
	public void setActiveTab(TabWindow newActive) {
		this.activeTab = newActive;
	}
	
	public void bootUp() {
		super.bootUp();
		this.tabs = new ArrayList<TabWindow>();
		this.tabSelection = this.mainFactory.getElemFactory().getButtonFrame(this);
		this.setSize(350, 700);
		this.xDataStart = 0;
		this.yDataStart = 0;
		this.tabSelection.setLocation(300, 0);
	}
	
	//Checks if there are buttons on this tabDirectory
	public boolean hasButtons() {
		if(this.tabSelection == null) {
			return false;
		}
		if(this.tabSelection.getButtons().isEmpty()) {
			return false;
		}
		return true;
	}
	
	/*
	 * Not a final code for liberating screenElements to a possible tab in this directory
	 */
	public boolean liberateScreenElementToHere(ScreenElement socketItem) {
		if(socketItem.getClass().equals(ScreenSimpleChar.class)) {
			ScreenSimpleChar character = (ScreenSimpleChar) socketItem;
			ScrollCharSocket searchSocket = charSocketSearch();
			if(searchSocket != null) {
				searchSocket.addNewElement(character);
				return true;
			}
			return false;
		}
		else if(socketItem.getClass().equals(ScreenSimpleItem.class)) {
			ScreenSimpleItem item = (ScreenSimpleItem) socketItem;
			return true;
		}
		return false;
	}
	
	/*
	 * Search that might return a char socket if there is one among the tabs
	 */
	private ScrollCharSocket charSocketSearch() {
		for(TabWindow oneTab : this.tabs) {
			for(ScreenElement oneElement : oneTab.getElements()) {
				if(oneElement.getClass().equals(ScrollCharSocket.class)) {
					return (ScrollCharSocket) oneElement;
				}
			}
		}
		return null;
	}
	
	/*
	 * Test code for searchign different type of sockets
	 */
	private<T extends BaseScrollSocket> T SocketSearct(Class<T> searchClass) {
		for(TabWindow oneTab : this.tabs) {
			for(ScreenElement oneElement : oneTab.getElements()) {
				if(oneElement.getClass().equals(searchClass)) {
					return (T) oneElement;
				}
			}
		}
		return null;
	}

	@Override
	public void fillTopElementList(List<IStackable> list) {
		if(this.activeTab!=null) {
			list.add(activeTab);
		}
		if(this.hasButtons()) {
			list.add(tabSelection);
		}
		
	}

	@Override
	public void fillTopTextList(List<ScreenText> list) {
		// TODO Auto-generated method stub
		
	}
	
	public void safeDelete() {
		for(TabWindow oneTab : this.tabs) {
			oneTab.safeDelete();
		}
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOffScreenScript() {
		// TODO Auto-generated method stub
		
	}

}

package gameState;

import java.util.ArrayList;

import dataValues.CharValue;
import dataValues.FoodValue;
import factories.IAutoMake;
import factories.MainFactory;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import generalGraphics.ButtonLookType;
import itemBase.TradeItemName;
import land.TravelManager;
import other.StaticFunctions;
import party.PartyStance;
import screenElements.ScreenButton;
import screenElements.ScreenPopUp;
import sreenElementsSockets.ScreenSimpleChar;
import view.GameView;
import view.IGameView;
import view.ViewName;

//Base class for game states, classes that load in all neccesary data for a specific game
public abstract class BaseGameState implements IAutoMake, IGameState {
	protected MainFactory mainFactory;
	protected GameView battleView;
	protected ViewName travelView;
	protected TravelManager travelManager;
	protected ScreenButton nextPartButton;
	protected ArrayList<CharValue> charValues;

	@Override
	public void loadInDetails() {
		this.makePlayerCharacters();
		loadInInventory();
		loadInCaravanJobs();
		loadInTravelOptions();
		loadInBattleViewBase(this.battleView);
		bootLoadFillViewList();
		addNextPartButtonListeners(this.nextPartButton);
		startGameScript();
		
	}
	
	//method for making player characters
	private void makePlayerCharacters() {
		loadInPlayerCharacters(this.charValues);
		this.mainFactory.getCharacterManager().setCharactersWithCharValues(this.charValues);
		sendAllCharactersToIdle();
	}
	
	//Makes simple Screen char versions of all the characters and sends them to designated idle location
	private void sendAllCharactersToIdle() {
		ArrayList<ScreenSimpleChar> characters = new ArrayList<ScreenSimpleChar>();
		for (IGameCharacter oneChar : this.mainFactory.getCharacterManager().getCharacters()) {
			ScreenSimpleChar tempChar = this.mainFactory.getElemFactory().getScreenSimpleChar(oneChar);
			characters.add(tempChar);
		}
		this.mainFactory.getGameStateController().addCharListToIdleList(characters);
	}
	
	//Loads in characters meant for the game
	protected abstract void loadInPlayerCharacters(ArrayList<CharValue> listOfChars);
	
	//Loads in the view that the game will start from
	protected abstract void bootLoadFillViewList();
	
	//Loads in the designated battle view template
	protected abstract void loadInBattleViewBase(IGameView viewSaveLocation);
	
	//Loads in other used views
	//protected abstract void loadInOtherViews();

	//Generates all the items in players inventory
	protected abstract void loadInInventory();
	
	//Loads in current travel options for the player
	protected abstract void loadInTravelOptions();
	
	//Sets up the jobs available for the player in the travel view
	protected abstract void loadInCaravanJobs();
	
	//For adding specific listeners for what should come after finishing this module
	protected abstract void addNextPartButtonListeners(ScreenButton nextPartButton);
	
	//makes basic next part button that will stay as a logical only button
	protected void makeNextPartButton() {
		this.nextPartButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.POPUPSELECT, "Next part");
	}
	
	//getter for modules designated next part button
	public ScreenButton getNextPartButton() {
		return this.nextPartButton;
	}
	
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}
	
	//Method for triggering the first view
	public abstract void startGameScript();
	
	//Method for getting designated traveling view
	public void startUpTravelView() {
		this.mainFactory.getParty().setPartyStance(PartyStance.Travel);
		this.mainFactory.getViewManager().changeToView(findTravelView());
	}
	
	//Method that returns the view this game should start with
	public IGameView findTravelView() {
		return this.mainFactory.getViewManager().searchForView(travelView);
	}


	@Override
	public void bootUp() {
		this.battleView = this.mainFactory.getElemFactory().getGameView(ViewName.Battle);
		this.travelManager = this.mainFactory.getTravelManager();
		this.charValues = new ArrayList<CharValue>();
		this.makeNextPartButton();
	}
	
	@Override
	public IGameView getStandardBattleView() {
		//return battleView;
		return this.mainFactory.getViewManager().searchForView(ViewName.Battle);
	}
	
	//For adding extra char values from outside the module setup
	public void addExtraCharValues(ArrayList<CharValue> extraValues) {
		this.charValues.addAll(extraValues);
	}
	
	//Makes food values with maximum expiration date allowed for them
	protected FoodValue getFoodValuesMaxExp(TradeItemName itemName, int amount) {
		return new FoodValue(itemName, amount, this.mainFactory.getFoodExpTable().getMaxExpirationDateForFood(itemName));
	}
	
	//Makes a popup that will lead player to a next game state
	protected ScreenPopUp makeNextModulePopUp() {
		ScreenPopUp newPopUp = this.mainFactory.getElemFactory().getScreenPopUp("50red");
		newPopUp.fillTextOneLine("You won the fight");
		newPopUp.addButton(makeNextModuleButton(newPopUp));
		newPopUp.updateSizeForTime();
		return newPopUp;
	}
	
	//makes a next module button that will be placed on a next module popup
	private ScreenButton makeNextModuleButton(ScreenPopUp popUp) {
		ScreenButton newButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.POPUPSELECT, "Forward!");
		newButton.addButtonEventListener(this.mainFactory.getElemFactory().getCloseIStackableElementListener(popUp));
		newButton.addButtonEventListener(this.mainFactory.getElemFactory().getActivateAnotherButtonListener(this.getNextPartButton()));
		return newButton;
	}

}

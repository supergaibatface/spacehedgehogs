package itemBase;

import java.util.List;

import itemChange.IItemListener;
import screenInfoPage.BaseInfoPage;
import sreenElementsSockets.ScreenSimpleItem;

public interface IGameItem {
	
	//for adding an item change listener, input is the new listener
	public void addItemChangeEventListener(IItemListener listener);
	
	//for removing an item change listener, input is the old listener
	public void removeItemChangeEventListener(IItemListener listener);
	
	//for adding an amount of food, int is the added amount
	public void addAmount(int addition);
	
	//for setting new amount value
	public void setAmount(int newTotalAmount);
	
	//for checking if the item has certain tag, input is the tag name
	public boolean hasTag(ItemTagName tagName);
	
	//method for getting all the tags that an item has
	public List<ItemTagName> getTags();
	
	//for removing a certain amount of item, input is the amount
	public boolean canRemoveAmount(int amount);
	
	//checks if its possible to remove a certain amount from the item stack
	public boolean removeAmount(int amount);
	
	/*
	 * for splitting up the item stack into two stacks, input is how many of the item stack items will be in the new stack
	 * and the output is the new stack
	 */
	public IGameItem SplitItem(int amount);
	
	//checks if the item has the same name as the input name
	public boolean equals(TradeItemName name);
	
	//checks if two item stacks are of the same type
	public boolean equals(IGameItem item);
	
	//checks if the item stack is not empty
	public boolean hasPossesion();
	
	//getter for the item name
	public TradeItemName getName();
	
	//getter for the current amount of the item stack
	public int getSupply();
	
	//getter for item image name of the stack
	public String getImageName();
	
	//getter for the stacks full weight
	public int getFullWeight();
	
	//getter for one unit weight
	public int getOneElementWeight();
	
	//command that informs the stack of a turn change (mostly for expiring food items)
	public void turnChanged();
	
	//command for merging two item stacks, only works if their item names match
	public boolean merge(IGameItem item);
	
	//command for making an copy of the same item with the same amount
	public IGameItem makeCopy();
	
	//getter for the items front text (text that will be displayed on a screen version)
	public List<String> getFrontText();
	
	//checks if there is already a socket avatar version of this item
	public boolean hasSocketAvatar();
	
	//getter for this items socket avatar
	public ScreenSimpleItem getSocketAvatar();
	
	//setter for this items socket avatar element
	public void setSocketAvatar(ScreenSimpleItem avatar);
	
	//method for safely deleting an item stack
	public void safeDelete();
	
	//getter for the info page of the item
	public BaseInfoPage getInfoPage();

}

package timedSystems;

import java.util.ArrayList;
import java.util.Timer;

import factories.IAutoMake;
import factories.MainFactory;

//Timer that will trigger screen refreshing
public class SystemTimer implements IAutoMake{
	private MainFactory mainFactory;
	private Timer repaintTimer;
	private Timer objectTimer;

	//setter for main factory
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	//bootup method that will deal with calling screen repaint command
	@Override
	public void bootUp() {
		this.repaintTimer = new Timer("Timer");
	    long delay  = 100;
	    long period = 100;
	    this.repaintTimer.scheduleAtFixedRate(this.mainFactory.getRepaintTask(), delay, period);
	    this.objectTimer = new Timer("ObjectTimer");
	}
	
	//Setup for new timer that counts down on timed objects, input is the object that will dissapear at some point
	public void setNewObjectTimerTask(ITimedObject objectForCounting) {
		TimeObjectCountTask newTask = new TimeObjectCountTask(objectForCounting);
		long delay = 1000;
		long period = 1000;
		this.objectTimer.scheduleAtFixedRate(newTask, delay, period);
	}

}

package view;

import java.util.ArrayList;
import java.util.List;

import buttonLimiters.ItemLimiter;
import buttonLimiters.TerrainLimiter;
import dataValues.CharValue;
import exceptionHandling.ExceptionManager;
import factories.IAutoMake;
import factories.MainFactory;
import gameState.IGameState;
import generalGraphics.ButtonLookType;
import itemBase.ItemTagName;
import itemBase.TradeItemName;
import jobs.JobManager;
import other.StaticFunctions;
import party.PartyStance;
import propertyBars.PropertyBarName;
import screenElements.ScreenButton;
import screenElements.ScreenDataBox;
import screenElements.ScreenPopUp;
import screenElements.ScreenScrollPanel;
import screenElements.ScreenSoloImage;
import screenManagement.GElementManager;
import skills.SkillName;
import socketStoring.CharSocketBuilder;
import socketStoring.FullCharacterListProviding;
import socketStoring.FullItemProviding;
import socketStoring.NewItemSocketBuilder;
import socketStoring.TagItemProviding;
import sreenElementsSockets.ScrollCharSocket;
import sreenElementsSockets.ScrollItemSocket;
import viewExceptions.NotManagementViewException;
import viewExceptions.ViewNotFoundException;

/*
 * Class that possess a list of all the view objects in the game
 */
public class ViewManager implements IAutoMake{
	private MainFactory mainFactory;
	private GElementManager geManager;
	private List<IGameView> views;
	private boolean debug = true;
	private ExceptionManager exceptionManager;
	
	//constructor for the manager
	public ViewManager() {
		
	}
	
	//Boot up method, meant for starting up the object after creation
	public void bootUp() {
		this.exceptionManager = this.mainFactory.getExceptionManager();
		this.geManager = this.mainFactory.getGElementManager();
		this.views = new ArrayList<IGameView>();
	}

	//setter for main factory object
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
	}
	
	//method for changing to menu view
	public void changeToMenu() {
		changeToView(this.searchForView(ViewName.MainMenu));
	}
	
	//method for changing to a view
	public void changeToView(IGameView newView) {
		this.geManager.changeToView(newView);
	}
	
	public void changeToView(ViewName name) {
		this.changeToView(this.searchForView(name));
	}
	
	//Method for leaving battle view
	public void leaveBattleView() {
		this.mainFactory.getGameStateController().returnFromBattle();
	}
	
	/*
	 * Method for searching for a view by the views name
	 */
	public IGameView searchForView(ViewName name) {
		try {
			return this.managementViewMatch(name);
		}
		catch(Exception exception) {
			this.exceptionManager.dealWithException(exception);
		}
		try {
			return this.searchViewFromOwnList(name);
		}
		catch(Exception exception) {
			this.exceptionManager.dealWithException(exception);
		}
		return this.giveDebugView();
	}
	
	private IGameView searchViewFromOwnList(ViewName name) throws Exception {
		for(IGameView oneView: this.views) {
			if(oneView.equals(name)) {
				return oneView;
				
			}
		}
		throw new ViewNotFoundException(name);
	}
	
	private IGameView managementViewMatch(ViewName name) throws Exception {
		if(name == ViewName.MainMenu) {
			return this.giveMainMenuView();
		}
		if(name == ViewName.DebugMenu) {
			return this.giveDebugOptionsMenu();
		}
		throw new NotManagementViewException(name);
	}
	
	private IGameView giveMainMenuView() {
		return this.mainFactory.getViewFactory().getMainMenuView();
	}
	
	private IGameView giveDebugOptionsMenu() {
		return this.mainFactory.getViewFactory().getDebugMenuView();
	}
	
	private IGameView giveDebugView() {
		return this.mainFactory.getViewFactory().getDebugView();
	}
	
	public void addNewView(IGameView newView) {
		this.views.add(newView);
	}
	
	public void clearSavedViews() {
		this.views.clear();
	}

}

package mouseSituationSet;

import java.util.ArrayList;

import commandAction.CABaseClass;
import commandAction.ICommandAction;
import factories.IAutoMake;
import factories.MainFactory;
import mouseControls.MouseActionConditionSet;

/*
 * Base abstract class for MouseSituationSets
 */
public abstract class BaseMouseSituationSet implements IAutoMake, IMouseSituationSet {
	protected MainFactory mainFactory;
	protected ArrayList<CABaseClass> commandArray;
	protected ArrayList<CABaseClass> commandArrayRight;
	protected MouseActionConditionSet conditions;
	protected ArrayList<Class> clickSearch;
	protected ArrayList<Class> clickSearchRight;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		commandArray = new ArrayList<CABaseClass>();
		commandArrayRight = new ArrayList<CABaseClass>();
		clickSearch = new ArrayList<Class>();
		clickSearchRight = new ArrayList<Class>();
		conditions = this.mainFactory.getMouseConditionSet();
		makeAllCAElements();
	}
	
	/*
	 * Method for adding new Command Action Elements to this situationSet
	 * Inputs: 
	 * 	newCommand: Command Action element that will be added
	 */
	public void addNewCAElement(CABaseClass newCommand) {
		if(newCommand.isRightClickCA()) {
			this.commandArrayRight.add(newCommand);
		
		}
		else {
			this.commandArray.add(newCommand);
		}
		this.insertClickSearchElement(newCommand);
	}
	
	/*
	 * Inserts a click search element from and added command action class
	 * Inputs:
	 * 	newCommand: the added new CommandAction
	 */
	private boolean insertClickSearchElement(CABaseClass newCommand) {
		if(!newCommand.hasClickTargetClass()) {
			return false;
		}
		if(newCommand.isLeftClickCA()) {
			return this.clickSearchInsertLeftBranch(newCommand);
		}
		else {
			return this.clickSearchInsertRightBranch(newCommand);
		}
	}
	
	//Branch for inserting a CA  to list of left click ones
	private boolean clickSearchInsertLeftBranch(CABaseClass newCommand) {
		Class newClass = newCommand.getClickTargetClass();
		for(Class oneClass : this.clickSearch) {
			if(oneClass.equals(newClass)) {
				return false;
			}
		}
		this.clickSearch.add(newClass);
		return true;
	}
	
	//Branch for inserting a CA to list of right click ones
	private boolean clickSearchInsertRightBranch(CABaseClass newCommand) {
		Class newClass = newCommand.getClickTargetClass();
		for(Class oneClass : this.clickSearchRight) {
			if(oneClass.equals(newClass)) {
				return false;
			}
		}
		this.clickSearchRight.add(newClass);
		return true;
	}
	
	//Getter for saved command actions
	public ArrayList<CABaseClass> getCommands() {
		if(this.conditions.isLatestMouseButtonLeft()) {
			return this.commandArray;
		}
		if(this.conditions.isLatestMoustButtonRight()) {
			return this.commandArrayRight;
		}
		return new ArrayList<CABaseClass>();
		
	}
	
	//Method that will fill the list for this mouse situations command actions
	protected abstract void makeAllCAElements();

	@Override
	public abstract boolean checkSituation();

	@Override
	public ArrayList<Class> getSearchClasses(int mouseClick) {
		if(mouseClick == 1) {
			return this.clickSearch;
		}
		if(mouseClick == 3) {
			return this.clickSearchRight;
		}
		return new ArrayList<Class>();
	}
	
	//debug method for printing out all the classes that this set will search for when left clicked
	public void printOutSearchClasses() {
		for(Class oneClass : this.clickSearch) {
			System.out.println(oneClass);
		}
	}
	
	//debug method for printing out all the classes that this set will search for when right clicked
	public void printOutRightSearchClasses() {
		for(Class oneClass : this.clickSearchRight) {
			System.out.println(oneClass);
		}
	}

}

package other;

import java.util.ArrayList;
import java.util.List;

import screenElements.ScreenElement;

//Collection of static functions
public class StaticFunctions {
	
	/*
	 * Makes a copy of input array
	 * Inputs:
	 * 	inputArray: array that will be copied
	 */
	public static <T> ArrayList<T> copyArray(ArrayList<T> inputArray) {
		ArrayList<T> newArray = new ArrayList<T>();
		for(T oneElement : inputArray) {
			newArray.add(oneElement);
		}
		return newArray;
	}
	
	/*
	 * Makes a copy of input array
	 * Inputs:
	 * 	inputArray: array that will be copied
	 */
	public static <T> List<T> copyList(List<T> inputArray) {
		ArrayList<T> newArray = new ArrayList<T>();
		for(T oneElement : inputArray) {
			newArray.add(oneElement);
		}
		return newArray;
	}
	
	/*
	 * Method that adds element to array if its an unique element
	 * Inputs:
	 * 	array: location where the element would be added
	 * 	newElement: element that would be added
	 */
	public static <T> void addToArrayIfUnique(ArrayList<T> array, T newElement) {
		if(!array.contains(newElement)) {
			array.add(newElement);
		}
	}
	
	/*
	 * Method that filters out visible ScreenElements from an array
	 * Inputs:
	 * 	searchArray: array of screenElements to be filtered
	 */
	public static <T extends ScreenElement> ArrayList<T> filterOutVisible(ArrayList<T> searchArray) {
		ArrayList<T> filteredList = new ArrayList<T>();
		for(T oneElement: searchArray) {
			if(oneElement.isVisible()) {
				filteredList.add(oneElement);
			}
		}
		return filteredList;
	}
	
	/*
	 * Checks if intput element is of the inputted class
	 * Inputs:
	 * 	element: element that will be checked
	 * 	className: class value that element will be checked against
	 */
	public static <T> boolean isElementOfClass(T element, Class className) {
		if(element == null) {
			return false;
		}
		if(!element.getClass().equals(className)) {
			return false;
		}
		return true;
	}
	
	/*
	 * for writing out debugValues
	 * Inputs:
	 * 	value: boolean value that turns on debug mode
	 * 	text: string text that will be written to the console
	 */
	public static void writeOutDebug(boolean value, String text) {
		if(value) {
			System.out.println(text);
		}
	}
	
	/* Example of using system time to measure
	long startTime = StaticFunctions.getSystemTime();
	long finTime = StaticFunctions.getEllapsedSystemTime(startTime);
	System.out.println("finsihed for: Making player characters, in "+finTime);
	*/
	
	/* Example of using system time to measure
	long startTime = StaticFunctions.getSystemTime();
	StaticFunctions.writeOutElapsedSystemTime(startTime, desc);
	*/
	
	public static long getSystemTime() {
		return System.currentTimeMillis();
	}
	
	public static long getEllapsedSystemTime(long previousNanoTime) {
		return getSystemTime() - previousNanoTime;
	}
	
	public static void writeOutElapsedSystemTime(long previousNanoTime, String targetDesc) {
		long finTime = StaticFunctions.getEllapsedSystemTime(previousNanoTime);
		System.out.println("time for: "+targetDesc+" is "+finTime);
	}


}

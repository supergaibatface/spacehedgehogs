package buttonListeners;

import questBase.IQuest;

public class AddNewQuestListener extends BaseButtonListener {
	private IQuest newQuest;
	
	public AddNewQuestListener(IQuest newQuest) {
		this.newQuest = newQuest;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.mainFactory.getQuestFactory().getQuestManager().addNewQuest(this.newQuest);
		
	}

	@Override
	public String toInfo() {
		return "Starts the "+newQuest.getName()+" quest.";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

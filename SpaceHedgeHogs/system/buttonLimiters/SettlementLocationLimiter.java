package buttonLimiters;

import settlementLocation.ISettlementLocation;

public class SettlementLocationLimiter extends BaseButtonLimiter {
	private ISettlementLocation masterLocation;

	public SettlementLocationLimiter(ISettlementLocation masterLocation) {
		super(false, false);
		this.masterLocation = masterLocation;
	}

	@Override
	public boolean check() {
		if(!masterLocation.isAccessable()) {
			return false;
		}
		return true;
	}

	@Override
	public String reasonForFail() {
		return "Settlement location is inaccesible";
	}

	@Override
	public void bootUp() {
		
	}

	@Override
	public void doCost() {
		
	}

}

package buttonListeners;

import java.util.EventListener;

//Interface for on screen button press listener
public interface ISButtonListener extends EventListener {
	
	//method for registering button events
	public void screenButtonEventOccured(ScreenButtonEvent evt);
	
	//turns the listener into a informative string sentence
	public String toInfo();
}

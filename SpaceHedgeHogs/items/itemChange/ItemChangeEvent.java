package itemChange;

import java.util.EventObject;

//Event that is called when an existing inventory item stacks value changes
public class ItemChangeEvent extends EventObject {
	private static final long serialVersionUID = 1L;
	private boolean amountChanged = false;;
	private boolean expirationChanged = false;

	//constructor
	public ItemChangeEvent(Object arg0) {
		super(arg0);
	}

	//returns boolean if food amount has changed
	public boolean isAmountChanged() {
		return amountChanged;
	}

	//sets if food amount has changed
	public void setAmountChanged(boolean amountChanged) {
		this.amountChanged = amountChanged;
	}

	//checks if amount of items that will be expiring next turn has changed
	public boolean isExpirationChanged() {
		return expirationChanged;
	}

	//sets if amount of items that will expire next turn has changed
	public void setExpirationChanged(boolean expirationChanged) {
		this.expirationChanged = expirationChanged;
	}
	
	

}

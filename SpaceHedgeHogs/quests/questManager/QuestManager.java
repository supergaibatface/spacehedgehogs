package questManager;

import java.util.ArrayList;
import java.util.List;

import factories.IAutoMake;
import factories.MainFactory;
import generalGraphics.ButtonLookType;
import questBase.IQuest;
import screenElements.ScreenButton;
import screenElements.ScreenDataBox;

public class QuestManager implements IAutoMake {
	private MainFactory mainFactory;
	private List<IQuest> currentQuests;
	private ScreenDataBox questDisplay;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.makeQuestList();
		this.makeQuestDisplay();
		
	}
	
	private void makeQuestList() {
		this.currentQuests = new ArrayList<IQuest>();
	}
	
	private void makeQuestDisplay() {
		this.questDisplay = this.mainFactory.getElemFactory().getScreenDataBox(0, 0, "100Lime");
		this.questDisplay.setSize(300, 800);
		this.questDisplay.fillTextOneLine("Quests");
		this.updateQuestList();
	}
	
	public ScreenDataBox getQuestDisplay() {
		return this.questDisplay;
	}
	
	private void updateQuestList() {
		this.questDisplay.removeAllButtons();
		for(IQuest oneQuest : this.currentQuests) {
			this.makeOneQuestButton(oneQuest, questDisplay);
		}
	}
	
	private void makeOneQuestButton(IQuest oneQuest, ScreenDataBox addLocation) {
		ScreenButton questButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.POPUPSELECT, oneQuest.getName());
		questButton.addButtonEventListener(this.mainFactory.getElemFactory().getOpenElementOnTopLayerListener(oneQuest.getQuestInfoPage()));
		addLocation.addButton(questButton);
	}
	
	public void addNewQuest(IQuest newQuest) {
		this.currentQuests.add(newQuest);
		newQuest.setActive(true);
		this.updateQuestList();
	}

}

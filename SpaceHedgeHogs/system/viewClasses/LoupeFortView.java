package viewClasses;

import fortLoupe.Loupe;
import screenElements.ScreenBackground;
import screenElements.ScreenButton;
import view.BaseGameView;
import view.ViewName;

public class LoupeFortView extends BaseGameView {
	private Loupe logicalLocation;
	
	public LoupeFortView() {
		super(ViewName.Loupe);
	}

	protected void bootElements() {
		makeLeaveButton();
		//this.setBackground();
		this.setUpLogicalLocation();
		this.getQuestList();
	}
	
	@Override
	protected String bootGiveBackgroundImageName() {
		return "TestBG";
	}
	
	private void makeLeaveButton() {
		ScreenButton leaveButton = this.getAGoToDebugViewButton(1470, 900, "Continue your travels");
		leaveButton.addActionLimiter(this.mainFactory.getElemFactory().getQuestLimiter(this.mainFactory.getQuestFactory().getLoupeMainQuest()));
		this.buttons.add(leaveButton);
	}
	
	/*private void setBackground() {
		this.background = this.mainFactory.getElemFactory().getBackground("TestBG");
	}*/
	
	private void setUpLogicalLocation() {
		this.logicalLocation = this.mainFactory.getLogicalLocationFactory().getFortLoupe();
		this.addLogicalLocation(logicalLocation);
	}
	
	private void getQuestList() {
		this.wrappedDataBoxes.add(this.mainFactory.getElemFactory().getWrapperForDataBox(1500, 50, this.mainFactory.getQuestFactory().getQuestManager().getQuestDisplay()));
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

}

package fortLoupe;

import settlement.BaseSettlement;

public class Loupe extends BaseSettlement {
	private LoupeGatehouse gatehouseLocation;
	private LoupeKeep keepLocation;
	private LoupeCamp campLocation;
	private LoupeDungeon dungeonLocation;
	
	public void bootUp() {
		super.bootUp();
		this.makeGatehouse();
		this.makeKeep();
		this.makeCamp();
		this.makeDungeon();
	}
	
	private void makeGatehouse() {
		this.gatehouseLocation = this.mainFactory.getLogicalLocationFactory().getLoupeGatehouse();
		this.registerLocationButton(gatehouseLocation);
	}
	
	private void makeKeep() {
		this.keepLocation = this.mainFactory.getLogicalLocationFactory().getLoupeKeep();
		this.registerLocationButton(keepLocation);
	}
	
	private void makeCamp() {
		this.campLocation = this.mainFactory.getLogicalLocationFactory().getLoupeCamp();
		this.registerLocationButton(campLocation);
		//add stuff to make the camp view aswell
	}
	
	private void makeDungeon() {
		this.dungeonLocation = this.mainFactory.getLogicalLocationFactory().getLoupeDungeon();
		this.registerLocationButton(dungeonLocation);
	}

}

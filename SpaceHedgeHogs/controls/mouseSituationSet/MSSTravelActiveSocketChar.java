package mouseSituationSet;

import screenElements.ScreenElement;
import sreenElementsSockets.ScreenSimpleChar;

//Represents travel view mouse set with an active char in a socket
public class MSSTravelActiveSocketChar extends BaseMouseSituationSet {

	@Override
	protected void makeAllCAElements() {
		this.addNewCAElement(this.mainFactory.getCASocketSelect());
		this.addNewCAElement(this.mainFactory.getCAButtonPress());
		this.addNewCAElement(this.mainFactory.getCACharDeselect());
	}

	@Override
	public boolean checkSituation() {
		if(this.mainFactory.getGameStateController().inBattle()) {
			return false;
		}
		ScreenElement selectedItem = conditions.getSelectedItem();
		if(selectedItem == null) {
			return false;
		}
		if(!selectedItem.getClass().equals(ScreenSimpleChar.class)) {
			return false;
		}
		return true;
	}

}

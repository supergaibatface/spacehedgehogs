package itemBase;

import java.util.ArrayList;
import java.util.List;

import javax.swing.event.EventListenerList;

import factories.IAutoMake;
import factories.MainFactory;
import itemChange.IItemListener;
import itemChange.ItemChangeEvent;
import sreenElementsSockets.ScreenSimpleChar;
import sreenElementsSockets.ScreenSimpleItem;

//base class for all item classes
public abstract class BaseItem implements IAutoMake, IGameItem {
	protected MainFactory mainFactory;
	protected EventListenerList listenerList = new EventListenerList();
	protected TradeItemName name;
	protected String imageName;
	protected ScreenSimpleItem socketAvatar;
	protected int oneUnitWeight;
	protected List<ItemTagName> itemTags;
	
	//constructor for the base item, input is the item name
	public BaseItem(TradeItemName name) {
		this.name = name;
	}

	@Override
	public void addItemChangeEventListener(IItemListener listener) {
		listenerList.add(IItemListener.class, listener);
		
	}

	@Override
	public void removeItemChangeEventListener(IItemListener listener) {
		listenerList.remove(IItemListener.class, listener);
		
	}
	
	//Method for sending a ItemChangeEvent to all the listeners, input evt: ItemChangeEvent style event
	protected void fireItemChangeEvent(ItemChangeEvent evt) {
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i + 2) {
			if (listeners[i] == IItemListener.class) {
				((IItemListener) listeners[i + 1]).itemChangeEventOccured(evt);
			}
		}
	}
	
	//Method that makes a new screenButtonEvent and fires it through the firebuttonevent
	protected void callAmountChangeEvent() {
		fireItemChangeEvent(this.getAmountChangeEvent());
		this.sendWeightChangeToInventory();
	}
	
	//method that returns an event for amount changing
	protected ItemChangeEvent getAmountChangeEvent() {
		ItemChangeEvent newEvent = new ItemChangeEvent(this);
		newEvent.setAmountChanged(true);
		return newEvent;
	}
	
	//command that tells inventory about item weight change
	protected void sendWeightChangeToInventory() {
		this.mainFactory.getInventory().inventoryWeightChange(this);
	}

	@Override
	public void addAmount(int addition) {
		addAmountLogic(addition);
		this.callAmountChangeEvent();
	}
	
	@Override
	public void setAmount(int newTotalAmount) {
		this.removeAmount(this.getSupply());
		this.addAmount(newTotalAmount);
	}
	
	//script for dealing with a new added amount for the stack
	public abstract void addAmountLogic(int addition);
	
	//checks if there is a positive amount of this item
	public boolean hasPossesion() {
		return (this.getSupply() > 0);
	}

	@Override
	public boolean hasTag(ItemTagName tagName) {
		return this.itemTags.contains(tagName);
	}

	@Override
	public List<ItemTagName> getTags() {
		return new ArrayList<ItemTagName>(this.itemTags);
	}

	@Override
	public boolean canRemoveAmount(int amount) {
		if(amount <= this.getSupply()) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean removeAmount(int amount) {
		if(this.removeAmountLogic(amount)) {
			this.amountDecreaseEventCalls();
			return true;
		}
		else {
			return false;
		}
	}
	
	//logic that deals with a certain removed amount
	public abstract boolean removeAmountLogic(int amount);
	
	//event call for amount decreasing
	protected void amountDecreaseEventCalls() {
		callAmountChangeEvent();
		if(this.getSupply() < 1) {
			this.mainFactory.getInventory().removeItemFromListing(this);
		}
	}

	@Override
	public abstract IGameItem SplitItem(int amount);

	@Override
	public boolean equals(TradeItemName name) {
		return (this.name.equals(name));
	}

	@Override
	public boolean equals(IGameItem item) {
		return this.equals(item.getName());
	}

	@Override
	public TradeItemName getName() {
		return this.name;
	}

	@Override
	public abstract int getSupply();

	@Override
	public String getImageName() {
		return this.imageName;
	}

	@Override
	public int getFullWeight() {
		return this.getOneElementWeight() * this.getSupply();
	}
	
	@Override
	public int getOneElementWeight() {
		return this.oneUnitWeight;
	}

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.imageName = this.bootGiveItemImageName();
		this.oneUnitWeight = this.bootGiveOneUnitWeight();
		this.itemTags = new ArrayList<ItemTagName>();
	}
	
	protected abstract String bootGiveItemImageName();
	
	protected abstract int bootGiveOneUnitWeight();
	
	protected void addAnotherItemTag(ItemTagName newTagName) {
		this.itemTags.add(newTagName);
	}
	
	protected String bootGivePlaceholderImage() {
		return "100X100Mark";
	}
	
	@Override
	public void turnChanged() {
		
	}
	
	@Override
	public boolean merge(IGameItem item) {
		if(item.getName() == this.getName()) {
			if(this.mergeLogic(item)) {
				this.callAmountChangeEvent();
				return true;
			}
			
		}
		return false;
	}
	
	@Override
	public List<String> getFrontText() {
		List<String> lines = new ArrayList<String>();
		lines.add(this.makeBasicNameAmountLine());
		this.addExtraLines(lines);
		return lines;
	}
	
	//Makes a basic line that is used on screen representations of this item
	protected String makeBasicNameAmountLine() {
		StringBuilder newStringBuilder = new StringBuilder();
		newStringBuilder.append(this.name.toString());
		newStringBuilder.append(": ");
		newStringBuilder.append(this.getSupply());
		return newStringBuilder.toString();
	}
	
	@Override
	public boolean hasSocketAvatar() {
		return this.socketAvatar != null;
	}
	
	@Override
	public ScreenSimpleItem getSocketAvatar() {
		return this.socketAvatar;
	}
	
	@Override
	public void setSocketAvatar(ScreenSimpleItem avatar) {
		this.socketAvatar = avatar;
	}
	
	@Override
	public void safeDelete() {
		this.socketAvatar.removeThisElementFromCurrentLocation();
		this.socketAvatar = null;
	}
	
	//method that adds extra lines for screep representation of the item
	protected abstract void addExtraLines(List<String> lines);
	
	//logic for merging to stacks of the same item
	protected abstract boolean mergeLogic(IGameItem item);

}

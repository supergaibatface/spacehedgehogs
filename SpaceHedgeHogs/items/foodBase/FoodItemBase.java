package foodBase;

import java.util.ArrayList;
import java.util.List;

import itemBase.BaseItem;
import itemBase.IGameItem;
import itemBase.ItemTagName;
import itemBase.TradeItemName;
import itemChange.ItemChangeEvent;
import screenInfoPage.BaseInfoPage;
import screenInfoPage.FoodInfoPage;

public abstract class FoodItemBase extends BaseItem {
	private List<AmountExpirationPair> amount;
	private int baseExpiration;
	private FoodInfoPage infoPage;

	public FoodItemBase(TradeItemName name) {
		super(name);
	}
	
	@Override
	public void bootUp() {
		super.bootUp();
		this.amount = new ArrayList<AmountExpirationPair>();
		this.baseExpiration = this.bootGiveBaseExpiration();
		this.makeItemInfoPage();
		addTags();
	}
	
	protected abstract int bootGiveBaseExpiration();
	
	//method that makes an info page for the food item
	private void makeItemInfoPage() {
		this.infoPage = this.mainFactory.getElemFactory().getFoodInfoPage(this, "Orange");
	}
	
	private void addTags() {
		addAnotherItemTag(ItemTagName.Food);
	}

	@Override
	public IGameItem makeCopy() {
		FoodItemBase newStack = this.giveBaseClass();
		newStack.mergeLogic(this);
		return newStack;
	}
	
	protected abstract FoodItemBase giveBaseClass();

	@Override
	public BaseInfoPage getInfoPage() {
		return this.infoPage;
	}

	@Override
	public void addAmountLogic(int addition) {
		if(this.amount.size() > 0) {
			if(this.amount.get(amount.size() - 1).getExpirationTurn() == baseExpiration) {
				this.amount.get(amount.size() - 1).addAmount(addition);
			}
			else {
				this.amount.add(new AmountExpirationPair(addition, this.baseExpiration));
			}
		}
		else {
			this.amount.add(new AmountExpirationPair(addition, this.baseExpiration));
		}
		
	}
	
	/*
	 * method for adding an amount of food
	 * Input:
	 * 	addition: amount of food added
	 * 	expirationTurn: the time it expires
	 */
	public void addAmount(int addition, int expirationTurn) {
		int location = this.hasElementsWithExpiration(expirationTurn);
		if(location != -1) {
			this.amount.get(location).addAmount(addition);
		}
		else {
			this.amount.add(this.getInsertLocation(expirationTurn), new AmountExpirationPair(addition, expirationTurn));
		}
		if(expirationTurn < 2) {
			this.callExpirationChangeEvent();
		}
		this.callAmountChangeEvent();
	}
	
	//checks if there are stacks that expire at certain time, input is the expiration time
	private int hasElementsWithExpiration(int expirationDate) {
		for(AmountExpirationPair onePair: this.amount) {
			if(onePair.getExpirationTurn() == expirationDate) {
				return this.amount.indexOf(onePair);
			}
		}
		return -1;
	}
	
	//returns the location where a items with certain expiration turn need to inserted, input is the expiration turn
	private int getInsertLocation(int expirationTurn) {
		int insertLocation = 0;
		for(AmountExpirationPair onePair: this.amount) {
			if(onePair.getExpirationTurn() < expirationTurn) {
				insertLocation= this.amount.indexOf(onePair);
			}
			else {
				break;
			}
		}
		return insertLocation;
	}
	
	//Overall method for starting and sending out an expiration change event
	protected void callExpirationChangeEvent() {
		fireItemChangeEvent(this.getExpirationChangeEvent());
	}
	
	//Returns a new expiration change event
	protected ItemChangeEvent getExpirationChangeEvent() {
		ItemChangeEvent newEvent = new ItemChangeEvent(this);
		newEvent.setExpirationChanged(true);
		return newEvent;
	}
	

	@Override
	public boolean removeAmountLogic(int amount) {
		if(this.canRemoveAmount(amount)) {
			return this.amountRemovingCalculations(amount);
		}
		return false;
	}
	
	//calculation logic for item amount removing, input is the amount getting removed
	private boolean amountRemovingCalculations(int amount) {
		int stillToRemove = amount;
		while(stillToRemove > 0) {
			if(this.amount.get(0).getAmount() < stillToRemove) {
				stillToRemove = stillToRemove - this.amount.get(0).getAmount();
				this.amount.remove(0);
			}
			else {
				this.amount.get(0).removeAmount(stillToRemove);
				stillToRemove = 0;
			}
		}
		return true;
	}

	@Override
	public IGameItem SplitItem(int amount) {
		return null;
	}

	@Override
	public int getSupply() {
		int fullAmount = 0;
		for(AmountExpirationPair onePair : this.amount) {
			fullAmount = fullAmount + onePair.getAmount();
		}
		return fullAmount;
	}

	@Override
	protected void addExtraLines(List<String> lines) {
		if(this.amount.get(0).getExpirationTurn() == 1) {
			lines.add(this.amount.get(0).getAmount() + " will expire");
		}
		
	}

	@Override
	protected boolean mergeLogic(IGameItem item) {
		return convertedMergeLogic((FoodItemBase) item);
	}
	
	//Item mergining logic that works with food items expiration stacks
	private boolean convertedMergeLogic(FoodItemBase item) {
		for(AmountExpirationPair onePair: item.getItemStacks()) {
			mergeOneStack(onePair);
		}
		return true;
	}
	
	//getter for all the expiration stacks
	public List<AmountExpirationPair> getItemStacks() {
		return this.amount;
	}
	
	//method for merging one expiration stack with a new item stack
	private void mergeOneStack(AmountExpirationPair onePair) {
		this.addAmount(onePair.getAmount(), onePair.getExpirationTurn() );
	}
	
	//getter for the base expiration value
	public int getBaseExpiration() {
		return this.baseExpiration;
	}
	
	//checks if there are any stacks that expire with next turn
	public boolean hasExpiringFood() {
		if(this.amount.isEmpty()) {
			return false;
		}
		if(this.amount.get(0).getExpirationTurn() == 1) {
			return true;
		}
		return false;
	}
	
	//getter function for the amount of food that expires next turn
	public int getExpiringAmount() {
		if(!hasExpiringFood()) {
			return 0;
		}
		else {
			return this.amount.get(0).getAmount();
		}
	}
	
	//debug method for printing out current food item status with all the expiration stacks
	public void printOutCurrentContent() {
		System.out.println("Food item: "+this.name);
		for(AmountExpirationPair onePair: this.amount) {
			System.out.println(onePair.getAmount() + ": expiring in "+onePair.getExpirationTurn());
		}
		System.out.println("Total amount: "+this.getSupply());
		System.out.println("-----------------------");
	}
	
	@Override
	public void turnChanged() {
		this.expireByTurn();
	}
	
	
	//command for expiring the entire food stack with all its 
	private void expireByTurn() {
		for(AmountExpirationPair onePair: this.amount) {
			onePair.expireByOne();
		}
		if(this.amount.get(0).hasExpired()) {
			this.amount.remove(0);
			
		}
		//this should change if we could change how we display expiring amount in name
		this.amountDecreaseEventCalls();
		
		this.callExpirationChangeEvent();
	}

}

package buttonListeners;

import fieldCharCommand.FieldCharCommandControl;
import fieldCharCommand.FieldCharCommandStateName;

//Listener that is triggered for changing button selection on fieldCharCommandState
public class ChangeFCCCSelectionListener extends BaseButtonListener {
	private FieldCharCommandControl control;
	private FieldCharCommandStateName stateName;
	
	/*
	 * constructor
	 * Inputs:
	 * 	controls: FieldCharCommandControl databox where the change will occur
	 * 	stateName: enum that represents the new value for field char command control
	 */
	public ChangeFCCCSelectionListener(FieldCharCommandControl controls, FieldCharCommandStateName stateName) {
		this.control = controls;
		this.stateName = stateName;
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		control.selectionChangeReset();
		control.changeCurrentState(stateName);
		
	}

	@Override
	public String toInfo() {
		return "Reacts to a change in FieldCharCommandControl";
	}

}

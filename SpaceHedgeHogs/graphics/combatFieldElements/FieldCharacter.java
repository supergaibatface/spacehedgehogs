package combatFieldElements;

import java.util.ArrayList;
import java.util.List;

import central.AICharPatterns;
import frameworks.IStackable;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import generalControls.TravelPath;
import inventory.ItemBag;
import other.StaticFunctions;
import propertyBars.PropertyBar;
import propertyBars.PropertyBarName;
import screenElements.ScreenBar;
import screenElements.ScreenElement;
import screenElements.ScreenText;

//Class that represents a character on the movement field
public class FieldCharacter extends FieldElement {
	private IGameCharacter master;
	private ItemBag itemBag;
	private ControlFaction controlFaction;
	private ScreenBar healthBar;
	private boolean debug = false;
	private AICharPatterns AIPatterns;
	private ScreenText nameLabel;

	/*
	 * constructor
	 * Inputs:
	 * 	master: GameCharacter version of the field character
	 * 	faction: controlFaction enum that decides who controls the field character
	 */
	public FieldCharacter(IGameCharacter master, ControlFaction faction) {
		super();
		this.master = master;
		this.controlFaction = faction;
	}
	
	public void bootUp() {
		super.bootUp();
		this.setSize(this.lenAss.getFieldSquareSize(), this.lenAss.getFieldSquareSize());
		this.itemBag = mainFactory.getElemFactory().getItemBag();
		this.xDataStart = 0;
		this.yDataStart = 0;
		this.makeHealthBar();
	}
	
	//Makes a visual health bar that represents the master characters health
	private void makeHealthBar() {
		this.healthBar = master.getPropertyBar(PropertyBarName.Health).generateVisualBar(lenAss.getFieldSquareSize(), lenAss.getFieldSquareSize() / 5);
		this.healthBar.setWrapper(this);
		this.healthBar.setCoordinates(0, lenAss.getFieldSquareSize() / 5 * 4);
	}
	
	/*
	 * Method for adding a specific AI pattern to this char, it will be used incase computer should give commands to it
	 * Inputs:
	 * 	AIPatterns: AI char pattern element
	 */
	public void attachAIPattern(AICharPatterns AIPatterns) {
		this.AIPatterns = AIPatterns;
	}
	
	/*
	 * Checks if the field char has an AI pattern attached to it
	 */
	public boolean hasAIPatterns() {
		return this.AIPatterns != null;
	}
	
	/*
	 * Getter for the AI patterns on this element
	 */
	public AICharPatterns getAIPattern() {
		return this.AIPatterns;
	}
	
	//Checks if this field character has a label
	public boolean hasLabel() {
		return this.nameLabel != null;
	}
	
	/*
	 * Makes a label for this field character according to master characters name
	 */
	public void makeLabel() {
		this.nameLabel = mainFactory.getElemFactory().getText(this.master.getName());
		this.nameLabel.setWrapper(this);
		this.nameLabel.setCoordinates(20, 20);
	}
	
	/*
	 * Method that moves character on a field with a travelPath
	 * Inputs:
	 * 	path: travel path that represents characters movement
	 */
	public void moveByPath(TravelPath path) {
		while(path.getPath().size() > 1) {
			if(takeAStep(path.getStart(), path.getFirstStep())) {
				path.updatePathForFirstStepTaken();
			}
		}
	}
	
	/*
	 * Method that moves the character by one square
	 * Inputs:
	 * 	currentLocation: square that the character currently stands on
	 * 	nextLocation: square that the character will move to
	 */
	private boolean takeAStep(MovementSquare currentLocation, MovementSquare nextLocation) {
		PropertyBar movementBar = this.master.getPropertyBar(PropertyBarName.Movement);
		if(currentLocation.equals(this.location) && movementBar.getCurrentValue() > 0) {
			currentLocation.moveCharacterAway();
			nextLocation.moveCharacterHere(this);
			movementBar.decreaseByValue(1);
			StaticFunctions.writeOutDebug(debug, "Did a step from:" + currentLocation.printOutCoordinates() + ", to:" + nextLocation.printOutCoordinates());
			return true;
		}
		return false;
	}
	
	//Getter for master character
	public IGameCharacter getMasterCharacter() {
		return this.master;
	}
	
	//Getter for item bag (chars personal inventory)
	public ItemBag getItemBag() {
		return this.itemBag;
	}
	
	//Getter for the characters control faction
	public ControlFaction getFaction() {
		return this.controlFaction;
	}
	
	//Checker for visual health bar
	public boolean hasHealthBar() {
		return this.healthBar != null;
	}
	
	//Getter for visual health bar
	public ScreenBar getHealthBar() {
		return this.healthBar;
	}
	
	//Method that checks if this character is still alive
	public void deathCheck() {
		PropertyBar healthBar = this.master.getPropertyBar(PropertyBarName.Health);
		if(healthBar.getCurrentValue() < 1) {
			StaticFunctions.writeOutDebug(debug, "Char is dead");
			this.despawn();
		}
	}
	
	//command that will despawn this field character
	public void despawn() {
		this.location.moveCharacterAway();
		this.location = null;
	}
	
	@Override
	public void fillTopElementList(List<IStackable> list) {
		super.fillTopElementList(list);
		list.add(healthBar);
		
	}
	
	@Override
	public void fillTopTextList(List<ScreenText> list) {
		if(this.hasLabel()) {
			list.add(nameLabel);
		}
	}

}

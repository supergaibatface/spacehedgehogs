package screenInfoPage;

import java.util.ArrayList;
import java.util.List;

import frameworks.TextFrame;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import propertyBarListeners.UpdateInfoPageHp;
import propertyBarListeners.UpdateInfoPageStamina;
import propertyBars.PropertyBar;
import propertyBars.PropertyBarName;
import screenElements.ScreenButton;
import screenElements.ScreenScrollPanel;
import screenElements.ScreenSoloImage;
import screenElements.ScreenText;
import skillListeners.UpdateInfoPageSkills;
import skills.Skill;

//Info page for screen character type of elements
public class ScreenCharInfoPage extends BaseInfoPage {
	private IGameCharacter master;
	private ScreenSoloImage charProfilePic;
	private UpdateInfoPageSkills skillUpdateListener;
	private TextFrame charSkills;
	private TextFrame hpText;
	private UpdateInfoPageHp hpInfoUpdateListener;
	private TextFrame staminaText;
	private UpdateInfoPageStamina staminaInfoUpdateListener;
	private TextFrame moveAndAttackText;

	//constructor, input is the element represented by this info page
	public ScreenCharInfoPage(IGameCharacter master) {
		super(1000, 900);
		this.master = master;
	}
	
	public void bootUp() {
		super.bootUp();
		this.setCoordinates(200, 50);
		this.addCharProfilePic();
		this.makeSkillTextFrame();
		this.addHpText();
		this.addStaminaText();
		this.addMoveAndAttackText();
	}

	@Override
	protected void setLocationForCloseButton(ScreenButton closeButton) {
		closeButton.setCoordinates(950, 20);
		
	}
	
	protected void setLocationForNameField(TextFrame nameField) {
		nameField.setLocation(50, 80);
	}
	
	protected void writeNameFieldLines(List<String> lines) {
		lines.add("Name: "+this.master.getName());
	}
	
	//Adds a picture of the character onto the info page
	private void addCharProfilePic() {
		this.charProfilePic = this.mainFactory.getElemFactory().getSoloImage(300, 300, this.master.getImgName());
		this.charProfilePic.setCoordinates(600, 80);
		this.addNewElement(this.charProfilePic);
	}
	
	//Adds text about char hp to the info page
	private void addHpText() {
		this.hpText = this.mainFactory.getElemFactory().getTextFrame(this);
		this.hpText.setLocation(50, 120);
		this.writeHpText();
		this.addNewElement(hpText);
		this.setUpHpUpdateListener();
	}
	
	//Writes the correct hp text into the hpText textFrame element of this page
	private void writeHpText() {
		this.hpText.fillTexts(collectBarText(this.master.getPropertyBar(PropertyBarName.Health), "Health"));
	}
	
	//Adds text about stamina to the info page
	private void addStaminaText() {
		this.staminaText = this.mainFactory.getElemFactory().getTextFrame(this);
		this.staminaText.setLocation(50, 140);
		this.writeStaminaText();
		this.addNewElement(staminaText);
		this.setUpStamineUpdateListener();
	}
	
	//Writes correct stamina text into the staminaText textFrame element of this page
	private void writeStaminaText() {
		this.staminaText.fillTexts(collectBarText(this.master.getPropertyBar(PropertyBarName.Stamina), "Stamina"));
	}
	
	//Adds a text element that shows move and attack nr texts
	private void addMoveAndAttackText() {
		this.moveAndAttackText = this.mainFactory.getElemFactory().getTextFrame(this);
		this.moveAndAttackText.setLocation(50, 180);
		this.writeMoveAndAttackText();
		this.addNewElement(moveAndAttackText);
		//still needs a type of listener for when max attacks and moves can change
	}
	
	//Writes text values into move and attack textFrame
	private void writeMoveAndAttackText() {
		this.moveAndAttackText.fillTexts(this.getMoveAndAttackText());
	}
	
	/*
	 * Collects a specific bars value text into a list
	 * Input:
	 * 	property: property bar thats values will be displayed
	 * 	frontText: text added infront of the value representation
	 */
	private List<String> collectBarText(PropertyBar property, String frontText) {
		List<String> lines = new ArrayList<String>();
		lines.add(frontText+ ": "+property.getCurrentValue()+"/"+property.getMaxValue());
		return lines;
	}
	
	//Fetches the move and attack text values
	private List<String> getMoveAndAttackText() {
		List<String> lines = new ArrayList<String>();
		lines.add("Moves per turn: "+this.master.getPropertyBar(PropertyBarName.Movement).getMaxValue());
		lines.add("Attacks per turn: "+this.master.getPropertyBar(PropertyBarName.Attacks).getMaxValue());
		return lines;
	}
	
	//Makes a text frame for skills
	private void makeSkillTextFrame() {
		this.charSkills = this.mainFactory.getElemFactory().getTextFrame(this);
		this.charSkills.setLocation(50, 240);
		this.updateSkills();
		this.addNewElement(charSkills);
		this.setUpSkilltextFrameUpdateListeners();
	}
	
	//sets up listeners that will change skill text frame content when needed
	private void setUpSkilltextFrameUpdateListeners() {
		this.skillUpdateListener = this.mainFactory.getElemFactory().getUpdateInfoPageSkills(this);
		for(Skill oneSkill : this.master.getAllSkill()) {
			oneSkill.addListener(skillUpdateListener);
		}
	}
	
	//sets up listener that updates hp value on this infoPage
	private void setUpHpUpdateListener() {
		this.hpInfoUpdateListener = this.mainFactory.getElemFactory().getUpdateInfoPageHp(this);
		this.master.getPropertyBar(PropertyBarName.Health).addListener(hpInfoUpdateListener);
	}
	
	//sets up listener that updates stamina value on this infoPage
	private void setUpStamineUpdateListener() {
		this.staminaInfoUpdateListener = this.mainFactory.getElemFactory().getUpdateInfoPageStamina(this);
		this.master.getPropertyBar(PropertyBarName.Stamina).addListener(staminaInfoUpdateListener);
	}
	
	//Public method for updating text values of skills displayed on this page
	public void callUpdateForSkills() {
		this.updateSkills();
	}
	
	//private method that updates the actual skill text values
	private void updateSkills() {
		this.charSkills.fillTexts(this.makeLinesAboutSkills(master));
	}

	//returns list of lines that describe the characters current skills
	public List<String> makeLinesAboutSkills(IGameCharacter character) {
		List<String> lines = new ArrayList<String>();
		for(Skill oneSkill : character.getAllSkill()) {
			lines.add(oneSkill.getName().toString()+", level: " + oneSkill.getLevel()+", experience: " + oneSkill.getExp());
		}
		return lines;
	}
	
	//Public method for calling for an update of hp text values
	public void callUpdateForHp() {
		this.writeHpText();
	}
	
	//Public method for calling for an update of stamina text values
	public void callUpdateForStamina() {
		this.writeStaminaText();
	}
	

}

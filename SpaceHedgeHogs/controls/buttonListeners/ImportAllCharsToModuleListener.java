package buttonListeners;

import java.util.ArrayList;

import dataValues.CharValue;
import gameState.IGameState;

//Imports characters to a new module from CharacterManager
public class ImportAllCharsToModuleListener extends BaseButtonListener {
	private IGameState gameState;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	gameState: game state where the characters will be imported to
	 */
	public ImportAllCharsToModuleListener(IGameState gameState) {
		this.gameState = gameState;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.gameState.addExtraCharValues(this.mainFactory.getCharacterManager().makeCharValuesOutOfAll());
		
	}

	@Override
	public String toInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

package ingredientItemBase;

import battleItemBase.BattleItemBase;
import itemBase.ItemTagName;
import itemBase.SingleStackItemBase;
import itemBase.TradeItemName;
import screenInfoPage.ItemInfoPage;

public abstract class IngredientItemBase extends SingleStackItemBase {

	public IngredientItemBase(TradeItemName name) {
		super(name);
	}

	@Override
	protected ItemInfoPage bootGiveInfoPage() {
		return this.mainFactory.getElemFactory().getItemInfoPage(this, "Orange");
	}
	
	public void bootUp() {
		super.bootUp();
		this.addTags();
	}
	
	private void addTags() {
		addAnotherItemTag(ItemTagName.Ingredient);
	}


}

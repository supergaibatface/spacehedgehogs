package loupeNpcs;

import dialogueCharacters.BaseDialogueCharacter;
import dialogueFlags.DialogueFlag;
import view.CutsceneData;
import view.CutsceneDataCollection;

public class LoupeArmoryChief extends BaseDialogueCharacter {
	private CutsceneDataCollection defaultConversation;
	private CutsceneDataCollection questConversation;

	@Override
	protected void getCurrentDialogueInstance() {
		if(this.flagStand.checkFlagState(DialogueFlag.LoupeMainPartyPersonalQuests)) {
			this.mainFactory.getCutsceneBuilder().startACutscene(this.questConversation);
			this.flagStand.takeDownFlag(DialogueFlag.LoupeMainPartyPersonalQuests);
			return;
		}
		this.mainFactory.getCutsceneBuilder().startACutscene(defaultConversation);
		
	}

	@Override
	protected boolean hasAllowedDialogue() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected String giveNameToChar() {
		return "Armory chief";
	}
	
	public void bootUp() {
		super.bootUp();
		this.defaultConversation = this.makeStandardResponse();
		this.questConversation = this.makeQuestDialogue();
	}
	
	private CutsceneDataCollection makeStandardResponse() {
		CutsceneDataCollection cusceneBase = this.getNewDialogueBaseCutscene("response to quest");
		CutsceneData newFrame = new CutsceneData("TestBG", "Do you have bussiness in the armory?");
		cusceneBase.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "Just looking arounnd.");
		cusceneBase.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "Sure is nice having spare time.");
		cusceneBase.addNewCutScene(newFrame);
		return cusceneBase;
	}
	
	private CutsceneDataCollection makeQuestDialogue() {
		CutsceneDataCollection cusceneBase = this.getNewDialogueBaseCutscene("response to quest");
		CutsceneData newFrame = new CutsceneData("TestBG", "Do you have bussiness in the armory?");
		cusceneBase.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "Yes, we are planning on leaving the fort but we could use some arms out there and im certain you have more then enough here, perhaps we could make a deal?");
		cusceneBase.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "Hmm, the fort captain has already given out the spare arsenal for previous guards who wanted to 'leave' but i still got plenty of the old stash, older then me");
		cusceneBase.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "We have had personel shortages since before they stationed me here, so these ones haven't had the maintenance they need, they might look bad but they just need some elbow grease");
		cusceneBase.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "There is a blacksmithy in the fort, so if you got the people, you can fix these weapons up, or scrap them for all i care.");
		cusceneBase.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "Do what you want, id rather not think about you guys walking out and getting killed by the first beast you encounter.");
		cusceneBase.addNewCutScene(newFrame);
		return cusceneBase;
	}

}

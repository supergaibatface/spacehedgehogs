package craftingBluePrints;

import java.util.List;

import itemBase.IGameItem;

public class SwordBlueprint extends BaseBlueprint {

	public SwordBlueprint() {
		super("sword making");
	}

	@Override
	protected int bootAssignNeccesaryWorkValue() {
		return 20;
	}

	@Override
	protected IGameItem bootGiveTargetValue() {
		return this.mainFactory.getItemFactory().getSwordEquipItem();
	}

	@Override
	protected int bootGiveTargetValueAmount() {
		return 1;
	}

	@Override
	protected void bootAddCraftingComponents(List<IGameItem> saveLocation) {
		IGameItem componentItem = this.mainFactory.getItemFactory().getBrokenSwordIngredientItem();
		componentItem.addAmount(1);
		saveLocation.add(componentItem);
	}

}

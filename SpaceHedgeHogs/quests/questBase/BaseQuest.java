package questBase;

import java.util.ArrayList;
import java.util.List;

import factories.IAutoMake;
import factories.MainFactory;
import questStepBase.IQuestStep;
import screenInfoPage.QuestInfoPage;

public abstract class BaseQuest implements IAutoMake, IQuest {
	protected MainFactory mainFactory;
	protected String questName;
	protected boolean active;
	protected QuestInfoPage infoPage;
	protected List<IQuestStep> questSteps;

	@Override
	public String getName() {
		return this.questName;
	}
	
	protected abstract String returnQuestName();

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.questName = this.returnQuestName();
		this.active = false;
		this.makeQuestStepList();
		this.makeInfoPage();
	}
	
	protected void makeQuestStepList() {
		this.questSteps = new ArrayList<IQuestStep>();
		this.addQuestSteps(questSteps);
	}
	
	protected void makeInfoPage() {
		this.infoPage = this.mainFactory.getElemFactory().getQuestInfoPage(this, "Orange");
	}
	
	@Override
	public boolean isCompleted() {
		return this.checkStepCompletion();
	}
	
	protected boolean checkStepCompletion() {
		return findCurrentStep() == null;
	}
	
	@Override
	public IQuestStep getCurrentQuestStep() {
		return this.findCurrentStep();
	}
	
	protected IQuestStep findCurrentStep() {
		for(IQuestStep oneStep : this.questSteps) {
			if(!oneStep.isCompleted()) {
				return oneStep;
			}
		}
		return null;
	}
	
	@Override
	public boolean isActive() {
		return this.active;
	}
	
	@Override
	public void setActive(boolean newValue) {
		this.active = newValue;
	}
	
	@Override
	public QuestInfoPage getQuestInfoPage() {
		return this.infoPage;
	}
	
	protected abstract void addQuestSteps(List<IQuestStep> questStepList);
	
	public List<IQuestStep> getQuestSteps() {
		return this.questSteps;
	}
	
	public List<IQuestStep> getVisibleQuestSteps() {
		List<IQuestStep> newList = new ArrayList<IQuestStep>();
		for(IQuestStep oneStep : this.questSteps) {
			newList.add(oneStep);
			if(!oneStep.isCompleted()) {
				break;
			}
		}
		return newList;
	}
	
	@Override
	public void recieveQuestStepProgressChange(IQuestStep sourceStep) {
		if(!this.hasStep(sourceStep)) {
			return;
		}
		this.updateInfoPage();
	}
	
	public void updateInfoPage() {
		this.infoPage.callDataUpdate();
	}
	
	public boolean hasStep(IQuestStep questStep) {
		if(this.questSteps.contains(questStep)) {
			return true;
		}
		return false;
	}

}

package generalRNG;

//class that represent the lottery tickets of a single object
public class LotteryTicket {
	private Object ticketHolder;
	private int startNr;
	private int amount;
	
	/*
	 * constructor
	 * Input:
	 * 	holder: object that holds these tickets
	 * 	start: start number that depends on the current contents of the lottery box
	 * 	amount: the nr of tickets assigned to the holder object
	 */
	public LotteryTicket(Object holder, int start, int amount) {
		this.ticketHolder = holder;
		this.startNr = start;
		this.amount = amount;
	}
	
	//get location nr for the last ticket of this holders set
	public int getLastThisNr() {
		return getNumberAfterThis() - 1;
	}
	
	//get the first number that is after this holders tickets
	public int getNumberAfterThis() {
		return startNr + amount;
	}
	
	//getter for the holder object
	public Object getHolder() {
		return this.ticketHolder;
	}
	
	//getter for the first tickets number in this batch
	public int getStartNr() {
		return this.startNr;
	}
	
	//checks if input number is one of the ticket numbers that belong to this object
	public boolean isItThisNr(int nr) {
		if(nr < this.startNr) {
			return false;
		}
		if(nr > this.getLastThisNr()) {
			return false;
		}
		return true;
	}
	
	//debug print function
	public void printOutTicketValue() {
		System.out.println("Ticket for "+this.ticketHolder);
		System.out.println("*Starts on "+this.startNr);
		System.out.println("*Ends on "+this.getLastThisNr());
	}

}

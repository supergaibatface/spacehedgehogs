package buttonListeners;

import screenElements.ScreenElement;

public class OpenInfoSelectionListener extends BaseButtonListener {
	private ScreenElement masterElement;
	private String labelText;
	
	public OpenInfoSelectionListener(ScreenElement masterElement, String labelText) {
		this.masterElement = masterElement;
		this.labelText = labelText;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.mainFactory.getExtraCommandsDataboxController().openBoxThroughButton(evt.getButton(), masterElement, labelText);
	}

	@Override
	public String toInfo() {
		return "Opens extra commands box of "+masterElement.getId();
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	public void setLabelText(String newText) {
		this.labelText = newText;
	}

}

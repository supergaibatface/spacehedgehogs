package buttonListeners;

import land.LandArea;

//Button listener that will change the chosen land area to a specific area when triggered
public class LandAreaChangeListener extends BaseButtonListener {
	private LandArea landArea;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	landArea: the land area that will be made active when the listener is triggered
	 */
	public LandAreaChangeListener(LandArea landArea) {
		this.landArea = landArea;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.mainFactory.getTravelManager().setSelectedArea(landArea);
		
	}

	@Override
	public String toInfo() {
		return "caravan will travel on: " + landArea.toString();
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

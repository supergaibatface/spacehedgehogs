package itemChange;

import screenInfoPage.ItemBasePage;

//Listener that updates info pages item amount section of changes
public class ItemInfoPageAmountListener extends BaseItemListener {
	private ItemBasePage itemInfoPage;
	
	//constructor, input is the info page that will be updated
	public ItemInfoPageAmountListener(ItemBasePage infoPage) {
		this.itemInfoPage = infoPage;
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected boolean reactCheck(ItemChangeEvent evt) {
		if(!evt.isAmountChanged()) {
			return false;
		}
		return true;
	}

	@Override
	public void changeReacting(ItemChangeEvent evt) {
		this.itemInfoPage.itemAmountChangeCall();;
		
	}

}

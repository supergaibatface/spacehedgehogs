package newJobs;

import java.util.ArrayList;
import java.util.List;

import barChange.BarLimiter;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import screenElements.ScreenButton;
import sreenElementsSockets.ScrollCharSocket;

//interface for all new type of jobs
public interface IJob {
	
	//checks if there is a stamina limiter on the job
	public boolean hasStamineLimiter();
	
	//getter for the jobs stamina limiter
	public BarLimiter getStaminaLimiter();
	
	//method for making a list of characters go through the jobs work, input is the list
	public void workThrough(List<IGameCharacter> characters);
	
	//For doing a full requirements check on a character, input is the character, output is boolean if the char passed the requirements
	public boolean fullReqCheck(IGameCharacter gameChar);
	
	//script for when a new element is added to the jobs socket
	public void newElementAdded(IGameCharacter newChar);
	
	//script for when an old element is removed from the jobs socket
	public void oldElementRemoved(IGameCharacter oldChar);
	
	//method that is called when a job is assigned to the socket, this informs the job of that happening
	public void registerSocketForJob(ScrollCharSocket socket);
	
	//method for deleting the job, its called when its socket gets deleted
	public void safeDelete();
	
	public void socketGoingOnScreenScriptPoint();
	
	public List<ScreenButton> getExtraCommandButtons();
}

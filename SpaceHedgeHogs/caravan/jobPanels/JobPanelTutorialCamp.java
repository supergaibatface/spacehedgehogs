package jobPanels;

import java.util.ArrayList;
import java.util.List;

import jobs.JobTopic;

public class JobPanelTutorialCamp extends BaseJobPanel {

	@Override
	protected int getJobPanelLength() {
		return 1400;
	}

	@Override
	protected int getOneTopicHeight() {
		return 500;
	}

	@Override
	protected int getBufferSpaceSize() {
		return  10;
	}
	
	protected void fillInPassiveJobTopic(JobTopic saveLocation) {
		saveLocation.AddNewSocket(this.getNewStandardSocketForJob(this.mainFactory.getElemFactory().getWaterSearchJob(), "water search"));
		saveLocation.AddNewSocket(this.getNewStandardSocketForJob(this.mainFactory.getElemFactory().getFoodSearchJob(), "food search"));
		saveLocation.AddNewSocket(this.getNewStandardSocketForJob(this.mainFactory.getElemFactory().getRestJob(), "resting"));
	}

}

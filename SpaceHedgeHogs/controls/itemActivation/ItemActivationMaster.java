package itemActivation;

import combatFieldElements.FieldCharacter;
import combatFieldElements.MovementSquare;
import factories.IAutoMake;
import factories.MainFactory;
import itemBase.IGameItem;
import itemBase.ItemTagName;

//Class that deals with activating items in battle
public class ItemActivationMaster implements IAutoMake {
	private MainFactory mainFactory;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	/*
	 * Method that will either spawn the item on the field or give it to a field character
	 * Inputs: 
	 * 	activatedItem: item that will be activated
	 * 	location: location where the activation will take place
	 */
	public void ActivateItem(IGameItem activatedItem, MovementSquare location) {
		if(activatedItem.hasTag(ItemTagName.FieldPlacement)) {
			location.SpawnItem(activatedItem);
		}
		else {
			addingToItemBag(activatedItem, location);
		}
	}
	
	/*
	 * Script for giving an item to a character
	 */
	private void addingToItemBag(IGameItem selectedItem, MovementSquare location) {
		FieldCharacter targetChar = (FieldCharacter) location.getFieldElement();
		targetChar.getItemBag().addTradeItemAmount(selectedItem);
	}
	

}

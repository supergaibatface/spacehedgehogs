package fortLoupe;

import java.util.List;

import dialogueCharacters.IDialogueCharacter;
import party.PartyStance;
import settlementLocation.BaseSettlementLocation;
import view.ViewName;

public class LoupeCamp extends BaseSettlementLocation {

	public LoupeCamp() {
		super("Camp");
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void clickScript() {
		this.mainFactory.getViewManager().changeToView(ViewName.LoupeCamp);
		this.mainFactory.getParty().setPartyStance(PartyStance.Camp);
	}

	@Override
	protected int giveButtonX() {
		return 500;
	}

	@Override
	protected int giveButtonY() {
		return 100;
	}

	@Override
	protected int giveButtonLength() {
		return 100;
	}

	@Override
	protected int giveButtonHeight() {
		return 100;
	}

	@Override
	protected String giveButtonImgName() {
		return "100X100Mark";
	}

	@Override
	protected String giveButtonLabel() {
		return "Camp";
	}

	@Override
	protected void setUpLocationCharacters(List<IDialogueCharacter> addLocation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected boolean giveStartAccessabilityValue() {
		return false;
	}

}

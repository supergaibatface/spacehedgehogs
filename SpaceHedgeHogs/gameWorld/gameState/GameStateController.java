package gameState;

import java.util.ArrayList;

import battleManagement.BattleController;
import central.AICharacterSet;
import combatFieldElements.MovementField;
import factories.IAutoMake;
import factories.MainFactory;
import gameCharacters.CharacterManager;
import gameCharacters.GameCharacter;
import generalGraphics.ButtonLookType;
import graphicalWrapping.CharSpawnInfo;
import jobChoiceController.JobChoiceController;
import screenElements.ScreenButton;
import screenElements.ScreenPopUp;
import spawning.ISpawnTimeModule;
import sreenElementsSockets.ScreenSimpleChar;
import sreenElementsSockets.ScrollCharSocket;
import time.WorldTime;
import view.CutsceneData;
import view.CutsceneDataCollection;
import view.GameView;
import view.IGameView;
import view.ViewName;

//Class that manages game states and acts as the central hub for loading in a gameState
public class GameStateController implements IAutoMake{
	private MainFactory mainFactory;
	private IGameState currentGameState;
	private BattleController battleController;
	private ArrayList<AICharacterSet> enemyList;
	private ArrayList<ScreenSimpleChar> idleList;
	private ScrollCharSocket registeredIdleSocket;
	//private JobChoiceController jobChoiceController;
	private boolean battleTutorialFlag = false;
	
	//Constructor
	public GameStateController() {
		
	}
	
	//Boot up method, meant for starting up the object after creation
	public void bootUp() {
		this.enemyList = new ArrayList<AICharacterSet>();
		this.idleList = new ArrayList<ScreenSimpleChar>();
	}
	
	//For sending a screenSimpleChar to currently designated idle location
	public void addCharacterToIdleList(ScreenSimpleChar charAvatar) {
		if(this.hasRegisteredIdleSocket()) {
			this.registeredIdleSocket.addNewElement(charAvatar);
		}
		else {
			this.idleList.add(charAvatar);
		}
	}
	
	//For adding a whole array of chars to designated idle location
	public void addCharListToIdleList(ArrayList<ScreenSimpleChar> listOfAvatars) {
		if(this.hasRegisteredIdleSocket()) {
			for(ScreenSimpleChar oneChar : listOfAvatars) {
				this.registeredIdleSocket.addNewElement(oneChar);
			}
		}
		else {
			this.idleList.addAll(listOfAvatars);
		}
	}
	
	//for setting a socket as the new idle location
	public void registerSocketAsIdleSocket(ScrollCharSocket newSocket) {
		this.registeredIdleSocket = newSocket;
	}
	
	//for unregistereing a specific socket as an idle socket, only works if input socket matches actual registered socket
	public void unregisterThisSocket(ScrollCharSocket oldSocket) {
		if(this.registeredIdleSocket.equals(oldSocket)) {
			this.registeredIdleSocket = null;
		}
	}
	
	//Checks if there is a socket registered as idle location
	public boolean hasRegisteredIdleSocket() {
		return this.registeredIdleSocket != null;
	}
	
	//Public method for adding a enemy to the stack for the next fight
	public void addEnemysForAFight(AICharacterSet newEnemy) {
		this.enemyList.add(newEnemy);
	}
	
	//Public method for adding multiple enemies to the stack for the next fight
	public void addEnemysForAFight(ArrayList<AICharacterSet> newEnemies) {
		this.enemyList.addAll(newEnemies);
	}
	
	//Checks if a fight needs to be called
	public boolean needsBattle() {
		return !this.enemyList.isEmpty();
	}
	
	//setter for main factory object
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
	}
	
	//Method for loading in the default view of the current game state
	public void loadInTravelView() {
		this.currentGameState.startUpTravelView();
	}
	
	//Method for loading in main menu
	public void bootUpMenu() {
		this.mainFactory.getViewManager().changeToMenu();
	}
	
	//Checks if the player is in battle right now
	public boolean inBattle() {
		return this.battleController!=null;
	}
	
	//Method for starting a battle
	public boolean startBattle() {
		if(!this.needsBattle()) {
			return false;
		}
		if(!battleTutorialBranch()) {
			return false;
		}
		this.battleController = this.mainFactory.getElemFactory().getBattleController();
		sendEnemiesToController();
		changeToBattleView();
		this.battleController.setUpAIForBattle(this.mainFactory.getElemFactory().getTimeSpawnEveryTurnOne());
		this.battleController.doPreTurnAction(1);
		
		return true;
	}
	
	//Method for starting a battle
	public boolean startBattle(MovementField battleField, int preTurnAction, ISpawnTimeModule spawnTimeModule) {
		if(!this.needsBattle()) {
			return false;
		}
		if(!battleTutorialBranch()) {
			return false;
		}
		this.battleController = this.mainFactory.getElemFactory().getBattleController();
		this.battleController.setBattleField(battleField);
		sendEnemiesToController();
		changeToBattleView();
		this.battleController.setUpAIForBattle(spawnTimeModule);
		this.battleController.doPreTurnAction(preTurnAction);
		
		return true;
	}
	
	//For ending a battle
	public boolean endBattle() {
		if(!this.inBattle()) {
			return false;
		}
		this.mainFactory.getActivityController().makeInactive();
		this.mainFactory.getTurnController().changeToTravelTime();
		this.deleteBattleController();
		this.loadInTravelView();
		
		return true;
	}
	
	//deletes battle controller
	private void deleteBattleController() {
		this.battleController = null;
	}
	
	
	//Code branch that trigger tutorial cutscene if its needed
	private boolean battleTutorialBranch() {
		if(!this.battleTutorialFlag) {
			this.mainFactory.getCutsceneBuilder().startACutscene(this.makeBattleTutorialCutscene());
			this.battleTutorialFlag = true;
			return false;
		}
		return true;
	}
	
	//sets flag for triggering battle tutorial cutscene
	public void setBattleTutorialFlag(boolean value) {
		this.battleTutorialFlag = value;
	}
	
	//Sends stacked enemies to a battleController
	private void sendEnemiesToController() {
		for(AICharacterSet oneEnemy : this.enemyList) {
			this.battleController.addEnemy(oneEnemy);
		}
		this.enemyList.clear();
	}
	
	//Changes the actual view to designated battle view
	private void changeToBattleView() {
		IGameView battleView = this.currentGameState.getStandardBattleView();
		this.mainFactory.getViewManager().changeToView(battleView);
		this.mainFactory.getTurnController().changeToBattleTime();
		if(battleView.hasScrollableMainScreen()) {
			battleView.getScrollableMainScreen().addNewElement(this.battleController.getMovementField());
		}
		else {
			//this.mainFactory.getGElementManager().setMovementField(this.battleController.getMovementField());
		}
	}
	
	//Method for returning from battle view
	public void returnFromBattle() {
		this.currentGameState.startUpTravelView();
	}
	
	//Makes an end game popup that will direct player back to main menu
	public ScreenPopUp makeAndGetEndGamePopUp(String popUpMessage, String buttonMessage, String popUpImage) {
		ScreenPopUp endPopUp = this.mainFactory.getElemFactory().getScreenPopUp("50red");
		endPopUp.setEventImage(popUpImage);
		endPopUp.fillTextOneLine(popUpMessage);
		endPopUp.addButton(this.makeEndGameButton(buttonMessage));
		endPopUp.autoSize();
		return endPopUp;
	}
	
	//makes an end game button that will be placed on an end game pop up and will lead player back to main menu
	private ScreenButton makeEndGameButton(String message) {
		ScreenButton endButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.POPUPSELECT, message);
		endButton.addButtonEventListener(this.mainFactory.getElemFactory().getClearTravelHistoryListener());
		endButton.addButtonEventListener(this.mainFactory.getElemFactory().getViewChangeListener(ViewName.MainMenu));
		return endButton;
	}
	
	//Boots up input gameState
	public void bootUpGameState(IGameState newState) {
		if(this.currentGameState != null) {
			this.clearGameState();
		}
		this.currentGameState = newState;
		newState.loadInDetails();
	}
	
	//For deleting current game state and everything it set up
	public void clearGameState() {
		if(this.inBattle()) {
			this.endBattle();
		}
		this.mainFactory.getTravelManager().clearTravelHistory();
		this.mainFactory.getCharacterManager().clearCharacterValues();
		this.mainFactory.getInventory().clearInventory();
		this.mainFactory.getViewManager().clearSavedViews();
		this.idleList.clear();
	}
	
	//getter for battle controller
	public BattleController getBattleController() {
		return this.battleController;
	}
	
	//Makes battle tutorial cutscene
	private CutsceneDataCollection makeBattleTutorialCutscene() {
		CutsceneDataCollection tutorial = this.mainFactory.getElemFactory().getCutsceneDataCollection("BattleTutorial");
		CutsceneData newFrame = new CutsceneData("travelBG", "Welcome to your first battle");
		tutorial.addNewCutScene(newFrame);
		tutorial.addFinalButtonEventListener(this.mainFactory.getElemFactory().getBattleStartListener());
		return tutorial;
		
	}
	
	//getter for idle list, list is backup for when there is no registered idle socket
	public ArrayList<ScreenSimpleChar> getIdleList() {
		return this.idleList;
	}
	

}

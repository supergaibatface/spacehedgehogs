package buttonListeners;

import situationChange.ISituationChange;

public class GoSituationChangeListener extends BaseButtonListener {
	private ISituationChange situationChangeObject;
	
	public GoSituationChangeListener(ISituationChange situationChange) {
		this.situationChangeObject = situationChange;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.situationChangeObject.goThroughSituationChange();
		
	}

	@Override
	public String toInfo() {
		return "Situation changes";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

package dialogueCharacters;

import java.util.ArrayList;
import java.util.List;

import dialogueFlags.DialogueFlag;
import dialogueFlags.IDialogueFlagStand;
import factories.IAutoMake;
import factories.MainFactory;
import npcListeners.INpcListener;
import view.CutsceneDataCollection;

public abstract class BaseDialogueCharacter implements IAutoMake, IDialogueCharacter {
	protected MainFactory mainFactory;
	protected String name;
	protected List<INpcListener> listeners;
	protected IDialogueFlagStand flagStand;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.name = this.giveNameToChar();
		this.listeners = new ArrayList<INpcListener>();
		this.bootDialogueFlagStand();
	}
	
	protected void bootDialogueFlagStand() {
		this.flagStand = this.mainFactory.getNPCFactory().getNewDialogueFlagStand();
	}

	@Override
	public boolean startDialogue() {
		this.getCurrentDialogueInstance();
		return true;
	}
	
	protected abstract void getCurrentDialogueInstance();

	@Override
	public boolean canStartDialogue() {
		return this.hasAllowedDialogue();
	}
	
	protected abstract boolean hasAllowedDialogue();
	
	public String getName() {
		return this.name;
	}
	
	protected abstract String giveNameToChar();
	
	public void finishedDialogue(CutsceneDataCollection finishedDialogue) {
		System.out.println("got a finish call for a dialogue instance");
		this.informListenersOfDialogueEnd(finishedDialogue);
	}
	
	protected CutsceneDataCollection getNewDialogueBaseCutscene(String name) {
		CutsceneDataCollection cutsceneBase = this.mainFactory.getElemFactory().getCutsceneDataCollection(name);
		cutsceneBase.addFinalButtonEventListener(this.mainFactory.getElemFactory().getDialogueFinishListener(cutsceneBase, this));
		return cutsceneBase;
	}
	
	public boolean addNewListener(INpcListener newListener) {
		this.listeners.add(newListener);
		return true;
	}
	
	public boolean removeOldListener(INpcListener oldListener) {
		this.listeners.remove(oldListener);
		return true;
	}
	
	protected void informListenersOfDialogueEnd(CutsceneDataCollection finishedDialogue) {
		for(INpcListener oneListener : this.listeners) {
			oneListener.callEndOfADialogue(finishedDialogue);
		}
	}
	
	public boolean sendDialogueFlag(DialogueFlag flag) {
		System.out.println("Raising flag "+flag+" for dialogue character named: "+this.name);
		return this.flagStand.raiseFlag(flag);
	}

}

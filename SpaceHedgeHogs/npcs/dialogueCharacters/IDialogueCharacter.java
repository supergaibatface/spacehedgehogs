package dialogueCharacters;

import dialogueFlags.DialogueFlag;
import npcListeners.INpcListener;
import view.CutsceneDataCollection;

public interface IDialogueCharacter {
	
	public boolean startDialogue();
	
	public boolean canStartDialogue();
	
	public String getName();
	
	public void finishedDialogue(CutsceneDataCollection finishedDialogue);
	
	public boolean addNewListener(INpcListener newListener);
	
	public boolean removeOldListener(INpcListener oldListener);
	
	public boolean sendDialogueFlag(DialogueFlag flag);

}

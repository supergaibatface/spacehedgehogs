package propertyBarListeners;

import screenElements.ScreenBar;

//Listener that updates a screenBar value when the logical bar value changes
public class ScreenBarValueUpdater extends BaseBarChangeListener {
	private ScreenBar barToUpdate;
	
	public ScreenBarValueUpdater(ScreenBar barToUpdate) {
		this.barToUpdate = barToUpdate;
	}

	@Override
	public void barChangeHappened(int newValue) {
		this.barToUpdate.updateBarValue();
		
	}

}

package loupeNpcs;

import dialogueCharacters.BaseDialogueCharacter;
import view.CutsceneData;
import view.CutsceneDataCollection;
import view.ViewName;

public class LoupeGateGuard extends BaseDialogueCharacter {
	private CutsceneDataCollection introductions;
	
	public void bootUp() {
		super.bootUp();
		this.introductions = this.makeStartCutscene();
	}

	@Override
	protected void getCurrentDialogueInstance() {
		this.mainFactory.getCutsceneBuilder().startACutscene(introductions);
		
	}

	@Override
	protected boolean hasAllowedDialogue() {
		return true;
	}
	
	protected String giveNameToChar() {
		return "Gate guard";
	}
	
	public void setNewName(String newName) {
		this.name = newName;
	}
	
	private CutsceneDataCollection makeStartCutscene() {
		CutsceneDataCollection intoduction = this.getNewDialogueBaseCutscene("Loupe gateGuard intorduction");
		CutsceneData newFrame = new CutsceneData("travelBG", "The gate infront of you is closed.");
		intoduction.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "You dont see any men on the walls");
		intoduction.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "You hammer at the gate door hoping for a response");
		intoduction.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "Nothing....");
		intoduction.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "You yell and try knocking again");
		intoduction.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "Who, goes there?");
		intoduction.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "Finally! someone is actually there.");
		intoduction.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "We are guards from the capital looking for safe harbor.");
		intoduction.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "Wait there!");
		intoduction.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "This wait ended up being rather long. but then the gate lock clicked and the door opened.");
		intoduction.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "Come in, dont cause trouble and go talk to the capitain, he is in the keep.");
		intoduction.addNewCutScene(newFrame);
		//intoduction.addFinalButtonEventListener(this.mainFactory.getElemFactory().getViewChangeListener(ViewName.Loupe));
		return intoduction;
	}

}

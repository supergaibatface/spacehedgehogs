package fortLoupe;

import java.util.List;

import dialogueCharacters.IDialogueCharacter;
import settlementLocation.BaseSettlementLocation;

public class LoupeKeep extends BaseSettlementLocation {

	public LoupeKeep() {
		super("Loupe keep");
	}

	@Override
	protected void clickScript() {
		this.mainFactory.getGElementManager().openElementOnTopLayer(actionDisplayList);
	}

	@Override
	protected int giveButtonX() {
		return 300;
	}

	@Override
	protected int giveButtonY() {
		return 100;
	}

	@Override
	protected int giveButtonLength() {
		return 100;
	}

	@Override
	protected int giveButtonHeight() {
		return 100;
	}

	@Override
	protected String giveButtonImgName() {
		return "100X100Mark";
	}

	@Override
	protected String giveButtonLabel() {
		return "Keep";
	}

	@Override
	protected void setUpLocationCharacters(List<IDialogueCharacter> addLocation) {
		addLocation.add(this.mainFactory.getNPCFactory().getNPCLoupeCaptain());
		addLocation.add(this.mainFactory.getNPCFactory().getNPCLoupeArmoryChief());
		
	}

	@Override
	protected boolean giveStartAccessabilityValue() {
		return false;
	}

}

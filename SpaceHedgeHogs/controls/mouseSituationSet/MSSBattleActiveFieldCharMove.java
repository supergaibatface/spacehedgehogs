package mouseSituationSet;

import combatFieldElements.FieldCharacter;
import combatFieldElements.MovementSquare;
import fieldCharCommand.FieldCharCommandStateName;
import screenElements.ScreenButton;
import screenElements.ScreenElement;

//Represents battle views situation with a selected character and move fieldCharCommand
public class MSSBattleActiveFieldCharMove extends BaseMouseSituationSet {

	@Override
	protected void makeAllCAElements() {
		this.addNewCAElement(this.mainFactory.getCAFieldCharMove());
		this.addNewCAElement(this.mainFactory.getCAButtonPress());
		
		this.addNewCAElement(this.mainFactory.getCACharSelect());
		this.addNewCAElement(this.mainFactory.getCAItemSelecting());
		this.addNewCAElement(this.mainFactory.getCAFieldCharSelect());
		
		this.addNewCAElement(this.mainFactory.getCACharDeselect());
		
	}

	@Override
	public boolean checkSituation() {
		if(!this.mainFactory.getGameStateController().inBattle()) {
			return false;
		}
		ScreenElement selectedItem = conditions.getSelectedItem();
		if(selectedItem == null) {
			return false;
		}
		if(!selectedItem.getClass().equals(FieldCharacter.class)) {
			return false;
		}
		if(!this.mainFactory.getFieldCharCommandControl().getCurrentState().equals(FieldCharCommandStateName.MOVE)) {
			return false;
		};
		return true;
	}

}

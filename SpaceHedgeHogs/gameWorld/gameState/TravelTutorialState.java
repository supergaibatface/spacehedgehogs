package gameState;

import java.util.ArrayList;
import java.util.List;

import dataValues.CharValue;
import dataValues.FoodValue;
import dataValues.ItemValue;
import foodBase.FoodItemBase;
import itemBase.IGameItem;
import itemBase.TradeItemName;
import jobChoiceController.JobChoiceController;
import land.LandArea;
import land.TerrainName;
import other.StaticFunctions;
import party.PartyStance;
import screenElements.ScreenButton;
import skills.SavedSkill;
import skills.SavedSkillSheet;
import skills.SkillName;
import travelGoals.GoalMarker;
import view.CutsceneData;
import view.CutsceneDataCollection;
import view.GameView;
import view.IGameView;
import view.ViewName;

public class TravelTutorialState extends BaseGameState {
	//private ArrayList<ItemValue> itemValues;
	private List<IGameItem> extraItems;
	
	//List of item values made for the game state
	private void makeItemValues() {
		/*this.itemValues = new ArrayList<ItemValue>();
		this.itemValues.add(this.getFoodValuesMaxExp(TradeItemName.Berry, 5));
		this.itemValues.add(new FoodValue(TradeItemName.Berry, 2, 5));
		this.itemValues.add(new FoodValue(TradeItemName.Berry, 2, 3));
		this.itemValues.add(new FoodValue(TradeItemName.Berry, 2, 1));*/
		this.extraItems = new ArrayList<IGameItem>();
		FoodItemBase newItem = this.mainFactory.getItemFactory().getBerryItem();
		newItem.addAmount(5);
		newItem.addAmount(2, 5);
		newItem.addAmount(2, 3);
		newItem.addAmount(2, 1);
		this.extraItems.add(newItem);
	}

	@Override
	protected void loadInPlayerCharacters(ArrayList<CharValue> listOfChars) {
	}

	@Override
	protected void bootLoadFillViewList() {
		this.mainFactory.getViewManager().addNewView(this.mainFactory.getViewFactory().getLoupeFortView());
		this.mainFactory.getViewManager().addNewView(this.mainFactory.getViewFactory().getLoupeCampView());
		IGameView newView = this.mainFactory.getViewFactory().getTravelTutorialView();
		this.mainFactory.getViewManager().addNewView(newView);
		this.travelView = newView.getName();
		
		newView = mainFactory.getViewFactory().getTutorialCampView();
		this.mainFactory.getViewManager().addNewView(newView);
	}
	
	/*@Override
	protected void bootLoadFillViewList() {
		long startTime = StaticFunctions.getSystemTime();
		this.mainFactory.getViewManager().addNewView(this.mainFactory.getViewFactory().getLoupeFortView());
		StaticFunctions.writeOutElapsedSystemTime(startTime, "loading loupe fort view");
		
		startTime = StaticFunctions.getSystemTime();
		this.mainFactory.getViewManager().addNewView(this.mainFactory.getViewFactory().getLoupeCampView());
		StaticFunctions.writeOutElapsedSystemTime(startTime, "getting loupe camp view");
		
		startTime = StaticFunctions.getSystemTime();
		IGameView newView = this.mainFactory.getViewFactory().getTravelTutorialView();
		this.mainFactory.getViewManager().addNewView(newView);
		this.travelView = newView.getName();
		StaticFunctions.writeOutElapsedSystemTime(startTime, "getting and setting main travel view");
		
		startTime = StaticFunctions.getSystemTime();
		newView = mainFactory.getViewFactory().getTutorialCampView();
		this.mainFactory.getViewManager().addNewView(newView);
		StaticFunctions.writeOutElapsedSystemTime(startTime, "getting and setting main travels camp view");
	}*/
	
	private void setPartyStance() {
		this.mainFactory.getParty().setPartyStance(PartyStance.Travel);
	}

	@Override
	protected void loadInBattleViewBase(IGameView viewSaveLocation) {
		//should never get used in this state
		viewSaveLocation = this.mainFactory.getViewFactory().getBattleTutorial3View();
		
	}

	@Override
	protected void loadInInventory() {
		this.makeItemValues();
		//this.mainFactory.getInventory().makeItemsWithItemValues(this.itemValues);
		this.mainFactory.getInventory().addMultipleItemsToInventory(extraItems);
		
	}

	@Override
	protected void loadInTravelOptions() {
		LandArea newArea = this.travelManager.makeAndGetTestLandArea(TerrainName.Field, 100);
		this.travelManager.addTravelOption(newArea);
		GoalMarker destination = this.mainFactory.getElemFactory().getGoalMarker(100, this.mainFactory.getEventManager().getReachFortLoupeEvent());
		destination.setDestinationDescription("Fort Loupe");
		this.travelManager.addGoalMarker(destination);
		
	}

	@Override
	protected void loadInCaravanJobs() {
		this.mainFactory.getJobManager().makeTravelTutorialJobs();
		this.mainFactory.getEventManager().makeTutorialEvents();
		
	}

	@Override
	public void startGameScript() {
		this.mainFactory.getCutsceneBuilder().startACutscene(this.makeStartCutscene());
		this.setPartyStance();
		this.mainFactory.getViewManager().changeToView(travelView);
		
	}

	@Override
	protected void addNextPartButtonListeners(ScreenButton nextPartButton) {
		/*nextPartButton.addButtonEventListener(this.mainFactory.getElemFactory().getImportAllCharsToModuleListener(this.mainFactory.getSettlementTutorialState()));
		nextPartButton.addButtonEventListener(this.mainFactory.getElemFactory().getLoadInGameStateListener(this.mainFactory.getSettlementTutorialState()));*/
		
	}
	
	//Makes tutorial cutscene for the game
	private CutsceneDataCollection makeStartCutscene() {
		CutsceneDataCollection tutorial = this.mainFactory.getElemFactory().getCutsceneDataCollection("Intro2");
		CutsceneData newFrame = new CutsceneData("travelBG", "Its over...");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "The enemies in front of you are dead.");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "... and the royal family is dead.");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "The rest of your comrades made it to the palace only to be cut down by the same beasts who killed the emperor.");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "The war is lost.... with the royal line cut, and the army scattered, so is the empire.");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "But you are still alive, and so are the ones here with you.");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "Jack and Jamesy were already trying to get out of the city.");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "Its clear, you have to leave.");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "There is a fort, a prison to be precise, away from the city.");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "The horde came from the west so hopefully the prison to the east has safe walls and an able garrison.");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "So you'r band cuts through the city, avoiding fights. Through an abandoned gate and on to the road.");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "And now eastwards, to the fort, to safety!");
		tutorial.addNewCutScene(newFrame);
		//tutorial.addFinalButtonEventListener(this.mainFactory.getElemFactory().getLoadInDefaultViewListener());
		return tutorial;
	}

}

package exceptionHandling;

import characterExceptions.NoCustomCharacterClassException;
import viewExceptions.NotManagementViewException;
import viewExceptions.ViewNotFoundException;

public class ExceptionManager {
	private final boolean ignoreNoCustomCharacter = false;
	private final boolean ignoreNotManagementView = true;
	private final boolean ignoreViewNotFound = false;
	
	public void dealWithException(Exception oneException) {
		if(oneException instanceof NoCustomCharacterClassException) {
			dealWithNoCustomCharacter(oneException);
			return;
		}
		if(oneException instanceof NotManagementViewException) {
			dealWithNotManagementView(oneException);
			return;
		}
		if(oneException instanceof ViewNotFoundException) {
			dealWithViewNotFound(oneException);
			return;
		}
		System.out.println("Unmanaged exception");
		this.properExceptionProcedure(oneException);
	}
	
	private void dealWithNoCustomCharacter(Exception oneException) {
		if(!this.ignoreNoCustomCharacter) {
			this.properExceptionProcedure(oneException);
		}
	}
	
	private void dealWithNotManagementView(Exception oneException) {
		if(!this.ignoreNotManagementView) {
			this.properExceptionProcedure(oneException);
		}
	}
	
	private void dealWithViewNotFound(Exception oneException) {
		if(!this.ignoreViewNotFound) {
			this.properExceptionProcedure(oneException);
		}
		
	}
	
	private void properExceptionProcedure(Exception oneException) {
		System.out.println(oneException.getMessage());
	}

}

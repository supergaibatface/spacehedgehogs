package eventModules;

import events.EventTagName;
import events.GameEvent;
import foodBase.FoodItemBase;
import itemBase.TradeItemName;

//Events around finding food
public class FoodSearchEvents1 extends BaseEventModule {

	@Override
	protected void makeEvents() {
		this.firstSetEvents();
		this.secondSetEvents();
		
	}
	
	//combines different events into first set of food events
	private void firstSetEvents() {
		this.makeBerryBushEvent();
		
		this.makeWaterStremEvent();
		
		this.makeAbandonedCartEvent();
		
		this.makeDeerHuntingEvent();
	}
	
	private void makeBerryBushEvent() {
		GameEvent newEvent = this.mainFactory.getElemFactory().getGameEvent(1);
		newEvent.addTag(EventTagName.SearchForFood);
		newEvent.addText("The scout found some berry bushes");
		newEvent.addEventImg("100X100Mark");
		this.events.add(newEvent);
		int choiceNr = newEvent.addNewChoiceWithText("Pick some Berries");
		newEvent.getEventChoiceByNr(choiceNr).addItemGain(getFoodItemWithAmountValue(this.mainFactory.getItemFactory().getBerryItem(), 15));
		//Make extra choice to spend more stamina and get extra people to help pick
	}
	
	private void makeWaterStremEvent() {
		GameEvent newEvent = this.mainFactory.getElemFactory().getGameEvent(1);
		newEvent.addTag(EventTagName.SearchForWater);
		newEvent.addText("The scout found a stream of water");
		newEvent.addEventImg("100X100Mark");
		this.events.add(newEvent);
		int choiceNr = newEvent.addNewChoiceWithText("Fill up all water");
		newEvent.getEventChoiceByNr(choiceNr).addWaterGainToFull();
		//Make extra choice to spend more stamina and get extra people to bring water
	}
	
	private void makeAbandonedCartEvent() {
		GameEvent newEvent = this.mainFactory.getElemFactory().getGameEvent(1);
		newEvent.addTag(EventTagName.SearchForFood);
		newEvent.addTag(EventTagName.SearchForWater);
		newEvent.addText("There is an abandoned cart");
		newEvent.addEventImg("100X100Mark");
		this.events.add(newEvent);
		int choiceNr = newEvent.addNewChoiceWithText("Grab what you can find");
		newEvent.getEventChoiceByNr(choiceNr).addItemGain(getFoodItemWithAmountValue(this.mainFactory.getItemFactory().getBerryItem(), 25));
		newEvent.getEventChoiceByNr(choiceNr).addWaterGain(50);
		//Make extra choice to spend more stamina to get more but risk with getting found by enemies
	}
	
	private void makeDeerHuntingEvent() {
		GameEvent newEvent = this.mainFactory.getElemFactory().getGameEvent(1);
		newEvent.addTag(EventTagName.SearchForFood);
		newEvent.addText("Found deer tracks");
		newEvent.addEventImg("100X100Mark");
		this.events.add(newEvent);
		
		GameEvent secondStep = this.mainFactory.getElemFactory().getGameEvent(1);
		secondStep.addText("found the deer");
		newEvent.addNewChoiceForEvent(secondStep, "Try to find the deer");
		
		GameEvent thirdStep = this.mainFactory.getElemFactory().getGameEvent(1);
		thirdStep.addText("Managed to kill the deer");
		secondStep.addNewChoiceForEvent(thirdStep, "Try to hunt down the deer");
		int choiceNr = thirdStep.addNewChoiceWithText("Bring the meat back to the caravan");
		thirdStep.getEventChoiceByNr(choiceNr).addItemGain(getFoodItemWithAmountValue(this.mainFactory.getItemFactory().getFreshMeatItem(), 50));
		//Add failure chances to this.
	}
	
	//groups series of event making commands into second set of events
	private void secondSetEvents() {
		this.makeMushroomRootFindingEvent();
		this.makeHerbFindingEvent();
		this.makeOldHouseSiteEvent();
		this.makeBarrelsFullOfRainWaterEvent();
	}
	
	private void makeMushroomRootFindingEvent() {
		GameEvent newEvent = this.mainFactory.getElemFactory().getGameEvent(1);
		newEvent.addTag(EventTagName.SearchForFood);
		newEvent.addText("There is a clearing with plenty of shrubbery, somethine must be edible here.");
		newEvent.addEventImg("100X100Mark");
		this.events.add(newEvent);
		int choiceNr = newEvent.addNewChoiceWithText("Those mushrooms should be safe");
		newEvent.getEventChoiceByNr(choiceNr).addItemGain(getFoodItemWithAmountValue(this.mainFactory.getItemFactory().getMushroomItem(), 5));
		choiceNr = newEvent.addNewChoiceWithText("Looks like there are some edible roots here.");
		newEvent.getEventChoiceByNr(choiceNr).addItemGain(getFoodItemWithAmountValue(this.mainFactory.getItemFactory().getRootsItem(), 5));
	}
	
	private void makeHerbFindingEvent() {
		GameEvent newEvent = this.mainFactory.getElemFactory().getGameEvent(1);
		newEvent.addTag(EventTagName.SearchForFood);
		newEvent.addText("The plants growing here, yes these herbs are definetly good to eat");
		newEvent.addEventImg("100X100Mark");
		this.events.add(newEvent);
		int choiceNr = newEvent.addNewChoiceWithText("Grab as many as you can");
		newEvent.getEventChoiceByNr(choiceNr).addItemGain(getFoodItemWithAmountValue(this.mainFactory.getItemFactory().getHerbsFoodItem(), 10));
		choiceNr = newEvent.addNewChoiceWithText("These herbs are not worth taking with us");
	}
	
	private void makeOldHouseSiteEvent() {
		GameEvent newEvent = this.mainFactory.getElemFactory().getGameEvent(1);
		newEvent.addTag(EventTagName.SearchForFood);
		newEvent.addText("A clearing, signs of an old stone fence, this must have been an old house site");
		newEvent.addEventImg("100X100Mark");
		this.events.add(newEvent);
		int choiceNr = newEvent.addNewChoiceWithText("There is a old apple tree!");
		newEvent.getEventChoiceByNr(choiceNr).addItemGain(getFoodItemWithAmountValue(this.mainFactory.getItemFactory().getAppleFoodItem(), 8));
		choiceNr = newEvent.addNewChoiceWithText("There should be some edible roots burried around here.");
		newEvent.getEventChoiceByNr(choiceNr).addItemGain(getFoodItemWithAmountValue(this.mainFactory.getItemFactory().getRootsItem(), 8));
	}
	
	private void makeBarrelsFullOfRainWaterEvent() {
		GameEvent newEvent = this.mainFactory.getElemFactory().getGameEvent(1);
		newEvent.addTag(EventTagName.SearchForWater);
		newEvent.addText("In the middle of the forest there look to be some barrels full of rainwater");
		newEvent.addEventImg("100X100Mark");
		this.events.add(newEvent);
		int choiceNr = newEvent.addNewChoiceWithText("We need this water more then who ever owns the barrels");
		newEvent.getEventChoiceByNr(choiceNr).addWaterGain(200);
		choiceNr = newEvent.addNewChoiceWithText("This water might not be safe, better leave it.");
	}

}

package view;

import java.util.ArrayList;

import factories.IAutoMake;
import factories.MainFactory;
import generalGraphics.ButtonLookType;
import party.PartyStance;
import screenElements.ScreenButton;
import screenElements.ScreenDataBox;

//Builder that makes cutscenes showings from a collection of individual scenes
public class CutsceneBuilder implements IAutoMake{
	private MainFactory mainFactory;
	private CutsceneDataCollection dataCollection;
	private boolean leadToBattleView;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	//Checks if there is any data saved into this builder
	public boolean hasData() {
		return this.dataCollection != null;
	}
	
	//Makes a gameview based on cutscene data
	private GameView makeACutscene(CutsceneData data) {
		GameView cutscene = this.mainFactory.getElemFactory().getGameView(ViewName.Cutscene);
		cutscene.setBackground(data.getBackgroundName());
		cutscene.addDataBox(this.makeTextDataBox(data.getText()), 0, 700);
		cutscene.addButton(this.makeNextButton());
		cutscene.addButton(this.makeSkipButton());
		return cutscene;
	}
	
	//Makes a text box for displaying text in a cutscene
	private ScreenDataBox makeTextDataBox(String text) {
		ScreenDataBox textBox = this.mainFactory.getElemFactory().getScreenDataBox(0, 0, "100Lime");
		textBox.setSize(1500, 250);
		ArrayList<String> textGroup = this.mainFactory.getStringToScreenConv().stringSplitter2(text, this.mainFactory.getLengthAssigner().getOneTextSize(), 1400);
		textBox.fillDataBoxWithTexts(textGroup);
		return textBox;
	}
	
	//Makes a next button that advances the cutscene
	private ScreenButton makeNextButton() {
		ScreenButton nextButton = this.mainFactory.getElemFactory().getScreenButton(1520, 700, ButtonLookType.NEXT, "next");
		nextButton.addButtonEventListener(this.mainFactory.getElemFactory().getAdvanceCutsceneListener());
		return nextButton;
	}
	
	//Makes a skip button that skips the cutscene
	private ScreenButton makeSkipButton() {
		ScreenButton nextButton = this.mainFactory.getElemFactory().getScreenButton(1520, 800, ButtonLookType.NEXT, "skip");
		nextButton.addButtonEventListener(this.mainFactory.getElemFactory().getSkipCutseneListener());
		return nextButton;
	}
	
	//Public method for going to the next scene of the cutscene
	public void advanceScene(ScreenButton button) {
		if(this.hasData()) {
			if(this.dataCollection.hasEnded()) {
				this.leaveFromCutscene(button);
			}
			else {
				this.loadNextFrame();
			}
		}
	}
	
	//Public method for skipping current cutscene
	public void skipCutscene(ScreenButton button) {
		if(this.hasData()) {
			this.leaveFromCutscene(button);
		}
	}
	
	//Ends cutscene showing
	private void leaveFromCutscene(ScreenButton button) {
		this.dataCollection.triggerFinalButtonListeners(button);
		this.dataCollection = null;
		this.mainFactory.getScreen().changeElementManager(this.mainFactory.getGElementManager());
	}
	
	//Loads in view that represents next cutscene frame
	private void loadNextFrame() {
		GameView nextFrame = this.makeACutscene(this.dataCollection.getAndAdvanceData());
		//this.mainFactory.getViewManager().changeToView(nextFrame);
		this.mainFactory.getCutsceneGElemenetManager().changeToView(nextFrame);
	}
	
	//Starts a cutscene viewing based on cusceneDataCollection
	public void startACutscene(CutsceneDataCollection cutsceneData) {
		this.dataCollection = cutsceneData;
		this.dataCollection.resetBookmark();
		loadNextFrame();
		this.mainFactory.getScreen().changeElementManager(this.mainFactory.getCutsceneGElemenetManager());
	}

}

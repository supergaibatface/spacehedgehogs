package central;

import java.util.ArrayList;

import attacking.AttackAI;
import battleManagement.BattleController;
import combatFieldElements.FieldCharacter;
import factories.IAutoMake;
import factories.MainFactory;
import gameCharacters.GameCharacter;
import graphicalWrapping.CharSpawnInfo;
import moving.MovementAI;
import other.StaticFunctions;
import propertyBars.PropertyBarName;
import screenElements.ScreenDataBox;
import spawning.AISpawnController;
import spawning.ISpawnTimeModule;
import targetChoosing.TargetChoosingAI;

//Main Ai hub that combines different parts into on working logic
public class MainAIHub implements IAutoMake {
	private MainFactory mainFactory;
	private BattleController battleController;
	private AISpawnController spawnController;
	private MovementAI movementAI;
	private TargetChoosingAI targetingAI;
	private AttackAI attackAI;
	private ArrayList<FieldCharacter> controlledChars;
	private boolean debug = true;
	private ScreenDataBox enemyInfo;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.spawnController = this.mainFactory.getAISpawnController();
		this.controlledChars = new ArrayList<FieldCharacter>();
		this.movementAI = this.mainFactory.getMovementAI();
		this.targetingAI = this.mainFactory.getTargetingAI();
		this.attackAI = this.mainFactory.getAttackAI();
		this.enemyInfo = this.mainFactory.getElemFactory().getScreenDataBox(0, 0, "LightBlue");
	}
	
	//main method for booting up the AI player
	public void gearUpForBattle(BattleController battleController, ISpawnTimeModule spawnTimeModule) {
		this.battleController = battleController;
		this.prepUp(spawnTimeModule);
	}
	
	//Sets up the things inside the AIHub
	private void prepUp(ISpawnTimeModule spawnTimeModule) {
		ArrayList<AICharacterSet> unwrappedEnemies = new ArrayList<AICharacterSet>();
		for(AICharacterSet oneEnemy : this.battleController.getAllSpawnInfos()) {
			unwrappedEnemies.add(this.mainFactory.getCharacterManager().copyCharacterSet(oneEnemy));
		}
		this.battleController.emptySpawnInfoList();
		this.spawnController.setUpSpawning(unwrappedEnemies, spawnTimeModule);
	}
	
	//Command for assigning the AI a move that happens before actual turns start
	public void givePreTurn() {
		this.forceSpawnOneEnemy();
	}
	
	//Command for giving the AI one turn worth of moves
	public void giveOneTurn() {
		this.resetEnemyLimiters();
		this.updateControlList();
		this.spawnPhase();
		this.movementPhase();
		this.attackPhase();
	}
	
	//Deals with spawning new characters onto the battle field
	private void spawnPhase() {
		if(this.spawnController.stillWorkToDo()) {
			this.spawnController.giveSpawnTurn();
		}
	}
	
	//Method for forcing the spawn of one enemy, ignores all limiting logic
	private void forceSpawnOneEnemy() {
		if(this.spawnController.stillWorkToDo()) {
			this.addFieldCharToControlledChars(this.spawnController.spawnOneEnemy());
		}
	}
	
	//Adds a character to the list of AI controller characters
	public void addFieldCharToControlledChars(FieldCharacter newChar) {
		this.controlledChars.add(newChar);
	}
	
	//Deals with moving characters on the field
	private void movementPhase() {
		ArrayList<FieldCharacter> sortedChars = this.getSortedListForDealingWithOwn();
		for(FieldCharacter oneChar : sortedChars) {
			oneCharMovementPhase(oneChar);
		}
	}
	
	//Deals with moving one specific character on the field
	private void oneCharMovementPhase(FieldCharacter oneChar) {
		FieldCharacter target = this.targetingAI.findClosestCharTo(oneChar);
		if(target == null) {
			StaticFunctions.writeOutDebug(debug, "No possible targets found for AI");
			return;
		}
		this.movementAI.moveMode(oneChar, target);
	}
	
	//Deals with attacking characters
	private void attackPhase() {
		ArrayList<FieldCharacter> sortedChars = this.getSortedListForDealingWithOwn();
		for(FieldCharacter oneChar : sortedChars) {
			oneCharAttackPhase(oneChar);
		}

	}
	
	//Deals with one specific character perfomring attacks
	private void oneCharAttackPhase(FieldCharacter oneChar) {
		FieldCharacter target = this.targetingAI.findClosestCharTo(oneChar);
		if(target == null) {
			StaticFunctions.writeOutDebug(debug, "No possible targets found for AI");
			return;
		}
		this.attackAI.AttackMode(oneChar, target);
	}
	
	//Gets a list of AI controlled characters
	public ArrayList<FieldCharacter> getSortedListForDealingWithOwn() {
		return this.controlledChars;
	}
	
	//Updates controlled character list by removing dead chars
	private void updateControlList() {
		ArrayList<FieldCharacter> eliminateList = new ArrayList<FieldCharacter>();
		for(FieldCharacter oneChar : this.controlledChars) {
			if(oneChar.getMasterCharacter().getPropertyBar(PropertyBarName.Health).isEmpty()) {
				eliminateList.add(oneChar);
			}
		}
		for(FieldCharacter removeChar : eliminateList) {
			this.controlledChars.remove(removeChar);
		}
	}
	
	//getter for all controlled characters
	public ArrayList<FieldCharacter> getControlledCharList() {
		return this.controlledChars;
	}
	
	//resets the one turn spawn limit for AI for next turn
	public void resetEnemyLimiters() {
		this.spawnController.resetTurnHardLimitCounter();
	}
	
	//Checks if there are still enemies for the player to defeat
	public boolean hasEnemiesToDefeat() {
		if(this.areThereAliveEnemiesOnField()) {
			return true;
		}
		if(this.spawnController.stillWorkToDo()) {
			return true;
		}
		//System.out.println("Out of enemies to fight");
		return false;
	}
	
	//Checks if there are currently enemies alive on the map
	public boolean areThereAliveEnemiesOnField() {
		for(FieldCharacter oneChar : this.controlledChars) {
			if(!oneChar.getMasterCharacter().getPropertyBar(PropertyBarName.Health).isEmpty()) {
				return true;
			}
		}
		return false;
	}
	
	//getter for screenDataBox that contains info about enemy AI
	public ScreenDataBox getEnemyInfo() {
		return this.enemyInfo;
	}
	
	//Updates the values displayed on enemy info databox
	public void updateEnemyInfo() {
		this.fillDataBox();
	}
	
	//Fills enemy info databox with text
	private void fillDataBox() {
		this.enemyInfo.fillDataBoxWithTexts(this.collectLines());
		this.enemyInfo.updateSizeForTime();
	}
	
	//For collecting lines from other sections to display on enemy info databox
	private ArrayList<String> collectLines() {
		ArrayList<String> lines = new ArrayList<String>();
		this.spawnController.fillListWithInfo(lines);
		
		return lines;
	}

}

package buttonLimiters;

//Interface for button limiters
public interface IButtonLimiter {
	
	//checks if the limiter allows action
	public boolean check();
	
	//fullfills the cost of this limit
	public void doCost();
	
	//Checks if there is a need to notify player when limiter activates
	public boolean needsNotify();
	
	//Gives string explanation why this limiter activated, used for actual notification text
	public String reasonForFail();
	
	//boolean flag that would have the limiter throw a pop up instantly after it gets triggered
	public boolean instantPopUp();

}

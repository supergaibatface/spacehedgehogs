package graphicalWrapping;

import factories.IAutoMake;
import factories.MainFactory;
import screenElements.ScreenDataBox;

//Class that feeds game mode specific into overall game representing databoxes
public class DataBoxWrapper implements IAutoMake {
	private MainFactory mainFactory;
	private int xCoord;
	private int yCoord;
	private ScreenDataBox dataBox;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	xCoord: x coordinate for the databox
	 * 	yCoord: y coordinate for the databox
	 * 	databox: the databox that needs to be fed the data
	 */
	public DataBoxWrapper(int xCoord, int yCoord, ScreenDataBox dataBox) {
		this.xCoord = xCoord;
		this.yCoord = yCoord;
		this.dataBox = dataBox;
	}

	//setter for main factory
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	//bootup method
	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
	}
	
	//Feeds coordinates into the databox and returns the databox afterwards
	public ScreenDataBox getBoxWithCoordinates() {
		this.dataBox.setCoordinates(xCoord, yCoord);
		return this.dataBox;
	}

}

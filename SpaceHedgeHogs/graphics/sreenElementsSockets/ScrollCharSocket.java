package sreenElementsSockets;

import java.util.ArrayList;
import java.util.List;

import frameworks.IStackable;
import frameworks.SimpleCharacterFrame;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import generalGraphics.ButtonLookType;
import generalGraphics.ClickBox;
import jobs.Job;
import newJobs.IJob;
import screenElements.ScreenButton;
import screenElements.ScreenDataBox;
import screenElements.ScreenElement;
import screenElements.ScreenScrollPanel;
import screenElements.ScreenSoloImage;
import screenElements.ScreenText;

//Scroll socket for ScreenSimpleChars
public class ScrollCharSocket extends BaseScrollSocket<ScreenSimpleChar> {
	private SimpleCharacterFrame charFrame;
	protected IJob job;
	protected boolean idleSocket;
	protected ScreenSoloImage visualGuide;
	protected ScreenButton extraCommandsButton;

	//constructor
	public ScrollCharSocket(int x, int y, int length, int height) {
		super(x, y, length, height);
		
	}
	
	public void bootUp() {
		super.bootUp();
		this.idleSocket = false;
		this.makeVisualGuide();
		this.makeGrayOut();
		this.grayElement.setForceNonScroll(true);
		this.makeExtraCommandsOpenButton();
	}
	
	private void makeExtraCommandsOpenButton() {
		this.extraCommandsButton = this.mainFactory.getElemFactory().getScreenButton(160, -40, ButtonLookType.DELETEITEM, "O");
		this.extraCommandsButton.addButtonEventListener(this.mainFactory.getElemFactory().getOpenInfoSelectionListener(this, "test"));
		this.extraCommandsButton.setWrapper(this);
		this.extraCommandsButton.setForceNonScroll(true);
		this.extraCommandsButton.makeInvisible();
	}
	
	private void updateExtraCommands() {
		if(this.hasJob()) {
			this.deleteExtraCommands();
			addExtraCommandsFromList(this.job.getExtraCommandButtons());
			this.extraCommandsButton.makeVisible();
		}
	}
	
	private void addExtraCommandsFromList(List<ScreenButton> buttonsToAdd) {
		for(ScreenButton oneButton : buttonsToAdd) {
			this.addExtraCommand(oneButton);
		}
	}
		
	
	@Override
	protected void setUpElementFrame() {
		this.charFrame = this.mainFactory.getElemFactory().getSimpleCharacterFrame(this);
		this.charFrame.setLocation(0, 0);
		this.elements.add(charFrame);
	}
	
	//makes an element that will be displayed when player can send an active char to this socket
	protected void makeVisualGuide() {
		this.visualGuide = this.mainFactory.getElemFactory().getSoloImage(this.getLength(), this.getHeight(), "greenT40");
		this.visualGuide.setWrapper(this);
		this.visualGuide.setCoordinates(0, 0);
		this.visualGuide.setForceNonScroll(true);
		this.visualGuide.makeInvisible();
	}
	
	//checks and corrects the visiblity of visual guides for this element
	public void acitivtyVisualGuideCheck() {
		if(this.checkAvailability()) {
			this.visualGuide.makeVisible();
			this.grayElement.makeInvisible();
		}
		else {
			this.visualGuide.makeInvisible();
			this.grayElement.makeVisible();
		}
	}
	
	//Method for turning all visual guides invisible
	public void clearVisualQues() {
		this.visualGuide.makeInvisible();
		this.grayElement.makeInvisible();
	}

	/*
	 * debug method for printing out all element children
	 */
	private void printOutChildren() {
		this.charFrame.printOutAllChildern();
	}



	@Override
	protected boolean elementAddingLogic(ScreenElement element) {
		if(element.getClass() == ScreenSimpleChar.class) {
			return this.elementAddingForScreenSimpleChar((ScreenSimpleChar) element);
		}
		return false;
	}
	
	//Element adding logic for adding a ScreenSimpleChar to this socket
	protected boolean elementAddingForScreenSimpleChar(ScreenSimpleChar element) {
		if(this.charFrame.addNewElement(element)) {
			this.callOnJobElementAddLogic(element);
			return true;
		}
		else {
			return false;
		}
	}
	
	//Activates the new element adding logic on the job element
	protected void callOnJobElementAddLogic(ScreenSimpleChar newElement) {
		if(this.hasJob()) {
			this.job.newElementAdded(newElement.getMaster());
		}
	}
	
	@Override
	protected boolean elementRemovingLogic(ScreenElement element) {
		if(element.getClass() == ScreenSimpleChar.class) {
			return this.elementRemovingForScreenSimpleChar((ScreenSimpleChar) element);
		}
		return false;
	}
	
	//Element removing logic for removing a ScreenSimpleChar from this socket
	protected boolean elementRemovingForScreenSimpleChar(ScreenSimpleChar element) {
		if(this.charFrame.removeElement(element)) {
			this.callOnJobElementRemoveLogic(element);
			return true;
		}
		else {
			return false;
		}
	}
	
	//Activates the element removing logic on the job element
	protected void callOnJobElementRemoveLogic(ScreenSimpleChar newElement) {
		if(this.hasJob()) {
			this.job.oldElementRemoved(newElement.getMaster());
		}
	}
	
	//Makrs this socket to act as an idle socket, will automatically register itself when used for a view
	public void designateAsIdleSocket() {
		this.idleSocket = true;
	}
	
	//Registers this as the active idle socket in game state controller
	public void checkIdleRegistering() {
		if(this.idleSocket) {
			this.mainFactory.getGameStateController().registerSocketAsIdleSocket(this);
		}
	}
	
	//Checks if socket has a job assigned to it
	public boolean hasJob() {
		return this.job != null;
	}
	
	//Setter for a job on this socket
	public boolean setJob(IJob job) {
		this.job = job;
		this.mainFactory.getJobManager().registerSocket(this);
		if(this.job.hasStamineLimiter()) {
			addStaminaLimiterText(this.job.getStaminaLimiter().getAmount());
		}
		this.job.registerSocketForJob(this);
		this.updateExtraCommands();
		return true;
	}
	
	//Getter for this sockets job
	public IJob getJob() {
		return this.job;
	}
	
	//method for making all assigned characters do assigned job
	public void doJobs() {
		if(hasJob()) {
			this.job.workThrough(getMasterElements());
		}
	}
	
	/*
	 * Returns fill list of game characters assigned to this socket
	 */
	public ArrayList<IGameCharacter> getMasterElements() {
		ArrayList<IGameCharacter> newList = new ArrayList<IGameCharacter>();
		for(ScreenSimpleChar oneChar: this.charFrame.getAllElements()) {
			newList.add(oneChar.getMaster());
		}
		return newList;
	}
	
	/*
	 * Adds a stamina limiter to this socket
	 * Input:
	 * 	amount: the amount of stamina needed for this socket
	 */
	private void addStaminaLimiterText(int amount) {
		if(this.hasTitle()) {
			List<String> lines = this.getPreviousTitleLines();
			lines.add("Stamina cost: " + amount);
			this.setTitle(lines);
		}
	}
	
	//Gets the lines previously used for the title
	private List<String> getPreviousTitleLines() {
		List<String> lines = new ArrayList<String>();
		for(ScreenText oneText : this.titleFrame.getTexts()) {
			lines.add(oneText.getText());
		}
		return lines;
	}
	
	//Method for checking if the limiter allowes this button to be pressed
	public boolean checkAvailability() {
		return this.checkAvailabilityForActiveChar();
	}
	
	//Checks if the currently active char can be sent to this socket
	private boolean checkAvailabilityForActiveChar() {
		if(!this.mainFactory.getActivityController().getActivityState()) {
			return true;
		}
		if(!this.mainFactory.getActivityController().isActiveSimpleChar()) {
			return true;
		}
		if(this.checkAvailabilityForChar(this.mainFactory.getActivityController().getSelectedCharAsSimpleChar())) {
			return true;
		}
		return false;
	}
	
	//Checks if input character can be sent to this socket
	private boolean checkAvailabilityForChar(ScreenSimpleChar oneCharacter) {
		if(!this.hasJob()) {
			return true;
		}
		if(this.job.fullReqCheck(oneCharacter.getMaster())) {
			return true;
		}
		return false;
	}
	
	//Method that expells all characters from this socket who longer dont meet requirements of this socket
	public void expelCharsWhoDontFit() {
		for(ScreenSimpleChar oneChar : this.charFrame.getAllElements()) {
			if(!checkAvailabilityForChar(oneChar)) {
				this.removeElement(oneChar);
				this.mainFactory.getGameStateController().addCharacterToIdleList(oneChar);
			}
		}
	}

	//Method that updates gray elements visual value, based on this elements requirements being met or not
	public void correctGrayOutVisiblity() {
		if(this.checkAvailability()) {
			this.grayElement.makeInvisible();
		}
		else {
			this.grayElement.makeVisible();
		}
	}

	@Override
	public ArrayList<ScreenSimpleChar> getElements() {
		return this.charFrame.getAllElements();
	}

	@Override
	public void removeAllElements() {
		this.charFrame.removeAllElements();
	}
	
	//Way to liberate all stored elements in case of a safe delete
	private void deleteThisLiberateElements() {
		this.charFrame.liberateAllElements();
	}

	@Override
	public void safeDelete() {
		if(this.idleSocket) {
			this.mainFactory.getGameStateController().unregisterThisSocket(this);
		}
		this.deleteThisLiberateElements();
		if(this.hasJob()) {
			this.job.safeDelete();
		}
	}

	@Override
	protected String emptyMessage() {
		return "No character assigned";
	}
	
	@Override
	public void fillTopElementList(List<IStackable> list) {
		super.fillTopElementList(list);
		list.add(visualGuide);
		list.add(grayElement);
		list.add(this.extraCommandsButton);
	}
	
	@Override
	protected void goingOnScreenScript() {
		super.goingOnScreenScript();
		if(this.hasJob()) {
			this.job.socketGoingOnScreenScriptPoint();
		}
		
	}
	
	


}

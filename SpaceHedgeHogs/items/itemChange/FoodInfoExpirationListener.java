package itemChange;

import screenInfoPage.FoodInfoPage;

//Listener that updates food info page details when parts of the logical item stack expires
public class FoodInfoExpirationListener extends BaseItemListener {
	private FoodInfoPage infoPage;
	
	//constructor, input is the page that will be updated
	public FoodInfoExpirationListener(FoodInfoPage infoPage) {
		this.infoPage = infoPage;
	}
	

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void changeReacting(ItemChangeEvent evt) {
		infoPage.updateFoodDetailsText();
		
	}

	@Override
	protected boolean reactCheck(ItemChangeEvent evt) {
		if(!evt.isExpirationChanged()) {
			return false;
		}
		return true;
	}

}

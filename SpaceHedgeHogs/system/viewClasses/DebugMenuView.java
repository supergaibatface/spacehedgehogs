package viewClasses;

import java.util.ArrayList;

import dataValues.CharValue;
import generalGraphics.ButtonLookType;
import screenElements.ScreenBackground;
import screenElements.ScreenButton;
import screenElements.ScreenDataBox;
import skills.SkillName;
import view.BaseGameView;
import view.ViewName;

public class DebugMenuView extends BaseGameView {

	public DebugMenuView() {
		super(ViewName.DebugMenu);
	}
	
	@Override
	protected String bootGiveBackgroundImageName() {
		return "TestBG";
	}

	@Override
	protected void bootElements() {
		
		ScreenDataBox menuBox = bootMakeMenuOptionsBox();
		this.addDataBox(menuBox, 700, 200);
		menuBox.addButton(bootMakeButtonBattleTutorial1());
		menuBox.addButton(bootMakeButtonBattleTutorial2());
		menuBox.addButton(bootMakeButtonBattleTutorial3());
		menuBox.addButton(bootMakeButtonTravelTutorial());
		menuBox.addButton(bootMakeButtonLoupe());
		menuBox.autoSize();
	}
	
	private ScreenDataBox bootMakeMenuOptionsBox() {
		ScreenDataBox menuBox = this.mainFactory.getElemFactory().getScreenDataBox(0, 0, "Orange");
		return menuBox;
	}
	
	private ScreenButton bootMakeButtonBattleTutorial1() {
		ScreenButton startGame = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.POPUPSELECT, "Battle tutorial 1");
		startGame.addButtonEventListener(this.mainFactory.getElemFactory().getLoadInGameStateListener(this.mainFactory.getBattleTutorialState()));
		return startGame;
	}
	
	private ScreenButton bootMakeButtonBattleTutorial2() {
		ScreenButton startGame = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.POPUPSELECT, "Battle tutorial 2");
		startGame.addButtonEventListener(this.mainFactory.getElemFactory().getAddCharToGameStateListener(this.mainFactory.getBattleTutorialState2(), this.getCharValuesList2()));
		startGame.addButtonEventListener(this.mainFactory.getElemFactory().getLoadInGameStateListener(this.mainFactory.getBattleTutorialState2()));
		return startGame;
	}
	
	//for getting a list of missing chars for a debug version
	private ArrayList<CharValue> getCharValuesList2() {
		ArrayList<CharValue> listOfValues = new ArrayList<CharValue>();
		addDefaultChar(listOfValues, "Player");
		return listOfValues;
	}
	
	//for getting a list of missing chars for a debug version
	private void addDefaultChar(ArrayList<CharValue> valueList, String charName) {
		CharValue newCharValue = new CharValue(charName, "charspot");
		newCharValue.addSavedSkillValue(SkillName.PhysicalForm, 20);
		newCharValue.addSavedSkillValue(SkillName.MeleeCombat, 20);
		valueList.add(newCharValue);
	}
	
	private ScreenButton bootMakeButtonBattleTutorial3() {
		ScreenButton startGame = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.POPUPSELECT, "Battle tutorial 3");
		startGame.addButtonEventListener(this.mainFactory.getElemFactory().getAddCharToGameStateListener(this.mainFactory.getBattleTutorialState3(), this.getCharValuesList3()));
		startGame.addButtonEventListener(this.mainFactory.getElemFactory().getLoadInGameStateListener(this.mainFactory.getBattleTutorialState3()));
		return startGame;
	}
	
	//for getting a list of missing chars for a debug version
	private ArrayList<CharValue> getCharValuesList3() {
		ArrayList<CharValue> listOfValues = new ArrayList<CharValue>();
		addDefaultChar(listOfValues, "Player");
		addDefaultChar(listOfValues, "Bob");
		addDefaultChar(listOfValues, "Michael");
		return listOfValues;
	}
	
	private ScreenButton bootMakeButtonTravelTutorial() {
		ScreenButton startGame = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.POPUPSELECT, "Travel tutorial mode");
		startGame.addButtonEventListener(this.mainFactory.getElemFactory().getAddCharToGameStateListener(this.mainFactory.getTravelTutorialState(), this.getCharValuesList4()));
		startGame.addButtonEventListener(this.mainFactory.getElemFactory().getLoadInGameStateListener(this.mainFactory.getTravelTutorialState()));
		return startGame;
	}
	
	//for getting a list of missing chars for a debug version
	private ArrayList<CharValue> getCharValuesList4() {
		ArrayList<CharValue> listOfValues = new ArrayList<CharValue>();
		addDefaultChar(listOfValues, "Player");
		addDefaultChar(listOfValues, "Bob");
		addDefaultChar(listOfValues, "Michael");
		addDefaultChar(listOfValues, "James");
		addDefaultChar(listOfValues, "Jack");
		return listOfValues;
	}
	
	private ScreenButton bootMakeButtonLoupe() {
		ScreenButton startGame = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.POPUPSELECT, "Loupe fort view");
		startGame.addButtonEventListener(this.mainFactory.getElemFactory().getAddCharToGameStateListener(this.mainFactory.getTravelTutorialState(), this.getCharValuesList4()));
		startGame.addButtonEventListener(this.mainFactory.getElemFactory().getLoadInGameStateListener(this.mainFactory.getTravelTutorialState()));
		startGame.addButtonEventListener(this.mainFactory.getElemFactory().getAddNewQuestListener(this.mainFactory.getQuestFactory().getLoupeMainQuest()));
		startGame.addButtonEventListener(this.mainFactory.getElemFactory().getViewChangeListener(ViewName.Loupe));
		return startGame;
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}
	
	

}

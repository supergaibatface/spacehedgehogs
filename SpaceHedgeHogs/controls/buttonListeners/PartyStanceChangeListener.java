package buttonListeners;

import party.PartyStance;

/*
 * Button listener that will change saved party stance value
 */
public class PartyStanceChangeListener extends BaseButtonListener {
	private PartyStance partyStanceValue;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	partyStance: value that will be made into active value on click
	 */
	public PartyStanceChangeListener(PartyStance partyStance) {
		this.partyStanceValue = partyStance;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.mainFactory.getParty().setPartyStance(partyStanceValue);
		
	}

	@Override
	public String toInfo() {
		return "Changes party stance to "+this.partyStanceValue;
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

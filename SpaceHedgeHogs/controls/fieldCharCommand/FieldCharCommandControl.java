package fieldCharCommand;

import java.util.ArrayList;

import activityListeners.FieldCommandControlVisualListener;
import factories.IAutoMake;
import factories.MainFactory;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import generalGraphics.ButtonLookType;
import land.LandArea;
import propertyBarListeners.FieldCharCommandUpdater;
import propertyBars.PropertyBarName;
import screenElements.ScreenButton;
import screenElements.ScreenDataBox;
import screenManagement.GElementManager;

/*
 * Controller in charge of FieldCharCommand set of controls (actions that a selected field char can take)
 */
public class FieldCharCommandControl implements IAutoMake {
	private MainFactory mainFactory;
	private ScreenDataBox dataBox;
	private GElementManager GEManager;
	private FieldCharCommandStateName currentState;
	private IGameCharacter activeChar;
	private FieldCharCommandUpdater listener;
	private FieldCommandControlVisualListener activityVisualListener;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		makeDataBox();
		this.GEManager = this.mainFactory.getGElementManager();
		this.currentState = FieldCharCommandStateName.INACTIVE;
		this.listener = this.mainFactory.getElemFactory().getFieldCharCommandUpdater();
		this.makeActivityVisualListener();
	}
	
	/*
	 * Makes the databox through what the player can itneract with fieldCharCommands
	 */
	private void makeDataBox() {
		this.dataBox = mainFactory.getElemFactory().getScreenDataBox(0, 0, "LightBlue");
		makeMovementButton();
		makeAttackButton();
		makeItemUseButton();
		this.dataBox.addRadioFunctionToButtons();
		this.dataBox.updateSizeForTime();
		this.dataBox.makeInvisible();
	}
	
	//makes a listener that waits for activity changes
	private void makeActivityVisualListener() {
		this.activityVisualListener = this.mainFactory.getElemFactory().getFieldCommandControlVisualListener(this);
		this.mainFactory.getActivityController().addListener(activityVisualListener);
	}
	
	//Sets a new active char for field char commands
	private void saveNewActiveChar() {
		deleteActiveChar();
		this.activeChar = this.mainFactory.getActivityController().getSelectedCharAsFieldChar().getMasterCharacter();
		this.setUpListeners();
		this.writeCharValues();
	}
	
	//Removes saved active char values
	private void deleteActiveChar() {
		if(this.activeChar != null) {
			this.removeListeners();
		}
		this.activeChar = null;
	}
	
	//Method for outside elements to trigger text update
	public void callTextUpdate() {
		this.writeCharValues();
	}
	
	//writes the active char allowed move situation to the databox
	private void writeCharValues() {
		//GameCharacter activeChar = this.mainFactory.getActivityController().getSlectedCharAsFieldChar().getMasterCharacter();
		ArrayList<String> lines = new ArrayList<String>();
		lines.add("Nr of moves left: " + activeChar.getPropertyBar(PropertyBarName.Movement).getCurrentValue());
		lines.add("Nr of attacks left: " + activeChar.getPropertyBar(PropertyBarName.Attacks).getCurrentValue());
		this.dataBox.fillDataBoxWithTexts(lines);
		this.dataBox.updateSizeForTime();
	}
	
	//Adds listeners that listens for property bar changes
	private void setUpListeners() {
		this.activeChar.getPropertyBar(PropertyBarName.Movement).addListener(listener);
		this.activeChar.getPropertyBar(PropertyBarName.Attacks).addListener(listener);
	}
	
	//Removes the property bar listeners from the active char
	private void removeListeners() {
		this.activeChar.getPropertyBar(PropertyBarName.Movement).removeListener(listener);
		this.activeChar.getPropertyBar(PropertyBarName.Attacks).removeListener(listener);
	}
	
	/*
	 * Getter for the designated movement button
	 */
	private ScreenButton getMovementButton() {
		return this.dataBox.getButtons().get(0);
	}
	
	/*
	 * Getter for the designated attack button
	 */
	private ScreenButton getAttackButton() {
		return this.dataBox.getButtons().get(1);
	}
	
	/*
	 * Getter for the designated item button
	 */
	private ScreenButton getItemButton() {
		return this.dataBox.getButtons().get(2);
	}
	
	/*
	 * Makes a designated movement control button
	 */
	private void makeMovementButton() {
		ScreenButton testButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.TRAVELSELECT, "Move");
		testButton.addButtonEventListener(this.mainFactory.getElemFactory().getChangeFCCCSelectionListener(this, FieldCharCommandStateName.MOVE));
		testButton.addButtonEventListener(this.mainFactory.getElemFactory().getShowMovementPathsListener());
		this.dataBox.addButton(testButton);
	}
	
	/*
	 * Makes a designated attack control button
	 */
	private void makeAttackButton() {
		ScreenButton testButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.TRAVELSELECT, "Attack");
		testButton.addButtonEventListener(this.mainFactory.getElemFactory().getChangeFCCCSelectionListener(this, FieldCharCommandStateName.ATTACK));
		testButton.addButtonEventListener(this.mainFactory.getElemFactory().getShowAttackSquaresListener());
		this.dataBox.addButton(testButton);
	}
	
	/*
	 * Makes a designated item use control button
	 */
	private void makeItemUseButton() {
		ScreenButton testButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.TRAVELSELECT, "Item");
		testButton.addButtonEventListener(this.mainFactory.getElemFactory().getChangeFCCCSelectionListener(this, FieldCharCommandStateName.ITEM));
		this.dataBox.addButton(testButton);
	}
	
	//Getter for the controller databox
	public ScreenDataBox getDataBox() {
		return this.dataBox;
	}
	
	//standard command for activating field char command controls, also set movement as the default selection
	public void activateCommandSelection() {
		this.currentState = FieldCharCommandStateName.MOVE;
		this.saveNewActiveChar();
		this.dataBox.makeVisible();
		activateMovement();
	}
	
	//Standard command for deactivating field char command controls
	public void deactivateCommandSelection() {
		this.dataBox.makeInvisible();
		this.deleteActiveChar();
	}
	
	//Sets movement action as the selected command control
	private void activateMovement() {
		ScreenButton movementButton = getMovementButton();
		movementButton.activate();
	}
	
	/*
	 * Method for reactivating current selected command control value
	 * the methods called here are hard coded
	 */
	public void reactivateCurrentSelection() {
		ScreenButton selectedButton;
		switch(this.currentState) {
			case MOVE:
				selectedButton = this.getMovementButton();
				break;
			case ATTACK:
				selectedButton = this.getAttackButton();
				break;
			case ITEM:
				selectedButton = this.getItemButton();
				break;
			default:
				return;	
		}
		selectedButton.activate();
	}
	
	
	//Resets command control selection
	public void selectionChangeReset() {
		this.mainFactory.getGameStateController().getBattleController().clearVisuals();
	}
	
	//Returns current state of field char command control
	public FieldCharCommandStateName getCurrentState() {
		return this.currentState;
	}
	
	/*
	 * Changes the current state of fieldCharCommandControll
	 * Inputs:
	 * 	stateName: name of the new active state
	 */
	public void changeCurrentState(FieldCharCommandStateName stateName) {
		this.currentState = stateName;
	}
	
	//way to safely delete field char command controlls
	public void safeDelete() {
		this.mainFactory.getActivityController().removeListener(activityVisualListener);
		this.activityVisualListener = null;
	}


}

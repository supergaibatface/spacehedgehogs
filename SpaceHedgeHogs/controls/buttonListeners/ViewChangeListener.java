package buttonListeners;

import view.GameView;
import view.IGameView;
import view.ViewName;

//button listener that will change the view when triggered
public class ViewChangeListener extends BaseButtonListener {
	private ViewName viewName;
	private IGameView view;
	
	/*
	 * constructor
	 * Inputs:
	 * 	viewName: name of the view that the screen will be changed to
	 */
	public ViewChangeListener(ViewName viewName) {
		this.viewName = viewName;
	}

	//Method that is triggered by the event
	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.mainFactory.getViewManager().changeToView(viewName);
		
	}

	//boot up method
	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String toInfo() {
		return "Changes to " + this.viewName;
	}

}

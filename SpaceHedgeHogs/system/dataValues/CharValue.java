package dataValues;

import propertyBars.PropertyBarName;
import propertyBars.SavedPropertySheet;
import skills.SavedSkill;
import skills.SavedSkillSheet;
import skills.SkillName;

//Minimalist class for saving values for individual characters
public class CharValue {
	private String name;
	private String imgName;
	private SavedSkillSheet savedSkillSheet;
	private SavedPropertySheet savedPropertySheet;
	
	//constructor
	public CharValue(String name, String imgName) {
		this.name = name;
		this.imgName = imgName;
	}
	
	//getter for an assigned game character name
	public String getName() {
		return this.name;
	}
	
	//getter for an assigned profile image for the character
	public String getImgName() {
		return this.imgName;
	}

	//getter for saved skill values
	public SavedSkillSheet getSavedSkillSheet() {
		return savedSkillSheet;
	}
	
	//getter for saved property bar values
	public SavedPropertySheet getSavedPropertySheet() {
		return savedPropertySheet;
	}
	
	//Makes a hub object for saving skill values
	private void makeSavedSkillSheet() {
		this.savedSkillSheet = new SavedSkillSheet();
	}
	
	//makes a hub object for saving property bar values
	private void makeSavedPropertySheet() {
		this.savedPropertySheet = new SavedPropertySheet();
	}
	
	/*
	 * Saves a new skill value to this charValues
	 * Inputs:
	 * 	name: name of the skill
	 * 	value: int value of total exp of this skill
	 */
	public void addSavedSkillValue(SkillName name, int value) {
		if(!this.hasSavedSkillSheet()) {
			this.makeSavedSkillSheet();
		}
		this.savedSkillSheet.addNewSavedSkill(new SavedSkill(name, value));
	}
	
	/*
	 * Saves a new property bar value to this charValues
	 * Inputs:
	 * 	name: name of the property bar
	 * 	value: int number of current value of the bar
	 */
	public void addSavedPropertyValue(PropertyBarName name, int value) {
		if(!this.hasSavedPropertySheet()) {
			this.makeSavedPropertySheet();
		}
		this.savedPropertySheet.generateAndSaveNewBar(name, value);
	}
	
	//Checks if the charValues has any saved skills
	public boolean hasSavedSkillSheet() {
		return this.savedSkillSheet != null;
	}
	
	//Checks if charValues has any saved property bars
	public boolean hasSavedPropertySheet() {
		return this.savedPropertySheet != null;
	}

}

package foodBase;

import java.util.List;

import factories.IAutoMake;
import factories.MainFactory;
import inventoryFilters.TagFilter;
import itemBase.IGameItem;
import itemBase.ItemTagName;
import other.StaticFunctions;

//Class meant for interacting with the entire stock of stored food, mostly for consumption purpose
public class FoodManager implements IAutoMake {
	private MainFactory mainFactory;
	private TagFilter foodFilter;
	private boolean debug = true;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.setUpFoodFilter();
		
	}
	
	//getter for the designated full party one turn food consumption
	public int getFullPartyFoodConsumption() {
		return this.mainFactory.getCharacterManager().getPartySize() * this.getOneCharFoodConsumption();
	}
	
	//checks if there are enough stocks to travel for another turn
	public boolean hasEnoughFoodForTravel() {
		return hasEnoughFood(getFullPartyFoodConsumption());
	}
	
	//Method that deletes (consumes) one turns worth of food
	public boolean doOneTravelTurnFoodConsumption() {
		StaticFunctions.writeOutDebug(debug, "doing one turn of travel food consuption");
		this.consumeAmount(this.getFullPartyFoodConsumption());
		return true;
	}
	
	//private method for setting up the filter that this object can get food items with
	private void setUpFoodFilter() {
		this.foodFilter = this.mainFactory.getElemFactory().getTagFilter(ItemTagName.Food);
	}
	
	//Checks if there is atleast as much food as the input requirement amount
	public boolean hasEnoughFood(int requirement) {
		if(getFoodAmount() < requirement) {
			return false;
		}
		else {
			return true;
		}
	}
	
	//getter for current amount of food
	public int currentFoodAmount() {
		return this.getFoodAmount();
	}
	
	//private getter for food amount
	private int getFoodAmount() {
		return calculateFoodAmount(this.foodFilter.getFilteredItems());
	}
	
	//Calculation method that summarises a list of food items for the total amount, input is the list
	private int calculateFoodAmount(List<IGameItem> foodItems) {
		int foodAmount = 0;
		for(IGameItem oneItem : foodItems) {
			foodAmount = foodAmount + oneItem.getSupply();
		}
		return foodAmount;
	}
	
	//Method for deleting a certain amount of food, input is the amount to get deleted
	public void consumeAmount(int foodAmount) {
		int remaining = this.consumeExpiringFood(foodAmount);
		if(remaining > 0) {
			remaining = consumeAtRandom(remaining);
		}
		if(remaining > 0) {
			System.out.println("Problem in FoodManager, "+remaining+" amount of food was not consumed");
		}
		
	}
	
	//Method for consuming an amount of food but from random sources of existing food items
	private int consumeAtRandom(int totalAmount) {
		int stillToEat = totalAmount;
		for(IGameItem oneItem : this.foodFilter.getFilteredItems()) {
			stillToEat = eatRandomParts(stillToEat, (FoodItemBase) oneItem);
			if(stillToEat < 1) {
				break;
			}
		}
		return stillToEat;
	}
	
	//Method that at one point should randomly eat from a random expiration stack of an food item (right now consumes the ones that will expire first)
	private int eatRandomParts(int totalAmount, FoodItemBase food) {
		int canEatAmount = food.getSupply();
		if(canEatAmount < totalAmount) {
			food.removeAmount(canEatAmount);
			return totalAmount - canEatAmount;
		}
		else {
			food.removeAmount(totalAmount);
			return 0;
		}
	}
	
	//Method that first consumes food stacks that are known to expire next turn, input is how much can be consumed at max
	private int consumeExpiringFood(int limit) {
		int stillToEat = limit;
		for(IGameItem oneItem : this.foodFilter.getFilteredItems()) {
			stillToEat = eatExpiringParts(stillToEat, (FoodItemBase) oneItem);
			if(stillToEat < 1) {
				break;
			}
		}
		StaticFunctions.writeOutDebug(debug, "Finished eating expiring foor, "+stillToEat+" must still be eaten.");
		return stillToEat;
	}
	
	/*
	 * Consumes the expiring amount from a designated source of food item
	 * Input:
	 * 	totalAmount: how much food needs to eaten, can be larger then what designated item has, but will only
	 * 					take as much as will expire next turn
	 * 	food: the food item from where the food will be taken from
	 */
	private int eatExpiringParts(int totalAmount, FoodItemBase food) {
		int canEatAmount = food.getExpiringAmount();
		if(canEatAmount <= totalAmount) {
			food.removeAmount(canEatAmount);
			StaticFunctions.writeOutDebug(debug, "Consumed all "+canEatAmount+" of expiring "+food.getName());
			return totalAmount - canEatAmount;
		}
		else {
			food.removeAmount(totalAmount);
			StaticFunctions.writeOutDebug(debug, "Consumed "+totalAmount+" of expiring "+food.getName());
			return 0;
		}
	}
	
	//getter for one char food consumption for a turn
	public int getOneCharFoodConsumption() {
		return 1;
	}

}

package buttonListeners;

import screenElements.ScreenScrollPanel;

/*
 * Button listener that will trigger scrolling of a scrollpanel
 */
public class PanelScrollListener extends BaseButtonListener{
	private final boolean xAxis;
	private final boolean increaseScroll;
	private final ScreenScrollPanel panel;
	private int oneScrollAmount;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	target: scrollpanel that will be scrolled
	 * 	xAxis: if true, scrolls panel on the X axis, if false scroll on Y axis
	 * 	increaseScroll: if true will scroll to higher value (scrolls down), if false scrolls to lower value (scrolls up)
	 */
	public PanelScrollListener(ScreenScrollPanel target, boolean xAxis, boolean increaseScroll) {
		this.xAxis = xAxis;
		this.increaseScroll = increaseScroll;
		this.panel = target;
		this.oneScrollAmount = 20;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		if(this.xAxis) {
			this.xAxisScrollLogic();
		}
		else {
			this.yAxisScrollLogic();
		}
		
	}
	
	/*
	 * Logic for scrolling the x Axis
	 */
	private void xAxisScrollLogic() {
		if(this.increaseScroll) {
			this.panel.scrollRight(oneScrollAmount);
		}
		else {
			this.panel.scrollLeft(oneScrollAmount);
		}
	}
	
	/*
	 * Logic for scrolling the y Axis
	 */
	private void yAxisScrollLogic() {
		if(this.increaseScroll) {
			this.panel.scrollDown(oneScrollAmount);
		}
		else {
			this.panel.scrollUp(oneScrollAmount);
		}
	}

	@Override
	public String toInfo() {
		String direction;
		if(this.xAxis) {
			if(this.increaseScroll) {
				direction = "right";
			}
			else {
				direction = "left";
			}
			
		}
		else {
			if(this.increaseScroll) {
				direction = "down";
			}
			else {
				direction = "up";
			}
		}
		return "Scrolls screen panel to the "+direction;
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

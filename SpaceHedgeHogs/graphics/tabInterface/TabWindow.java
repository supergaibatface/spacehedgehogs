package tabInterface;

import java.util.ArrayList;
import java.util.List;

import frameworks.IStackable;
import screenElements.ScreenElement;
import screenElements.ScreenText;

/*
 * Class that represent a tabwindow, these can be stored in a tab directory and switched between each other
 */
public class TabWindow extends ScreenElement {
	private ArrayList<ScreenElement> elements;
	
	public TabWindow() {
		super(0, 0);
		// TODO Auto-generated constructor stub
	}
	
	public void bootUp() {
		super.bootUp();
		this.elements = new ArrayList<ScreenElement>();
		this.setSize(300, 700);
	}
	
	//Method for adding a element to this tab directory
	public void addElement(ScreenElement newElement) {
		this.elements.add(newElement);
		newElement.setWrapper(this);
	}
	
	//Getter for elements in this tab directory
	public ArrayList<ScreenElement> getElements() {
		return this.elements;
	}

	@Override
	public void fillTopElementList(List<IStackable> list) {
		list.addAll(this.elements);
		
	}

	@Override
	public void fillTopTextList(List<ScreenText> list) {
		
	}
	
	public void safeDelete() {
		for(IStackable oneElement : this.elements) {
			oneElement.safeDelete();
		}
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOffScreenScript() {
		// TODO Auto-generated method stub
		
	}

}

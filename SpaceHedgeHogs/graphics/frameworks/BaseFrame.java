package frameworks;

import java.util.ArrayList;

import factories.IAutoMake;
import factories.MainFactory;
import generalGraphics.ClickBox;
import screenElements.ScreenButton;
import screenElements.ScreenElement;
import screenElements.ScreenText;
import screenManagement.GraphicalLengthAssigner;

/*
 * Legacy hierarcy step for frameworks that need to have a wrapper, most code now in IndependentBaseFrame
 * data frameworks are used for displaying a collection of simmilar objects in a graphical element
 */
public abstract class BaseFrame extends IndependentBaseFrame {
	
	/*
	 * Constructor
	 * Inputs:
	 * 	wrapper: IStacable type element that this frame would be placed on
	 */
	public BaseFrame(IStackable wrapper) {
		this.setWrapper(wrapper);
	}

}

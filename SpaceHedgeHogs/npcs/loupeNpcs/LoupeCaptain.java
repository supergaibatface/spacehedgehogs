package loupeNpcs;

import dialogueCharacters.BaseDialogueCharacter;
import view.CutsceneData;
import view.CutsceneDataCollection;
import view.ViewName;

public class LoupeCaptain extends BaseDialogueCharacter {
	private CutsceneDataCollection introductions;
	
	public void bootUp() {
		super.bootUp();
		this.introductions = this.makePlaceholderCutscene();
	}

	@Override
	protected void getCurrentDialogueInstance() {
		this.mainFactory.getCutsceneBuilder().startACutscene(introductions);
		
	}

	@Override
	protected boolean hasAllowedDialogue() {
		return true;
	}

	@Override
	protected String giveNameToChar() {
		// TODO Auto-generated method stub
		return "Loupe captain";
	}
	
	private CutsceneDataCollection makePlaceholderCutscene() {
		CutsceneDataCollection intoduction = this.getNewDialogueBaseCutscene("Loupe captain dialogue");
		CutsceneData newFrame = new CutsceneData("travelBG", "Loupe captain dialogue.");
		intoduction.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "test frame");
		intoduction.addNewCutScene(newFrame);
		//intoduction.addFinalButtonEventListener(this.mainFactory.getElemFactory().getViewChangeListener(ViewName.Loupe));
		return intoduction;
	}

}

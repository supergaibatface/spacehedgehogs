package jobPanels;

public interface IJobPanel {
	
	//public void callLocationUpdate();
	
	public void callSocketHighlightUpdate();
	
	public void callCleanHighlights();
	
	public void expellUnfitCharacters();

}

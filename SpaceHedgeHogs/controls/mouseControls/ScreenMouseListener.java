package mouseControls;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import factories.IAutoMake;
import factories.MainFactory;

//Listener class for mouse events
public class ScreenMouseListener implements MouseListener, IAutoMake{
	private MainFactory mainFactory;
	private MouseCommands commands;
	
	//create method that requires a rectangle and screen to be added
	public ScreenMouseListener() {
		
	}
	
	//Boot up method, meant for starting up the object after creation
	public void bootUp() {
		this.commands = this.mainFactory.getMouseCommands();
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {	
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	//Registers the click and calls out the mouse click event from mouse command object
	@Override
	public void mousePressed(MouseEvent event1) {
		this.commands.mouseClick(event1.getX(), event1.getY(), event1.getButton());
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	//setter for main factory object
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
	}

}

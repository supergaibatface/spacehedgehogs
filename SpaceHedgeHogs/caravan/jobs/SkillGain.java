package jobs;

import factories.IAutoMake;
import factories.MainFactory;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import skills.SkillName;

//Represents an increase in skills exp gained through an action for a character
public class SkillGain implements IAutoMake {
	private MainFactory mainFactory;
	private SkillName assignedSkill;
	private int expValue;
	
	//constructor
	public SkillGain(SkillName assignedSkillName, int expValue) {
		this.assignedSkill = assignedSkillName;
		this.expValue = expValue;
	}

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	//Gives saved values worth of exp to the designated skill on the input character
	public void giveExp(IGameCharacter gameChar) {
		gameChar.improveSkill(assignedSkill, expValue);
	}

}

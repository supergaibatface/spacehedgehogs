package craftingBluePrints;

import factories.IAutoMake;
import factories.MainFactory;

public class CraftingProject implements IAutoMake {
	private MainFactory mainFactory;
	private IBlueprint targetBlueprint;
	private int investedWork;
	
	public CraftingProject(IBlueprint targetBlueprint) {
		this.targetBlueprint = targetBlueprint;
	}

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.investedWork = 0;
		
	}
	
	private int getTargetWorkAmount() {
		return this.targetBlueprint.getNeccesaryWorkValue();
	}
	
	public IBlueprint getMasterBlueprint() {
		return this.targetBlueprint;
	}
	
	public int getInvestedWork() {
		return this.investedWork;
	}
	
	public int addWorkValueAndGetLeftOverValue(int addedValue) {
		return addWorkAndCalculateSpareWorkValue(addedValue);
	}
	
	private int addWorkAndCalculateSpareWorkValue(int addedValue) {
		int leftOverValue = this.calculateLeftOverWorkValue(addedValue);
		this.investedWork = investedWork + (addedValue - leftOverValue);
		return leftOverValue;
	}
	
	private int calculateLeftOverWorkValue(int addedValue) {
		if(addedValue < this.calculateStillNeededWorkAmount()) {
			return 0;
		}
		return addedValue - this.calculateStillNeededWorkAmount();
	}
	
	public boolean isComplete() {
		return investedWork >= this.targetBlueprint.getNeccesaryWorkValue();
	}
	
	public int getStillNeededWorkAmount() {
		return this.calculateStillNeededWorkAmount();
	}
	
	private int calculateStillNeededWorkAmount() {
		return this.targetBlueprint.getNeccesaryWorkValue() - investedWork;
	}
	
	public boolean tryToAddFinishedItem() {
		if(isComplete()) {
			//this.mainFactory.getInventory().addItemAmount(this.targetBlueprint.getFinishedProjetValue());
			this.mainFactory.getInventory().addItemToInventory(this.targetBlueprint.getFinishedProjetValue());
			return true;
		}
		return false;
	}
	

}

package combatFieldElements;

import java.util.ArrayList;
import java.util.List;

import frameworks.IStackable;
import screenElements.ScreenElement;
import screenElements.ScreenText;

//Screen element child that expands into all elements that can be displayed on a movementfields squares
public class FieldElement extends ScreenElement {
	protected MovementSquare location;

	//constructor
	public FieldElement() {
		super(0, 0);
		// TODO Auto-generated constructor stub
	}
	
	//getter for elements location square
	public MovementSquare getLocation() {
		return this.location;
	}
	
	//checks if this element has be spawned on a movement field
	public boolean hasLocation() {
		return this.location != null;
	}
	
	//Setter for the movementsquare that this element would locate on
	public void setLocation(MovementSquare square) {
		if(hasLocation()) {
			this.location.makeCharLeave();
		}
		this.wrapper = square;
		this.location = square;
	}

	@Override
	public void fillTopElementList(List<IStackable> list) {
		
	}

	@Override
	public void fillTopTextList(List<ScreenText> list) {
		
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOffScreenScript() {
		// TODO Auto-generated method stub
		
	}

}

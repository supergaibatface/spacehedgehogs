package buttonListeners;

import itemBase.IGameItem;

//button listener that adds items to inventory after triggering
public class ItemAddListener extends BaseButtonListener {
	private IGameItem item;

	/*
	 * constructor
	 * Inputs:
	 * 	item: TradeItem that will be added to the intentory
	 */
	public ItemAddListener(IGameItem item) {
		this.item = item;
	}

	//bootup method
	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

	//event triggering command
	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		//this.mainFactory.getInventory().addItemAmount(this.item);
		this.mainFactory.getInventory().addItemToInventory(this.item);
		this.mainFactory.getNoticeController().addNotice("Gained: "+this.item.getSupply()+" x "+this.item.getName(), false );
	}

	@Override
	public String toInfo() {
		return buildInfo();
	}
	
	//Builds the listener info with string builder
	public String buildInfo() {
		StringBuilder newBuilder = new StringBuilder();
		newBuilder.append("Adds ");
		newBuilder.append(this.item.getSupply());
		newBuilder.append(" ");
		newBuilder.append(this.item.getName());
		return newBuilder.toString();
	}

}

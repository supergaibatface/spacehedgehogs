package controllers;

import java.util.ArrayList;
import java.util.List;

import activityListeners.IActivityListener;
import combatFieldElements.FieldCharacter;
import factories.IAutoMake;
import factories.MainFactory;
import generalGraphics.ActivityInfo;
import screenElements.ScreenElement;
import screenElements.ScreenSelection;
import sreenElementsSockets.ScreenSimpleChar;
import sreenElementsSockets.ScreenSimpleItem;

//Controller class for activity, all activation logic must go through this class
public class ActivityController implements IAutoMake {
	private MainFactory mainFactory;
	private ActivityInfo activityInfo;
	private ScreenElement whoSelected;
	private ScreenSelection selectionImg;
	private ActivityInfo info;
	private List<IActivityListener> listeners;
	
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}
	@Override
	public void bootUp() {
		this.selectionImg = this.mainFactory.getScreenSelection();
		this.activityInfo = this.mainFactory.getActivityInfo();
		this.listeners = new ArrayList<IActivityListener>();
		
	}
	
	/*
	 * Method for making active a ScreenSimpleChar
	 */
	public boolean makeActive(ScreenSimpleChar character) {
		baseMakeActive(character);
		this.activityInfo.setActiveValues(character.getMaster());
		return true;
	}

	/*
	 * Activation method for FieldCharacter
	 * Inputs: 
	 * 	charElement: the character that will be turned active
	 */
	public boolean makeActive(FieldCharacter charElement) {
		baseMakeActive(charElement);
		this.activityInfo.setActiveValues(charElement.getMasterCharacter());
		return true;
	}
	
	/*
	 * Make Active base version for a generic ScreenElement
	 * Inputs:
	 * 	newElement: screenElement that will be made active
	 */
	public boolean makeActive(ScreenElement newElement) {
		baseMakeActive(newElement);
		return true;
	}
	
	/*
	 * BaseMakeActive command that holds the standard code for activating something
	 * Inputs:
	 * 	newElement: element that will be made active
	 */
	private boolean baseMakeActive(ScreenElement newElement) {
		if(getActivityState()) {
			makeInactive();
		}	
		this.selectionImg.setActive(newElement);
		this.whoSelected = newElement;
		this.informListeners();
		return true;
	}
	
	/*
	 * Command for making selection inactive
	 */
	public void makeInactive() {
		this.selectionImg.setInactive();
		this.activityInfo.setInactiveValues();
		this.whoSelected = null;
		this.informListeners();
	}
	
	
	
	//Method for when the character is sent to another socket, also makes the selection inactive
	public boolean takeCharAway() {
		if(getActivityState()) {
			if(isActiveSimpleChar()) {
				ScreenSimpleChar selectedChar = (ScreenSimpleChar) whoSelected;
				selectedChar.removeThisElementFromCurrentLocation();
				this.makeInactive();
				return true;
			}
		}
		this.makeInactive();
		return false;
	}
	
	//Method for when the character is sent to another socket, also makes the selection inactive
	public boolean takeItemAway() {
		if(getActivityState()) {
			if(isActivitySimpleItem()) {
				this.makeInactive();
				return true;
			}
		}
		this.makeInactive();
		return false;
		
	}
	
	/*
	 * Checks if current activation is in a screenSocket
	 */
	public boolean isActivityInSocket() {
		if(isActivitySimpleItem()) {
			return true;
		}
		if(isActiveSimpleChar()) {
			return true;
		}
		return false;
	}
	
	//Informs listeners about activity change
	private void informListeners() {
		for(IActivityListener oneListener : this.listeners) {
			oneListener.activityChange(whoSelected);
		}
	}
	
	//adds an activity listener
	public boolean addListener(IActivityListener newListener) {
		if(this.listeners.contains(newListener)) {
			return false;
		}
		else {
			this.listeners.add(newListener);
			return true;
		}
	}
	
	//removes an activity listener
	public boolean removeListener(IActivityListener oldListener) {
		if(this.listeners.contains(oldListener)) {
			this.listeners.remove(oldListener);
			return true;
		}
		else {
			return false;
		}
	}
	
	/*
	 * Checks if current active thing is a ScreenSimpleChar
	 */
	public boolean isActiveSimpleChar() {
		return this.whoSelected.getClass().equals(ScreenSimpleChar.class);
	}
	
	/*
	 * Checks if current active thing is a FieldCharacter
	 */
	public boolean isActivityFieldCharacter() {
		return this.whoSelected.getClass().equals(FieldCharacter.class);
	}
	
	/*
	 * Checks if current active thing is an InventoryItem
	 */
	public boolean isActivitySimpleItem() {
		return this.whoSelected.getClass().equals(ScreenSimpleItem.class);
	}
	
	//getter for the activity state
	public boolean getActivityState() {
		return this.whoSelected != null;
	}
		
	//getter for selected character, use only if the activity state is true
	public ScreenElement getSelectedChar() {
		return this.whoSelected;
	}
	
	/*
	 * Gets and converts selected item to FieldCharacter
	 */
	public FieldCharacter getSelectedCharAsFieldChar() {
		return (FieldCharacter) this.whoSelected;
	}
	
	/*
	 * Gets and converts selected item to ScreenSimpleChar
	 */
	public ScreenSimpleChar getSelectedCharAsSimpleChar() {
		return (ScreenSimpleChar) this.whoSelected;
	}
	
}

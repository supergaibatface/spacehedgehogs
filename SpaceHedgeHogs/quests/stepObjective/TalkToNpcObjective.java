package stepObjective;

import dialogueCharacters.IDialogueCharacter;
import questStepBase.IQuestStep;
import stepObjectiveBase.BaseStepObjective;

public class TalkToNpcObjective extends BaseStepObjective {
	private IDialogueCharacter targetCharacter;
	//private boolean checkFlag = false;
	
	public TalkToNpcObjective(IQuestStep masterStep, IDialogueCharacter targetCharacter) {
		super(masterStep);
		this.targetCharacter = targetCharacter;
	}

	@Override
	protected String returnObjectiveDescription() {
		return "Talk to "+targetCharacter.getName();
	}

	/*@Override
	protected boolean performCompletionCheck() {
		return this.checkFlag;
	}*/
	
	/*public void announceObjectiveCompletion() {
		setCompletionFlag(true);
		this.announceChange();
	}*/
	
	@Override
	protected void makeListeners() {
		this.targetCharacter.addNewListener(this.mainFactory.getNPCFactory().getEndDialogueQuestNPCListener(this));
	}

}

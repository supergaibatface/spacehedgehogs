package fieldPainters;

import java.util.ArrayList;

import calculations.RangeCalculator;
import combatFieldElements.ControlFaction;
import combatFieldElements.FieldCharacter;
import combatFieldElements.MovementField;
import combatFieldElements.MovementSquare;
import factories.IAutoMake;
import factories.MainFactory;
import other.StaticFunctions;
import squareFilters.NoOccupiedSquares;

//FieldPainter that will find all squares that are close to all characters from one faction
public class FromAlliedCharactersPainter implements IFieldPainter, IAutoMake {
	private MainFactory mainFactory;
	private int range;
	private RangeCalculator rangeCalc;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	range: the range from allied characters used for searching squares
	 */
	public FromAlliedCharactersPainter(int range) {
		this.range = range;
	}

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.rangeCalc = this.mainFactory.getAttackRangeCalculator();
		
	}

	@Override
	public ArrayList<MovementSquare> getActiveSquares() {
		return findSquares();
	}
	
	/*
	 * Logic for finding designated squares
	 */
	private ArrayList<MovementSquare> findSquares() {
		MovementField currentField = this.mainFactory.getGameStateController().getBattleController().getMovementField();
		ArrayList<MovementSquare> foundSquares = new ArrayList<MovementSquare>();
		for(FieldCharacter oneChar : currentField.getFieldCharacters(ControlFaction.PLAYER)) {
			for(MovementSquare oneSquare : this.rangeCalc.getTargets(range, oneChar.getLocation(), currentField, new NoOccupiedSquares())) {
				StaticFunctions.addToArrayIfUnique(foundSquares, oneSquare);
			}	
		}
		return foundSquares;
	}

}

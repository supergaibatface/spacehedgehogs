package playerActionListener;

//interface for player action listener
public interface IPlayerActionListener {
	
	//method that is called when a player makes a mouse click, input is the clicked mouse button
	public boolean playerMouseActionHappened(int mouseClick);

}

package jobChoiceController;

import java.util.ArrayList;

import jobs.Job;
import jobs.JobName;
import jobs.JobTopic;
import land.TerrainName;
import sreenElementsSockets.ScrollCharSocket;

/*
 * Job Topic Controller that deals with the changing of terrain
 */
public class TerrainTopicController extends BaseJobTopicController{

	//constructor
	public TerrainTopicController(JobTopic newTopic) {
		super(newTopic);
	}

	@Override
	protected ArrayList<Object> makeModifierValuePackage() {
		ArrayList<Object> modifierPackage = new ArrayList<Object>();
		modifierPackage.add(findCurrentTerrainName());
		return modifierPackage;
	}

	/*
	 * Finds current terrain name
	 */
	private TerrainName findCurrentTerrainName() {
		if(this.mainFactory.getTravelManager().getSelectedArea() != null) {
			return this.mainFactory.getTravelManager().getSelectedArea().getTerrain();
		}
		return null;
	}

	@Override
	protected void addSockets() {
		TerrainName terrainName = this.findCurrentTerrainName();
		if(terrainName == TerrainName.Field) {
			ScrollCharSocket newSocket = this.makeBaseSocket(2, "LightGreen", "Field Test");
			Job newJob = this.mainFactory.getJobManager().getTestJob();
			newSocket.setJob(newJob);
			this.assignedTopic.AddNewSocket(newSocket);
		}
		if(terrainName == TerrainName.Forest) {
			ScrollCharSocket newSocket = this.makeBaseSocket(2, "LightGreen", "Forest Test");
			Job newJob = this.mainFactory.getJobManager().getTestJob();
			newSocket.setJob(newJob);
			this.assignedTopic.AddNewSocket(newSocket);
		}
		if(terrainName == TerrainName.Swamp) {
			ScrollCharSocket newSocket = this.makeBaseSocket(2, "LightGreen", "Swamp Test");
			Job newJob = this.mainFactory.getJobManager().getTestJob();
			newSocket.setJob(newJob);
			this.assignedTopic.AddNewSocket(newSocket);
		}
		
	}

}

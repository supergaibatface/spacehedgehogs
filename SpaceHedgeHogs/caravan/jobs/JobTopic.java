package jobs;

import java.util.ArrayList;
import java.util.List;

import frameworks.BaseFrame;
import frameworks.IStackable;
import screenElements.ScreenElement;
import screenElements.ScreenText;
import sreenElementsSockets.ScreenSimpleChar;
import sreenElementsSockets.ScrollCharSocket;
import tabInterface.TabDirectory;

//A graphical collection of one type of job sockets
public class JobTopic extends BaseFrame {
	private ArrayList<ScrollCharSocket> sockets;
	private int oneSocketLength = 0;
	private int betweenSize = 10;
	private int oneSocketHeight = 0;
	private boolean didUpdate = false;
	
	public void bootUp() {
		this.sockets = new ArrayList<ScrollCharSocket>();
	}

	//constructor
	public JobTopic(IStackable wrapper) {
		super(wrapper);
	}
	
	//Resets the content in this topic (deletes existing stuff)
	public void resetTopic() {
		clearElementsFromSockets();
		this.sockets.clear();
	}
	
	//Removes all elements in sockets assigned to this topic
	public void clearElementsFromSockets() {
		for(ScrollCharSocket oneSocket : sockets) {
			oneSocket.safeDelete();
		}
	}
	
	//Adds a list of sockets to this topic
	public void addNewSockets(ArrayList<ScrollCharSocket> newSockets) {
		for(ScrollCharSocket oneSocket : newSockets) {
			this.AddNewSocket(oneSocket);
		}
	}
	
	//Updates size values of this topic with a socket
	private void updateSizesThroughSocket(ScrollCharSocket oneSocket) {
		this.oneSocketLength = oneSocket.getLength();
		this.oneSocketHeight = oneSocket.getHeight();
		this.didUpdate = true;
	}
	
	//Adds a singular new socket to this topic
	public void AddNewSocket(ScrollCharSocket newSocket) {
		if(!didUpdate) {
			updateSizesThroughSocket(newSocket);
		}
		newSocket.setWrapper(this);
		newSocket.setCoordinates(newSocketX(), newSocketY());
		this.sockets.add(newSocket);
	}
	
	//getter for x coordinate of a new socket
	private int newSocketX() {
		return (this.oneSocketLength + this.betweenSize) * this.sockets.size();
	}
	
	//getter for y coordinate of a new socket
	private int newSocketY() {
		return 0;
	}

	@Override
	public int getFullHeight() {
		return this.oneSocketHeight;
	}

	@Override
	public int getFullLength() {
		if(this.sockets.isEmpty()) {
			return 0;
		}
		else {
			return (this.oneSocketLength * this.sockets.size()) + (this.betweenSize * (this.sockets.size() -1));
		}
	}
	
	//getter for all sockets stored here
	public ArrayList<ScrollCharSocket> getAllSockets() {
		return this.sockets;
	}
	
	//Method that calls for an update check on this topics sockets
	public void callVisualQues() {
		for(ScrollCharSocket oneSocket : this.sockets) {
			oneSocket.acitivtyVisualGuideCheck();
		}
	}
	
	//Method that clears all visual ques on this topics sockets
	public void clearVisualQues() {
		for(ScrollCharSocket oneSocket : this.sockets) {
			oneSocket.clearVisualQues();
		}
	}
	
	//Passes an expell command to the sockets in this job topic
	public void expellCharsWhoDontFit() {
		for(ScrollCharSocket oneSocket : this.sockets) {
			oneSocket.expelCharsWhoDontFit();
		}
	}

	@Override
	public void fillTopElementList(List<IStackable> list) {
		list.addAll(this.sockets);
	}

	@Override
	public void fillTopTextList(List<ScreenText> list) {
		
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOffScreenScript() {
		// TODO Auto-generated method stub
		
	}

}

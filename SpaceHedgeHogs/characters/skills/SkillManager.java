package skills;

import java.util.ArrayList;

import events.EventTagName;
import factories.IAutoMake;
import factories.MainFactory;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;

//Manager class that holds a list of all skill templates in the game
public class SkillManager implements IAutoMake {
	private MainFactory mainFactory;
	private ArrayList<Skill> skills;
	private ArrayList<Integer> expLvlBar;
	
	//constructor
	public SkillManager() {
		
	}

	//setter for main factory
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	//method for booting up the object after constructing
	@Override
	public void bootUp() {
		this.skills = new ArrayList<Skill>();
		this.expLvlBar = new ArrayList<Integer>();
		makeStandardLevels();
		makeStandardSkills();
	}
	
	//method for asking lvl value by experience value
	public int askForLevel(int experience) {
		for (Integer one : expLvlBar) {
			if(one > experience) {
				return expLvlBar.indexOf(one);
			}
		}
		return expLvlBar.size();
	}
	
	/*
	 * method for asking for the amount of exp you need for a specific level
	 * return 0 if there is no info on the level
	 */
	public int askForLvlExp(int lvl) {
		if(lvl > 0  && (lvl - 1) <= expLvlBar.size()) {
			return expLvlBar.get(lvl - 1);
		}
		return 0;
	}
	
	/*
	 * Method for assigning skill to a game character based on the template skills
	 * assigned to the object of this skill manager
	 * this is the only allowed way to assign skills to characters atm
	 */
	public void getSkillsForCharacter(IGameCharacter oneChar) {
		for (Skill oneSkill: skills) {
			Skill newSkill = this.mainFactory.getElemFactory().copySkill(oneSkill);
			oneChar.addSkill(newSkill);
		}
	}
	
	//Method for getting a list of all the skill templates assigned to this skill manager
	public ArrayList<Skill> getSkillList() {
		return this.skills;
	}
	
	//Search function for skills, returns null if skill isn't found
	public Skill getSkillByName(SkillName name) {
		for (Skill oneSkill: this.getSkillList()) {
			if(oneSkill.equals(name)) {
				return oneSkill;
			}
		}
		return null;
	}
	
	//Method for making a new skill template for this skill manager
	private void makeNewSkill(SkillName name) {
		Skill newSkill = this.mainFactory.getElemFactory().getSkill(name);
		this.skills.add(newSkill);
	}
	
	//hard coded method for making standard lvl up caps
	private void makeStandardLevels() {
		addNewLevel(5);
		addNewLevel(10);
		addNewLevel(15);
		addNewLevel(20);
		addNewLevel(25);
		addNewLevel(30);
	}
	
	//hard coded method for making skill templates
	private void makeStandardSkills() {
		makeNewSkill(SkillName.PhysicalForm);
		makeNewSkill(SkillName.MeleeCombat);
		makeNewSkill(SkillName.RangedCombat);
		makeNewSkill(SkillName.Scouting);
		makeNewSkill(SkillName.Crafting);
		makeNewSkill(SkillName.BaggageManagement);
	}
	
	/*
	 * Method for making a new lvl cap
	 * Input:
	 * 	exp: the amount of exp a char needs to reach next lvl
	 */
	private void addNewLevel(int exp) {
		if(this.expLvlBar.isEmpty()) {
			this.expLvlBar.add(0);
		}
		int lastValue = expLvlBar.get(expLvlBar.size() - 1);
		this.expLvlBar.add(lastValue + exp);
	}

}

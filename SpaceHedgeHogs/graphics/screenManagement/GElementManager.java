package screenManagement;

import java.util.ArrayList;
import java.util.List;

import factories.IAutoMake;
import factories.MainFactory;
import frameworks.IStackable;
import screenElements.ScreenBackground;
import screenElements.ScreenButton;
import screenElements.ScreenDataBox;
import screenElements.ScreenElement;
import screenElements.ScreenPopUp;
import screenElements.ScreenScrollPanel;
import screenElements.ScreenTimedDataBox;
import screenInfoPage.BaseInfoPage;
import tabInterface.TabDirectory;
import view.GameView;
import view.IGameView;

/*
 * Manager for Graphical elements, possess lists of all the elements that will be drawn on the screen
 * uses View objects to change the interior lists and settings
 */
public class GElementManager extends BaseGElementManager {
	//private MainFactory mainFactory;
	private IScreenLayer backgroundLayer;
	private IScreenLayer gameFieldLayer;
	private IScreenLayer interfaceLayer;
	private IScreenLayer topLayer;
	private IScreenLayer popupLayer;
	
	@Override
	protected String giveManagerName() {
		return "Main Graphical Element Manager";
	}

	@Override
	protected void addLayersToList(List<IScreenLayer> layerList) {
		this.makeGraphicLayers(layerList);
		
	}
	
	private void makeGraphicLayers(List<IScreenLayer> layerList) {
		this.backgroundLayer = this.makeStandardLayer(layerList);
		this.gameFieldLayer = this.makeStandardLayer(layerList);
		this.interfaceLayer = this.makeStandardLayer(layerList);
		this.topLayer = this.makeStandardLayer(layerList);
		this.popupLayer = this.makePopupLayer(layerList);
	}
	
	private IScreenLayer makeStandardLayer(List<IScreenLayer> layerList) {
		IScreenLayer newLayer = this.mainFactory.getElemFactory().getStandardGraphicLayer();
		layerList.add(newLayer);
		return newLayer;
	}
	
	private IScreenLayer makePopupLayer(List<IScreenLayer> layerList) {
		IScreenLayer newLayer = this.mainFactory.getElemFactory().getPopupGraphicsLayer();
		layerList.add(newLayer);
		return newLayer;
	}
	
	
	/*
	 * Adder for timed dataBoxes
	 */
	public void addTimedObject(ScreenTimedDataBox newTimedObject) {
		this.topLayer.addElementToLayer(newTimedObject);
	}
	
	/*
	 * Method for removing a timed databox
	 */
	public void removeTimedObject(ScreenTimedDataBox oldTimedObject) {
		this.topLayer.removeElement(oldTimedObject);
	}
	
	/*
	 * Method for adding a new pop up window to the screen
	 * Input:
	 * 	onePopUp: the pop up that will be opened on the screen
	 */
	public void openElementOnPopupLayer(ScreenPopUp onePopUp) {;
		this.popupLayer.addElementToLayer(onePopUp);
	}
	
	//Method for opening a info page on the screen, input is that infoPage
	public void openElementOnTopLayer(IStackable newElement) {
		this.topLayer.addElementToLayer(newElement);
	}
	
	//adds a right click databox to the management list
	public void setRightClickDataBox(ScreenDataBox rightClickDataBox) {
		this.topLayer.addElementToLayer(rightClickDataBox);
	}
	
	public boolean hasPopUps() {
		return !this.popupLayer.isEmpty();
	}
	
	//method for checking if a background has been set
	public boolean hasBackground() {
		return this.backgroundLayer.isEmpty();
	}

	@Override
	protected IScreenLayer giveBackgroundDestination() {
		return this.backgroundLayer;
	}

	@Override
	protected IScreenLayer giveButtonDestination() {
		return this.interfaceLayer;
	}

	@Override
	protected IScreenLayer giveDataBoxDestination() {
		return this.interfaceLayer;
	}

	@Override
	protected IScreenLayer giveScrollPanelDestination() {
		return this.gameFieldLayer;
	}

	@Override
	protected IScreenLayer giveTabDirectoryDestination() {
		return this.interfaceLayer;
	}

	@Override
	protected IScreenLayer givePopUpDestination() {
		return this.popupLayer;
	}
}

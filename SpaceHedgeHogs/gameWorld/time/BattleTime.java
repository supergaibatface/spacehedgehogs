package time;

import java.util.ArrayList;
import java.util.List;

import factories.IAutoMake;
import factories.MainFactory;
import timeListeners.ITimeListener;

//Represents turns in a battle
public class BattleTime implements IAutoMake {
	private MainFactory mainFactory;
	private List<ITimeListener> timeListeners;
	private int turn;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		turn = 0;
		this.timeListeners = new ArrayList<ITimeListener>();
	}
	
	//Method that gets an array of lines for the turn counter dataBox
	public ArrayList<String> getLines() {
		ArrayList<String> lines = new ArrayList<String>();
		lines.add("Turn: " + this.turn);
		return lines;
	}
	
	//Method for advancing the turn
	public void nextTurn() {
		this.turn++;
		this.triggerTimeListeners();
	}
	
	//Resets the turn counter
	public void resetTurnCounter() {
		this.turn = 0;
	}
	
	//getter for current turn value
	public int getCurrenTurn() {
		return this.turn;
	}
	
	//for notifying all time listeners that the turn changed
	private void triggerTimeListeners() {
		for(ITimeListener oneTimeListener : this.timeListeners) {
			oneTimeListener.timeAdvanced(true);
		}
	}
	
	//Adds a listener that listens for turn changes
	public void addTimeListener(ITimeListener newListener) {
		this.timeListeners.add(newListener);
	}
	
	//Removes a listener that waits for turn changes
	public void removeTimeListener(ITimeListener oldListener) {
		this.timeListeners.remove(oldListener);
	}

}

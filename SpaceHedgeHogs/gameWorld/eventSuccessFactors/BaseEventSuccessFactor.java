package eventSuccessFactors;

import factories.IAutoMake;
import factories.MainFactory;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;

//Base class for making event success factors
public abstract class BaseEventSuccessFactor implements IAutoMake, IEventSuccessFactor {
	protected MainFactory mainFactory;

	@Override
	public abstract boolean getValueType(IGameCharacter oneChar);

	@Override
	public abstract int getValueFactor(IGameCharacter oneChar);

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public abstract void bootUp();

}

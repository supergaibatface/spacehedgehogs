package screenManagement;

import java.util.List;

import frameworks.IStackable;
import view.IGameView;

public interface IGElementManager {
	
	/*
	 * Method for changing view on the screen
	 * Inputs:
	 * 	newView: GameView type of object that represents the view that screen gets switched to
	 * 	characters: Array list of all the character elements that will be added to the screen to a designated slot
	 */
	public void changeToView(IGameView newView);
	
	public List<IStackable> getAllElementsFromLayers();
	
	public boolean removeElementFromScreen(IStackable oldElement);
	
	public void addQuickPopUp(IStackable onePopUp);
	
	public void addPopUp(IStackable onePopUp);

}

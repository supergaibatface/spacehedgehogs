package inventory;

import factories.IAutoMake;
import factories.MainFactory;

//class that represents one group of weight carrying ability from a singular source
public class WeightCarryingAbility implements IAutoMake {
	private int capability;
	private MainFactory mainFactory;

	//getter for the carrying capability
	public int getCapability() {
		return capability;
	}

	//setter for the carrying capability
	public void setCapability(int capability) {
		this.capability = capability;
		this.triggerChangeVisual();
	}

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.capability = 0;
		this.registerOnController();
		
	}
	
	//method for registering itself on a weight carrying controller
	private void registerOnController() {
		this.mainFactory.getInventoryWeightController().registerWeightCarry(this);
	}
	
	//method for unregistering itself on a weight carrying controller
	private void unregisterOnController() {
		this.mainFactory.getInventoryWeightController().unRegisterWeightCarry(this);
	}
	
	//method for informing weight carrying controller about a change in carrying ability
	private void triggerChangeVisual() {
		this.mainFactory.getInventoryWeightController().visualUpdate();
	}
	
	//method for safely deleting this carrying ability element
	public void safeDelete() {
		this.unregisterOnController();
	}

}

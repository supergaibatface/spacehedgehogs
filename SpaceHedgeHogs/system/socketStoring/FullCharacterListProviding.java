package socketStoring;

import java.util.ArrayList;

import gameCharacters.GameCharacter;
import other.StaticFunctions;
import sreenElementsSockets.ScreenSimpleChar;

//Provider that takes a full character list from character manager and provides a screenSimpleChar version of it
public class FullCharacterListProviding extends BaseContentProviding<ScreenSimpleChar> {

	@Override
	public ArrayList<ScreenSimpleChar> getArrayOfSupplies() {
		return this.makeCharacters();
	}
	
	//Method for making all the visual character ingame characters
	private ArrayList<ScreenSimpleChar> makeCharacters() {
		ArrayList<ScreenSimpleChar> characters = new ArrayList<ScreenSimpleChar>();
		characters.addAll(this.mainFactory.getGameStateController().getIdleList());
		this.mainFactory.getGameStateController().getIdleList().clear();
		return characters;
	}

}

package jobChoiceController;

import java.util.ArrayList;

import factories.IAutoMake;
import factories.MainFactory;
import jobs.JobTopic;

/*
 * Controller for jobFrame, through this job frame is set up
 */
public class JobChoiceController implements IAutoMake {
	private MainFactory mainFactory;
	private ArrayList<IJobTopicController> jobTopicControllers;
	
	/*
	 * Hard coded method for setting up job topic controllers
	 */
	private void setUpJobTopicControllers() {
		//default jobs
		this.jobTopicControllers.add(this.mainFactory.getElemFactory().getDefaultTopicController(getNewJobTopicTEMP()));
		//terrain jobs
		this.jobTopicControllers.add(this.mainFactory.getElemFactory().getTerrainTopicController(getNewJobTopicTEMP()));
		//party stance jobs
		this.jobTopicControllers.add(this.mainFactory.getElemFactory().getPartyStanceTopicController(getNewJobTopicTEMP()));
		//battle jobs
		this.jobTopicControllers.add(this.mainFactory.getElemFactory().getBattleTopicController(getNewJobTopicTEMP()));
		//quest jobs
		this.jobTopicControllers.add(this.mainFactory.getElemFactory().getQuestTopicController(getNewJobTopicTEMP()));
	}
	
	//Sets up basic controllers
	private void setUpBasicControllers() {
		this.jobTopicControllers.add(this.mainFactory.getElemFactory().getBasicSurvivalTopicController(getNewJobTopicTEMP()));
		this.jobTopicControllers.add(this.mainFactory.getElemFactory().getBasicPartyStanceController(getNewJobTopicTEMP()));
	}
	
	/*
	 * Hard coded method for setting up job topic controllers for the proto show game version
	 */
	private void setUpProtoShowTopicControllers() {
		//default jobs
		this.jobTopicControllers.add(this.mainFactory.getElemFactory().getDefaultJobTopicControllerForProtoShow(getNewJobTopicTEMP()));
		//party stance jobs
		this.jobTopicControllers.add(this.mainFactory.getElemFactory().getPartyStanceJobTopicControllerForProtoShow(getNewJobTopicTEMP()));
	}
	
	/*
	 * Calls update on the jobFrame
	 */
	public void updateValues() {
		/*if(this.hasJobFrame()) {
			for(IJobTopicController oneController : this.jobTopicControllers) {
				oneController.updateSocketValues();
			}
			this.jobFrame.callLocationUpdate();
		}*/
	}

	private JobTopic getNewJobTopicTEMP() {
		return this.mainFactory.getElemFactory().getJobTopic(null);
	}

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.jobTopicControllers = new ArrayList<IJobTopicController>();
	}
	
	

}

package newJobs;

import java.util.ArrayList;
import java.util.List;

import barChange.BarGain;
import barChange.BarLimiter;
import eventSuccessFactors.IEventSuccessFactor;
import events.EventRequest;
import events.EventTagName;
import factories.IAutoMake;
import factories.MainFactory;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import generalGraphics.ButtonLookType;
import jobs.SkillGain;
import propertyBars.PropertyBarName;
import screenElements.ScreenButton;
import screenInfoPage.BaseInfoPage;
import sreenElementsSockets.ScrollCharSocket;

//Base class for all new type of jobs
public abstract class BaseJob implements IAutoMake, IJob {
	protected MainFactory mainFactory;
	protected List<EventTagName> workTags;
	protected List<BarLimiter> barRequirments;
	protected List<BarGain> barGains;
	protected List<SkillGain> skillGains;
	protected List<IEventSuccessFactor> eventSuccessFactors;
	protected ScrollCharSocket registeredSocket;
	protected List<ScreenButton> extraCommands;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.makeArrays();
		this.setUpJobElements();
		makeExtraCommandsList();
	}
	
	//method for making all the lists that keep information about the job
	protected void makeArrays() {
		this.workTags = new ArrayList<EventTagName>();
		this.barRequirments = new ArrayList<BarLimiter>();
		this.barGains = new ArrayList<BarGain>();
		this.skillGains = new ArrayList<SkillGain>();
		this.eventSuccessFactors = new ArrayList<IEventSuccessFactor>();
	}
	
	//method that sets up all the saved values of this job
	protected void setUpJobElements() {
		this.setWorkTags(this.workTags);
		this.setBarCost(this.barRequirments);
		this.setBarGains(this.barGains);
		this.setSkillGains(this.skillGains);
		this.addEventSuccessFactors(this.eventSuccessFactors);
	}
	
	//method for setting all work tags, input is the list where they need to be saved
	protected abstract void setWorkTags(List<EventTagName> tagList);
	
	//method for saving all bar costs, input is the list where they need to be saved
	protected abstract void setBarCost(List<BarLimiter> limiterList);
	
	//method for saving all bar gains, input is the list where they need to be saved
	protected abstract void setBarGains(List<BarGain> gainList);
	
	//method for setting all skill gains, input is the list where they need to be saved
	protected abstract void setSkillGains(List<SkillGain> gainList);
	
	//method for setting all event success factors, input is the list where they need to be saved
	protected abstract void addEventSuccessFactors(List<IEventSuccessFactor> factorList);
	
	/*
	 * Does a full check if an assigned character matches all character and item requirements to do this job
	 * Input:
	 * 	gameChar: game character that will be checked
	 */
	public boolean fullReqCheck(IGameCharacter gameChar) {
		if(!this.matchesCharacterRequirements(gameChar)) {
			return false;
		}
		if(!this.matchesItemRequirements()) {
			return false;
		}
		return true;
	}
	
	//Checks if character matches character based job requirements 
	private boolean matchesCharacterRequirements(IGameCharacter gameChar) {
		for(BarLimiter oneLimiter : this.barRequirments) {
			if(!oneLimiter.checkChar(gameChar)) {
				return false;
			}
		}
		return true;
	}
	
	//Checks if player matches the item requirements of doing this job
	private boolean matchesItemRequirements() {
		return true;
	}
	
	//Method that enacts job requirement prices for doing it
	public void doResults(IGameCharacter target) {
		doExtraCodePerWorkingCharacter(target);
		doBarCosts(target);
		doBarGains(target);
		doSkillGains(target);
	}
	
	//Does required bar costs on a char
	private void doBarCosts(IGameCharacter target) {
		for(BarLimiter oneBar : this.barRequirments) {
			oneBar.doCost(target);
		}
	}
	
	//Does designated bar gains on a character
	private void doBarGains(IGameCharacter target) {
		for(BarGain oneGain : this.barGains) {
			oneGain.doGain(target);
		}
	}
	
	//Assigns new skill gains to a character
	private void doSkillGains(IGameCharacter target) {
		for(SkillGain oneSkillGain : this.skillGains) {
			oneSkillGain.giveExp(target);
		}
	}
	
	protected abstract void doExtraCodePerWorkingCharacter(IGameCharacter target);
	
	//Method for making a list of characters assigned to a job work through it
	public void workThrough(List<IGameCharacter> characters) {
		for(IGameCharacter oneChar: characters) {
			doResults(oneChar);
		}
		this.sendEventRequest(characters);
	}
	
	//method that sends event requests to the event caller for a list of characters, input is the list
	private void sendEventRequest(List<IGameCharacter> characters) {
		if(characters.isEmpty()) {
			return;
		}
		this.mainFactory.getEventCaller().sendEventRequest(this.makeEventRequest(characters));
	}
	
	//method for making an event request that fits this job, input is the list of characters for who are in the request
	private EventRequest makeEventRequest(List<IGameCharacter> characters) {
		EventRequest finalRequest = new EventRequest(characters, this.workTags);
		addSuccessFactorsToEventRequest(finalRequest);
		return finalRequest;
	}
	
	//Method for adding saved success factors to the event request, input is the request
	private void addSuccessFactorsToEventRequest(EventRequest eventRequest) {
		for(IEventSuccessFactor oneFactor : this.eventSuccessFactors) {
			eventRequest.addEventSuccessFactor(oneFactor);
		}
	}
	
	//Checks if there has been a stamine limiter assigned to this job
	public boolean hasStamineLimiter() {
		for(BarLimiter oneBar : this.barRequirments) {
			if(oneBar.getLimitedBarName() == PropertyBarName.Stamina) {
				return true;
			}
		}
		return false;
	}
	
	//Getter for the stamina limiter
	public BarLimiter getStaminaLimiter() {
		for(BarLimiter oneBar : this.barRequirments) {
			if(oneBar.getLimitedBarName() == PropertyBarName.Stamina) {
				return oneBar;
			}
		}
		return null;
	}
	
	@Override
	public void registerSocketForJob(ScrollCharSocket socket) {
		this.registeredSocket = socket;
	}
	
	//checks if this job has been registered to a socket
	public boolean hasRegisteredSocket() {
		return this.registeredSocket != null;
	}
	
	public void socketGoingOnScreenScriptPoint() {
		this.socketGoingOnScreenCodeSpace();
	}
	
	protected abstract void socketGoingOnScreenCodeSpace();
	
	public List<ScreenButton> getExtraCommandButtons() {
		return this.extraCommands;
	}
	
	private void makeExtraCommandsList() {
		this.extraCommands = new ArrayList<ScreenButton>();
		this.extraCommands.add(this.makeInfoPageButton());
		this.addButtonsToExtraCommandsList(extraCommands);
	}
	
	protected abstract void addButtonsToExtraCommandsList(List<ScreenButton> saveLocationList);
	
	protected ScreenButton makeInfoPageButton() {
		ScreenButton newButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.POPUPSELECT, "job info");
		newButton.addButtonEventListener(this.mainFactory.getElemFactory().getOpenElementOnTopLayerListener(this.getInfoPageForThisJob()));
		return newButton;
	}
	
	private BaseInfoPage getInfoPageForThisJob() {
		return this.mainFactory.getElemFactory().getJobInfoPage(this);
	}
	
	

}

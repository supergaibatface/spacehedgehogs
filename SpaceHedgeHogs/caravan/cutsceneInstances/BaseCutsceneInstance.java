package cutsceneInstances;

import buttonListeners.ISButtonListener;
import factories.IAutoMake;
import factories.MainFactory;
import view.CutsceneDataCollection;

public abstract class BaseCutsceneInstance implements IAutoMake, ICutsceneInstance {
	protected MainFactory mainFactory;
	protected CutsceneDataCollection savedCutscene;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.initalizeCutscene();
		this.makeCutsceneScenes(savedCutscene);
		
	}
	
	public void startSavedCutscene() {
		this.mainFactory.getCutsceneBuilder().startACutscene(savedCutscene);
	}
	
	public CutsceneDataCollection getSavedCutScene() {
		return this.savedCutscene;
	}
	
	protected void initalizeCutscene() {
		this.savedCutscene = this.mainFactory.getElemFactory().getCutsceneDataCollection(this.giveCutsceneName());
	}
	
	protected abstract String giveCutsceneName();
	
	protected abstract void makeCutsceneScenes(CutsceneDataCollection saveLocation);
	
	public void addButtonListenersToEnd(ISButtonListener listener) {
		this.savedCutscene.addFinalButtonEventListener(listener);
	}
	

}

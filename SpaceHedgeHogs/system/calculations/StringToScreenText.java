package calculations;

import java.awt.Font;
import java.awt.FontMetrics;
import java.util.ArrayList;

import factories.IAutoMake;
import factories.MainFactory;
import screenElements.ScreenText;

//Converter class that will turn string to screen texts using string builder
public class StringToScreenText implements IAutoMake{
	private MainFactory mainFactory;
	private FontMetrics fontMetrics;

	//setter for main factory
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	//bootup method
	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	/*
	 * Method for fetching font metrics
	 */
	private void getFontMetrics(int size) {
		this.fontMetrics = this.mainFactory.getScreen().getFontMetrics(new Font("Tahoma", Font.PLAIN, size));
	}
	
	//Cuts up the string into an array of words
	public ArrayList<String> stringSplitter(String text) {
		String[] words = text.split(" ");
		int limit = this.getLineLength();
		return lineBuilding(words, limit);
		
	}
	
	/*
	 * String splitter that will later throw text into line builder that uses graphical line length
	 * Inputs:
	 * 	text: text that will be split up
	 * 	dontSize: font size used for the text
	 * 	graphicalSize: length of one line
	 */
	public ArrayList<String> stringSplitter2(String text, int fontSize, int graphicalSize) {
		String[] words = text.split(" ");
		return lineBuildingWithLength(words, fontSize, graphicalSize);
	}
	
	/*
	 * Builds lines of string according to how many letters are allowed into a line
	 * Inputs:
	 * 	words: array of all the words that need to be asigned
	 * 	lineLength: int value of how many letters are allowed into a line
	 */
	private ArrayList<String> lineBuilding(String[] words, int lineLength) {
		ArrayList<String> newLines = new ArrayList<String>(); 
		StringBuilder builder = new StringBuilder(lineLength);
		for (String word : words) {
			wordPlacer(word, builder, newLines, lineLength);
		}
		newLines.add(builder.toString());
		return newLines;
		
	}
	
	/*
	 * Line builder that builds lines that get as close to designated graphical size as possible
	 * Inputs:
	 * 	words: array of individual words
	 * 	fontSize: size of the text font
	 * 	graphicalSize: the assigned max length for one line
	 */
	private ArrayList<String> lineBuildingWithLength(String[] words, int fontSize, int graphicalSize) {
		ArrayList<String> newLines = new ArrayList<String>();
		this.getFontMetrics(fontSize);
		StringBuilder builder = new StringBuilder();
		for (String word : words) {
			graphicalLengthWordPlacer(word, builder, newLines, graphicalSize);
		}
		newLines.add(builder.toString());
		return newLines;
	}
	
	/*
	 * Command that places words into a line
	 * Inputs:
	 * 	word: word that is currently decided on
	 * 	builder: string builder that has last open line saved to id
	 * 	lines: array of already assembled lines
	 * 	graphicalSize: max length of a line
	 */
	private void graphicalLengthWordPlacer(String word, StringBuilder builder, ArrayList<String> lines, int graphicalSize) {
		if(builder.length() == 0) {
			builder.append(word);
			builder.append(" ");
		}
		else if((this.fontMetrics.stringWidth(builder.toString()) + this.fontMetrics.stringWidth(word)) < graphicalSize) {
			//still fits the line
			builder.append(word);
			builder.append(" ");
		}
		else {
			lines.add(builder.toString());
			builder.setLength(0);
			builder.append(word);
			builder.append(" ");
		}
		
	}
	
	/*
	 * Places words into a string builder till a line is finished and then starts a another line if needed
	 * Inputs:
	 * 	word: current word that needs to be placed
	 * 	builder: the string builder used for building lines
	 * 	lines: the array that contains finished text lines
	 * 	lineLength: the amount of letter allowed into a line
	 */
	private void wordPlacer(String word, StringBuilder builder, ArrayList<String> lines, int lineLength) {
		if(builder.length() == 0) {
			builder.append(word);
		}
		else if(builder.length() + word.length() + 1 < lineLength){
			builder.append(" ");
			builder.append(word);
		}
		else {
			lines.add(builder.toString());
			builder.setLength(0);
			builder.append(word);
		}
	}
	
	//getter for allowd line length
	public int getLineLength() {
		return 20;
	}
	
	

}

package propertyBarListeners;

//interface for all property bar change listeners
public interface IBarChangeListener {
	
	//method that is called when property bar value changes
	public void barChangeHappened(int newValue);

}

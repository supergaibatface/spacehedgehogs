package itemBase;

import java.util.ArrayList;

//enum for all the item names in the game
public enum TradeItemName {
	Berry,
	Mushroom,
	Roots,
	FreshMeat,
	Herbs,
	Apple,
	Sword, 
	Barricade,
	Horse,
	BrokenSword;
	
	//Constructor
	private TradeItemName() {
	}

}

package buttonLimiters;

//limiter class that limits player action when there ins't enough carrying ability
public class NotEnoughCarryLimiter extends BaseButtonLimiter {

	/*
	 * constructor
	 * Input:
	 * 	notifyOnFailure: if true, will sent a notification when player doesn't meet the requirement
	 * 	instantPopUp: if true will open the popup automatically when player fails
	 */
	public NotEnoughCarryLimiter(boolean notifyOnFailure, boolean instantPopUp) {
		super(notifyOnFailure, instantPopUp);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean check() {
		return this.mainFactory.getInventoryWeightController().hasEnoughCarryingAbility();
	}

	@Override
	public String reasonForFail() {
		// TODO Auto-generated method stub
		return "Not enough people are assigned to luggage management";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doCost() {
		// TODO Auto-generated method stub
		
	}

}

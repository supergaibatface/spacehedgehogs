package factories;

import screenElements.ScreenElement;

public abstract class BaseFactory {
	
	protected abstract MainFactory giveMainFactory();
	
	/*
	 * method for setting main factory, booting up and loading an image to a graphical element
	 * Inputs:
	 * 	element: screen element that will be set up
	 * 	image: string name of the elements skin
	 */
	protected <T extends ScreenElement> T setUpNewGraphicalElement(T element, String image) {
		this.setUpNewElement(element);
		element.setImage(image);
		return element;
	}
	
	/*
	 * method for setting main factory and booting up a object before returning it
	 * Input:
	 * 	element: the object that will be set up
	 */
	protected <T extends IAutoMake> T setUpNewElement(T element) {
		element.setMainFactory(this.giveMainFactory());
		element.bootUp();
		return element;
	}
	
	public <T extends IAutoMake> T setUpNewNonPerservativeValue(T element) {
		return setUpNewElement(element);
	}

}

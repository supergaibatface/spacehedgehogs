package view;

import java.util.ArrayList;
import java.util.List;

import jobChoiceController.JobChoiceController;
import screenElements.ScreenBackground;
import screenElements.ScreenButton;
import screenElements.ScreenDataBox;
import screenElements.ScreenScrollPanel;
import tabInterface.TabDirectory;

public interface IGameView {
	
	//checks if the view has a background set
	public boolean hasBackground();
	
	//getter for background element
	public ScreenBackground getBackground();
	
	//Getter for the button elements
	public List<ScreenButton> getButtons();
	
	//getter for data boxes
	public List<ScreenDataBox> getDataBoxes();
	
	//checks if there is a scrollable main screen
	public boolean hasScrollableMainScreen();
	
	//getter for the scroll panel
	public ScreenScrollPanel getScrollableMainScreen();
	
	//Getter for the main tab directory
	public TabDirectory getMainTabDirectory();
	
	//equal function for comparing view names
	public boolean equals(ViewName name);
	
	//Method for adding a new wrapped databox
	public void addDataBox(ScreenDataBox newDataBox, int xCoord, int yCoord);
	
	/*
	 * Setter for the game time coordinates
	 * Inputs:
	 *  x: x coordinate for the text (bottom left corner of the box that the text is in)
	 * 	y: y coordinate for the text (bottom left corner of the box that the text is in)
	 */
	public void setGameTimeCoord(int x, int y);
	
	public ViewName getName();
	
	public void goingOnScreenAlert();

}

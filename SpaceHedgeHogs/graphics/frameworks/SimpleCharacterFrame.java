package frameworks;

import java.util.ArrayList;
import java.util.List;

import other.StaticFunctions;
import screenElements.ScreenElement;
import screenElements.ScreenText;
import sreenElementsSockets.BaseScrollSocket;
import sreenElementsSockets.ScreenSimpleChar;
import sreenElementsSockets.ScrollCharSocket;

/*
 * Framework for storing simple screen characters
 */
public class SimpleCharacterFrame extends BaseFrame {
	private ArrayList<ScreenSimpleChar> characters;
	private int extraRoomLen;

	//Constructor
	public SimpleCharacterFrame(IStackable wrapper) {
		super(wrapper);
		// TODO Auto-generated constructor stub
	}
	
	public void bootUp() {
		super.bootUp();
		this.characters = new ArrayList<ScreenSimpleChar>();
		this.extraRoomLen = 10;
	}
	
	/*
	 * Method for adding new elements
	 * Input:
	 * 	newCharacter: new character that will be added to the frame
	 */
	public boolean addNewElement(ScreenSimpleChar newCharacter) {
		if(!canAddNewElement()) {
			return false;
		}
		newCharacter.setCoordinates(getNewElementX(), getNewElementY());
		this.characters.add(newCharacter);
		newCharacter.setWrapper(this);
		return true;
	}
	
	//Liberates all stored characters on this frame, sends them to idle list in the gameState
	public void liberateAllElements() {
		this.mainFactory.getGameStateController().addCharListToIdleList(characters);
		this.characters.clear();
	}
	
	//Method that shows if its allowed to add new element here
	public boolean canAddNewElement() {
		return true;
	}
	
	//Method that provides a x coordinate for a new element
	public int getNewElementX() {
		return 0;
	}
	
	//Method that provides a y coordinate for a new element
	public int getNewElementY() {
		if(this.characters.isEmpty()) {
			return 0;
		}
		return this.getFullHeight() + this.extraRoomLen;
	}
	
	//Method for removing elements from the frame
	public boolean removeElement(ScreenSimpleChar oldCharacter) {
		if(!characters.contains(oldCharacter)) {
			return false;
		}
		int location = this.characters.indexOf(oldCharacter);
		this.characters.remove(oldCharacter);
		this.moveLocationsUpByOne(location);
		return true;
	}
	
	//Removes all elements from the frame
	public void removeAllElements() {
		this.characters.clear();
	}
	
	//Getter for all elements on this framework
	public ArrayList<ScreenSimpleChar> getAllElements() {
		return StaticFunctions.copyArray(this.characters);
	}
	
	//Returns the wrapper as scrollSocket, if wrapper is not scroll socket returns null
	public ScrollCharSocket getWrapperAsScrollSocket() {
		if(this.wrapper.getClass().equals(ScrollCharSocket.class)) {
			return (ScrollCharSocket) this.wrapper;
		}
		return null;
	}

	/*
	 * Debug method for printing out all child elements
	 */
	public void printOutAllChildern() {
		System.out.println("Starting with new print out children command");
		for(ScreenSimpleChar oneChar : this.characters) {
			printOutOneChild(oneChar);
		}
		System.out.println("------------------");
	}
	
	/*
	 * Debug method for printing out one child
	 */
	private void printOutOneChild(ScreenSimpleChar oneChar) {
		if(oneChar.getDisplayData().isUsesData()) {
			System.out.println("element uses data");
			oneChar.getDisplayData().printOutData(oneChar.getMaster().getName());
			System.out.println("---------------");
		}
		else {
			System.out.println("element "+oneChar.getMaster().getName() + " doesn't use data");
			System.out.println("---------------");
		}
	}

	
	/*
	 * From a point in the array moves all elements graphical locations up by one, 
	 * used for fixing locations when an element is removed from the socket
	 */
	protected void moveLocationsUpByOne(int startFrom) {
		for(int i = startFrom; i < this.characters.size(); i++) {
			ScreenSimpleChar temp = this.characters.get(i);
			temp.setCoordinates(xForElement(), yForElement(i, temp.getHeight()));
		}
	}
	
	/*
	 * gives x for case of recalculating element coordinates
	 */
	public int xForElement() {
		return this.getNewElementX();
	}

	/*
	 * Calculates the y coordinate for the element that is in the socket
	 * Inputs
	 * 	nr: the elements placement number in the socket
	 * 	elementHeight: the height of the element that will be fitted into the socket
	 * Output
	 * 	y coordinate as an integer
	 */
	private int yForElement(int nr, int elementHeight) {
		return (elementHeight + this.extraRoomLen) * nr;
	}

	@Override
	public int getFullHeight() {
		int height = 0;
		for(ScreenSimpleChar oneChar : this.characters) {
			height = height + oneChar.getHeight() + this.extraRoomLen;
		}
		if(height > 0) {
			height = height - this.extraRoomLen;
		}
		return height;
	}

	@Override
	public int getFullLength() {
		if(this.characters.isEmpty()) {
			return 0;
		}
		else {
			return this.characters.get(0).getLength();
		}
	}

	@Override
	public void fillTopElementList(List<IStackable> list) {
		if(!this.characters.isEmpty()) {
			list.addAll(characters);
		}
		
	}

	@Override
	public void fillTopTextList(List<ScreenText> list) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOffScreenScript() {
		// TODO Auto-generated method stub
		
	}

}

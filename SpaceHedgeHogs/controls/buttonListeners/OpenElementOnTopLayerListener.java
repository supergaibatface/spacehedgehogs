package buttonListeners;

import frameworks.IStackable;

//Will open a desginated info page on screen (will list it in graphical management)
public class OpenElementOnTopLayerListener extends BaseButtonListener {
	private IStackable element;
	
	//constructor, input is the info page that will be opened
	public OpenElementOnTopLayerListener(IStackable element) {
		this.element = element;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.mainFactory.getGElementManager().openElementOnTopLayer(this.element);
		
	}

	@Override
	public String toInfo() {
		return "Opens info page";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

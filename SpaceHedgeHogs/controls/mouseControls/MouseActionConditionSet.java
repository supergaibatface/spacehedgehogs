package mouseControls;

import java.util.ArrayList;
import java.util.List;

import factories.IAutoMake;
import factories.MainFactory;
import screenElements.ScreenElement;

//Class that keeps track of current player actions, what is active what has been clicked on
public class MouseActionConditionSet implements IAutoMake{
	private MainFactory mainFactory;
	private ScreenElement selectedItem;
	private List<ScreenElement> allClickedElements;
	private int clickX;
	private int clickY;
	private boolean clickedOnPopUp;
	private boolean activePopUp;
	private int latestMoustButton;
	
	//constructor
	public MouseActionConditionSet() {
		
	}

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.allClickedElements = new ArrayList<ScreenElement>();
		this.latestMoustButton = 0;
		updateCondition();
	}
	
	//Method for updating current conditions
	public void updateCondition() {
		clearPrevious();
		if(this.mainFactory.getActivityController().getActivityState()) {
			this.selectedItem = this.mainFactory.getActivityController().getSelectedChar();
		}
		if(!this.mainFactory.getGElementManager().hasPopUps()) {
			this.activePopUp = true;
		}
		else {
			this.activePopUp = false;
		}
	}

	/*
	 * Sets a list of new clicked elements
	 * Inputs:
	 * 	elements: list of elements that were clicked
	 * 	onPopUp: boolean value that shows if the click happened ontop of a popUp
	 * 	mouseButton: int value for the mouse button that was clicked
	 */
	public void setNewClickedItems(List<ScreenElement> elements, boolean onPopUp, int mouseButton) {
		this.allClickedElements.addAll(elements);
		this.clickedOnPopUp = onPopUp;
		this.latestMoustButton = mouseButton;
	}

	/*
	 * setter for the exact coordinates for the last click
	 * Inputs: 
	 * 	x: x coordinate of the click
	 * 	y: y coordinate of the click
	 */
	public void setNewClickCoords(int x, int y) {
		this.clickX = x;
		this.clickY = y;
	}
	
	public int getClickX() {
		return this.clickX;
	}
	
	public int getClickY() {
		return this.clickY;
	}
	
	/*
	 * Clears current condition values
	 */
	public void clearValues() {
		this.clearPrevious();
	}

	/*
	 * Clears individual saved situation values
	 */
	private void clearPrevious() {
		this.allClickedElements.clear();
		this.selectedItem = null;
	}
	
	/*
	 * checks if player clicked on a popUp
	 */
	public boolean isClickedOnPopup() {
		return this.clickedOnPopUp;
	}
	
	/*
	 * Checks if there is an active popUp
	 */
	public boolean isActivePopup() {
		return this.activePopUp;
	}
	
	/*
	 * Returns current activated element
	 */
	public ScreenElement getSelectedItem() {
		return this.selectedItem;
	}
	
	/*
	 * return current clicked item
	 */
	public ScreenElement getClickedItem(int nr) {
		if(nr < this.allClickedElements.size()) {
			return this.allClickedElements.get(nr);
		}
		return null;
	}
	
	/*
	 * return current clicked items
	 */
	public List<ScreenElement> getClickedItems() {
		return this.allClickedElements;
	}
	
	
	/*
	 * Debug pring function
	 */
	public void printingOutConditions() {
		System.out.println("Start of conditions");
		if(this.selectedItem == null) {
			System.out.println("selected item: null");
		}
		else {
			System.out.println("selected item: "+ this.selectedItem.getClass());
		}
		if(this.allClickedElements.size() == 0) {
			System.out.println("clicked item: none");
		}
		else {
			for(ScreenElement oneElement : this.allClickedElements) {
				System.out.println("clicked item: "+ oneElement.getClass());
			}
		}
		
		System.out.println("is on pop up: "+this.clickedOnPopUp);
		System.out.println("---------------");
	}

	//getter for the int value of the last mouse click
	public int getLatestMoustButton() {
		return latestMoustButton;
	}
	
	//check if latest mouse click was left click
	public boolean isLatestMouseButtonLeft() {
		return this.latestMoustButton == 1;
	}
	
	//checks if latest mouse click was right click
	public boolean isLatestMoustButtonRight() {
		return this.latestMoustButton == 3;
	}
	
	

}

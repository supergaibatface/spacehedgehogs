package mouseSituationSet;

//Represents battle views default situation set with nothing active
public class MSSBattleDefault extends BaseMouseSituationSet {

	@Override
	protected void makeAllCAElements() {
		this.addNewCAElement(this.mainFactory.getCAButtonPress());
		this.addNewCAElement(this.mainFactory.getCACharSelect());
		this.addNewCAElement(this.mainFactory.getCAItemSelecting());
		this.addNewCAElement(this.mainFactory.getCAFieldCharSelect());
		this.addNewCAElement(this.mainFactory.getCARScreenCharacter());
		this.addNewCAElement(this.mainFactory.getCARScreenItem());
	}

	@Override
	public boolean checkSituation() {
		if(this.conditions.getSelectedItem() != null) {
			return false;
		}
		if(!this.mainFactory.getGameStateController().inBattle()) {
			return false;
		}
		
		return true;
	}

}

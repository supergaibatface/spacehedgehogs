package propertyBars;

import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;

//Object for storing a property bars value in basic form
public class SavedPropertyBar {
	private PropertyBarName barName;
	private int currentValue;
	
	/*
	 * Constructor:
	 * Inputs:
	 * 	name: Property bar name of the saved bar
	 * 	currentValue: int version of the bars current value
	 */
	public SavedPropertyBar(PropertyBarName name, int currentValue) {
		this.barName = name;
		this.currentValue = currentValue;
	}

	//Getter for bars name
	public PropertyBarName getBarName() {
		return barName;
	}

	//setter for the bars name
	public void setBarName(PropertyBarName barName) {
		this.barName = barName;
	}

	//getter for the current value
	public int getCurrentValue() {
		return currentValue;
	}

	//setter for the current value
	public void setCurrentValue(int currentValue) {
		this.currentValue = currentValue;
	}
	
	/*
	 * Will implement the saved bar value on a given character
	 * Inputs:
	 * 	targetChar: character whos specified property bar will be set to the saved value
	 */
	public boolean fillBarOnChar(IGameCharacter targetChar) {
		PropertyBar foundBar = targetChar.getPropertyBar(barName);
		if(foundBar != null) {
			foundBar.setCurrentValue(currentValue);
			return true;
		}
		else {
			return false;
		}
	}

}

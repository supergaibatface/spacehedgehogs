package buttonListeners;

import propertyBars.PropertyBar;

//Listener that adds specific value to a bar on activation, if value is set to 0 then the bar will fully refilled
public class AddValueToBarListener extends BaseButtonListener {
	private PropertyBar savedBar;
	private int value;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	bar: bar that will be filled
	 * 	value: the amount that will be restored, if 0 will refill the whole bar
	 */
	public AddValueToBarListener(PropertyBar bar, int value) {
		this.savedBar = bar;
		this.value = value;
	}
	
	/*
	 * Constructor
	 * Inputs:
	 * 	bar: bar that will be refilled fully
	 */
	public AddValueToBarListener(PropertyBar bar) {
		this.savedBar = bar;
		this.value = 0;
	}
	
	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		if(this.value == 0) {
			this.savedBar.fillUp();
		}
		else {
			this.savedBar.increaseByValue(value);
		}
		
	}

	@Override
	public String toInfo() {
		if(this.value == 0) {
			return "Fills up the " + this.savedBar.getName();
		}
		else {
			return "Adds " + this.value + " to " + this.savedBar.getName();
		}
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

package inventoryChange;

import java.util.EventObject;

import itemBase.IGameItem;

//Inventory new item event that is triggered when a new item is added to the inventory
public class InventoryNewItemEvent extends EventObject {
	private static final long serialVersionUID = 1L;
	private IGameItem newItem;

	//constructor
	public InventoryNewItemEvent(Object arg0, IGameItem newItem) {
		super(arg0);
		this.newItem = newItem;
	}
	
	//Getter for the new item
	public IGameItem getNewItem() {
		return this.newItem;
	}

}

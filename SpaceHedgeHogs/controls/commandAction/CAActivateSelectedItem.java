package commandAction;

import combatFieldElements.MovementSquare;
import itemActivation.ItemActivationMaster;
import itemBase.IGameItem;
import other.StaticFunctions;
import screenElements.ScreenElement;
import sreenElementsSockets.ScreenSimpleItem;

//Command action that represents activating a selected item from Socket
public class CAActivateSelectedItem extends CABaseClass<MovementSquare> {
	private ItemActivationMaster activationSelector;
	private MovementSquare selectedSquare;
	private boolean debug = false;
	
	public void bootUp() {
		super.bootUp();
		this.activationSelector = this.mainFactory.getItemActivationMaster();
	}

	@Override
	public boolean inputIsCorrect() {
		int nr = this.hasItemTypeClicked();
		if(nr<0) {
			return false;
		}
		selectedSquare = this.castToClickTargetClass(conditions.getClickedItem(nr));
		if(!this.checkSquareFromActiveSquares(selectedSquare)) {
			return false;
		}
		return true;
	}

	@Override
	public void actionImplement() {
		ScreenSimpleItem selectedItem = (ScreenSimpleItem) conditions.getSelectedItem();
		IGameItem transferItem = selectedItem.getMaster().SplitItem(1);
		StaticFunctions.writeOutDebug(debug, "remaining items: " + selectedItem.getMaster().getName() + " amount: " + selectedItem.getMaster().getSupply());
		if(!selectedItem.getMaster().hasPossesion()) {
			selection.takeItemAway();
		}
		activationSelector.ActivateItem(transferItem, selectedSquare);
		this.mainFactory.getGameStateController().getBattleController().clearVisuals();	
	}

	@Override
	public Class<MovementSquare> setClickTargetClass() {
		return MovementSquare.class;
	}

	@Override
	protected int giveMouseButtonNr() {
		return this.giveLeftMouseButtonValue();
	}

}

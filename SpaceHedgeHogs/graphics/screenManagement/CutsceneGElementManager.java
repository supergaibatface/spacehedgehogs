package screenManagement;

import java.util.List;

import view.IGameView;

public class CutsceneGElementManager extends BaseGElementManager {
	private IScreenLayer backgroundLayer;
	private IScreenLayer interfaceLayer;
	private IScreenLayer uncounterLayer;

	@Override
	protected void addLayersToList(List<IScreenLayer> layerList) {
		this.makeGraphicLayers(layerList);
		
	}
	
	private void makeGraphicLayers(List<IScreenLayer> layerList) {
		this.backgroundLayer = this.makeStandardLayer(layerList);
		this.interfaceLayer = this.makeStandardLayer(layerList);
		this.uncounterLayer = this.mainFactory.getElemFactory().getStandardGraphicLayer();
		
	}
	
	private IScreenLayer makeStandardLayer(List<IScreenLayer> layerList) {
		IScreenLayer newLayer = this.mainFactory.getElemFactory().getStandardGraphicLayer();
		layerList.add(newLayer);
		return newLayer;
	}

	@Override
	protected String giveManagerName() {
		return "Cutscene Graphical Element Manager";
	}

	@Override
	protected IScreenLayer giveBackgroundDestination() {
		return this.backgroundLayer;
	}

	@Override
	protected IScreenLayer giveButtonDestination() {
		return this.interfaceLayer;
	}

	@Override
	protected IScreenLayer giveDataBoxDestination() {
		return this.interfaceLayer;
	}

	@Override
	protected IScreenLayer giveScrollPanelDestination() {
		return this.uncounterLayer;
	}

	@Override
	protected IScreenLayer giveTabDirectoryDestination() {
		return this.uncounterLayer;
	}

	@Override
	protected IScreenLayer givePopUpDestination() {
		return this.interfaceLayer;
	}
	
	protected void viewSwitch(IGameView newView) {
		super.viewSwitch(newView);
		writingDebugMessageForViewSwitch();
	}
	
	private void writingDebugMessageForViewSwitch() {
		if(!this.uncounterLayer.isEmpty()) {
			System.out.println("Bug: there are elements in uncounted layer for Cutscene Graphical Element Manager");
		}
	}

}

package buttonListeners;

import java.util.ArrayList;

import calculations.MovementCalculator;
import combatFieldElements.FieldCharacter;
import combatFieldElements.MovementSquare;
import controllers.ActivityController;
import generalControls.TravelPath;
import propertyBars.PropertyBarName;

//ButtonListener that will trigger showing movement paths on a movement field
public class ShowMovementPathsListener extends BaseButtonListener {
	private ActivityController activity;
	private MovementCalculator moveCalc;

	@Override
	public void bootUp() {
		this.activity = this.mainFactory.getActivityController();
		this.moveCalc = this.mainFactory.getMovementCalculator();
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		FieldCharacter selectedChar = this.activity.getSelectedCharAsFieldChar();
		ArrayList<TravelPath> paths = this.moveCalc.makePossiblePaths(selectedChar.getLocation(), 
				selectedChar.getMasterCharacter().getPropertyBar(PropertyBarName.Movement).getCurrentValue());
		this.mainFactory.getGameStateController().getBattleController().setMovementPaths(paths);
	}

	@Override
	public String toInfo() {
		return "Shows active characters movement paths";
	}

}

package buttonListeners;

import screenElements.ScreenButton;

/*
 * Button listeners that triggers a different button press on pressing a button
 */
public class ActivateAnotherButtonListener extends BaseButtonListener {
	private ScreenButton secondButton;
	
	/*
	 * constructor
	 * Inputs
	 * 	secondButtons: the other button that will get activated
	 */
	public ActivateAnotherButtonListener(ScreenButton secondButton) {
		this.secondButton = secondButton;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.secondButton.activate();
		
	}

	@Override
	public String toInfo() {
		return "activates another button";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

package squareFilters;

import combatFieldElements.ControlFaction;
import combatFieldElements.FieldCharacter;
import combatFieldElements.MovementSquare;

//filters out squares that are occupied by chars from a designated faction
public class NoThisFactionChars implements ISquareFilter {
	private ControlFaction filterFaction;
	
	//sets the faction that will be filtered out with this filter
	public NoThisFactionChars(ControlFaction oneFaction) {
		this.filterFaction = oneFaction;
	}

	@Override
	public boolean filterSquare(MovementSquare oneSquare) {
		if(oneSquare.isOccupied()) {
			if(oneSquare.getFieldElement().getClass().equals(FieldCharacter.class)) {
				FieldCharacter oneChar = (FieldCharacter) oneSquare.getFieldElement();
				if(oneChar.getFaction().equals(filterFaction)) {
					return false;
				}
			}
		}
		return true;
	}

}

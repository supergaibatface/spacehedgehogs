package newJobs;

import java.util.List;

import barChange.BarGain;
import barChange.BarLimiter;
import eventSuccessFactors.IEventSuccessFactor;
import events.EventTagName;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import jobs.SkillGain;
import propertyBars.PropertyBarName;
import screenElements.ScreenButton;
import skills.SkillName;

//Job that is about looking for food
public class FoodSearchJob extends BaseJob {

	@Override
	protected void setWorkTags(List<EventTagName> tagList) {
		tagList.add(EventTagName.SearchForFood);
		
	}

	@Override
	protected void setBarCost(List<BarLimiter> limiterList) {
		limiterList.add(this.mainFactory.getElemFactory().getPropertyBarLimiter(PropertyBarName.Stamina, 20));
		
	}
	
	@Override
	protected void setBarGains(List<BarGain> gainList) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void setSkillGains(List<SkillGain> gainList) {
		gainList.add(this.mainFactory.getElemFactory().getSkillGain(SkillName.Scouting, 3));
		
	}

	@Override
	public void newElementAdded(IGameCharacter newChar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void oldElementRemoved(IGameCharacter oldChar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void safeDelete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void addEventSuccessFactors(List<IEventSuccessFactor> factorList) {
		factorList.add(this.mainFactory.getElemFactory().getSimpleSkillFactor(SkillName.Scouting));
		
	}

	@Override
	protected void socketGoingOnScreenCodeSpace() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void addButtonsToExtraCommandsList(List<ScreenButton> saveLocationList) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void doExtraCodePerWorkingCharacter(IGameCharacter target) {
		// TODO Auto-generated method stub
		
	}

}

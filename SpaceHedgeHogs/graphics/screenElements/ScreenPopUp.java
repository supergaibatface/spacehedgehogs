package screenElements;

import java.util.ArrayList;
import java.util.List;

import frameworks.IStackable;
import generalGraphics.ButtonLookType;

//Popup element that has a message and buttons prompting a response from the player
public class ScreenPopUp extends ScreenDataBox{
	private ScreenSoloImage eventImage;

	//constructor
	public ScreenPopUp(int x, int y) {
		super(x, y);
		// TODO Auto-generated constructor stub
	}
	
	//bootup method
	public void bootUp() {
		super.bootUp();
		calculateSize();
	}
	
	/*
	 * Getter for a button that closes this popup
	 * Inputs:
	 * 	lookType: for the generated button
	 * 	buttonText: text displayed on the generated button
	 */
	public ScreenButton getNewCloseButton(ButtonLookType lookType, String buttonText) {
		ScreenButton closeButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, lookType, buttonText);
		closeButton.addButtonEventListener(this.mainFactory.getElemFactory().getCloseIStackableElementListener(this));
		this.addButton(closeButton);
		return closeButton;
	}
	
	//method for calculating the size of the pop up screen
	private void calculateSize() {
		this.setSize(this.calculateLength(), this.calculateFullHeight());
	}
	
	/*
	 * Updates the size of popup
	 */
	public void updateSize() {
		calculateSize();
	}
	
	//calculates the lentgh of the pop up screen
	private int calculateLength() {
		int length = this.xDataStart + this.lenAss.getOneChoiceButtonLength() + this.xDataStart;
		return length;
	}
	
	//Calculates full height of the popup
	protected int calculateFullHeight() {
		int height = super.calculateFullHeight();
		height = height + getImgAreaHeight() + getExtraRoom();
		return height;
		
	}
	
	//Calculates the area taken up by popups image
	private int getImgAreaHeight() {
		if(hasEventImage()) {
			return this.eventImage.getHeight() + getExtraRoom();
		}
		else {
			return 0;
		}
	}
	
	//Fills the popups text area with lines in the input array
	public void fillDataBoxWithTexts(List<String> newLines) {
		super.fillDataBoxWithTexts(newLines);
		if(hasButtons()) {
			this.updateButtonLocations();
		}
		updateTextLocation();
	}
	
	//Fills the popup with only one line of text
	/*public void fillTextOneLine(String oneLine) {
		ArrayList<String> newLines = new ArrayList<String>();
		newLines.add(oneLine);
		this.fillDataBoxWithTexts(newLines);
	}*/
	
	// Updates the location of the text, in cases of new elements added on top
	private void updateTextLocation() {
		if(this.hasEventImage()) {
			this.textFrame.setLocation(0, getTextStartY());
		}
	}
	
	// returns the y coordinate that text start from
	public int getTextStartY() {
		int start = 0;
		if(this.hasEventImage()) {
			start = start + this.getImgAreaHeight() + this.lenAss.getExtraRoomLength();
		}
		return start;
	}
	
	//returns the y coordinate that buttons start from
	public int getButtonStartY() {
		int height = super.getButtonStartY();
		if(this.hasEventImage()) {
			height = height + this.getImgAreaHeight() + this.lenAss.getExtraRoomLength();
		}
		return height;
	}
	
	//Updates all the button location in case something is added on top
	private void updateButtonLocations() {
		this.buttonFrame.setLocation(0, getButtonStartY());
		this.calculateSize();
	}
	
	//NB THIS MUST BE MADE MORE EFFICENT
	public void deleteContent() {
		this.textFrame = null;
		this.buttonFrame = null;
		this.calculateSize();
	}
	
	/*
	 * Sets an image for the event
	 * This image will be displayed with the popup, before the text
	 */
	public void setEventImage(String imgName) {
		this.eventImage = this.mainFactory.getElemFactory().getSoloImage(this.lenAss.getOneChoiceButtonLength(), this.lenAss.getOneChoiceButtonLength(), imgName);
		this.eventImage.setCoordinates(0, 0);
		this.eventImage.setWrapper(this);
	}
	
	/*
	 * Returns the extra room size value
	 */
	public int getExtraRoom() {
		return this.mainFactory.getLengthAssigner().getExtraRoomLength();
	}
	
	/*
	 * Checks if the popup has an event image set
	 */
	public boolean hasEventImage() {
		return this.eventImage != null;
	}
	
	/*
	 * returns the event image
	 */
	public ScreenSoloImage getEventImg() {
		return this.eventImage;
	}
	
	@Override
	public void fillTopElementList(List<IStackable> list) {
		super.fillTopElementList(list);
		if(this.hasEventImage()) {
			list.add(eventImage);
		}
	}



}

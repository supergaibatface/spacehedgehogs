package inventoryFilters;

import java.util.List;

import itemBase.IGameItem;
import itemBase.ItemTagName;

//Filters through only items that possess a certain tag
public class TagFilter extends BaseInventoryFilter {
	private ItemTagName filterTag;
	
	//constructor, input is the tag that is filtered for
	public TagFilter(ItemTagName filterTag) {
		this.filterTag = filterTag;
	}

	@Override
	public List<IGameItem> getFilteredItems() {
		return this.mainFactory.getInventory().filterForTag(filterTag);
	}

	@Override
	protected boolean itemFilterScript(IGameItem checkItem) {
		return checkItem.hasTag(filterTag);
	}

}

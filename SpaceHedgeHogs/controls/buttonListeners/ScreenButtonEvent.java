package buttonListeners;

import java.util.EventObject;

import screenElements.ScreenButton;

//Event that happens when an on screen button is pressed
public class ScreenButtonEvent extends EventObject {
	private static final long serialVersionUID = 1L;
	private ScreenButton button;

	//constructor
	public ScreenButtonEvent(Object source) {
		super(source);
		this.button = (ScreenButton) source;
	}
	
	//getter for the button that fired the event
	public ScreenButton getButton() {
		return this.button;
	}

}

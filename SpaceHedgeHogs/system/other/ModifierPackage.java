package other;

import java.util.ArrayList;

//Combines multiple modifierMatch objects into a package
public class ModifierPackage {
	ArrayList<ModifierMatch> modifiers;
	
	//constructor
	public ModifierPackage() {
		modifiers = new ArrayList<ModifierMatch>();
	}
	
	//Makes full list of modifierMatches based on an array of those objects
	public void bootUpTo(ArrayList<Object> firstSetOfModifiers) {
		this.modifiers.clear();
		for(Object oneObject : firstSetOfModifiers) {
			modifiers.add(new ModifierMatch(oneObject));
		}
	}
	
	/*
	 * Checks the saved array of matches with current values of those modifiers
	 * Output:
	 * 	returns true if all modifiers are still the same value
	 * 	returns false if any one modifier has changed since last saved
	 */
	public boolean modifierSetEquals(ArrayList<Object> checkModifiers) {
		if(this.modifiers.size() != checkModifiers.size()) {
			return false;
		}
		for(int i = 0; i < this.modifiers.size(); i++) {
			if(!this.modifiers.get(i).equals(checkModifiers.get(i))) {
				return false;
			}
		}
		return true;
		
	}
	
	/*
	 * Saves new current values to the modifiers
	 */
	public void writeNewValues(ArrayList<Object> checkModifiers) {
		for(int i = 0; i < this.modifiers.size(); i++) {
			this.modifiers.get(i).saveNewModifier(checkModifiers.get(i));
		}
	}

}

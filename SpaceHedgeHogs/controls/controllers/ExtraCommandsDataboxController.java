package controllers;

import java.util.ArrayList;
import java.util.List;

import buttonListeners.CloseIStackableElementListener;
import buttonListeners.ISButtonListener;
import factories.IAutoMake;
import factories.MainFactory;
import generalGraphics.ButtonLookType;
import screenElements.ScreenButton;
import screenElements.ScreenDataBox;
import screenElements.ScreenElement;

public class ExtraCommandsDataboxController implements IAutoMake {
	private MainFactory mainFactory;
	private List<ScreenDataBox> databoxes;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.databoxes = new ArrayList<ScreenDataBox>();
	}
	
	public void openBoxThroughButton(ScreenButton buttonThatOpens, ScreenElement extraInfoSourceElement, String text) {
		updateDataBoxList();
		ScreenDataBox extraCommandsBox = this.fullMakeExtraCommandsDataBoxForScreenElement(extraInfoSourceElement);
		openDataBoxOnScreen(extraCommandsBox, buttonThatOpens.getOffWrapperX()+40, buttonThatOpens.getOffWrapperY()+40);
	}
	
	private ScreenDataBox fullMakeExtraCommandsDataBoxForScreenElement(ScreenElement extraCommandsSource) {
		ScreenDataBox newExtraCommandsBox = this.getNewDataBox();
		newExtraCommandsBox.fillTextOneLine("Test box");
		addScreenElementExtraCommandsToDataBox(newExtraCommandsBox, extraCommandsSource);
		newExtraCommandsBox.addButton(this.makeCloseDataBoxButton(newExtraCommandsBox));
		newExtraCommandsBox.autoSize();
		return newExtraCommandsBox;
	}
	
	private ScreenDataBox getNewDataBox() {
		return this.mainFactory.getElemFactory().getScreenDataBox(0, 0, "Orange");
	}
	
	private void addScreenElementExtraCommandsToDataBox(ScreenDataBox targetDataBox, ScreenElement extraCommandSource) {
		for(ScreenButton oneButton : extraCommandSource.getExtraCommands()) {
			targetDataBox.addButton(this.makeOwnButtonForDataBox(oneButton, targetDataBox));
		}
	}
	
	protected ScreenButton makeOwnButtonForDataBox(ScreenButton originalButton, ScreenDataBox targetDataBox) {
		ScreenButton newButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, getUsableButtonLookType(), originalButton.getLabel().getText());
		for(ISButtonListener oneListener : originalButton.getListeners()) {
			newButton.addButtonEventListener(oneListener);
		}
		newButton.addButtonEventListener(this.getCloseDataBoxListener(targetDataBox));
		return newButton;
	}
	
	
	private void openDataBoxOnScreen(ScreenDataBox dataBox, int x, int y) {
		dataBox.setCoordinates(x, y);
		this.databoxes.add(dataBox);
		this.mainFactory.getGElementManager().openElementOnTopLayer(dataBox);
	}
	
	private ScreenButton makeCloseDataBoxButton(ScreenDataBox targetBox) {
		ScreenButton closeButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, getUsableButtonLookType(), "Close selection");
		closeButton.addButtonEventListener(getCloseDataBoxListener(targetBox));
		return closeButton;
	}
	
	private CloseIStackableElementListener getCloseDataBoxListener(ScreenDataBox boxToClose) {
		return this.mainFactory.getElemFactory().getCloseIStackableElementListener(boxToClose);
	}
	
	private void closeDataBox(ScreenDataBox oldBox) {
		if(this.databoxes.contains(oldBox)) {
			this.mainFactory.getGElementManager().removeElementFromScreen(oldBox);
			this.databoxes.remove(oldBox);
		}
	}
	
	private void updateDataBoxList() {
		for(ScreenDataBox oneBox : new ArrayList<ScreenDataBox>(this.databoxes)) {
			if(!this.mainFactory.getGElementManager().hasIndepenendentElementOnScreen(oneBox)) {
				this.databoxes.remove(oneBox);
			}
		}
	}
	
	private ButtonLookType getUsableButtonLookType() {
		return ButtonLookType.POPUPSELECT;
	}
	

}

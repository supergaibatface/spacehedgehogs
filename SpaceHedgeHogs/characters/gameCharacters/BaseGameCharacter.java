package gameCharacters;

import java.util.ArrayList;
import java.util.List;

import dialogueCharacters.IDialogueCharacter;
import factories.IAutoMake;
import factories.MainFactory;
import propertyBars.PropertyBar;
import propertyBars.PropertyBarName;
import screenInfoPage.ScreenCharInfoPage;
import skills.Skill;
import skills.SkillName;
import sreenElementsSockets.ScreenSimpleChar;

public abstract class BaseGameCharacter implements IAutoMake, IGameCharacter {
	protected String name;
	protected String imgName;
	protected MainFactory mainFactory;
	protected List<Skill> skills;
	protected List<PropertyBar> propertyBars;
	protected ScreenSimpleChar socketAvatar;
	protected ScreenCharInfoPage infoPage;
	protected IDialogueCharacter dialogueElement;
	
	/*
	 * constructor, 
	 * inputs: 
	 * 	name: String version of the characters name
	 * 	ingName: String version of the pictures name (without file extension)
	 */
	public BaseGameCharacter(String name, String imgName) {
		this.name = name;
		this.imgName = imgName;
	}
	
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.makeSkills();
		this.makePropertyBars();
		this.dialogueElement = bootDialogueElement();
		this.makeScreenCharInfoPage();
	}
	
	//makes a info page for this character
	protected void makeScreenCharInfoPage() {
		this.infoPage = this.mainFactory.getElemFactory().getScreenCharInfoPage(this, "Orange");
	}
	
	protected void makeSkills() {
		this.skills = new ArrayList<Skill>();
		this.makeStandardSkills();
	}
	
	protected void makeStandardSkills() {
		this.skills.add(this.makeNewSkill(SkillName.PhysicalForm));
		this.skills.add(this.makeNewSkill(SkillName.MeleeCombat));
		this.skills.add(this.makeNewSkill(SkillName.RangedCombat));
		this.skills.add(this.makeNewSkill(SkillName.Scouting));
		this.skills.add(this.makeNewSkill(SkillName.Crafting));
		this.skills.add(this.makeNewSkill(SkillName.BaggageManagement));
	}
	
	//Method for making a new skill template for this skill manager
	protected Skill makeNewSkill(SkillName name) {
		Skill newSkill = this.mainFactory.getElemFactory().getSkill(name);
		return newSkill;
	}
	
	protected void makePropertyBars() {
		this.propertyBars = new ArrayList<PropertyBar>();
		makeStandardPropertyBars();
	}
	
	protected void makeStandardPropertyBars() {
		this.propertyBars.add(this.makePropertyBar(PropertyBarName.Health, 100, 0));
		this.propertyBars.add(this.makePropertyBar(PropertyBarName.Stamina, 100, 0));
		this.propertyBars.add(this.makePropertyBar(PropertyBarName.Movement, 2, 0));
		this.propertyBars.add(this.makePropertyBar(PropertyBarName.Attacks, 2, 0));
	}
	
	protected PropertyBar makePropertyBar(PropertyBarName barName, int maxValue, int minValue) {
		PropertyBar newBar = this.mainFactory.getElemFactory().getPropertyBar(barName);
		newBar.setMaxValue(maxValue);
		newBar.setMinValue(minValue);
		newBar.fillUp();
		return newBar;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String getImgName() {
		return this.imgName;
	}

	@Override
	public ScreenCharInfoPage getInfoPage() {
		return this.infoPage;
	}

	@Override
	public void addSkill(Skill newSkill) {
		this.skills.add(newSkill);
		
	}

	@Override
	public void addPropertyBar(PropertyBar newBar) {
		this.propertyBars.add(newBar);
	}

	@Override
	public ArrayList<Skill> getAllSkill() {
		return new ArrayList<Skill>(this.skills);
	}

	@Override
	public ArrayList<PropertyBar> getAllPropertyBars() {
		return new ArrayList<PropertyBar>(this.propertyBars);
	}

	@Override
	public PropertyBar getPropertyBar(PropertyBarName name) {
		for(PropertyBar oneBar : this.propertyBars) {
			if(oneBar.equals(name)) {
				return oneBar;
			}
		}
		return null;
	}

	@Override
	public void improveSkill(SkillName skillname, int experience) {
		Skill rightSkill = this.findSkill(skillname);
		rightSkill.addExp(experience);
	}

	@Override
	public Skill findSkill(SkillName name) {
		for(Skill oneSkill : this.skills) {
			if(oneSkill.equals(name)) {
				return oneSkill;
			}
		}
		return null;
	}

	@Override
	public boolean hasSocketAvatar() {
		return this.socketAvatar != null;
	}

	@Override
	public ScreenSimpleChar getSocketAvatar() {
		return this.socketAvatar;
	}

	@Override
	public void setSocketAvatar(ScreenSimpleChar avatar) {
		this.socketAvatar = avatar;
		
	}

	@Override
	public void safeDelete() {
		this.socketAvatar.removeThisElementFromCurrentLocation();
		this.socketAvatar = null;
	}

	@Override
	public boolean hasDialogueElement() {
		return this.dialogueElement != null;
	}

	@Override
	public void addDialogueElement(IDialogueCharacter dialogueElement) {
		this.dialogueElement = dialogueElement;
	}
	
	protected abstract IDialogueCharacter bootDialogueElement();
	
	public IDialogueCharacter getDialogueCharacter() {
		return this.dialogueElement;
	}

}

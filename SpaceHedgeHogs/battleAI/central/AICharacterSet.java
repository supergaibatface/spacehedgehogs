package central;

import gameCharacters.GameCharacter;

//Wrapper object for AI characters that combine character details with AI behaviour pattern
public class AICharacterSet {
	private GameCharacter characterInfo;
	private AICharPatterns commandDetails;
	
	/*
	 * constructor:
	 * Inputs:
	 * 	characterInfo: game character of this set
	 * 	commandDetails: ai behaviour pattern intended for this character
	 */
	public AICharacterSet(GameCharacter characterInfo, AICharPatterns commandDetails) {
		this.characterInfo = characterInfo;
		this.commandDetails = commandDetails;
	}

	//getter for the gameCharacter
	public GameCharacter getCharacterInfo() {
		return characterInfo;
	}

	//setter for the gameCharacter
	public void setCharacterInfo(GameCharacter characterInfo) {
		this.characterInfo = characterInfo;
	}

	//getter for the AI behaviour pattern
	public AICharPatterns getCommandDetails() {
		return commandDetails;
	}

	//setter for the AI behaviour patter
	public void setCommandDetails(AICharPatterns commandDetails) {
		this.commandDetails = commandDetails;
	}
	
	

}

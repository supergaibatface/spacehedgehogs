package jobChoiceController;

import jobs.JobTopic;

//Interface for job topic controllers
public interface IJobTopicController {
	
	//assigns a topic to a job topic controller
	public void assignTopic(JobTopic newTopic);
	
	//triggers an update check for visible sockets
	public void updateSocketValues();

}

package propertyBarListeners;

import screenInfoPage.ScreenCharInfoPage;

//Listener that updates info pages stamina value
public class UpdateInfoPageStamina extends BaseBarChangeListener {
	private ScreenCharInfoPage infoPage;
	
	//constructor, input is the info page that will be updated
	public UpdateInfoPageStamina(ScreenCharInfoPage infoPage) {
		this.infoPage = infoPage;
	}

	@Override
	public void barChangeHappened(int newValue) {
		this.infoPage.callUpdateForStamina();
		
	}

}

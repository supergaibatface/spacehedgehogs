package targetChoosing;

import java.util.ArrayList;

import combatFieldElements.ControlFaction;
import combatFieldElements.FieldCharacter;
import factories.IAutoMake;
import factories.MainFactory;
import other.StaticFunctions;

//AI for choosing targets for ai chars
public class TargetChoosingAI implements IAutoMake {
	private MainFactory mainFactory;
	private boolean debug = true;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	//Method that finds closest enemy char to a specific AI controlled char
	public FieldCharacter findClosestCharTo(FieldCharacter controlledChar) {
		ArrayList<FieldCharacter> potentialTargets = this.getListOfPlayerChars();
		int closestDistance = 999;
		FieldCharacter closestChar = null;
		for(FieldCharacter oneChar : potentialTargets) {
			if(getDistanceBetweenChars(controlledChar, oneChar) < closestDistance) {
				closestChar = oneChar;
				closestDistance = getDistanceBetweenChars(controlledChar, oneChar);
			}
		}
		if(closestChar == null) {
			StaticFunctions.writeOutDebug(debug, "Couldn't find a target for enemy");
		}
		return closestChar;
	}
	
	//Getter for player characters
	private ArrayList<FieldCharacter> getListOfPlayerChars() {
		return this.mainFactory.getGameStateController().getBattleController().getMovementField().getFieldCharacters(ControlFaction.PLAYER);
	}
	
	//Returns the distance between two characters (in squares)
	private int getDistanceBetweenChars(FieldCharacter oneChar, FieldCharacter secondChar) {
		return oneChar.getLocation().getDistanceFrom(secondChar.getLocation());
	}

}

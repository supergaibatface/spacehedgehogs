package skillListeners;

import factories.IAutoMake;
import factories.MainFactory;

//base class for all skill change listeners
public abstract class BaseSkillChangeListener implements IAutoMake, ISkillChangeListener {
	protected MainFactory mainFactory;

	@Override
	public abstract void skillChangeEvent();

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

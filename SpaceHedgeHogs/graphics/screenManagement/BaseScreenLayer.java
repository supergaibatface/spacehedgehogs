package screenManagement;

import java.util.ArrayList;
import java.util.List;

import factories.IAutoMake;
import factories.MainFactory;
import frameworks.IStackable;

public abstract class BaseScreenLayer implements IAutoMake, IScreenLayer {
	protected MainFactory mainFactory;
	protected List<IStackable> elements;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
	}

	@Override
	public void bootUp() {
		this.makeList();
		
	}
	
	private void makeList() {
		this.elements = new ArrayList<IStackable>();
	}
	
	@Override
	public List<IStackable> retrieveElementsIntoInputList(List<IStackable> fillList) {
		fillList.addAll(elements);
		return fillList;
	}
	
	@Override
	public boolean addElementToLayer(IStackable newElement) {
		if(hasElement(newElement)) {
			return false;
		}
		newElement.goingOnScreenAlert();
		this.elements.add(newElement);
		return true;
	}
	
	@Override
	public boolean addElementToTop(IStackable newElement) {
		if(hasElement(newElement)) {
			return false;
		}
		newElement.goingOnScreenAlert();
		this.elements.add(0, newElement);
		return true;
	}
	
	@Override
	public boolean hasElement(IStackable checkedElement) {
		return this.elements.contains(checkedElement);
	}
	
	@Override
	public boolean removeElement(IStackable oldElement) {
		if(!hasElement(oldElement)) {
			return false;
		}
		oldElement.goingOffScreenAlert();
		oldElement.safeDelete();
		this.elements.remove(oldElement);
		return true;
	}
	
	@Override
	public void removeAllElements() {
		for(IStackable oneElement : this.elements) {
			oneElement.goingOffScreenAlert();
			oneElement.safeDelete();
		}
		this.elements.clear();
	}
	
	@Override
	public boolean isEmpty() {
		return this.elements.isEmpty();
	}

}

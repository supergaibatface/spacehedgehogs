package mainPartyCharacters;

import dialogueCharacters.IDialogueCharacter;
import gameCharacters.BaseGameCharacter;

public class CharacterJack extends BaseGameCharacter{

	public CharacterJack() {
		super("Jack", "charspot");
	}

	@Override
	protected IDialogueCharacter bootDialogueElement() {
		return this.mainFactory.getCharacterFactory().getDialogueJack();
	}

}

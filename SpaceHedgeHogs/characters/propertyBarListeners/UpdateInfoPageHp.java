package propertyBarListeners;

import screenInfoPage.ScreenCharInfoPage;

//Listener that updates info pages hp value
public class UpdateInfoPageHp extends BaseBarChangeListener {
	private ScreenCharInfoPage infoPage;
	
	//constructor, input is the info page that will be updated
	public UpdateInfoPageHp(ScreenCharInfoPage infoPage) {
		this.infoPage = infoPage;
	}

	@Override
	public void barChangeHappened(int newValue) {
		this.infoPage.callUpdateForHp();
		
	}

}

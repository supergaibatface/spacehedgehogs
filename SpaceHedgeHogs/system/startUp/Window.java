package startUp;

import javax.swing.JFrame;

import factories.IAutoMake;
import factories.MainFactory;
import gameState.GameStateController;
import screenManagement.Screen;

//Class that represents a window where the programs graphical screen is placed
public class Window implements IAutoMake{
	private MainFactory mainFactory;
	private boolean debug = false;
	private JFrame frame;
	private Screen screen;
	
	//Creates window object
	public Window() {
		if(debug) {
			System.out.println("Window class has been made");
		}
	}
	
	//Boot up method, meant for starting up the object after creation
	public void bootUp() {
		this.makeCompleteFrame();
		this.showFrame();
	}
	
	//makes a complete frame, sets size and location
	public void makeCompleteFrame() {
		makeFrame();
		//setSize(1000,800);
		setLocation(0,0);
		this.makeScreen();
		this.frame.pack();
	}
	
	//turns the frame visible
	public void showFrame() {
		this.frame.setVisible(true);
	}
	
	//setter for main factory object
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
	}
	
	//make and add a canvas to the frame
	private void makeScreen() {
		this.screen = this.mainFactory.getScreen();
		//screen.setSize(1200,800);
		this.frame.add(screen);
	}
	
	//Creates a jframe object and assigns close operation
	//NB! Might have to bring creating a frame to factory
	private void makeFrame() {
		this.frame = new JFrame("Fck yea, headgehogs!");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//frame.setSize(mainFactory.getLengthAssigner().getScreenLength(), mainFactory.getLengthAssigner().getScreenHeight());
	}
	
	//gives the frame its size
	private void setSize(int length, int height) {
		this.frame.setSize(length, height);
	}
	
	
	//gives the frame its location
	private void setLocation(int xCoord, int yCoord) {
		this.frame.setLocation(xCoord, yCoord);
	}

}

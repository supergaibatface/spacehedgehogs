package playerActionListener;

//Close extra commands box that is opened with right click
public class RightMouseInfoCloser extends BasePlayerActionListner {

	@Override
	public boolean playerMouseActionHappened(int mouseClick) {
		if(mouseClick != 1) {
			return false;
		}
		if(this.mainFactory.getRightMouseInfoController().isVisible()) {
			this.mainFactory.getRightMouseInfoController().hideBox();
			return true;
		}
		return false;
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

package foodItems;

import foodBase.FoodItemBase;
import itemBase.TradeItemName;

public class RootsFoodItem extends FoodItemBase {

	public RootsFoodItem() {
		super(TradeItemName.Roots);
	}

	@Override
	protected int bootGiveBaseExpiration() {
		return 30;
	}

	@Override
	protected FoodItemBase giveBaseClass() {
		return this.mainFactory.getItemFactory().getRootsItem();
	}
	
	@Override
	protected String bootGiveItemImageName() {
		return this.bootGivePlaceholderImage();
	}

	@Override
	protected int bootGiveOneUnitWeight() {
		return 3;
	}

}

package frameworks;

import factories.IAutoMake;
import factories.MainFactory;

/*
 * class that represents a line of string that is made up of a field name and a value for that field name
 */
public class FieldAndValue implements IAutoMake {
	private MainFactory mainFactory;
	private String fieldName;
	private String fieldValue;
	
	//constructor
	public FieldAndValue() {
		
	}
	
	//constructor that also adds the fields name
	public FieldAndValue(String fieldName) {
		this.setFieldName(fieldName);
	}

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}
	
	//Setter for field name
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	
	//setter for field value with a string value
	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}
	
	//setter for a field value with a integer value
	public void setFieldValue(int fieldValue) {
		this.fieldValue = Integer.toString(fieldValue);
	}

	@Override
	public void bootUp() {
		if(fieldName == null) {
			this.fieldName = "";
		}
		this.fieldValue = "";
		
	}
	
	//getter for field name
	public String getFieldName() {
		return this.fieldName;
	}
	
	//getter for field value
	public String getFieldValue() {
		return this.fieldValue;
	}
	
	//makes a usable string of field name and value that can be written to a databox
	public String getFullLine() {
		StringBuilder newBuilder = new StringBuilder();
		newBuilder.append(fieldName);
		newBuilder.append(": ");
		newBuilder.append(fieldValue);
		return newBuilder.toString();
	}

}

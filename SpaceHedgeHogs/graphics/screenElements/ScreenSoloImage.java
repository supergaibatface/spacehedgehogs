package screenElements;

import java.util.ArrayList;
import java.util.List;

import frameworks.IStackable;

/*
 * Solo image that will not have anything added on top of it
 * Not meant to be interacted with
 */
public class ScreenSoloImage extends ScreenElement{

	/*
	 * Constructor
	 */
	public ScreenSoloImage(int length, int height) {
		super(0, 0);
		this.setSize(length, height);
	}

	@Override
	public void fillTopElementList(List<IStackable> list) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void fillTopTextList(List<ScreenText> list) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOffScreenScript() {
		// TODO Auto-generated method stub
		
	}

}

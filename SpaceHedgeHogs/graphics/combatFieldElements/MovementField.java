package combatFieldElements;

import java.util.ArrayList;
import java.util.List;

import battle.SpawnArea;
import battleManagement.BattleController;
import factories.IAutoMake;
import factories.MainFactory;
import frameworks.IStackable;
import frameworks.IndependentBaseFrame;
import generalGraphics.ClickBox;
import other.StaticFunctions;
import screenElements.ScreenElement;
import screenElements.ScreenText;
import screenManagement.GraphicalLengthAssigner;

//This object represents a field of squares that battle will take place on
public class MovementField extends IndependentBaseFrame {
	private BattleController battleController;
	private ArrayList<MovementLine> lines;
	private String activeColor;
	private String inactiveColor;
	private int squareSize;
	private int betweenSize;
	private ArrayList<SpawnArea> spawnAreas;
	
	/*
	 * Constructor
	 */
	public MovementField() {

	}

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.lines = new ArrayList<MovementLine>();
		this.lenAss = this.mainFactory.getLengthAssigner();
		this.x= 10;
		this.y= 10;
		this.squareSize = lenAss.getFieldSquareSize();
		this.betweenSize = lenAss.getFieldSquareBetween();
		this.setInactiveColor("100Brown");
		this.spawnAreas = new ArrayList<SpawnArea>();
	}
	
	/*
	 * Makes the field as a rectangle with input amount of squares and length and height
	 * Inputs:
	 * 	fieldLength: X coordinate size in squares
	 * 	fieldHeight: Y Coordinate size in squares
	 */
	public void setUpAsSquare(int fieldLength, int fieldHeight) {
		makeNrOfLines(fieldLength, fieldHeight);
	}
	
	/*
	 * Adds a square to the field with the specific coordinates (in squares)
	 * Inputs:
	 * 	x: X coordinate location
	 * 	y: Y coordinate location
	 */
	public void addSquareWithCoords(int x, int y) {
		MovementLine foundLine = this.findOrMakeLine(y);

		foundLine.makeOneSquare(x, squareSize, inactiveColor, betweenSize);
	}
	
	/*
	 * Adds a horizontal line of squares to the field
	 * Inputs:
	 * 	xStart: start of this new line
	 * 	xEnd: end of this new line
	 * 	y: y coordinat location for this new line
	 */
	public void addHorizontalLineOfSquares(int xStart, int xEnd, int y) {
		MovementLine foundLine = this.findOrMakeLine(y);
		
		foundLine.makeNrOfSquares(xStart, xEnd, squareSize, inactiveColor, betweenSize);
	}
	
	//Setter for the battle controller
	public void setBattleController(BattleController battleController) {
		this.battleController = battleController;
	}
	
	//Getter for the battle controller
	public BattleController getBattleController() {
		return this.battleController;
	}
	
	/*
	 * Adds a spawn area to this map
	 */
	public void addSpawnArea(SpawnArea newArea) {
		this.spawnAreas.add(newArea);
	}
	
	//getter for the spawn areas saved on this map
	public ArrayList<SpawnArea> getSpawnAreas() {
		return this.spawnAreas;
	}
	
	//Setter for the inactive colour image name
	public void setInactiveColor(String imgName) {
		this.inactiveColor = imgName;
	}
	
	//Setter for the active colour image name
	public void setActiveColor(String imgName) {
		this.activeColor = imgName;
	}
	
	/*
	 * Method that makes one line of squares
	 * Inputs:
	 * 	lineNr: coordinate number of the line that counts as y coordinate
	 * 	squareNr: number of squares in the line, counts as x coordinates
	 */
	private MovementLine makeOneLine(int lineNr, int squareNr) {
		MovementLine oneLine = this.mainFactory.getElemFactory().getMovementLine(this, lineNr);
		oneLine.setLocation(0, getLineY(lineNr));
		oneLine.makeNrOfSquares(squareNr, squareSize, inactiveColor, betweenSize);
		this.lines.add(oneLine);
		return oneLine;
	}
	
	/*
	 * Makes an empty line on this field on the input Y location
	 * Inputs:
	 * 	lineNr: y nr for the new line
	 */
	private MovementLine makeEmptyLine(int lineNr) {
		MovementLine oneLine = this.mainFactory.getElemFactory().getMovementLine(this, lineNr);
		oneLine.setLocation(0, getLineY(lineNr));
		this.lines.add(oneLine);
		return oneLine;
	}
	
	/*
	 * Method that calculates the visual y coordinate of a line
	 * Inputs:
	 * 	lineNr: the designated number for line
	 */
	private int getLineY(int lineNr) {
		return (this.squareSize + betweenSize) * lineNr;
	}
	
	/*
	 * Method that will make a designated amount of lines with certain amount of squares
	 * Inputs:
	 * 	squareNr: amount of squares in a line
	 * 	nr: amount of lines that need to be made
	 */
	public void makeNrOfLines(int squareNr, int nr) {
		for(int i = 0; i < nr; i++) {
			makeOneLine(i, squareNr);
		}
	}
	
	/*
	 * Search function for chars of a specific faction
	 * Inputs:
	 * 	faction: faction whos character will be searched out
	 */
	public ArrayList<FieldCharacter> getFieldCharacters(ControlFaction faction) {
		ArrayList<FieldCharacter> characters = new ArrayList<FieldCharacter>();
		for(MovementLine oneLine : this.lines) {
			searchOneLineForChars(faction, oneLine, characters);
		}
		return characters;
	}
	
	/*
	 * Further part of the getFieldCharacters function that will work with a specific line of squares
	 * Inputs:
	 * 	faction: faction whos characters are searched for
	 * 	oneLine: a line of squares that will be searched
	 * 	resultList: array list where the results will be added to
	 */
	private ArrayList<FieldCharacter> searchOneLineForChars(ControlFaction faction, MovementLine oneLine, ArrayList<FieldCharacter> resultList) {
		for(MovementSquare oneSquare : oneLine.getSquares()) {
			if(factionCharCheckOnSquare(faction, oneSquare)) {
				FieldCharacter oneCharacter = (FieldCharacter) oneSquare.getFieldElement();
				resultList.add(oneCharacter);
			}
		}
		return resultList;
	}
	
	/*
	 * dedicated check function for the searchOneLineForChars, returns true when it finds a character of the right faction
	 * Inputs:
	 * 	faction: faction whos characters will be searched for
	 * 	oneSquare: the square that will be checked for a character
	 */
	private boolean factionCharCheckOnSquare(ControlFaction faction, MovementSquare oneSquare) {
		if(!oneSquare.isOccupied()) {
			return false;
		}
		if(!StaticFunctions.isElementOfClass(oneSquare.getFieldElement(), FieldCharacter.class)) {
			return false;
		}
		FieldCharacter oneCharacter = (FieldCharacter) oneSquare.getFieldElement();
		if(!oneCharacter.getFaction().equals(faction)) {
			return false;
		}
		return true;
	}
	
	/*
	 * Method that checks if a point is located on this movement field
	 * Inputs:
	 * 	x: x coordinate of the point
	 * 	y: y coordinate of the point
	 */
	public boolean isThePointOnElement(int x, int y) {
		if(isBetween(x, this.getXDataStart(), this.getRightSideX())) {
			if(isBetween(y, this.getYDataStart(), this.getBottomY())) {
				return true;
			}
		}
		return false;
	}
	
	//Returns the Right side X coordinate of this element
	private int getRightSideX() {
		return this.getXDataStart() + this.getLength();
	}
	
	//Returns the bottom side Y coordinate of this element
	private int getBottomY() {
		return this.getYDataStart() + this.getHeight();
	}
	
	/*
	 * Method that checks if number is between two points
	 */
	private boolean isBetween(int number, int firstPoint, int secondPoint) {
		if(firstPoint < number && number < secondPoint) {
			return true;
		}
		return false;
	}
	
	//getter method for lines on this movementField
	public ArrayList<MovementLine> getLines() {
		return this.lines;
	}
	
	//Checks if movementField has any lines
	public boolean hasLines() {
		return this.lines.size() != 0;
	}
	
	/*
	 * Method for searching a specific line of squares
	 * Inputs:
	 * 	lineNr: nr of the line that is asked for
	 */
	public MovementLine getLineNr(int lineNr) {
		for(MovementLine oneLine : this.lines) {
			if(oneLine.getLineNr() == lineNr) {
				return oneLine;
			}
		}
		return null;
	}
	
	/*
	 * Method that looks for a line, if it cant find it, it will make a new line for that location
	 * Inputs: 
	 * 	y: y location nr for the line
	 */
	private MovementLine findOrMakeLine(int y) {
		MovementLine foundLine = this.getLineNr(y);
		if(foundLine == null) {
			foundLine = this.makeEmptyLine(y);
		}
		return foundLine;
	}
	
	//Getter for a square with specific coordinates
	public MovementSquare getSquareWithCoords(int x, int y) {
		return this.getLineNr(y).getSquareNr(x);
	}
	
	/*
	 * Method that paints square to active colour and also resets any previous painting
	 * Inputs:
	 * 	squares: list of squares that need to be painted to the active colour
	 */
	public void paintSquaresReset(ArrayList<MovementSquare> squares) {
		paintAllSquaresInactive();
		paintSquares(squares);
	}
	
	/*
	 * Method that paints square to active colour without resetting any previous visual ques
	 * Inputs:
	 * 	squares: list of squares that need to be painted to active colour
	 */
	public void paintSquaresNoReset(ArrayList<MovementSquare> squares) {
		paintSquares(squares);
	}
	
	//Method that clears previous visual ques (paints everything to inactive colour)
	public void clearColors() {
		paintAllSquaresInactive();
	}
	
	/*
	 * Method that paints a list of squares to active colour
	 * Inputs:
	 * 	squares: list of squares that will be painted
	 */
	private void paintSquares(ArrayList<MovementSquare> squares) {
		for(MovementSquare oneSquare : squares) {
			oneSquare.setImage(this.activeColor);
		}
	}
	
	//Method that paints all squares with the inactive look
	private void paintAllSquaresInactive() {
		paintAllSquaresTo(this.inactiveColor);
	}
	
	/*
	 * Method for painting all squares to some look
	 * Inputs:
	 * 	picName: string value of the new image look
	 */
	private void paintAllSquaresTo(String picName) {
		for(MovementLine oneLine : this.lines) {
			oneLine.paintAllSquaresTo(picName);
		}
	}
	
	/*
	 * Getter for all the squares of this field
	 * Inputs:
	 * 	fillHere: array where the squares will be added
	 */
	public ArrayList<ScreenElement> getAllSquares(ArrayList<ScreenElement> fillHere) {
		for(MovementLine oneLine: this.getLines()) {
			for(MovementSquare oneSquare : oneLine.getSquares()) {
				fillHere.add(oneSquare);
			}
		}
		return fillHere;
	}

	@Override
	protected int getFullHeight() {
		int lineNr = this.lines.size();
		if(lineNr == 0) {
			return 0;
		}
		else {
			return (lineNr * (this.squareSize + this.betweenSize)) - this.betweenSize;
		}
		
	}

	@Override
	protected int getFullLength() {
		if(this.lines.size() == 0) {
			return 0;
		}
		else {
			return this.getLongestLineLength();
		}
	}
	
	//Method for finding the length of the longest line on this map
	private int getLongestLineLength() {
		int length = 0;
		for(MovementLine oneLine : this.lines) {
			if(oneLine.getFullLength() > length) {
				length = oneLine.getFullLength();
			}
		}
		return length;
	}
	
	@Override
	public void fillTopElementList(List<IStackable> list) {
		list.addAll(this.lines);
	}

	@Override
	public void fillTopTextList(List<ScreenText> list) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOffScreenScript() {
		// TODO Auto-generated method stub
		
	}

}

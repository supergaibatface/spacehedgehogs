package buttonListeners;

import java.util.ArrayList;

import central.AICharacterSet;
import gameCharacters.GameCharacter;
import other.StaticFunctions;

/*
 * Button listener that will add enemies to game states list of enemies that need to be defeated
 */
public class AddEnemiesListener extends BaseButtonListener{
	private ArrayList<AICharacterSet> enemies;
	
	/*
	 * Constructor
	 * Input:
	 * 	enemies: arraylist of enemies who will be added incase the button is clicked
	 */
	public AddEnemiesListener(ArrayList<AICharacterSet> enemies) {
		this.enemies = StaticFunctions.copyArray(enemies);
	}
	
	/*
	 * Constructor
	 * Inputs:
	 * 	enemy: single enemy that will be added when button is pressed
	 */
	public AddEnemiesListener(AICharacterSet enemy) {
		this.enemies = new ArrayList<AICharacterSet>();
		this.enemies.add(enemy);
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.mainFactory.getGameStateController().addEnemysForAFight(enemies);
		
	}

	@Override
	public String toInfo() {
		return "Adds Enemys to fight";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	

}

package combatFieldElements;

import itemBase.IGameItem;

//Represents one inanimate item on a movement field
public class FieldItem extends FieldElement {
	private IGameItem master;
	
	/*
	 * constructor
	 * Inputs:
	 * 	masterItem: item that this object represents
	 */
	public FieldItem(IGameItem masterItem) {
		this.master = masterItem;
	}
	
	public void bootUp() {
		super.bootUp();
		this.setSize(this.lenAss.getFieldSquareSize(), this.lenAss.getFieldSquareSize());
	}
	
	//Getter for master item
	public IGameItem getMasterItem() {
		return this.master;
	}

}

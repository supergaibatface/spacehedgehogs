package commandAction;

import combatFieldElements.MovementField;
import screenElements.ScreenElement;
import sreenElementsSockets.ScreenSimpleItem;

//Command action for selecting an item from the inventory socket
public class CAItemSelecting extends CABaseClass<ScreenSimpleItem> {

	@Override
	public boolean inputIsCorrect() {
		return this.standardClickCheck();
	}

	@Override
	public void actionImplement() {
		ScreenElement selectedElement = conditions.getClickedItem(this.hasItemTypeClicked());
		this.selection.makeActive(selectedElement);
		ScreenSimpleItem selectedItem = (ScreenSimpleItem) selectedElement;
		MovementField field = this.mainFactory.getGameStateController().getBattleController().getMovementField();
		if(field != null) {
			this.mainFactory.getGameStateController().getBattleController().setActiveSquares(selectedItem.getActivationSquares());
			//field.getBattleController().setActiveSquares(selectedItem.getActivationSquares());
		}
	}

	@Override
	public Class<ScreenSimpleItem> setClickTargetClass() {
		return ScreenSimpleItem.class;
	}

	@Override
	protected int giveMouseButtonNr() {
		return this.giveLeftMouseButtonValue();
	}

}

package buttonListeners;

//Will clear all notices from notice controller upon activation
public class ClearAllNoticesListener extends BaseButtonListener {

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.mainFactory.getNoticeController().clearAllNotices();
		
	}

	@Override
	public String toInfo() {
		return "Clears all notices from notice controller";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

package questReward;

import itemBase.IGameItem;
import questRewardBase.BaseQuestReward;

public class GainItemsAsReward extends BaseQuestReward {
	private IGameItem rewardItem;
	
	public GainItemsAsReward(IGameItem rewardItem, int amount) {
		this.rewardItem = rewardItem;
		this.rewardItem.setAmount(amount);
	}

	@Override
	public String getRewardDescription() {
		return "Gains " + rewardItem.getSupply() + " of " + rewardItem.getName();
	}

	@Override
	protected void rewardScript() {
		this.mainFactory.getInventory().addItemToInventory(rewardItem);
	}

}

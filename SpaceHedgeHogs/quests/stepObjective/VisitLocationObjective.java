package stepObjective;

import questStepBase.IQuestStep;
import settlementLocation.ISettlementLocation;
import stepObjectiveBase.BaseStepObjective;

public class VisitLocationObjective extends BaseStepObjective {
	private ISettlementLocation visitLocation;

	public VisitLocationObjective(IQuestStep masterStep, ISettlementLocation targetDestination) {
		super(masterStep);
		this.visitLocation = targetDestination;
	}

	@Override
	protected String returnObjectiveDescription() {
		// TODO Auto-generated method stub
		return "Visit "+visitLocation.getName();
	}

	@Override
	protected void makeListeners() {
		this.visitLocation.addListener(this.mainFactory.getLogicalLocationFactory().getVisitLocationQuestListener(this));
		
	}

}

package other;

//Class for storing checkable values. Class itself is for realising when a modifier has gone through a change
public class ModifierMatch {
	private Object savedModifier;
	
	//empty constructor
	public ModifierMatch() {
		savedModifier = null;
	}
	
	//Constructor with a modifier
	public ModifierMatch(Object newMod) {
		this.saveNewModifier(newMod);
	}
	
	//Saves new vmodifier value
	public void saveNewModifier(Object newMod) {
		this.savedModifier = newMod;
	}
	
	//Method that checks if the modifier is still the same
	public boolean equals(Object newVersion) {
		if(savedModifier == null) {
			if(newVersion == null) {
				return true;
			}
			else {
				return false;
			}
		}
		return this.savedModifier.equals(newVersion);
	}

}

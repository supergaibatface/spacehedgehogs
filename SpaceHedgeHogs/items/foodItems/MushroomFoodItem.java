package foodItems;

import foodBase.FoodItemBase;
import itemBase.TradeItemName;

public class MushroomFoodItem extends FoodItemBase {

	public MushroomFoodItem() {
		super(TradeItemName.Mushroom);
	}

	@Override
	protected int bootGiveBaseExpiration() {
		return 16;
	}

	@Override
	protected FoodItemBase giveBaseClass() {
		return this.mainFactory.getItemFactory().getMushroomItem();
	}
	
	@Override
	protected String bootGiveItemImageName() {
		return this.bootGivePlaceholderImage();
	}

	@Override
	protected int bootGiveOneUnitWeight() {
		return 2;
	}

}

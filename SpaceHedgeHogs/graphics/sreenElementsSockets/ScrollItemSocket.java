package sreenElementsSockets;

import java.util.ArrayList;
import java.util.List;

import frameworks.SimpleItemFrame;
import inventoryFilters.IInventoryFilter;
import itemBase.IGameItem;
import screenElements.ScreenElement;

/*
 * Socket element for ScreenSimpleItems
 */
public class ScrollItemSocket extends BaseScrollSocket<ScreenSimpleItem> {
	private SimpleItemFrame itemFrame;
	protected IInventoryFilter inventoryFilter;

	//constructor
	public ScrollItemSocket(int x, int y, int length, int height) {
		super(x, y, length, height);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void setUpElementFrame() {
		this.itemFrame = this.mainFactory.getElemFactory().getSimpleItemFrame(this);
		this.itemFrame.setLocation(0, 0);
		this.elements.add(this.itemFrame);
	}

	@Override
	protected boolean elementAddingLogic(ScreenElement element) {
		if(element.getClass() == ScreenSimpleItem.class) {
			return pushItemAddThroughFilter((ScreenSimpleItem) element);
		}
		return false;
	}
	
	@Override
	protected boolean elementRemovingLogic(ScreenElement element) {
		if(element.getClass() == ScreenSimpleItem.class) {
			return this.itemFrame.removeElement((ScreenSimpleItem) element);
		}
		return false;
	}

	@Override
	public ArrayList<ScreenSimpleItem> getElements() {
		return this.itemFrame.getAllElements();
	}

	@Override
	public void removeAllElements() {
		this.itemFrame.removeAllElements();
		
	}

	@Override
	public void safeDelete() {
		this.removeAllElements();
		
	}

	@Override
	protected String emptyMessage() {
		return "No items here";
	}
	
	//Method that fills this socket with the item filter assigned here
	public void callFilterUpdate() {
		if(!hasFilter()) {
			return;
		}
		List<IGameItem> newItems = this.inventoryFilter.getNewItems(this);
		for(IGameItem newItem : newItems) {
			this.addNewElement(this.mainFactory.getElemFactory().getScreenSimpleItem(newItem, newItem.getImageName()));
		}
	}
	
	//Checks if this socket has an item filter on it
	public boolean hasFilter() {
		return this.inventoryFilter != null;
	}
	
	//setter for item filter on this element
	public void setFilter(IInventoryFilter filter) {
		this.inventoryFilter = filter;
	}
	
	private boolean pushItemAddThroughFilter(ScreenSimpleItem newItem) {
		if(this.hasFilter()) {
			if(!this.inventoryFilter.itemPassesFilter(newItem.getMaster())) {
				return false;
			}
		}
		return this.itemFrame.addNewElement(newItem);
	}



}

package inventoryChange;

import java.util.EventListener;

//Interface for inventory listeners
public interface IInventoryListener extends EventListener {
	
	//method for new inventory item events
	public void inventoryNewItemEventOccured(InventoryNewItemEvent evt);
}

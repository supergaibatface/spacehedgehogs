package buttonListeners;

//Decreases waterSupply by a turns worth
public class DecreaseWaterSupplyByTurnListener extends BaseButtonListener {

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.mainFactory.getWaterSupplyController().removeATurnsWorthOfWater();
		
	}

	@Override
	public String toInfo() {
		return "Removes one turns worth of water";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

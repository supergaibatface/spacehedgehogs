package situationChange;

import cutsceneInstances.ICutsceneInstance;
import factories.IAutoMake;
import factories.MainFactory;
import view.CutsceneDataCollection;

public abstract class BaseSituationChange implements IAutoMake, ISituationChange {
	protected MainFactory mainFactory;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void goThroughSituationChange() {
		this.inBetweenLogic();
		sendIntoNextViewLogic();
	}
	
	protected abstract void inBetweenLogic();
	
	protected abstract void sendIntoNextViewLogic();
	
	protected void startCutscene(CutsceneDataCollection cutsceneData) {
		this.mainFactory.getCutsceneBuilder().startACutscene(cutsceneData);
	}
	
	protected void startCutscene(ICutsceneInstance cutsceneInstance) {
		cutsceneInstance.startSavedCutscene();
	}

}

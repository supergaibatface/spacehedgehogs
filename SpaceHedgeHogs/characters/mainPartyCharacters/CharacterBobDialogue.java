package mainPartyCharacters;

import dialogueCharacters.BaseDialogueCharacter;

public class CharacterBobDialogue extends BaseDialogueCharacter {

	@Override
	protected void getCurrentDialogueInstance() {
		System.out.println("Tried to get Bobs dialogue instance");
		
	}

	@Override
	protected boolean hasAllowedDialogue() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected String giveNameToChar() {
		// TODO Auto-generated method stub
		return "Bob";
	}

}

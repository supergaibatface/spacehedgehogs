package buttonListeners;

import screenElements.ScreenButton;

/*
 * Button listener that will advance the cutscenen by one frame
 */
public class AdvanceCutsceneListener extends BaseButtonListener {

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.mainFactory.getCutsceneBuilder().advanceScene((ScreenButton) evt.getSource());
		
	}

	@Override
	public String toInfo() {
		return "Goes to next frame";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

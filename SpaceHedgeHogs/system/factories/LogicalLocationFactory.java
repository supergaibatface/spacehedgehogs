package factories;

import fortLoupe.Loupe;
import fortLoupe.LoupeCamp;
import fortLoupe.LoupeDungeon;
import fortLoupe.LoupeGatehouse;
import fortLoupe.LoupeKeep;
import settlementLocationListeners.VisitLocationQuestListener;
import stepObjective.VisitLocationObjective;
import viewClasses.LoupeFortView;

public class LogicalLocationFactory extends BaseMinorFactory {
	//private MainFactory mainFactory;
	private Loupe fortLoupe;
	private LoupeGatehouse fortLoupeGatehouse;
	private LoupeKeep fortLoupeKeep;
	private LoupeCamp fortLoupeCamp;
	private LoupeDungeon fortLoupeDungeon;

	/*@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}*/

	/*@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}*/
	
	//private method for making a object that implements IAutoMake interface
	/*private <T extends IAutoMake> T setUpNewElement(T obj) {
		obj.setMainFactory(this.mainFactory);
		obj.bootUp();
		return obj;
	}*/
	
	private void makeLoupe() {
		this.fortLoupe = setUpNewElement(new Loupe());
	}
	
	public Loupe getFortLoupe() {
		if(this.fortLoupe == null) {
			this.makeLoupe();
		}
		return fortLoupe;
	}
	
	private void makeLoupeGatehouse() {
		this.fortLoupeGatehouse = setUpNewElement(new LoupeGatehouse());
	}
	
	public LoupeGatehouse getLoupeGatehouse() {
		if(this.fortLoupeGatehouse == null) {
			this.makeLoupeGatehouse();
		}
		return this.fortLoupeGatehouse;
	}
	
	private void makeLoupeKeep() {
		this.fortLoupeKeep = setUpNewElement(new LoupeKeep());
	}
	
	public LoupeKeep getLoupeKeep() {
		if(this.fortLoupeKeep == null) {
			this.makeLoupeKeep();
		}
		return this.fortLoupeKeep;
	}
	
	private void makeLoupeCamp() {
		this.fortLoupeCamp = setUpNewElement(new LoupeCamp());
	}
	
	public LoupeCamp getLoupeCamp() {
		if(this.fortLoupeCamp == null) {
			this.makeLoupeCamp();
		}
		return this.fortLoupeCamp;
	}
	
	public VisitLocationQuestListener getVisitLocationQuestListener(VisitLocationObjective masterObjective) {
		return setUpNewElement(new VisitLocationQuestListener(masterObjective));
	}
	
	private void makeLoupeDungeon() {
		this.fortLoupeDungeon = setUpNewElement(new LoupeDungeon());
	}
	
	public LoupeDungeon getLoupeDungeon() {
		if(this.fortLoupeDungeon == null) {
			this.makeLoupeDungeon();
		}
		return this.fortLoupeDungeon;
	}
	

}

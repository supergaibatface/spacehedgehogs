package dataValues;

import java.util.ArrayList;

import itemBase.ItemTagName;
import itemBase.TradeItemName;
import other.StaticFunctions;

//Minimalist class for saving values for individual item stacks
public class ItemValue {
	protected TradeItemName name;
	protected int amount;
	
	//constructor
	public ItemValue(TradeItemName name, int amount) {
		this.name = name;
		this.amount = amount;
	}
	
	//getter for the item name
	public TradeItemName getName() {
		return this.name;
	}
	
	//getter for the amount of items in this stack
	public int getAmount() {
		return this.amount;
	}
	
	

}

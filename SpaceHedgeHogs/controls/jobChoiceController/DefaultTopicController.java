package jobChoiceController;

import java.util.ArrayList;

import jobs.JobTopic;

/*
 * Default topic controller
 */
public class DefaultTopicController extends BaseJobTopicController {

	//constructor
	public DefaultTopicController(JobTopic newTopic) {
		super(newTopic);
	}

	@Override
	protected ArrayList<Object> makeModifierValuePackage() {
		ArrayList<Object> modifierPackage = new ArrayList<Object>();
		
		return modifierPackage;
	}

	@Override
	protected void addSockets() {
		
	}

}

package inventoryFilters;

import java.util.List;

import itemBase.IGameItem;
import sreenElementsSockets.ScrollItemSocket;

//interface for all inventory filters
public interface IInventoryFilter {
	
	//Returns a list of all items that pass through this filter from the inventory
	public List<IGameItem> getFilteredItems();
	
	//Returns a list of new items in the inventory that are not yet in input socket
	public List<IGameItem> getNewItems(ScrollItemSocket socket);
	
	public boolean itemPassesFilter(IGameItem checkItem);

}

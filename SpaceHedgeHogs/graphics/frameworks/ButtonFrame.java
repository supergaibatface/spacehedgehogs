package frameworks;

import java.util.ArrayList;
import java.util.List;

import generalControls.RadioButtonFunction;
import screenElements.ScreenButton;
import screenElements.ScreenElement;
import screenElements.ScreenPopUp;
import screenElements.ScreenText;

/*
 * Frame class for holding buttons
 */
public class ButtonFrame extends BaseFrame {
	private ArrayList<ScreenButton> buttons;
	private RadioButtonFunction radioFunction;
	private boolean vertical;

	/*
	 * Constructor
	 * Input:
	 * 	wrapper: IStackable element that this frame is placed on
	 */
	public ButtonFrame(IStackable wrapper) {
		super(wrapper);
	}
	
	//Bootup method
	public void bootUp() {
		super.bootUp();
		this.buttons = new ArrayList<ScreenButton>();
		this.vertical = true;
	}
	
	//If this value is set true, the buttons will be lister vertically instead of horizontal
	public void setVertical(boolean newValue) {
		this.vertical = newValue;
	}
	
	/*
	 * Getter for the frames full height
	 */
	protected int getFullHeight() {
		if(this.buttons.isEmpty()) {
			return 0;
		}
		else if(!this.vertical) {
			return horizontalHeightCalc();
				
		}
		else {
			return verticalHeightCalc();
		}
		
	}
	
	/*
	 * Calculates height when placement is vertical
	 */
	protected int verticalHeightCalc() {
		int height = getNewButtonY(this.buttons.get(0));
		if(height > 0) {
			height = height - lenAss.getExtraRoomLength();
		}
		return height;
	}
	
	/*
	 * Calculates height when placement is horizontal
	 */
	protected int horizontalHeightCalc() {
		return this.buttons.get(0).getHeight();
	}
	
	/*
	 * Method for getting the full length of the framework
	 */
	protected int getFullLength() {
		if(this.buttons.isEmpty()) {
			return 0;
		}
		else if(this.vertical) {
			return verticalLengthCalc();
				
		}
		else {
			return horizontalLengthCalc();
		}
	}
	
	/*
	 * Calculates length when placement is vertical
	 */
	protected int verticalLengthCalc() {
		return this.buttons.get(0).getLength();
	}
	
	/*
	 * Calculates length when placement is horizontal
	 */
	protected int horizontalLengthCalc() {
		int length = getNewButtonX(this.buttons.get(0));
		if(length > 0) {
			length = length - lenAss.getExtraRoomLength();
		}
		return length;
	}
	
	/*
	 * Method for adding a new button to the frame
	 */
	public void addElement(ScreenButton newButton) {
		newButton.setCoordinates(getNewButtonX(newButton), getNewButtonY(newButton));
		newButton.setWrapper(this);
		this.buttons.add(newButton);
		radioButtonSetup();
		if(hasRadioButtonFunction()) {
			this.radioFunction.newButtonAdded(newButton);
		}
	}
	
	/*
	 * Recalculates the coordinates for remaining buttons
	 */
	private void recalculateButtons() {
		ScreenButton oneButton;
		for(int i = 0; i < this.buttons.size(); i++) {
			oneButton = buttons.get(i);
			buttons.get(i).setCoordinates(getButtonX(i, oneButton.getLength()), getButtonY(i, oneButton.getHeight()));
		}
	}
	
	/*
	 * Removes a button from the button frame
	 * Inputs:
	 * 	oldButton: the button that will be removed
	 */
	public void RemoveButton(ScreenButton oldButton) {
		if (this.buttons.contains(oldButton)) {
			this.buttons.remove(oldButton);
			recalculateButtons();
		}
	}
	
	//removes all buttons from the button frame
	public void removeAllButtons() {
		this.buttons.clear();
	}
	
	/*
	 * Method for getting a Y coordinate on the frame for a new button
	 */
	private int getNewButtonY(ScreenButton templateButton) {
		return this.getButtonY(this.buttons.size(), templateButton.getHeight());
	}
	
	/*
	 * for calculating a buttons Y coordinate depending on what is its location in the button array
	 * Inputs:
	 * 	nr: the buttons location in array
	 * 	buttonHeight: height of one button
	 */
	private int getButtonY(int nr, int buttonHeight) {
		if(this.vertical) {
			int yCoord = (buttonHeight + lenAss.getExtraRoomLength()) * nr;
			return yCoord;
		}
		else {
			return 0;
		}

	}
	
	/*
	 * Method for getting a X coordinate on the frame for a new button
	 */
	private int getNewButtonX(ScreenButton templateButton) {
		return this.getButtonX(this.buttons.size(), templateButton.getLength());
	}
	
	/*
	 * method for getting a x coordinate for a button with specific location nr
	 * Inputs:
	 * 	nr: locations nr of a button
	 * 	buttonLength: length of one button
	 */
	private int getButtonX(int nr, int buttonLength) {
		if(this.vertical) {
			return 0;
		}
		else {
			int xCoord = (buttonLength + lenAss.getExtraRoomLength()) * nr;
			return xCoord;
		}
	}
	
	/*
	 * Method for getting all the buttons
	 */
	public ArrayList<ScreenButton> getButtons() {
		return this.buttons;
	}
	
	//Checks if this frame has radio button functionality
	public boolean hasRadioButtonFunction() {
		return this.radioFunction != null;
	}
	
	/*
	 * Adds radioButtonFunction to this button frame
	 * Inputs:
	 * 	function: radio button function that will be added to this frame
	 */
	public void addRadioButtonFunction(RadioButtonFunction function) {
		this.radioFunction = function;
		radioButtonSetup();
		startUpRadioButton();
	}
	
	/*
	 * Method for setting up radio button functionality once it has been added to
	 * the button frame, changes will be made to the radio button functionality object
	 */
	private void radioButtonSetup() {
		if(hasRadioButtonFunction() && !this.buttons.isEmpty()) {
			if(!this.radioFunction.hasDefaultSizes()) {
				this.radioFunction.getDefaultSizes(this.buttons.get(0));
			}
		}
	}
	
	/*
	 * Radio button setup for the button frame side, meaning
	 * changes to the buttons
	 */
	private void startUpRadioButton() {
		for(ScreenButton oneButton: this.buttons) {
			this.radioFunction.newButtonAdded(oneButton);
		}
	}

	@Override
	public void fillTopElementList(List<IStackable> list) {
		if(!this.buttons.isEmpty()) {
			list.addAll(buttons);
		}
		
	}

	@Override
	public void fillTopTextList(List<ScreenText> list) {
		
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOffScreenScript() {
		// TODO Auto-generated method stub
		
	}


}

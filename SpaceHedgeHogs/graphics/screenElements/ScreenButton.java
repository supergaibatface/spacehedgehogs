package screenElements;

import java.util.ArrayList;
import java.util.List;

import javax.swing.event.EventListenerList;

import buttonLimiters.IButtonLimiter;
import buttonLimiters.ItemLimiter;
import buttonListeners.ISButtonListener;
import buttonListeners.ScreenButtonEvent;
import factories.MainFactory;
import frameworks.IStackable;
import generalGraphics.ButtonLookType;
import other.StaticFunctions;
import screenManagement.GraphicalLengthAssigner;

//Extends screen element, represents a graphical button
public class ScreenButton extends ScreenElement{
	protected EventListenerList listenerList = new EventListenerList();
	private ButtonLookType lookType;
	private String labelText;
	private ScreenText label;
	private boolean HasTarget = false;
	private ArrayList<IButtonLimiter> buttonLimiters;
	private ScreenButtonInfo buttonInfo;
	private boolean previousState = true;

	/*
	 * Constructor for ScreenButton
	 * Inputs:
	 * 	x: x-coordinate for screen elements location (top left corner)
	 * 	y: y-coordinate for screen elements location (top left corner)
	 * 	lookType: buttonLookType enum that will decide the look of the button
	 * 	label: String for the label that will be written onto the button
	 */
	public ScreenButton(int x, int y, ButtonLookType lookType, String label) {
		super(x, y);
		this.lookType = lookType;
		this.labelText = label;
	}
	
	//Boot up method, meant for starting up the object after creation
	public void bootUp() {
		super.bootUp();
		buttonSizeSwitch(lenAss);
		setCoordinates(this.getX(), this.getY());
		this.label = this.mainFactory.getElemFactory().getText(labelText);
		this.label.setCoordinates(0, this.lenAss.getOneTextSize());
		this.label.setWrapper(this);
		this.buttonLimiters = new ArrayList<IButtonLimiter>();
		this.makeGrayOut();
	}
	
	//extended version of setcoordinates that also changes the location of the label
	@Override
	public void setCoordinates(int x, int y) {
		super.setCoordinates(x, y);
		if(this.hasInfo()) {
			this.buttonInfo.updateLocation();
		}
	}
	
	/*
	 * Method for setting an wrapper element that this button will be placed on
	 */
	public void setWrapper(IStackable wrapperElem) {
		super.setWrapper(wrapperElem);
		if(this.hasInfo()) {
			this.buttonInfo.setWrapper(wrapperElem);
		}
	}
	
	//getter for checking if this button has a target
	public boolean hasTarget() {
		return this.HasTarget;
	}
	
	//Checks if the button has an info element
	public boolean hasInfo() {
		return this.buttonInfo != null;
	}
	
	/*
	 * Makes the button info element
	 */
	public void makeInfo() {
		this.buttonInfo = this.mainFactory.getElemFactory().getButtonInfo(this, "50purple");
		this.buttonInfo.feedListeners(getListeners());
	}
	
	//getter for the button info element
	public ScreenButtonInfo getInfo() {
		return this.buttonInfo;
	}
	
	/*
	 * Method for adding buttonlistener to the buttom, must be a listener that implements ISBUttonListener interface
	 * Input: 
	 * 	listener: button listener that uses ISButtonListener interface
	 */
	public void addButtonEventListener(ISButtonListener listener) {
		listenerList.add(ISButtonListener.class, listener);
	}
	
	/*
	 * Method for adding an action limiter to this button
	 */
	public void addActionLimiter(IButtonLimiter limiter) {
		this.buttonLimiters.add(limiter);
	}
	
	/*
	 * Method for removing buttonlistener from the buttom, must be a listener that implements ISBUttonListener interface
	 * Input: 
	 * 	listener: button listener that uses ISButtonListener interface
	 */
	public void removeButtonEventListener(ISButtonListener listener) {
		listenerList.remove(ISButtonListener.class, listener);
	}
	
	//Method that makes a new screenButtonEvent and fires it through the firebuttonevent
	public void activate() {
		if(this.checkAvailability()) {
			fullFillPrice();
			fireButtonEvent(new ScreenButtonEvent(this));
		}
	}
	
	//Method for sending a ScreenButtonEvent to all the listeners, input evt: ScreenButtonEvent style event
	private void fireButtonEvent(ScreenButtonEvent evt) {
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i + 2) {
			if (listeners[i] == ISButtonListener.class) {
				((ISButtonListener) listeners[i + 1]).screenButtonEventOccured(evt);
			}
		}
	}
	
	/*
	 * Builds a list of all the listeners assigned to this buttons
	 */
	public ArrayList<ISButtonListener> getListeners() {
		ArrayList<ISButtonListener> listenersFull = new ArrayList<ISButtonListener>();
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i + 2) {
			if (listeners[i] == ISButtonListener.class) {
				listenersFull.add(((ISButtonListener) listeners[i + 1]));
			}
		}
		return listenersFull;
	}
	
	//Getter for the ScreenText type label element of the button
	public ScreenText getLabel() {
		return this.label;
	}
	
	//Enum switch that gives the button its size according to type that was assigned to the button, input lenAss: GraphicaLengthAssigner object
	private void buttonSizeSwitch(GraphicalLengthAssigner lenAss) {
		this.setSize(this.lookType.lengthMod * lenAss.getButtonModSize(), this.lookType.heightMod * lenAss.getButtonModSize());
			
	}
	
	//Method for checking if the limiter allowes this button to be pressed
	public boolean checkAvailability() {
		for (IButtonLimiter limiter : this.buttonLimiters) {
			if(!limiter.check()) {
				previousStateCheck(false);
				return false;
			}
		}
		previousStateCheck(true);
		return true;
	}
	
	/*
	 * Method for sending a notification about this button being unavailable
	 */
	private void sendNotifications() {
		ArrayList<IButtonLimiter> noticeLimiters = new ArrayList<IButtonLimiter>();
		populateFailureNoticeList(noticeLimiters);
		if(!noticeLimiters.isEmpty()) {
			sendNoticesToController(noticeLimiters);
		}
	}
	
	/*
	 * Makes a list of notices that must be sent to the player
	 */
	private ArrayList<IButtonLimiter> populateFailureNoticeList(ArrayList<IButtonLimiter> inputList) {
		for(IButtonLimiter limiter : this.buttonLimiters) {
			if(limiter.needsNotify()) {
				if(!limiter.check()) {
					inputList.add(limiter);
				}
			}
		}
		return inputList;
	}
	
	//Makes a failure string based of a failed limiter, the limiter is the imput
	private String getStringOffLimiter(IButtonLimiter limiter) {
		return this.id + " is unavailable cause "+limiter.reasonForFail();
	}
	
	/*
	 * Method that sends a list of text notices to notice controller for display
	 */
	private void sendNoticesToController(ArrayList<IButtonLimiter> noticeList) {
		for(IButtonLimiter oneLimiter : noticeList) {
			this.mainFactory.getNoticeController().addNotice(getStringOffLimiter(oneLimiter), oneLimiter.instantPopUp());
		}
	}
	
	/*
	 * Checks buttons current state of availability and sends notifications if needed
	 */
	private void previousStateCheck(boolean newState) {
		if(this.previousState != newState) {
			this.previousState = newState;
			if(newState == false) {
				sendNotifications();
			}
		}
	}
	
	//Method that updates gray elements visual value, based on this buttons requirements being met or not
	public void grayOutCheck() {
		if(this.checkAvailability()) {
			this.grayElement.makeInvisible();
		}
		else {
			this.grayElement.makeVisible();
		}
	}
	
	//Removes the item from inventory that this button is limited by
	public void fullFillPrice() {
		for(IButtonLimiter limiter : this.buttonLimiters) {
			limiter.doCost();
		}
	}

	@Override
	public void fillTopElementList(List<IStackable> list) {
		if(this.hasInfo()) {
			list.add(buttonInfo);
		}
		if(hasGrayElement()) {
			this.grayOutCheck();
			list.add(grayElement);
		}
	}

	@Override
	public void fillTopTextList(List<ScreenText> list) {
		list.add(label);
		
	}
	
	//Getter for elements that can impact this elements autoSize function
	protected ArrayList<IStackable> getSizeElements() {
		ArrayList<IStackable> finalList= new ArrayList<IStackable>();
		return finalList;
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOffScreenScript() {
		// TODO Auto-generated method stub
		
	}

}

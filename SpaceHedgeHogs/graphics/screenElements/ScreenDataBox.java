package screenElements;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import frameworks.ButtonFrame;
import frameworks.IStackable;
import frameworks.TextFrame;
import generalControls.RadioButtonFunction;

/*
 * extends screen element represents a random data box that posses some info
 * needs a specific object that can feed it data for actually displaying something
 */
public class ScreenDataBox extends ScreenElement{
	protected ButtonFrame buttonFrame;
	protected TextFrame textFrame;
	protected RadioButtonFunction radioFunction;
	protected boolean vertical;

	//constructor
	public ScreenDataBox(int x, int y) {
		super(x, y);
	}
	
	
	//NB BOOTUP WILL USE ONLY ONE TYPE OF SIZE ATM, CHANGE LATER
	public void bootUp() {
		super.bootUp();
		this.setSize(this.lenAss.getCharInfoLength(), this.lenAss.getCharInfoHeight());
		this.vertical = true;
	}
	
	/*
	 * Setter for boolean value of data displaying, true is vertical displayment
	 */
	public void setVertical(boolean newValue) {
		this.vertical = newValue;
	}
	
	//getter for all the texts on the textbox
	public ArrayList<ScreenText> getTexts() {
		if(this.hasText()) {
			return this.textFrame.getTexts();
		}
		return null;
	}
	
	/*
	 * Method for setting the color of a specific text line on the databox
	 * Inputs:
	 * 	newColor: color object that will be set as the new color of the line
	 * 	lineNr: int line location nr which color will be changed
	 */
	public boolean setColorForALine(Color newColor, int lineNr) {
		if(this.hasText()) {
			return this.textFrame.setColorForALine(newColor, lineNr);
		}
		return false;
	}
	
	//through this outside objects will fill the databox with texts
	public void fillDataBoxWithTexts(List<String> newLines) {
		if(!this.hasText()) {
			this.textFrame = mainFactory.getElemFactory().getTextFrame(this);
		}
		this.textFrame.fillTexts(newLines);
		this.updateFrameLocation();
	}
	
	//Fills the popup with only one line of text
	public void fillTextOneLine(String oneLine) {
		ArrayList<String> newLines = new ArrayList<String>();
		newLines.add(oneLine);
		this.fillDataBoxWithTexts(newLines);
	}
	
	//Updates the size of the text box to be the first show view size for time databox
	public void updateSizeForTime() {
		int height = this.calculateFullHeight();
		int length = this.lenAss.getSocketLengthForSize(1);
		this.setSize(length, height);
	}
	
	/*
	 * Calculates and returns the full height value of the databox
	 */
	protected int calculateFullHeight() {
		int height = this.lenAss.getExtraRoomLength();
		height = height + this.getTextAreaSize();
		height = height + this.getButtonAreaSize();
		return height;
	}
	
	/*
	 * Calculates the size of the designated text area on databox
	 */
	protected int getTextAreaSize() {
		if(hasText()) {
			return this.textFrame.getHeight() + this.lenAss.getExtraRoomLength();
		}
		else {
			return 0;
		}
	}
	
	/*
	 * Calculates the size of the designated button area on databox
	 */
	protected int getButtonAreaSize() {
		if(hasButtons()) {
			return this.buttonFrame.getHeight() + this.lenAss.getExtraRoomLength();
		}
		else {
			return 0;
		}
	}
	
	/*
	 * Method for getting the start coordinates for the button area
	 */
	public int getButtonStartY() {
		int height = 0;
		if(this.hasText()) {
			height = height + this.textFrame.getFullHeight() + this.lenAss.getExtraRoomLength();
		}
		return height;
	}
	
	//Returns the start location of text area
	public int getTextStartY() {
		return 0;
	}
	
	//Checks if the databox has been assigned buttons
	public boolean hasButtons() {
		if(this.buttonFrame == null) {
			return false;
		}
		if(this.buttonFrame.getButtons().isEmpty()) {
			return false;
		}
		return true;
	}
	
	//Checks if databox has text asigned
	public boolean hasText() {
		return this.textFrame != null;
	}
	
	/*
	 * Method for adding a radio button functionality to databoxes buttons
	 */
	public boolean addRadioFunctionToButtons() {
		if(!hasButtons()) {
			return false;
		}
		else {
			this.radioFunction = this.mainFactory.getElemFactory().getRadioButtonFunction(buttonFrame, "50pink", "50yellow");
			return true;
		}
	}
	
	/*
	 * Method for updating the location of the frame
	 * Uses default location of the button frame to recalculate
	 * its location when new items have been added to other sections
	 */
	public void updateFrameLocation() {
		if(this.hasText()) {
			this.textFrame.setLocation(0, getTextStartY());
		}
		if(this.hasButtons()) {
			this.buttonFrame.setLocation(0, getButtonStartY());
		}
	}
	
	/*
	 * Method for adding a button to the data box
	 */
	public void addButton(ScreenButton button) {
		if(!hasButtons()) {
			this.buttonFrame = this.mainFactory.getElemFactory().getButtonFrame(this);
			this.buttonFrame.setVertical(this.vertical);
		}
		this.buttonFrame.addElement(button);
		updateFrameLocation();
	}
	
	//Removes all buttons from this databox
	public void removeAllButtons() {
		if(hasButtons()) {
			this.buttonFrame.removeAllButtons();
		}
	}
	
	//Removes one specific button from databox
	public void removeButton(ScreenButton oldButton) {
		this.buttonFrame.RemoveButton(oldButton);
	}
	
	/*
	 * getter for buttons
	 */
	public ArrayList<ScreenButton> getButtons() {
		return this.buttonFrame.getButtons();
	}
	
	//Checks if this databox has an assigned radio button function
	public boolean hasRadioButtonFunction() {
		return this.radioFunction != null;
	}
	
	//Getter for radioButton function on this element
	public RadioButtonFunction getRadioButtonFunction() {
		return this.radioFunction;
	}
	
	/*
	 * Getter for the button frame
	 */
	public ButtonFrame getButtonFrame() {
		return this.buttonFrame;
	}


	@Override
	public void fillTopElementList(List<IStackable> list) {
		if(this.hasButtons()) {
			list.add(buttonFrame);
		}
		if(this.hasText() ) {
			list.add(textFrame);
		}
		
	}


	@Override
	public void fillTopTextList(List<ScreenText> list) {
		
	}


	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}


	@Override
	protected void goingOffScreenScript() {
		// TODO Auto-generated method stub
		
	}

}

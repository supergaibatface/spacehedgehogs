package craftingCenter;

import java.util.ArrayList;
import java.util.List;

import craftingBluePrints.CraftingProject;
import craftingBluePrints.IBlueprint;
import craftingManagerListeners.ICraftingManagerListener;
import factories.IAutoMake;
import factories.MainFactory;
import screenInfoPage.BaseInfoPage;

public class CraftingManager implements IAutoMake {
	private MainFactory mainFactory;
	private List<IBlueprint> bluePrints;
	private BaseInfoPage craftingPage;
	private List<CraftingProject> craftingQue;
	private List<ICraftingManagerListener> listenerList;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.listenerList = new ArrayList<ICraftingManagerListener>();
		this.setUpBluePrintList();
		this.makeCraftingQue();
		
		this.makeCraftingPage();
	}
	
	private void setUpBluePrintList() {
		this.bluePrints = new ArrayList<IBlueprint>();
		this.addTestValuesToBluePrintList();
	}
	
	private void addTestValuesToBluePrintList() {
		this.bluePrints.add(this.mainFactory.getCraftingFactory().getSwordBluePrint());
	}
	
	private void makeCraftingPage() {
		this.craftingPage = this.mainFactory.getCraftingFactory().getCraftingPage(this, "Orange");
	}
	
	private void makeCraftingQue() {
		this.craftingQue = new ArrayList<CraftingProject>();
	}
	
	public List<IBlueprint> getAllBluePrints() {
		return new ArrayList<IBlueprint>(this.bluePrints);
	}
	
	public BaseInfoPage getCraftingPage() {
		return this.craftingPage;
	}
	
	public boolean addNewCraftingProject(IBlueprint targetBlueprint) {
		if(this.craftingQue.size() > 10) {
			return false;
		}
		if(!targetBlueprint.canMakeProject()) {
			return false;
		}
		try {
			this.makeAndAddNewProjectForBlueprint(targetBlueprint.makeGetNewCraftingProject());
			return true;
		}
		catch(Exception exception) {
			this.mainFactory.getExceptionManager().dealWithException(exception);
		}
		
		return false;
	}
	
	private void makeAndAddNewProjectForBlueprint(CraftingProject newProject) {
		//CraftingProject newProject = this.getNewCraftingProjectForBlueprint(targetBlueprint);
		this.craftingQue.add(newProject);
		this.announceNewProject(newProject);
		//System.out.println("Added a new crafting project for: "+targetBlueprint.getName()+" crafting list size now: "+this.craftingQue.size());
	}
	
	private CraftingProject getNewCraftingProjectForBlueprint(IBlueprint targetBlueprint) {
		return this.mainFactory.getCraftingFactory().getNewCraftingProject(targetBlueprint);
	}
	
	public List<CraftingProject> getCopyOfProjects() {
		return new ArrayList<CraftingProject>(this.craftingQue);
	}
	
	public boolean addNewListener(ICraftingManagerListener newListener) {
		if(this.listenerList.contains(newListener)) {
			return false;
		}
		this.listenerList.add(newListener);
		return true;
	}
	
	public boolean removeOldListener(ICraftingManagerListener oldListener) {
		if(this.listenerList.contains(oldListener)) {
			this.listenerList.remove(oldListener);
			return true;
		}
		return false;
	}
	
	private void announceNewProject(CraftingProject newProject) {
		for(ICraftingManagerListener oneListener : this.listenerList) {
			oneListener.newCraftingProjectAdded(newProject);
		}
	}
	
	private void announceOldProjectCancelled(CraftingProject oldProject) {
		for(ICraftingManagerListener oneListener : this.listenerList) {
			oneListener.oldCraftingProjectCancelled(oldProject);
		}
	}
	
	private void announceProjectCompleted(CraftingProject completedProject) {
		for(ICraftingManagerListener oneListener : this.listenerList) {
			oneListener.craftingProjectCompleted(completedProject);
		}
	}
	
	private void announceCraftingListChanged() {
		for(ICraftingManagerListener oneListener : this.listenerList) {
			oneListener.craftingListChanged();
		}
	}
	
	public void addWorkValue(int addedValue) {
		workValueAssigner(addedValue);
	}
	
	private void workValueAssigner(int addedValue) {
		int assigningValue = addedValue;
		while(assigningValue > 0) {
			CraftingProject workingProject = this.getCurrentUnfisihedProject();
			if(workingProject == null) {
				assigningValue = 0;
				break;
			}
			//System.out.println("Adding: "+assigningValue+" of work to "+workingProject.getMasterBlueprint().getName());
			assigningValue = workingProject.addWorkValueAndGetLeftOverValue(assigningValue);
			this.announceCraftingListChanged();
			if(workingProject.isComplete()) {
				workingProject.tryToAddFinishedItem();
				this.craftingQue.remove(workingProject);
				this.announceProjectCompleted(workingProject);
			}
			
		}
	}
	
	private CraftingProject getCurrentUnfisihedProject() {
		for(CraftingProject oneProject : this.craftingQue) {
			if(!oneProject.isComplete()) {
				return oneProject;
			}
		}
		return null;
	}

}

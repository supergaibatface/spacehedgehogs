package squareFilters;

import combatFieldElements.MovementSquare;

//Interface for filters that deal with checking a squares situation
public interface ISquareFilter {
	
	//base command that will check if the square matches requirements
	public boolean filterSquare(MovementSquare oneSquare);

}

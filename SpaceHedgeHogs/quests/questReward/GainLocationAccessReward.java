package questReward;

import questRewardBase.BaseQuestReward;
import settlementLocation.ISettlementLocation;

public class GainLocationAccessReward extends BaseQuestReward {
	private ISettlementLocation location;
	
	public GainLocationAccessReward(ISettlementLocation location) {
		this.location = location;
	}

	@Override
	protected void rewardScript() {
		location.changeAccessabilityTo(true);
		
	}

	@Override
	public String getRewardDescription() {
		return "Gain access to "+location.getName();
	}

}

package startUp;

import factories.MainFactory;

public class MainApp {

	//main function, starts the programm
	public static void main(String[] args) {
		MainFactory mainFactory = new MainFactory();
		mainFactory.bootUp();
	}

}

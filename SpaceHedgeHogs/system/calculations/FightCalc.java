package calculations;

import java.util.Random;

import combatFieldElements.FieldCharacter;
import factories.IAutoMake;
import factories.MainFactory;
import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import other.StaticFunctions;
import propertyBars.PropertyBarName;
import screenElements.ScreenTimedDataBox;
import skills.SkillName;

//Calculater for different battle values
public class FightCalc implements IAutoMake {
	private MainFactory mainFactory;
	private int baseDmg = 20;
	private int skillModifier = 5;
	private boolean debug = false;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
	}
	
	/*
	 * Method that simulates battle between two chars
	 * Inputs:
	 * 	fieldAttacker: attacker character
	 * 	fieldDefender: defending character
	 * 	range: the range at witch the battle is happening
	 */
	public void throwIntoBattle(FieldCharacter fieldAttacker, FieldCharacter fieldDefender, int range) {
		if(fieldAttacker.getMasterCharacter().getPropertyBar(PropertyBarName.Attacks).getCurrentValue() < 1) {
			return;
		}
		doFullAttack(fieldAttacker, fieldDefender);
		if(willThereBeCounter(fieldDefender.getMasterCharacter(), range)) {
			doFullAttack(fieldDefender, fieldAttacker);
		}
		
	}
	
	/*
	 * Combines individual parts of one character doing damage to another
	 * Inputs:
	 * 	damageDealer: character that will deal dmg
	 * 	damageReceiver: character that will take the dmg
	 */
	private void doFullAttack(FieldCharacter damageDealer, FieldCharacter damageReceiver) {
		int attackerDmg = generateMeleeDmgWithSkills(damageDealer.getMasterCharacter(), 
		damageReceiver.getMasterCharacter());
		removeAttackChance(damageDealer);
		dealDamage(damageReceiver, attackerDmg);
	}
	
	/*
	 * Method that generates damage dealt by an attacker
	 * Inputs:
	 * 	attacker: attacking character
	 * 	defender: defending character
	 */
	private int generateAttackerDmg(IGameCharacter attacker, IGameCharacter defender) {
		int lowPoint = 0;
		int highPoint = this.baseDmg;
		int skillDifference = attacker.findSkill(SkillName.MeleeCombat).getLevel() - defender.findSkill(SkillName.MeleeCombat).getLevel();
		if(skillDifference >= 0) {
			lowPoint = lowPoint + (this.skillModifier * skillDifference);
			highPoint = highPoint + (this.skillModifier * skillDifference);
		}
		else {
			//Skill difference is a negative value here
			highPoint = highPoint + skillDifference;
			if(highPoint < 5) {
				highPoint = 5;
			}
			
		}
		int randomDmg = new Random().nextInt(highPoint - lowPoint) + lowPoint;
		return randomDmg;
	}
	
	//Generates dmg value from character skills, inputs are attacking and defending cahracters
	private int generateMeleeDmgWithSkills(IGameCharacter attacker, IGameCharacter defender) {
		int lowPoint = this.getBaseLowPointDmg(attacker);
		int highPoint = this.getBaseHighPointDmg(attacker);
		int skillDifference = attacker.findSkill(SkillName.MeleeCombat).getLevel() - defender.findSkill(SkillName.MeleeCombat).getLevel();
		if(skillDifference >= 0) {
			//Attacker skill is higher
			lowPoint = lowPoint + skillDifference;
			highPoint = highPoint +  skillDifference;
		}
		else {
			//Attacker skill is lower, Skill difference is a negative value here
			highPoint = highPoint + skillDifference;
			if(highPoint < 5) {
				highPoint = 5;
			}
			lowPoint = lowPoint + skillDifference;
			if(lowPoint < 0) {
				lowPoint = 0;
			}
			
		}
		StaticFunctions.writeOutDebug(debug, "Generating dmg between: "+highPoint+" and " + lowPoint);
		int randomDmg = new Random().nextInt(highPoint - lowPoint) + lowPoint;
		return randomDmg;
	}
	
	//For getting base low point dmg amount, based on dmg dealing char
	private int getBaseLowPointDmg(IGameCharacter attacker) {
		return 5 * (attacker.findSkill(SkillName.PhysicalForm).getLevel() - 1);
	}
	
	//for getting base high point dmg amount, based on dmg dealing char
	private int getBaseHighPointDmg(IGameCharacter attacker) {
		return 10 * (attacker.findSkill(SkillName.PhysicalForm).getLevel());
	}
	
	/*
	 * Checks if there will be a counter attack
	 * Inputs:
	 * 	defender: character that was intially attacked
	 * 	attackRange: the range at what the attack was made
	 */
	private boolean willThereBeCounter(IGameCharacter defender, int attackRange) {
		if(attackRange > 1) {
			return false;
		}
		if(!charStillAlive(defender)) {
			return false;
		}
		if(defender.getPropertyBar(PropertyBarName.Attacks).getCurrentValue() < 1) {
			return false;
		}
		return true;
	}
	
	//Checks if a character is still alive
	private boolean charStillAlive(IGameCharacter charInQuestion) {
		if(charInQuestion.getPropertyBar(PropertyBarName.Health).getCurrentValue() > 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	//Removes one attack chance from input character
	private void removeAttackChance(FieldCharacter attacker) {
		attacker.getMasterCharacter().getPropertyBar(PropertyBarName.Attacks).decreaseByValue(1);
	}
	
	/*
	 * Method that realises dealt damage
	 * Inputs:
	 * 	target: character who was damaged
	 * 	dmg: the amount of damage that was dealt
	 */
	private void dealDamage(FieldCharacter target, int dmg) {
		target.getMasterCharacter().getPropertyBar(PropertyBarName.Health).decreaseByValue(dmg);
		this.mainFactory.getElemFactory().getScreenTimedDataBox(""+dmg, target, 1);
		target.deathCheck();
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

package spawning;

import java.util.ArrayList;
import java.util.Random;

import battle.SpawnArea;
import combatFieldElements.ControlFaction;
import combatFieldElements.MovementSquare;
import factories.IAutoMake;
import factories.MainFactory;
import other.StaticFunctions;

//Ai for selecting a square for spawning
public class SquareSelecter implements IAutoMake {
	private MainFactory mainFactory;
	private boolean debug = true;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	//Returns a random possible square for spawning
	public MovementSquare getRnadomSquareToSpawnIn() {
		MovementSquare chosenSquare = decidingLogic();
		
		return chosenSquare;
		
	}
	
	//Logic for finding a random square
	private MovementSquare decidingLogic() {
		ArrayList<MovementSquare> allSquares = this.sortOutSuitable(this.getFullListOfSpawnSquares());
		if(allSquares.isEmpty()) {
			StaticFunctions.writeOutDebug(debug, "After sorting there are no more choices for a spawning square");
			return null;
		}
		int highest = allSquares.size();
		int spawnNr = new Random().nextInt(highest);
		return allSquares.get(spawnNr);
	}
	
	//Sorts out available spawn squares the ai can use
	public ArrayList<MovementSquare> sortOutSuitable(ArrayList<MovementSquare> fullList) {
		ArrayList<MovementSquare> sortedList = new ArrayList<MovementSquare>();
		for(MovementSquare oneSquare : fullList) {
			if(!oneSquare.isOccupied()) {
				sortedList.add(oneSquare);
			}
		}
		return sortedList;
	}
	
	//Getter for a full list of spawn area squares
	private ArrayList<MovementSquare> getFullListOfSpawnSquares(){
		SpawnArea area = this.mainFactory.getGameStateController().getBattleController().findSpawnArea(ControlFaction.COMPUTER);
		if(area != null) {
			return area.getSquares();
		}
		else {
			StaticFunctions.writeOutDebug(debug, "There was no spawn area for the computer");
			return new ArrayList<MovementSquare>();
		}
	}

}

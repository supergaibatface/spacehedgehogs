package screenInfoPage;

import java.util.ArrayList;
import java.util.List;

import foodBase.FoodItemBase;
import frameworks.TextFrame;
import itemBase.IGameItem;

//Info page meant for food items
public class FoodInfoPage extends ItemBasePage {
	protected FoodItemBase master;
	protected TextFrame foodDetails;

	//constructor, input is the food item displayed on page
	public FoodInfoPage(FoodItemBase masterItem) {
		super();
		this.master = masterItem;
	}
	
	public void bootUp() {
		super.bootUp();
		this.makeFoodDetails();
	}

	@Override
	protected IGameItem getMasterAsInterface() {
		return this.master;
	}
	
	//adds food specific element fields to the info page
	protected void makeFoodDetails() {
		this.foodDetails = this.mainFactory.getElemFactory().getTextFrame(this);
		this.foodDetails.setLocation(50, 200);
		this.updateFoodDetailsText();
		this.addNewElement(this.foodDetails);
		this.setUpFoodDetailsListener();
	}

	//sets up listener that refreshes food details when needed
	private void setUpFoodDetailsListener() {
		this.master.addItemChangeEventListener(this.mainFactory.getElemFactory().getFoodInfoExpirationListener(this));
	}

	//public method for updating food specific details
	public void updateFoodDetailsText() {
		this.writeFoodDetailsText(getWriteFoodDetailsText());
	}
	
	//writes actual text values on the details element
	private void writeFoodDetailsText(List<String> lines) {
		this.foodDetails.fillTexts(lines);
	}
	
	//getter for list of strings used for food details
	private List<String> getWriteFoodDetailsText() {
		List<String> lines = new ArrayList<String>();
		lines.add("Food details:");
		lines.add("Max expiration time: "+this.master.getBaseExpiration());
		lines.add("Expiring next turn: "+this.master.getExpiringAmount());
		return lines;
	}

}

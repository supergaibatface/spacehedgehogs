package squareFilters;

import combatFieldElements.MovementSquare;

//Empty filter that passes through everything
public class NoFilter implements ISquareFilter {

	@Override
	public boolean filterSquare(MovementSquare oneSquare) {
		return true;
	}

}

package screenInfoPage;

import java.util.ArrayList;
import java.util.List;

import frameworks.IStackable;
import frameworks.TextFrame;
import generalGraphics.ButtonLookType;
import screenElements.ScreenButton;
import screenElements.ScreenScrollPanel;

//Base class for all info pages
public abstract class BaseInfoPage extends ScreenScrollPanel {
	protected ScreenButton closeButton;
	protected TextFrame nameField;

	/*
	 * constructor
	 * Inputs:
	 * 	lenght: length of the info page
	 * 	height: height of the info page
	 */
	public BaseInfoPage(int length, int height) {
		super(0, 0, length, height);
		// TODO Auto-generated constructor stub
	}
	
	public void bootUp() {
		super.bootUp();
		this.makeCloseButton();
		this.addNameField();
	}
	
	//Makes a close button for the info page
	protected void makeCloseButton() {
		this.closeButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.CLOSE, "X");
		this.closeButton.setWrapper(this);
		this.setLocationForCloseButton(this.closeButton);
		this.closeButton.addButtonEventListener(this.mainFactory.getElemFactory().getCloseIStackableElementListener(this));
		this.closeButton.setForceNonScroll(true);
	}
	
	//abstract method that sets the location coordinates for the close button, input is the actual close button
	protected abstract void setLocationForCloseButton(ScreenButton closeButton);
	
	//Adds a name field to the info page
	protected void addNameField() {
		this.nameField = this.mainFactory.getElemFactory().getTextFrame(this);
		this.setLocationForNameField(nameField);
		this.fillNameFieldValue();
		this.addNewElement(this.nameField);
	}
	
	//adds text value to the name field
	protected void fillNameFieldValue() {
		List<String> lines = new ArrayList<String>();
		this.writeNameFieldLines(lines);
		this.nameField.fillTexts(lines);
	}
	
	//Method for setting the location for name field, inputs is the element which coordinates must be set
	protected abstract void setLocationForNameField(TextFrame nameField);
	
	//Method that fills the string list used as content for name field, input is the list
	protected abstract void writeNameFieldLines(List<String> lines);
	
	@Override
	public void fillTopElementList(List<IStackable> list) {
		super.fillTopElementList(list);
		list.add(this.closeButton);
	}
	

}

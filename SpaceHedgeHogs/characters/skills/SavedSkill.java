package skills;

import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;

//object for saving and transfering a value of a skill
public class SavedSkill {
	private SkillName name;
	private int exp;
	
	/*
	 * construtor
	 * Inputs:
	 * 	name: name of the skill
	 * 	exp: int value for the exp of this skill
	 */
	public SavedSkill(SkillName name, int exp) {
		this.name = name;
		this.exp = exp;
	}

	//getter for the skill name
	public SkillName getName() {
		return name;
	}

	//setter for the name of this skill
	public void setName(SkillName name) {
		this.name = name;
	}

	//getter for the exp of this skill
	public int getExp() {
		return exp;
	}

	//setter for the exp of this skill
	public void setExp(int exp) {
		this.exp = exp;
	}
	
	//For setting this skill value on a gameCharacter
	public boolean fillSkill(IGameCharacter targetChar) {
		Skill foundSkill = targetChar.findSkill(name);
		if(foundSkill != null) {
			foundSkill.addExp(exp);
			return true;
		}
		else {
			return false;
		}
	}

}

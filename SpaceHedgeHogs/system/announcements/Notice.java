package announcements;

import factories.IAutoMake;
import factories.MainFactory;
import generalGraphics.ButtonLookType;
import screenElements.ScreenButton;
import screenElements.ScreenPopUp;

//Test type message that can be called on screen with a popup
public class Notice implements IAutoMake {
	private MainFactory mainFactory;
	private String text;
	
	//constructor
	public Notice(String text) {
		this.text = text;
	}

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	//getter for the notice text
	public String getText() {
		return this.text;
	}
	
	//Method that calls up a popup that represents this notice
	public void triggerPopUp() {
		this.mainFactory.getGElementManager().addQuickPopUp(this.makePopUp());
	}
	
	/*
	 * Makes the popup that represents this notice
	 */
	private ScreenPopUp makePopUp() {
		ScreenPopUp newPopUp = this.mainFactory.getElemFactory().getScreenPopUp("50purple");
		newPopUp.fillTextOneLine(text);
		
		newPopUp.getNewCloseButton(ButtonLookType.POPUPSELECT, "close");
		
		ScreenButton removeButton = newPopUp.getNewCloseButton(ButtonLookType.POPUPSELECT, "delete");
		removeButton.addButtonEventListener(this.mainFactory.getElemFactory().getRemoveNoticeListener(this));

		newPopUp.updateSize();
		return newPopUp;
	}

}

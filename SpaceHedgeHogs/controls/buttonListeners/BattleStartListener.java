package buttonListeners;

import java.util.ArrayList;

import gameCharacters.GameCharacter;
import graphicalWrapping.CharSpawnInfo;
import party.PartyStance;
import view.ViewName;

//Button listener that triggers start of the battle
public class BattleStartListener extends BaseButtonListener {
	public ViewName viewName;

	@Override
	public void bootUp() {
		viewName = ViewName.Battle;
		
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		if(this.mainFactory.getGameStateController().startBattle()) {
			this.mainFactory.getTurnController().changeToBattleTime();
			this.mainFactory.getParty().setPartyStance(PartyStance.Battle);
		}
		
	}

	@Override
	public String toInfo() {
		return "Starts battle";
	}

}

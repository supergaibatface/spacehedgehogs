package loupeQuests;

import java.util.List;

import dialogueFlags.DialogueFlag;
import questBase.BaseQuest;
import questStepBase.IQuestStep;

public class PreparingWeaponsJack extends BaseQuest {

	@Override
	protected String returnQuestName() {
		return "Preparing weapons";
	}

	@Override
	protected void addQuestSteps(List<IQuestStep> questStepList) {
		questStepList.add(this.makeFirstStep());
		
	}
	
	private IQuestStep makeFirstStep() {
		IQuestStep newStep = this.mainFactory.getQuestFactory().getStandardQuestStep("See if you can get weapons from armory", this);
		newStep.addNewStepObjective(this.mainFactory.getQuestFactory().getTalkToNpcObjective(newStep, this.mainFactory.getNPCFactory().getNPCLoupeArmoryChief()));
		newStep.addQuestStepReward(this.mainFactory.getQuestFactory().getGainItemsAsReward(this.mainFactory.getItemFactory().getBrokenSwordIngredientItem(), 5));
		return newStep;
	}

}

package buttonListeners;

/*
 * Button listener for loading in default view from gameStateController
 */
public class LoadInDefaultViewListener extends BaseButtonListener {

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.mainFactory.getGameStateController().loadInTravelView();
		
	}

	@Override
	public String toInfo() {
		return "Loads in default view";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

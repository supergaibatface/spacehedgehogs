package factories;

import announcements.NoticeController;
import attacking.AttackAI;
import battleManagement.PlayerLimiting;
import battleMaps.BattleMapManager;
import buttonListeners.JobListener;
import calculations.FightCalc;
import calculations.MovementCalculator;
import calculations.RangeCalculator;
import calculations.StringToScreenText;
import central.MainAIHub;
import characterClassMatching.CharacterClassFinder;
import commandAction.CAButtonPress;
import commandAction.CACharDeselect;
import commandAction.CACharSpawn;
import commandAction.CACharacterSelecting;
import commandAction.CAFieldCharAttack;
import commandAction.CAFieldCharMove;
import commandAction.CAFieldCharSelecting;
import commandAction.CAActivateSelectedItem;
import commandAction.CAItemSelecting;
import commandAction.CASocketSelecting;
import commandActionRightButton.CARScreenCharacter;
import commandActionRightButton.CARScreenItem;
import controllers.ActivityController;
import controllers.ExtraCommandsDataboxController;
import controllers.RightMouseInfoController;
import controllers.TurnController;
import events.EventCaller;
import events.EventManager;
import exceptionHandling.ExceptionManager;
import fieldCharCommand.FieldCharCommandControl;
import foodBase.FoodExpTable;
import foodBase.FoodManager;
import gameCharacters.CharacterManager;
import gameState.BattleTutorialState;
import gameState.BattleTutorialState2;
import gameState.BattleTutorialState3;
import gameState.GameStateController;
import gameState.TravelTutorialState;
import generalGraphics.ActivityInfo;
import inventory.Inventory;
import inventory.InventoryWeightController;
import itemActivation.ItemActivationMaster;
import jobChoiceController.JobChoiceController;
import jobPanels.BaseJobPanel;
import jobPanels.IJobPanel;
import jobPanels.JobPanelLoupeCamp;
import jobPanels.JobPanelTutorialCamp;
import jobPanels.JobPanelTutorialTravel;
import jobs.JobManager;
import land.TravelManager;
import mouseControls.MouseActionConditionSet;
import mouseControls.MouseCommands;
import mouseControls.ScreenMouseListener;
import mouseSituationSet.MSSBattleActiveFieldCharAttack;
import mouseSituationSet.MSSBattleActiveFieldCharItem;
import mouseSituationSet.MSSBattleActiveFieldCharMove;
import mouseSituationSet.MSSBattleActiveSocketChar;
import mouseSituationSet.MSSBattleActiveSocketItem;
import mouseSituationSet.MSSBattleDefault;
import mouseSituationSet.MSSDefault;
import mouseSituationSet.MSSTravelActiveSocketChar;
import moving.MovementAI;
import party.Party;
import screenElements.ScreenSelection;
import screenManagement.CutsceneGElementManager;
import screenManagement.GElementManager;
import screenManagement.GraphicalLengthAssigner;
import screenManagement.ImageLoader;
import screenManagement.Screen;
import skills.SkillManager;
import spawning.AISpawnController;
import spawning.SquareSelecter;
import startUp.Window;
import targetChoosing.TargetChoosingAI;
import time.BattleTime;
import time.WorldTime;
import timedSystems.RepaintTask;
import timedSystems.SystemTimer;
import view.CutsceneBuilder;
import view.ViewManager;
import water.WaterSupplyController;

/*
 * Main factory class that deals with singular objects that different objects in the program need to use
 * Deals with both making and handing out the refrencses to these objects
 */
public class MainFactory extends BaseFactory {
	private ExceptionManager exceptionManager;
	private NewElementFactory SEFactory;
	private CutsceneFactory cutsceneFactory;
	private SituationChangeFactory situationChangeFactory;
	private NPCFactory npcFactory;
	private QuestFactory questFactory;
	private CharacterFactory characterFactory;
	private ViewFactory viewFactory;
	private CraftingFactory craftingFactory;
	private ItemFactory itemFactory;
	private int masterLength = 200;
	private String grayOut = "grayOut";
	private CharacterManager charManager;
	private ViewManager viewManager;
	private GameStateController gameStateController;
	private Screen screen;
	private GraphicalLengthAssigner lenAss;
	private ImageLoader imgLoad;
	private Window window;
	private GElementManager GEManager;
	private MouseCommands mouseCommands;
	private ScreenMouseListener mouseListener;
	private ScreenSelection selection;
	private WorldTime worldTime;
	private SkillManager skillManager;
	private ActivityInfo activityInfo;
	private EventManager eventManager;
	private Inventory inventory;
	private JobManager jobManager;
	private JobListener jobListener;
	private StringToScreenText stringConverter;
	private TravelManager travelManager;
	private Party party;
	private RepaintTask repaintTask;
	private SystemTimer systemTimer;
	private TurnController turnController;
	private BattleTime battleTime;
	private ActivityController activityController;
	private MouseActionConditionSet conditionSet;
	private CAButtonPress caButtonPress;
	private CACharacterSelecting caCharacterSelect;
	private CASocketSelecting caSocketSelect;
	private CACharSpawn caCharSpawn;
	private CAFieldCharSelecting caFieldCharSelect;
	private CACharDeselect caCharDeselect;
	private CAFieldCharMove caFieldCharMove;
	private CAItemSelecting caItemSelect;
	private CAActivateSelectedItem caActivateSelectedItem;
	private CAFieldCharAttack caFieldCharAttack;
	private MovementCalculator moveCalc;
	private RangeCalculator rangeCalc;
	private FieldCharCommandControl fieldCharComControl;
	private FightCalc fightCalculator;
	private ItemActivationMaster itemActivationMaster;
	private MSSDefault mssDefault;
	private MSSBattleDefault mssBattleDefault;
	private MSSBattleActiveSocketChar mssBattleActiveSocketChar;
	private MSSBattleActiveSocketItem mssBattleActiveSocketItem;
	private MSSBattleActiveFieldCharMove mssBattleActiveFieldCharMove;
	private MSSBattleActiveFieldCharAttack mssBattleActiveFieldCharAttack;
	private MSSBattleActiveFieldCharItem mssBattleActiveFieldCharItem;
	private MSSTravelActiveSocketChar mssTravelActiveSocketChar;
	private CutsceneBuilder cutsceneBuilder;
	private SquareSelecter squareSelecterAI;
	private AISpawnController aiSpawnController;
	private MainAIHub mainAIHub;
	private MovementAI movementAI;
	private TargetChoosingAI targetingAI;
	private AttackAI attackAI;
	private NoticeController noticeController;
	private TravelTutorialState travelTutorialState;
	private WaterSupplyController waterSupplyController;
	private BattleMapManager battleMapManager;
	private BattleTutorialState battleTutorialState;
	private BattleTutorialState2 battleTutorialState2;
	private BattleTutorialState3 battleTutorialState3;
	private PlayerLimiting playerLimiting;
	private EventCaller eventCaller;
	private InventoryWeightController inventoryWeightController;
	private FoodExpTable foodExpirations;
	private FoodManager foodManager;
	private CARScreenCharacter carScreenCharacter;
	private RightMouseInfoController rightMouseInfoController;
	private CARScreenItem carScreenItem;
	private LogicalLocationFactory locationFactory;
	private CutsceneGElementManager cutsceneGEManager;
	private JobChoiceController jobChoiceController;
	private BaseJobPanel tutorialTravelJobPanel;
	private BaseJobPanel tutorialCampJobPanel;
	private BaseJobPanel loupeCampJobPanel;
	private ExtraCommandsDataboxController extraCommandsDataboxController;
	
	//constructor, this should be the first object made through main function
	public MainFactory() {
		
	}
	
	//Boot up method, meant for starting up the object after creation
	public void bootUp() {
		this.makeExceptionManager();
		this.makeWindow();
	}
	
	@Override
	protected MainFactory giveMainFactory() {
		return this;
	}

	
	//method for making the window singleton
	public void makeWindow() {
		this.window = new Window();
		setUpNewElement(this.window);
	}
	
	//private method for making a object that implements IAutoMake interface
	/*private <T extends IAutoMake> T setUpNewElement(T obj) {
		obj.setMainFactory(this);
		obj.bootUp();
		return obj;
	}*/
	
	private void makeExceptionManager() {
		this.exceptionManager = new ExceptionManager();
	}
	
	public ExceptionManager getExceptionManager() {
		return this.exceptionManager;
	}
	
	//getter for screen element factory
	public NewElementFactory getElemFactory() {
		if(this.SEFactory == null) {
			this.SEFactory = setUpNewElement(new NewElementFactory());
		}
		return this.SEFactory;
	}
	
	public CutsceneFactory getCutsceneFactory() {
		if(this.cutsceneFactory == null) {
			this.cutsceneFactory = setUpNewElement(new CutsceneFactory());
		}
		return this.cutsceneFactory;
	}
	
	public SituationChangeFactory getSituationChangeFactory() {
		if(this.situationChangeFactory == null) {
			this.situationChangeFactory = setUpNewElement(new SituationChangeFactory());
		}
		return this.situationChangeFactory;
	}
	
	public NPCFactory getNPCFactory() {
		if(this.npcFactory == null) {
			this.npcFactory = setUpNewElement(new NPCFactory());
		}
		return this.npcFactory;
	}
	
	public QuestFactory getQuestFactory() {
		if(this.questFactory == null) {
			this.questFactory = setUpNewElement(new QuestFactory());
		}
		return this.questFactory;
	}
	
	public CharacterFactory getCharacterFactory() {
		if(this.characterFactory == null) {
			this.characterFactory = setUpNewElement(new CharacterFactory());
		}
		return this.characterFactory;
	}
	
	public ViewFactory getViewFactory() {
		if(this.viewFactory == null) {
			this.viewFactory = setUpNewElement(new ViewFactory());
		}
		return this.viewFactory;
	}
	
	public CraftingFactory getCraftingFactory() {
		if(this.craftingFactory == null) {
			this.craftingFactory = setUpNewElement(new CraftingFactory());
		}
		return this.craftingFactory;
	}
	
	public ItemFactory getItemFactory() {
		if(this.itemFactory == null) {
			this.itemFactory = setUpNewElement(new ItemFactory());
		}
		return this.itemFactory;
	}
	
	//method for making a length assigner singleton
	private void makeLengthAssigner() {
		this.lenAss = new GraphicalLengthAssigner();
		setUpNewElement(this.lenAss);
	}
	
	//getter for graphical length assigner
	public GraphicalLengthAssigner getLengthAssigner() {
		if(this.lenAss == null) {
			makeLengthAssigner();
		}
		return this.lenAss;
	}
	
	//method for making game state singleton
	private void makeGameStateController() {
		this.gameStateController = new GameStateController();
		setUpNewElement(this.gameStateController);
	}
	
	//getter for game state
	public GameStateController getGameStateController() {
		if(this.gameStateController == null) {
			this.makeGameStateController();
		}
		return this.gameStateController;
	}
	
	//method for making a screen singleton
	private void makeScreen() {
		this.screen = new Screen();
		setUpNewElement(this.screen);
	}
	
	//getter for screen
	public Screen getScreen() {
		if(this.screen == null) {
			this.makeScreen();
		}
		return this.screen;
	}
	
	//method for making a view manager singleton
	private void makeViewManager() {
		this.viewManager = new ViewManager();
		setUpNewElement(this.viewManager);
	}
	
	// getter for view manager
	public ViewManager getViewManager() {
		if(this.viewManager == null) {
			this.makeViewManager();
		}
		return this.viewManager;
	}
	
	//method for making a image loader singleton
	private void makeImageLoader() {
		this.imgLoad = new ImageLoader();
		setUpNewElement(this.imgLoad);
	}
	
	//getter for image loader
	public ImageLoader getImageLoader() {
		if(this.imgLoad == null) {
			makeImageLoader();
		}
		return this.imgLoad;
	}
	
	//method for making graphical element manager singleton
	private void makeGEelementManager() {
		this.GEManager = new GElementManager();
		setUpNewElement(this.GEManager);
	}
	
	//getter for graphical element manager
	public GElementManager getGElementManager() {
		if(this.GEManager == null) {
			this.makeGEelementManager();
		}
		return this.GEManager;
	}
	
	//method for making a character manager singleton
	private void makeCharacterManager() {
		this.charManager = new CharacterManager();
		setUpNewElement(this.charManager);
	}
	
	//getter for character manager
	public CharacterManager getCharacterManager() {
		if(this.charManager == null) {
			this.makeCharacterManager();
		}
		return this.charManager;
	}
	
	//method for making a mouse listener singleton
	private void makeMouseListener() {
		this.mouseListener = new ScreenMouseListener();
		setUpNewElement(this.mouseListener);
	}
	
	//getter for mouse listener
	public ScreenMouseListener getMouseListener() {
		if(this.mouseListener == null) {
			this.makeMouseListener();
		}
		return this.mouseListener;
	}
	
	//method for making mouse commands singleton
	private void makeMouseCommands() {
		this.mouseCommands = new MouseCommands();
		setUpNewElement(this.mouseCommands);
	}
	
	//getter for mouse commands
	public MouseCommands getMouseCommands() {
		if(this.mouseCommands == null) {
			this.makeMouseCommands();
		}
		return this.mouseCommands;
	}
	
	//method for making a screen selection singleton
	private void makeScreenSelection() {
		this.selection = new ScreenSelection(0, 0);
		setUpNewElement(this.selection);
		this.selection.setImage("50x50Activity");
	}
	
	//getter for screen selection
	public ScreenSelection getScreenSelection() {
		if(this.selection == null) {
			this.makeScreenSelection();
		}
		return this.selection;
	}
	
	//method for making world time singleton
	private void makeWorldTime() {
		this.worldTime = new WorldTime();
		setUpNewElement(this.worldTime);
	}
	
	//getter for world time
	public WorldTime getWorldTime() {
		if(this.worldTime == null) {
			this.makeWorldTime();
		}
		return this.worldTime;
	}
	
	//method for making skill manager
	private void makeSkillManager() {
		this.skillManager = new SkillManager();
		setUpNewElement(this.skillManager);
	}
	
	//getter for skill manager
	public SkillManager getSkillManager() {
		if(this.skillManager == null) {
			this.makeSkillManager();
		}
		return this.skillManager;
	}
	
	//method for making activity info
	private void makeActivityInfo() {
		this.activityInfo = new ActivityInfo();
		setUpNewElement(this.activityInfo);
	}
	
	//getter for activity info
	public ActivityInfo getActivityInfo() {
		if(this.activityInfo == null) {
			this.makeActivityInfo();
		}
		return this.activityInfo;
	}
	
	//method for making an event manager
	private void makeEventManager() {
		this.eventManager = new EventManager();
		setUpNewElement(this.eventManager);
	}
	
	//getter for the event manager
	public EventManager getEventManager() {
		if(this.eventManager == null) {
			this.makeEventManager();
		}
		return this.eventManager;
	}
	
	//makes inventory
	private void makeInventory() {
		this.inventory = new Inventory();
		setUpNewElement(this.inventory);
	}
	
	//getter for inventory
	public Inventory getInventory() {
		if(this.inventory == null) {
			this.makeInventory();
		}
		return this.inventory;
	}
	
	//makes job manager
	private void makeJobManager() {
		this.jobManager = new JobManager();
		setUpNewElement(this.jobManager);
	}
	
	//getter for job manager
	public JobManager getJobManager() {
		if(this.jobManager == null) {
			this.makeJobManager();
		}
		return this.jobManager;
	}
	
	//makes job listener
	private void makeJobListener() {
		this.jobListener = new JobListener();
		setUpNewElement(this.jobListener);
	}
	
	//getter for job listener
	public JobListener getJobListener() {
		if(this.jobListener == null) {
			this.makeJobListener();
		}
		return this.jobListener;
	}
	
	//Makes string to screen text coverter
	private void makeStringToScreenText() {
		this.stringConverter = new StringToScreenText();
		setUpNewElement(this.stringConverter);
	}
	
	//Getter for the string to screen text converter
	public StringToScreenText getStringToScreenConv() {
		if(this.stringConverter == null) {
			this.makeStringToScreenText();
		}
		return this.stringConverter;
	}
	
	//makes travel manager
	private void makeTravelManager() {
		this.travelManager = new TravelManager();
		setUpNewElement(this.travelManager);
	}
	
	//getter for the travel manager
	public TravelManager getTravelManager() {
		if(this.travelManager == null) {
			this.makeTravelManager();
		}
		return this.travelManager;
	}
	
	//makes party object
	private void makeParty() {
		this.party = new Party();
		setUpNewElement(this.party);
	}
	
	//getter for the party object
	public Party getParty() {
		if(this.party == null) {
			this.makeParty();
		}
		return this.party;
	}
	
	private void makeRepaintTask() {
		this.repaintTask = new RepaintTask();
		setUpNewElement(this.repaintTask);
	}
	
	//Getter for the repaint task that will trigger repaints for the screen
	public RepaintTask getRepaintTask() {
		if(this.repaintTask == null) {
			this.makeRepaintTask();
		}
		return this.repaintTask;
	}
	
	private void makeSystemTimer() {
		this.systemTimer = new SystemTimer();
		setUpNewElement(this.systemTimer);
	}
	
	//Getter for system timer that times framerate
	public SystemTimer getSystemTimer() {
		if(this.systemTimer == null) {
			this.makeSystemTimer();
		}
		return this.systemTimer;
	}
	
	private void makeTurnController() {
		this.turnController = new TurnController();
		setUpNewElement(this.turnController);
	}
	
	public TurnController getTurnController() {
		if(this.turnController == null) {
			this.makeTurnController();
		}
		return this.turnController;
	}
	
	private void makeBattleTime() {
		this.battleTime = new BattleTime();
		setUpNewElement(this.battleTime);
	}
	
	public BattleTime getBattleTime() {
		if(this.battleTime == null) {
			this.makeBattleTime();
		}
		return this.battleTime;
	}
	
	private void makeActivityController() {
		this.activityController = new ActivityController();
		setUpNewElement(this.activityController);
	}
	
	public ActivityController getActivityController() {
		if(this.activityController == null) {
			this.makeActivityController();
		}
		return this.activityController;
	}
	
	private void makeMouseConditionSet() {
		this.conditionSet = new MouseActionConditionSet();
		setUpNewElement(this.conditionSet);
	}
	
	public MouseActionConditionSet getMouseConditionSet() {
		if(this.conditionSet == null) {
			this.makeMouseConditionSet();
		}
		return this.conditionSet;
	}
	
	private void makeCAButtonPress() {
		this.caButtonPress = new CAButtonPress();
		setUpNewElement(this.caButtonPress);
	}
	
	public CAButtonPress getCAButtonPress() {
		if(this.caButtonPress == null) {
			this.makeCAButtonPress();
		}
		return this.caButtonPress;
	}
		
	private void makeCACharacterSelect() {
		this.caCharacterSelect = new CACharacterSelecting();
		setUpNewElement(this.caCharacterSelect);
	}
	
	public CACharacterSelecting getCACharSelect() {
		if(this.caCharacterSelect == null) {
			this.makeCACharacterSelect();
		}
		return this.caCharacterSelect;
	}
	
	private void makeCASocketSelect() {
		this.caSocketSelect = new CASocketSelecting();
		setUpNewElement(this.caSocketSelect);
	}
	
	public CASocketSelecting getCASocketSelect() {
		if(this.caSocketSelect == null) {
			this.makeCASocketSelect();
		}
		return this.caSocketSelect;
	}
	
	private void makeCACharSpawn() {
		this.caCharSpawn = new CACharSpawn();
		setUpNewElement(this.caCharSpawn);
	}
	
	public CACharSpawn getCACharSpawn() {
		if(this.caCharSpawn == null) {
			this.makeCACharSpawn();
		}
		return this.caCharSpawn;
	}
	
	private void makeCAFieldCharSpawn() {
		this.caFieldCharSelect = new CAFieldCharSelecting();
		setUpNewElement(this.caFieldCharSelect);
	}
	
	public CAFieldCharSelecting getCAFieldCharSelect() {
		if(this.caFieldCharSelect == null) {
			this.makeCAFieldCharSpawn();
		}
		return this.caFieldCharSelect;
	}
	
	
	private void makeCACharDeselect() {
		this.caCharDeselect = new CACharDeselect();
		setUpNewElement(this.caCharDeselect);
	}
	
	public CACharDeselect getCACharDeselect() {
		if(this.caCharDeselect == null) {
			this.makeCACharDeselect();
		}
		return this.caCharDeselect;
	}
	
	private void makeCAFieldCharMove() {
		this.caFieldCharMove = new CAFieldCharMove();
		setUpNewElement(this.caFieldCharMove);
	}
	
	public CAFieldCharMove getCAFieldCharMove() {
		if(this.caFieldCharMove == null) {
			this.makeCAFieldCharMove();
		}
		return this.caFieldCharMove;
	}
	
	private void makeCAItemSelect() {
		this.caItemSelect = new CAItemSelecting();
		setUpNewElement(this.caItemSelect);
	}
	
	public CAItemSelecting getCAItemSelecting() {
		if(this.caItemSelect == null) {
			this.makeCAItemSelect();
		}
		return this.caItemSelect;
	}
	
	private void makeCAActivateSelectedItem() {
		this.caActivateSelectedItem = new CAActivateSelectedItem();
		setUpNewElement(this.caActivateSelectedItem);
	}
	
	public CAActivateSelectedItem getCAActivateSelectedItem() {
		if(this.caActivateSelectedItem == null) {
			this.makeCAActivateSelectedItem();
		}
		return this.caActivateSelectedItem;
	}
	
	private void makeCAFieldCharAttack() {
		this.caFieldCharAttack = new CAFieldCharAttack();
		setUpNewElement(this.caFieldCharAttack);
	}
	
	public CAFieldCharAttack getCAFieldCharAttack() {
		if(this.caFieldCharAttack == null) {
			this.makeCAFieldCharAttack();
		}
		return this.caFieldCharAttack;
	}
	
	/*private void makeCAScrollSocketSlecting() {
		this.caScrollSocketSelecting = new CAScrollSocketSelecting();
		setUpObject(this.caScrollSocketSelecting);
	}*/
	
	/*public CAScrollSocketSelecting getCAScrollSocketSelecting() {
		if(this.caScrollSocketSelecting == null) {
			this.makeCAScrollSocketSlecting();
		}
		return this.caScrollSocketSelecting;
	}*/
	
	/*private void makeCASimpleCharSelecting() {
		this.caSimpleCharSelecting = new CASimpleCharSelecting();
		setUpObject(this.caSimpleCharSelecting);
	}*/
	
	/*public CASimpleCharSelecting getCASimpleCharSelecting() {
		if(this.caSimpleCharSelecting == null) {
			this.makeCASimpleCharSelecting();
		}
		return this.caSimpleCharSelecting;
	}*/
	
	private void makeMovementCalculator() {
		this.moveCalc = new MovementCalculator();
		setUpNewElement(this.moveCalc);
	}
	
	public MovementCalculator getMovementCalculator() {
		if(this.moveCalc == null) {
			this.makeMovementCalculator();
		}
		return this.moveCalc;
	}
	
	private void makeAttackRangeCalc() {
		this.rangeCalc = new RangeCalculator();
		setUpNewElement(this.rangeCalc);
	}
	
	public RangeCalculator getAttackRangeCalculator() {
		if(this.rangeCalc == null) {
			this.makeAttackRangeCalc();
		}
		return this.rangeCalc;
	}
	
	private void makeFieldCharCommandControl() {
		this.fieldCharComControl = new FieldCharCommandControl();
		setUpNewElement(this.fieldCharComControl);
	}
	
	public FieldCharCommandControl getFieldCharCommandControl() {
		if(this.fieldCharComControl == null) {
			this.makeFieldCharCommandControl();
		}
		return this.fieldCharComControl;
	}
	
	private void makeFightCalculator() {
		this.fightCalculator = new FightCalc();
		setUpNewElement(this.fightCalculator);
	}
	
	public FightCalc getFightCalculator() {
		if(this.fightCalculator == null) {
			this.makeFightCalculator();
		}
		return this.fightCalculator;
	}
	
	private void makeItemActivationMaster() {
		this.itemActivationMaster = new ItemActivationMaster();
		setUpNewElement(this.itemActivationMaster);
	}
	
	public ItemActivationMaster getItemActivationMaster() {
		if(this.itemActivationMaster == null) {
			this.makeItemActivationMaster();
		}
		return this.itemActivationMaster;
	}
	
	private void makeMSSDefault() {
		this.mssDefault = new MSSDefault();
		setUpNewElement(this.mssDefault);
	}
	
	public MSSDefault getMSSDefault() {
		if(this.mssDefault == null) {
			this.makeMSSDefault();
		}
		return this.mssDefault;
	}
	
	private void makeMSSBattleDefault() {
		this.mssBattleDefault = new MSSBattleDefault();
		setUpNewElement(this.mssBattleDefault);
	}
	
	public MSSBattleDefault getMSSBattleDefault() {
		if(this.mssBattleDefault == null) {
			this.makeMSSBattleDefault();
		}
		return this.mssBattleDefault;
	}
	
	private void makeMSSBattleActiveSocketChar() {
		this.mssBattleActiveSocketChar = new MSSBattleActiveSocketChar();
		setUpNewElement(this.mssBattleActiveSocketChar);
	}
	
	public MSSBattleActiveSocketChar getMSSBattleActiveSocketChar() {
		if(this.mssBattleActiveSocketChar == null) {
			this.makeMSSBattleActiveSocketChar();
		}
		return this.mssBattleActiveSocketChar;
	}
	
	private void makeMSSBattleActiveSocketItem() {
		this.mssBattleActiveSocketItem = new MSSBattleActiveSocketItem();
		setUpNewElement(this.mssBattleActiveSocketItem);
	}
	
	public MSSBattleActiveSocketItem getMSSBattleActiveSocketItem() {
		if(this.mssBattleActiveSocketItem == null) {
			this.makeMSSBattleActiveSocketItem();
		}
		return this.mssBattleActiveSocketItem;
	}
	
	private void makeMSSBattleActiveFieldCharMove() {
		this.mssBattleActiveFieldCharMove = new MSSBattleActiveFieldCharMove();
		setUpNewElement(this.mssBattleActiveFieldCharMove);
	}
	
	public MSSBattleActiveFieldCharMove getMSSBattleActiveFieldCharMove() {
		if(this.mssBattleActiveFieldCharMove == null) {
			this.makeMSSBattleActiveFieldCharMove();
		}
		return this.mssBattleActiveFieldCharMove;
	}
	
	private void makeMSSBattleActiveFieldCharAttack() {
		this.mssBattleActiveFieldCharAttack = new MSSBattleActiveFieldCharAttack();
		setUpNewElement(this.mssBattleActiveFieldCharAttack);
	}
	
	public MSSBattleActiveFieldCharAttack getMSSBattleActiveFieldCharAttack() {
		if(this.mssBattleActiveFieldCharAttack == null) {
			this.makeMSSBattleActiveFieldCharAttack();
		}
		return this.mssBattleActiveFieldCharAttack;
	}
	
	private void makeMSSBattleActiveFieldCharItem() {
		this.mssBattleActiveFieldCharItem = new MSSBattleActiveFieldCharItem();
		setUpNewElement(this.mssBattleActiveFieldCharItem);
	}
	
	public MSSBattleActiveFieldCharItem getMSSBattleActiveFieldCharItem() {
		if(this.mssBattleActiveFieldCharItem == null) {
			this.makeMSSBattleActiveFieldCharItem();
		}
		return this.mssBattleActiveFieldCharItem;
	}
	
	private void makeMSSTravelActiveSocketChar() {
		this.mssTravelActiveSocketChar = new MSSTravelActiveSocketChar();
		setUpNewElement(this.mssTravelActiveSocketChar);
	}
	
	public MSSTravelActiveSocketChar getMSSTravelActiveSocketChar() {
		if(this.mssTravelActiveSocketChar == null) {
			this.makeMSSTravelActiveSocketChar();
		}
		return this.mssTravelActiveSocketChar;
	}

	private void makeCutsceneBuilder() {
		this.cutsceneBuilder = new CutsceneBuilder();
		setUpNewElement(this.cutsceneBuilder);
	}
	
	public CutsceneBuilder getCutsceneBuilder() {
		if(this.cutsceneBuilder == null) {
			this.makeCutsceneBuilder();
		}
		return this.cutsceneBuilder;
	}
	
	private void makeSquareSelecter() {
		this.squareSelecterAI = new SquareSelecter();
		setUpNewElement(this.squareSelecterAI);
	}
	
	public SquareSelecter getSquareSelecterAI() {
		if(this.squareSelecterAI == null) {
			this.makeSquareSelecter();
		}
		return this.squareSelecterAI;
	}
	
	private void makeAISpawnController() {
		this.aiSpawnController = new AISpawnController();
		setUpNewElement(this.aiSpawnController);
	}
	
	public AISpawnController getAISpawnController() {
		if(this.aiSpawnController == null) {
			this.makeAISpawnController();
		}
		return this.aiSpawnController;
	}
	
	private void makeMainAIHub() {
		this.mainAIHub = new MainAIHub();
		setUpNewElement(this.mainAIHub);
	}
	
	public MainAIHub getMainAIHub() {
		if(this.mainAIHub == null) {
			this.makeMainAIHub();
		}
		return this.mainAIHub;
	}
	
	private void makeMovementAI() {
		this.movementAI = new MovementAI();
		setUpNewElement(this.movementAI);
	}
	
	public MovementAI getMovementAI() {
		if(this.movementAI == null) {
			this.makeMovementAI();
		}
		return this.movementAI;
	}
	
	private void makeTargetingAI() {
		this.targetingAI = new TargetChoosingAI();
		setUpNewElement(this.targetingAI);
	}
	
	public TargetChoosingAI getTargetingAI() {
		if(this.targetingAI == null) {
			this.makeTargetingAI();
		}
		return this.targetingAI;
	}
	
	private void makeAttackAI() {
		this.attackAI = new AttackAI();
		setUpNewElement(this.attackAI);
	}
	
	public AttackAI getAttackAI() {
		if(this.attackAI == null) {
			this.makeAttackAI();
		}
		return this.attackAI;
	}
	
	private void makeNoticeController() {
		this.noticeController = new NoticeController();
		setUpNewElement(this.noticeController);
	}
	
	public NoticeController getNoticeController() {
		if(this.noticeController == null) {
			this.makeNoticeController();
		}
		return this.noticeController;
	}
	
	private void makeTravelTutorialState() {
		this.travelTutorialState = new TravelTutorialState();
		setUpNewElement(this.travelTutorialState);
	}
	
	public TravelTutorialState getTravelTutorialState() {
		if(this.travelTutorialState == null) {
			 makeTravelTutorialState();
		}
		return this.travelTutorialState;
	}
	
	private void makeWaterSupplyController() {
		this.waterSupplyController = new WaterSupplyController();
		setUpNewElement(this.waterSupplyController);
	}
	
	public WaterSupplyController getWaterSupplyController() {
		if(this.waterSupplyController == null) {
			makeWaterSupplyController();
		}
		return this.waterSupplyController;
	}
	
	private void makeBattleMapManager() {
		this.battleMapManager = new BattleMapManager();
		setUpNewElement(this.battleMapManager);
	}
	
	public BattleMapManager getBattleMapManager() {
		if(this.battleMapManager == null) {
			makeBattleMapManager();
		}
		return this.battleMapManager;
	}
	
	private void makeBattleTutorialState() {
		this.battleTutorialState = new BattleTutorialState();
		setUpNewElement(this.battleTutorialState);
	}
	
	public BattleTutorialState getBattleTutorialState() {
		if(this.battleTutorialState == null) {
			makeBattleTutorialState();
		}
		return this.battleTutorialState;
	}
	
	private void makeBattleTutorialState2() {
		this.battleTutorialState2 = new BattleTutorialState2();
		setUpNewElement(this.battleTutorialState2);
	}
	
	public BattleTutorialState2 getBattleTutorialState2() {
		if(this.battleTutorialState2 == null) {
			makeBattleTutorialState2();
		}
		return this.battleTutorialState2;
	}
	
	private void makeBattleTutorialState3() {
		this.battleTutorialState3 = new BattleTutorialState3();
		setUpNewElement(this.battleTutorialState3);
	}
	
	public BattleTutorialState3 getBattleTutorialState3() {
		if(this.battleTutorialState3 == null) {
			makeBattleTutorialState3();
		}
		return this.battleTutorialState3;
	}
	
	private void makePlayerLimiting() {
		this.playerLimiting = new PlayerLimiting();
		setUpNewElement(this.playerLimiting);
	}
	
	public PlayerLimiting getPlayerLimiting() {
		if(this.playerLimiting == null) {
			makePlayerLimiting();
		}
		return this.playerLimiting;
	}
	
	private void makeEventCaller() {
		this.eventCaller = new EventCaller();
		setUpNewElement(this.eventCaller);
	}
	
	public EventCaller getEventCaller() {
		if(this.eventCaller == null) {
			makeEventCaller();
		}
		return this.eventCaller;
	}
	
	private void makeInventoryWeightController() {
		this.inventoryWeightController = setUpNewElement(new InventoryWeightController());
	}
	
	public InventoryWeightController getInventoryWeightController() {
		if(this.inventoryWeightController == null) {
			makeInventoryWeightController();
		}
		return this.inventoryWeightController;
	}
	
	private void makeFoodExpirationTable() {
		this.foodExpirations = setUpNewElement(new FoodExpTable());
	}
	
	public FoodExpTable getFoodExpTable() {
		if(this.foodExpirations == null) {
			makeFoodExpirationTable();
		}
		return this.foodExpirations;
	}
	
	private void makeFoodManager() {
		this.foodManager = setUpNewElement(new FoodManager());
	}
	
	public FoodManager getFoodManager() {
		if(this.foodManager == null) {
			makeFoodManager();
		}
		return this.foodManager;
	}
	
	private void makeCARScreenCharacter() {
		this.carScreenCharacter = setUpNewElement(new CARScreenCharacter());
	}
	
	public CARScreenCharacter getCARScreenCharacter() {
		if(this.carScreenCharacter == null) {
			this.makeCARScreenCharacter();
		}
		return this.carScreenCharacter;
	}
	
	private void makeRightMouseInfoController() {
		this.rightMouseInfoController = setUpNewElement(new RightMouseInfoController());
	}
	
	public RightMouseInfoController getRightMouseInfoController() {
		if(this.rightMouseInfoController == null) {
			this.makeRightMouseInfoController();
		}
		return this.rightMouseInfoController;
	}
	
	private void makeCARScreenItem() {
		this.carScreenItem = setUpNewElement(new CARScreenItem());
	}
	
	public CARScreenItem getCARScreenItem() {
		if(this.carScreenItem == null) {
			this.makeCARScreenItem();
		}
		return this.carScreenItem;
	}
	
	public void makeLogicalLocationFactory() {
		this.locationFactory = setUpNewElement(new LogicalLocationFactory());
	}
	
	public LogicalLocationFactory getLogicalLocationFactory() {
		if(this.locationFactory == null) {
			this.makeLogicalLocationFactory();
		}
		return this.locationFactory;
	}
	
	private void makeCutsceneGElementManager() {
		this.cutsceneGEManager = setUpNewElement(new CutsceneGElementManager());
	}
	
	public CutsceneGElementManager getCutsceneGElemenetManager() {
		if(this.cutsceneGEManager == null) {
			this.makeCutsceneGElementManager();
		}
		return this.cutsceneGEManager;
	}
	
	private void makeJobChoiceController() {
		this.jobChoiceController = setUpNewElement(new JobChoiceController());
	}
	
	public JobChoiceController getJobChoiceController() {
		if(this.jobChoiceController == null) {
			this.makeJobChoiceController();
		}
		return this.jobChoiceController;
	}
	
	public BaseJobPanel getJobPanelForTravelTutorial() {
		if(this.tutorialTravelJobPanel == null) {
			this.tutorialTravelJobPanel = setUpNewElement(new JobPanelTutorialTravel());
		}
		return this.tutorialTravelJobPanel;
	}
	
	public BaseJobPanel getJobPanelForCampTutorial() {
		if(this.tutorialCampJobPanel == null) {
			this.tutorialCampJobPanel = setUpNewElement(new JobPanelTutorialCamp());
		}
		return this.tutorialCampJobPanel;
	}
	
	public BaseJobPanel getJobPanelForLoupeCamp() {
		if(this.loupeCampJobPanel == null) {
			this.loupeCampJobPanel = setUpNewElement(new JobPanelLoupeCamp());
		}
		return this.loupeCampJobPanel;
	}
	
	public ExtraCommandsDataboxController getExtraCommandsDataboxController() {
		if(this.extraCommandsDataboxController == null) {
			this.extraCommandsDataboxController = setUpNewElement(new ExtraCommandsDataboxController());
		}
		return this.extraCommandsDataboxController;
	}
	
	//getter for master length for graphical elements
	public int getMasterLength() {
		return this.masterLength;
	}
	
	//getter for out images name
	public String getGrayOutName() {
		return this.grayOut;
	}

}

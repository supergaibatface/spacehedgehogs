package spawning;

import java.util.ArrayList;

import central.AICharacterSet;
import combatFieldElements.ControlFaction;
import combatFieldElements.FieldCharacter;
import combatFieldElements.MovementSquare;
import factories.IAutoMake;
import factories.MainFactory;
import gameCharacters.GameCharacter;
import other.StaticFunctions;

//AI controller for spawning characters
public class AISpawnController implements IAutoMake {
	private MainFactory mainFactory;
	private ArrayList<AICharacterSet> charactersToSpawn;
	private SquareSelecter squareSelecter;
	private boolean debug = true;
	private ISpawnTimeModule timeModule;
	private int maxSpawnsPerTurn;
	private int currentSpawnsPerTurn;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.charactersToSpawn = new ArrayList<AICharacterSet>();
		this.squareSelecter = this.mainFactory.getSquareSelecterAI();
	}
	
	//Setup function to start spawning
	public void setUpSpawning(ArrayList<AICharacterSet> characters, ISpawnTimeModule spawnTimeModule) {
		this.charactersToSpawn = StaticFunctions.copyArray(characters);
		this.timeModule = spawnTimeModule;
		this.maxSpawnsPerTurn = 2;
		this.currentSpawnsPerTurn = 0;
	}
	
	//Main method for calling for a character spawn
	public FieldCharacter spawnOneEnemy() {
		if(!stillWorkToDo()) {
			StaticFunctions.writeOutDebug(debug, "Spawn enemies command was used despite the game not allowing to spawn anymore");
			return null;
		}
		AICharacterSet charToSpawn = this.decideOnCharToSpawn();
		MovementSquare location = this.getSpawnSquare();
		FieldCharacter spawnedChar = this.SpawnCharacter(charToSpawn.getCharacterInfo(), location);
		spawnedChar.attachAIPattern(charToSpawn.getCommandDetails());
		this.currentSpawnsPerTurn ++;
		this.mainFactory.getMainAIHub().updateEnemyInfo();
		return spawnedChar;
	}
	
	/*
	 * Gives the AI a turn of spawning
	 */
	public void giveSpawnTurn() {
		if(!this.hasSpawnTimeModule()) {
			StaticFunctions.writeOutDebug(debug, "Trying to spawn but there is no spawn module");
			return;
		}
		if(!this.stillWorkToDo()) {
			return;
		}
		while(this.timeModule.doWeSpawn() && this.hardLimitAllowsSpawn()) {
			this.mainFactory.getMainAIHub().addFieldCharToControlledChars(spawnOneEnemy());
		}
	}
	
	//checks if spawn controller still has characters it can spawn
	public boolean stillWorkToDo() {
		return !this.charactersToSpawn.isEmpty();
	}
	
	//Method that chooses a character to spawn
	private AICharacterSet decideOnCharToSpawn() {
		if(this.charactersToSpawn.isEmpty()) {
			StaticFunctions.writeOutDebug(debug, "The list of spawnable enemy chars is empty, cant choose one to spawn");
			return null;
			
		}
		else {
			AICharacterSet chosenChar = this.charactersToSpawn.remove(0);
			return chosenChar;
		}
	}
	
	//Gets squares that AI can spawn chars on
	private MovementSquare getSpawnSquare() {
		return this.squareSelecter.getRnadomSquareToSpawnIn();
	}
	
	//Spawns a designated character on a square
	private FieldCharacter SpawnCharacter(GameCharacter oneChar, MovementSquare location) {
		location.SpawnCharacter(oneChar, ControlFaction.COMPUTER);
		FieldCharacter fieldChar = (FieldCharacter) location.getFieldElement();
		return fieldChar;
	}
	
	//Checks if there is a module that controls when AI spawns characters
	public boolean hasSpawnTimeModule() {
		return this.timeModule != null;
	}
	
	//Hard limit that on AI spawning that acts even without any spawning logic
	public boolean hardLimitAllowsSpawn() {
		return this.currentSpawnsPerTurn < this.maxSpawnsPerTurn;
	}
	
	//restes hard limit counter
	public void resetTurnHardLimitCounter() {
		this.currentSpawnsPerTurn = 0;
	}
	
	//adds info on enemy spawning to an arrayList of strings
	public void fillListWithInfo(ArrayList<String> lines) {
		lines.add("Enemys still coming: "+this.charactersToSpawn.size());
	}
	

}

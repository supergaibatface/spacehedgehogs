package buttonListeners;

//Listener that triggeres traveling forward
public class TravelListener extends BaseButtonListener {

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		if(travelCheck()) {
			double distance = calculateDistance();
			this.mainFactory.getTravelManager().advance(distance);
		}
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	//Checks if its allowed to move forward
	private boolean travelCheck() {
		if (this.mainFactory.getTravelManager().canAdvance()) {
			return true;
		}
		else {
			return false;
		}
	}
	
	//Calculates how much is allowed to travel forward in one turn
	private double calculateDistance() {
		return this.mainFactory.getTravelManager().calculateDistance(this.mainFactory.getParty().getCaravanSpeed());
	}

	@Override
	public String toInfo() {
		return "Makes the caravan travel forward";
	}

}

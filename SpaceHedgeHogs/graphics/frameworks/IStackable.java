package frameworks;

import java.util.ArrayList;
import java.util.List;

import generalGraphics.ClickBox;
import screenElements.ScreenElement;
import screenElements.ScreenText;

/*
 * Interface for graphical elements that have their own coordinate system
 */
public interface IStackable {
	//public IStackable wrapper;
	
	/*
	 * Method for calculating X coordinate of an upper coordinate system
	 */
	public int getOffWrapperX();
	
	/*
	 * Method for calculating Y coordinate of an upper coordinate system
	 */
	public int getOffWrapperY();
	
	/*
	 * Method for getting the X coordinate of where data starts on this element
	 */
	public int getXDataStart();
	
	/*
	 * Method for getting the Y coordinate of where data starts on this element
	 */
	public int getYDataStart();
	
	//getter for x coordinate
	public int getX();
	
	//getter for y coordinate
	public int getY();
	
	//getter for how much x has been scrolled
	public int getXScroll();
	
	//getter for how much y has been scrolled
	public int getYScroll();
	
	//checks if this element inherits screenElement
	public boolean isScreenElement();
	
	//getter for element length
	public int getLength();
	
	//getter for element height
	public int getHeight();
	
	//setter for wrapper value
	public void setWrapper(IStackable wrapperElem);
	
	//Master command for overlap calculations
	public void overLapMaster(ClickBox visibileBox);
	
	//Getter for display box (actual visible area)
	public ClickBox getDisplayBox();
	
	//getter for the area this element covers as a ClickBox
	public ClickBox getClickBox();
	
	//Selfsearch for clickable Elements
	public List<ScreenElement> selfSearch(int clickX, int clickY, List<Class> searchClasses, List<ScreenElement> fillList);
	
	//Method for getting a list of on top elements
	public List<IStackable> getOnTopElements();
	
	//Method for getting on top texts
	public List<ScreenText> getOnTopTexts();
	
	//Debug method for printing out all the wrappers on this element
	public void printOutStackingHierarchy();
	
	//Method for safely deleting an element
	public void safeDelete();
	
	//UNUSED/NOT WORKING, script that at one point should activate as an element gets first added to the screen
	public void goingOnScreenAlert();
	
	public void goingOffScreenAlert();
	
	//Checks the value of local non scroll value
	public boolean isForceNonScroll();
	

}

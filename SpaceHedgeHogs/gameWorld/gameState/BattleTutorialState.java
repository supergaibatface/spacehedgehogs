package gameState;

import java.util.ArrayList;

import central.AICharacterSet;
import combatFieldElements.ControlFaction;
import combatFieldElements.FieldCharacter;
import combatFieldElements.MovementSquare;
import dataValues.CharValue;
import gameCharacters.GameCharacter;
import jobChoiceController.JobChoiceController;
import party.PartyStance;
import propertyBars.PropertyBar;
import propertyBars.PropertyBarName;
import screenElements.ScreenButton;
import screenElements.ScreenElement;
import skills.SavedSkill;
import skills.SavedSkillSheet;
import skills.SkillName;
import view.CutsceneData;
import view.CutsceneDataCollection;
import view.GameView;
import view.IGameView;
import view.ViewName;

public class BattleTutorialState extends BaseGameState {
	
	@Override
	protected void loadInPlayerCharacters(ArrayList<CharValue> listOfChars) {
		CharValue newCharValue = new CharValue("Player", "charspot");
		newCharValue.addSavedSkillValue(SkillName.PhysicalForm, 20);
		newCharValue.addSavedSkillValue(SkillName.MeleeCombat, 20);
		listOfChars.add(newCharValue);
		
	}

	@Override
	protected void bootLoadFillViewList() {
		/*GameView newView =  mainFactory.getViewManager().makeAndGetDebugView();
		this.travelView = newView.getName();*/
		this.travelView = ViewName.Debug;
	}

	@Override
	protected void loadInBattleViewBase(IGameView viewSaveLocation) {
		viewSaveLocation = this.mainFactory.getViewFactory().getBattleTutorial1View();
		this.mainFactory.getViewManager().addNewView(viewSaveLocation);
		//viewSaveLocation = this.mainFactory.getViewManager().makeAndGetFirstTutorialBattleView();
	}
	
	/*@Override
	protected void loadInOtherViews() {
		// TODO Auto-generated method stub
		
	}*/

	@Override
	protected void loadInInventory() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void loadInTravelOptions() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void loadInCaravanJobs() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startGameScript() {
		ArrayList<AICharacterSet> enemies = new ArrayList<AICharacterSet>();
		enemies.add(this.mainFactory.getCharacterManager().getEnemyCharacterSets().get(1));
		this.mainFactory.getGameStateController().addEnemysForAFight(enemies);
		
		this.mainFactory.getGameStateController().setBattleTutorialFlag(true);
		this.mainFactory.getGameStateController().startBattle(this.mainFactory.getBattleMapManager().getTutorialMovementField1(), 1, this.mainFactory.getElemFactory().getTimeSpawnEveryTurnOne());
		this.mainFactory.getGameStateController().getBattleController().insertSpecialWinPopUp(makeNextModulePopUp());
		this.spawnInPlayerChar();
		
		this.mainFactory.getViewManager().changeToView(ViewName.Battle);
		
		this.mainFactory.getCutsceneBuilder().startACutscene(this.makeStartCutscene());
		
	}
	
	//for sapwning in player character in this mode
	private void spawnInPlayerChar() {
		MovementSquare playerSquare = this.mainFactory.getGameStateController().getBattleController().findSpawnArea(ControlFaction.PLAYER).getSquares().get(0);
		FieldCharacter spawnedChar = playerSquare.SpawnCharacter(this.mainFactory.getCharacterManager().getCharacters().get(0), ControlFaction.PLAYER);
		spawnedChar.makeLabel();
	}

	@Override
	protected void addNextPartButtonListeners(ScreenButton nextPartButton) {
		nextPartButton.addButtonEventListener(this.mainFactory.getElemFactory().getImportAllCharsToModuleListener(this.mainFactory.getBattleTutorialState2()));
		nextPartButton.addButtonEventListener(this.mainFactory.getElemFactory().getLoadInGameStateListener(this.mainFactory.getBattleTutorialState2()));
		
	}
	
	//Makes tutorial cutscene for the game
	private CutsceneDataCollection makeStartCutscene() {
		CutsceneDataCollection tutorial = this.mainFactory.getElemFactory().getCutsceneDataCollection("Intro1");
		CutsceneData newFrame = new CutsceneData("travelBG", "Welcome to SpaceHedgeHogs (working title) traveling game, made by T�nis Prants");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "The swarm breached the gates.");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "There are fires all around! You need to make sure the royal family safe!");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "But first, you were sepearted. You need to find your soldiers.");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "They wont get through this without leadership, hell, you wont get through this without them....");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "... No one will survive alone.");
		tutorial.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "You are blocked off, a lone beast infront of you, its dazed but its in your way and it wont go down without a fight.");
		tutorial.addNewCutScene(newFrame);
		
		return tutorial;
	}

}

package battle;

import java.util.ArrayList;

import combatFieldElements.ControlFaction;
import combatFieldElements.MovementSquare;
import factories.IAutoMake;
import factories.MainFactory;

/*
 * This class represents areas where factions can bring in new tropps
 */
public class SpawnArea implements IAutoMake{
	private ControlFaction owner;
	private ArrayList<MovementSquare> squares;
	private MainFactory mainFactory;
	
	/*
	 * Constructor, Faction: the faction this spawn are is for
	 */
	public SpawnArea(ControlFaction faction) {
		this.owner = faction;
	}
	
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}
	@Override
	public void bootUp() {
		this.squares = new ArrayList<MovementSquare>();
		
	}
	
	/*
	 * Adds one new square to the spawn area
	 */
	public void addNewSquare(MovementSquare oneSquare) {
		this.squares.add(oneSquare);
	}
	
	/*
	 * Adds a whole array of squares to the spawn area at once
	 */
	public void addArrayOfSquares(ArrayList<MovementSquare> listOfSquares) {
		for(MovementSquare oneSquare : listOfSquares) {
			this.addNewSquare(oneSquare);
		}
	}
	
	/*
	 * Getter for all the squares in the spawn area
	 */
	public ArrayList<MovementSquare> getSquares() {
		return this.squares;
	}
	
	/*
	 * Checks if the input faction is the owner of this spawn area
	 */
	public boolean belongsToFaction(ControlFaction faction) {
		if(this.owner == faction) {
			return true;
		}
		else {
			return false;
		}
	}

}

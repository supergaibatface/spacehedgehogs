package dialogueFlagListeners;

import dialogueFlags.DialogueFlag;

public interface IDialogueFlagListener {
	
	public void triggerNewFlag(DialogueFlag newFlag);
	
	public void triggerFlagRemoval(DialogueFlag removedFlag);

}

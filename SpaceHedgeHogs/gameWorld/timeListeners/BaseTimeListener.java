package timeListeners;

import factories.IAutoMake;
import factories.MainFactory;

//Base class for time listeners
public abstract class BaseTimeListener implements IAutoMake, ITimeListener  {
	protected MainFactory mainFactory;

	@Override
	public abstract void timeAdvanced(boolean inBattle);

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

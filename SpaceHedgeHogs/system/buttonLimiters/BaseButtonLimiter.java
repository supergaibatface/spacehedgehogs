package buttonLimiters;

import factories.IAutoMake;
import factories.MainFactory;

//base abstract class for button limiters
public abstract class BaseButtonLimiter implements IAutoMake, IButtonLimiter{
	protected MainFactory mainFactory;
	protected boolean notifyOnFailure;
	protected boolean instantPopUp;
	
	/*
	 * constructor
	 * Inputs:
	 * 	notifyOnFalure: boolean value, if set to true the button limiter will generate a notice in case limiter activates
	 */
	public BaseButtonLimiter(boolean notifyOnFailure, boolean instantPopUp) {
		this.notifyOnFailure = notifyOnFailure;
		this.instantPopUp = instantPopUp;
	}

	//Checks if the limiter allows action
	@Override
	abstract public boolean check();

	//Main factory setter
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
	}
	
	//Returns string version of why the limiter activated
	public abstract String reasonForFail();

	//bootUp method
	@Override
	public abstract void bootUp();
	
	//method that fullfills the cost listed in this limiter (meaning limited amount of specific value will be removed)
	public abstract void doCost();
	
	
	//annoucnes if the specific limiter has been listed to be notified to player when it fails
	public boolean needsNotify() {
		return this.notifyOnFailure;
	}
	
	public boolean instantPopUp() {
		return this.instantPopUp;
	}

}

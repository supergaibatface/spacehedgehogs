package screenElements;

import java.util.ArrayList;
import java.util.List;

import combatFieldElements.FieldCharacter;
import frameworks.IStackable;

//Class that extends screen element and represents some character being active on screen
public class ScreenSelection extends ScreenElement{
	private ScreenElement whoSelected;

	//constructor, based on the screenElement type
	public ScreenSelection(int x, int y) {
		super(x, y);
	}
	
	//Boot up method, meant for starting up the object after creation
	public void bootUp() {
		super.bootUp();
		this.setSize(this.lenAss.getCharAvatarLength(), this.lenAss.getCharAvatarLength());
	}
	
	/*
	 * Method for selecting a character by making it active
	 * Inputs:
	 * 	character: FieldCharacter that will be made active
	 * Output:
	 * 	true: selecting the character was a success
	 * 	false: selecting failed
	 */
	public boolean setActive(FieldCharacter character) {
		this.setCoordinates(character.getX(), character.getY());
		this.setSize(character.getLength(), character.getHeight());
		this.whoSelected = character;
		return true;
	}
	
	/*
	 * Method for selecting a ScreenElement by making it active
	 * Inputs:
	 * 	newElement: ScreenElement that will be made active
	 * Output:
	 * 	true: selecting the element was a success
	 * 	false: selecting failed
	 */
	public boolean setActive(ScreenElement newElement) {
		this.setCoordinates(newElement.getX(), newElement.getY());
		this.setSize(newElement.getLength(), newElement.getHeight());
		this.whoSelected = newElement;
		return true;
	
	}
	
	//Method for going back to a state with no character being selected
	public void setInactive() {
		this.setCoordinates(0, 0);
		this.whoSelected = null;
	}
	
	//getter for the activity state
	public boolean getActivityState() {
		return this.whoSelected != null;
	}
	
	//getter for selected character, use only if the activity state is true
	public ScreenElement getSelectedChar() {
		return  this.whoSelected;

	}

	@Override
	public void fillTopElementList(List<IStackable> list) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void fillTopTextList(List<ScreenText> list) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOffScreenScript() {
		// TODO Auto-generated method stub
		
	}

}

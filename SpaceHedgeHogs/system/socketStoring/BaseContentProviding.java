package socketStoring;

import java.util.ArrayList;

import factories.IAutoMake;
import factories.MainFactory;
import screenElements.ScreenElement;

//Base class for objects that provide content for sockets on view switches
public abstract class BaseContentProviding<ElementType extends ScreenElement> implements ISocketContentProviding<ElementType>, IAutoMake {
	protected MainFactory mainFactory;

	@Override
	public abstract ArrayList<ElementType> getArrayOfSupplies();

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

package party;

import factories.IAutoMake;
import factories.MainFactory;

//Class that represents unified party stats
public class Party implements IAutoMake {
	private MainFactory mainFactory;
	private int caravanSpeed;
	private PartyStance partyStance;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.caravanSpeed = 10;
		
	}
	
	//Method for getting the standard caravan speed 
	public int getCaravanSpeed() {
		return this.caravanSpeed;
	}
	
	//setter for current party stance
	public void setPartyStance(PartyStance partyStance) {
		this.partyStance = partyStance;
		//NB THIS IS A HACK PART
		this.mainFactory.getWaterSupplyController().updateData();
		//END OF HACK
	}
	
	//getter for the current party stance
	public PartyStance getPartyStance() {
		return this.partyStance;
	}

}

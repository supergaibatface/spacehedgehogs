package view;

import java.util.ArrayList;
import java.util.List;

import factories.IAutoMake;
import factories.MainFactory;
import generalGraphics.ButtonLookType;
import graphicalWrapping.DataBoxWrapper;
import jobChoiceController.JobChoiceController;
import screenElements.ScreenBackground;
import screenElements.ScreenButton;
import screenElements.ScreenDataBox;
import screenElements.ScreenScrollPanel;
import settlement.ISettlement;
import socketStoring.INewSocketBuilder;
import sreenElementsSockets.BaseScrollSocket;
import tabInterface.TabDirectory;
import tabInterface.TabWindow;

public abstract class BaseGameView implements IAutoMake, IGameView {
	protected MainFactory mainFactory;
	protected ViewName name;
	protected ScreenBackground background;
	protected List<ScreenButton> buttons;
	protected List<ScreenDataBox> dataBoxes;
	protected List<DataBoxWrapper> wrappedDataBoxes;
	protected boolean gameTime;
	protected int gameTimeX;
	protected int gameTimeY;
	protected ScreenScrollPanel mainScreen;
	protected List<INewSocketBuilder> newSocketBuildersForTabs;
	protected TabDirectory tabDirectory;
	
	public BaseGameView(ViewName name) {
		this.name = name;
	}

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.buttons = new ArrayList<ScreenButton>();
		this.gameTime = false;
		this.wrappedDataBoxes = new ArrayList<DataBoxWrapper>();
		this.newSocketBuildersForTabs = new ArrayList<INewSocketBuilder>();
		this.background = this.bootGiveBackground();
		this.bootElements();
	}
	
	protected ScreenBackground bootGiveBackground() {
		return this.mainFactory.getElemFactory().getBackground(this.bootGiveBackgroundImageName());
	}
	
	protected abstract String bootGiveBackgroundImageName();
	
	protected abstract void bootElements();
	
	
	public boolean hasBackground() {
		return this.background != null;
	}
	
	public ScreenBackground getBackground() {
		return this.background;
	}
	
	public List<ScreenButton> getButtons() {
		return this.buttons;
	}
	
	public List<ScreenDataBox> getDataBoxes() {
		return this.getDataBoxesAndTime();
	}
	
	/*
	 * Method for getting databoxes with right coordinates from the databox wrappers
	 */
	private List<ScreenDataBox> unwrapDataBoxes() {
		List<ScreenDataBox> unwrappedBoxes = new ArrayList<ScreenDataBox>();
		for(DataBoxWrapper oneWrapper: this.wrappedDataBoxes) {
			unwrappedBoxes.add(oneWrapper.getBoxWithCoordinates());
		}
		return unwrappedBoxes;
	}
	
	protected List<ScreenDataBox> getDataBoxesAndTime() {
		List<ScreenDataBox> boxes = this.unwrapDataBoxes();
		if(this.hasTime()) {
			boxes.add(getGameTime());
		}
		return boxes;
	}
	
	//checks if the view has game time set, requires setting coordinates for game time databox
	public boolean hasTime() {
		return this.gameTime;
	}
	
	//Getter for the current game time
	protected ScreenDataBox getGameTime() {
		ScreenDataBox timeBox = this.mainFactory.getTurnController().getDataBox();
		timeBox.setCoordinates(gameTimeX, gameTimeY);
		return timeBox;
	}
	
	public boolean hasScrollableMainScreen() {
		return this.mainScreen != null;
	}
	
	public ScreenScrollPanel getScrollableMainScreen() {
		return this.mainScreen;
	}
	
	public TabDirectory getMainTabDirectory() {
		if(this.makeTabDirectory()) {
			this.fillTabDirectory();
			return this.tabDirectory;
		}
		return null;
	}
	
	//Makes a tab directory for this view
	protected boolean makeTabDirectory() {
		if(!this.newSocketBuildersForTabs.isEmpty()) {
			this.tabDirectory = this.mainFactory.getElemFactory().getTabDirectory(1500, 185, "100Lime");
			return true;
		}
		return false;
	}
	
	//Fills tabdirectory with tabs about saved data on the view
	protected void fillTabDirectory() {
		TabWindow newTab;
		for(INewSocketBuilder oneSocketBuilder : this.newSocketBuildersForTabs) {
			BaseScrollSocket newSocket = oneSocketBuilder.getFullSocket();
			newTab = this.tabDirectory.makeNewTabHere("50purple");
			addSocketToTab(newSocket, newTab);
		}
	}
	
	protected ScreenButton getAGoToDebugViewButton(int x, int y, String buttonText) {
		ScreenButton goDebug = this.mainFactory.getElemFactory().getScreenButton(x, y, ButtonLookType.NEXT, buttonText);
		goDebug.addButtonEventListener(this.mainFactory.getElemFactory().getViewChangeListener(ViewName.Debug));
		return goDebug;
	}
	
	/*
	 * Adds a socket to a tab
	 * Inputs:
	 * 	newSocket: socket that will be added to the tab
	 * 	targetTab: tab that the socket will put on
	 */
	protected void addSocketToTab(BaseScrollSocket newSocket, TabWindow targetTab) {
		newSocket.setCoordinates(0, 0);
		targetTab.addElement(newSocket);
		targetTab.addElement(newSocket.getAScrollButtonForThisPanel(false, true));
		targetTab.addElement(newSocket.getAScrollButtonForThisPanel(false, false));
		newSocket.doingScrollCalc();
	}
	
	protected void addLogicalLocation(ISettlement settlement) {
		for(ScreenButton oneLocationButton : settlement.getLocationButtons()) {
			this.addButtonTemp(oneLocationButton);
		}
	}
	
	protected void addButtonTemp(ScreenButton button) {
		this.buttons.add(button);
	}
	
	@Override
	public void addDataBox(ScreenDataBox newDataBox, int xCoord, int yCoord) {
		this.wrappedDataBoxes.add(this.mainFactory.getElemFactory().getWrapperForDataBox(xCoord, yCoord, newDataBox));
	}
	
	@Override
	public void setGameTimeCoord(int x, int y) {
		this.gameTime = true;
		this.gameTimeX = x;
		this.gameTimeY = y;
	}
	
	//getter for view name
	public ViewName getName() {
		return this.name;
	}
	
	//checks if the view name equals this views name
	public boolean equals(ViewName name) {
		return this.name.equals(name);
	}
	
	public void goingOnScreenAlert() {
		this.goingOnScreenScript();
	}
	
	protected abstract void goingOnScreenScript();

}

package fieldCharCommand;

//Enum that represents different states of the fieldCharCommandControl
public enum FieldCharCommandStateName {
	INACTIVE, MOVE, ATTACK, ITEM

}

package commandAction;

import combatFieldElements.MovementSquare;
import screenElements.ScreenElement;
import sreenElementsSockets.ScreenSimpleChar;
import sreenElementsSockets.ScrollCharSocket;

//Command action for selecting a new socket for an active char
public class CASocketSelecting extends CABaseClass<ScrollCharSocket> {

	@Override
	public boolean inputIsCorrect() {
		int nr = this.hasItemTypeClicked();
		if(nr<0) {
			return false;
		}
		if(!detailedItemCheck(this.castToClickTargetClass(conditions.getClickedItem(nr)))) {
			return false;
		}
		return true;
	}
	
	//method for extra checks needed for the clicked item
	private boolean detailedItemCheck(ScrollCharSocket socket) {
		if(!socket.hasRoom()) {
			return false;
		}
		if(socket.hasJob()) {
			if(!socket.getJob().fullReqCheck(selection.getSelectedCharAsSimpleChar().getMaster())){
				return false;
			}
		}
		return true;
	}

	@Override
	public void actionImplement() {
		ScreenSimpleChar selectedChar = selection.getSelectedCharAsSimpleChar();
		ScrollCharSocket selectedSocket = this.castToClickTargetClass(conditions.getClickedItem(this.hasItemTypeClicked()));
		this.selection.takeCharAway();
		selectedSocket.addNewElement(selectedChar);
	}

	@Override
	public Class<ScrollCharSocket> setClickTargetClass() {
		return ScrollCharSocket.class;
	}

	@Override
	protected int giveMouseButtonNr() {
		return this.giveLeftMouseButtonValue();
	}
	
}

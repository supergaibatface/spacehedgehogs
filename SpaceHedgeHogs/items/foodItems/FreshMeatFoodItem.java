package foodItems;

import foodBase.FoodItemBase;
import itemBase.TradeItemName;

public class FreshMeatFoodItem extends FoodItemBase {

	public FreshMeatFoodItem() {
		super(TradeItemName.FreshMeat);
	}

	@Override
	protected int bootGiveBaseExpiration() {
		return 4;
	}

	@Override
	protected FoodItemBase giveBaseClass() {
		return this.mainFactory.getItemFactory().getFreshMeatItem();
	}
	
	@Override
	protected String bootGiveItemImageName() {
		return this.bootGivePlaceholderImage();
	}

	@Override
	protected int bootGiveOneUnitWeight() {
		return 1;
	}

}

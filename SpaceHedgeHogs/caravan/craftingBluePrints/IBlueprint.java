package craftingBluePrints;

import java.util.List;

import itemBase.IGameItem;
import itemExceptions.CantStartCraftingProjectException;

public interface IBlueprint {
	
	public String getName();
	
	public int getNeccesaryWorkValue();
	
	public IGameItem getFinishedProjetValue();
	
	public List<IGameItem> getComponents();
	
	public boolean canMakeProject();
	
	public CraftingProject makeGetNewCraftingProject() throws CantStartCraftingProjectException;

}

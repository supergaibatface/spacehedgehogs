package skillListeners;

//interface for all skill change listeners
public interface ISkillChangeListener {
	
	//method that is triggered when skill change happens
	public void skillChangeEvent();

}

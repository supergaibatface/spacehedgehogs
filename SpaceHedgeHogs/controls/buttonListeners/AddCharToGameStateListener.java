package buttonListeners;

import java.util.ArrayList;

import dataValues.CharValue;
import gameState.IGameState;

//Listener that adds a list of extra chars to a game mode
public class AddCharToGameStateListener extends BaseButtonListener {
	private IGameState gameState;
	private ArrayList<CharValue> charValues;
	
	/*
	 * constructor
	 * Inputs:
	 * 	gameState: game state that the new characters will be added to
	 * 	charValues: charValues that will be added to the gameState
	 */
	public AddCharToGameStateListener(IGameState gameState, ArrayList<CharValue> charValues) {
		this.gameState = gameState;
		this.charValues = charValues;
	}
	
	//constructor
	public AddCharToGameStateListener(IGameState gameState, CharValue oneCharValues) {
		this.gameState = gameState;
		this.charValues = new ArrayList<CharValue>();
		this.charValues.add(oneCharValues);
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.gameState.addExtraCharValues(charValues);
		
	}

	@Override
	public String toInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

package propertyBars;

import java.util.ArrayList;
import java.util.List;

import factories.IAutoMake;
import factories.MainFactory;
import propertyBarListeners.IBarChangeListener;
import screenElements.ScreenBar;
import screenManagement.GraphicalLengthAssigner;

//Class that represents a bar value, that has a max and min value and a current value between those.
public class PropertyBar implements IAutoMake{
	private MainFactory mainFactory;
	private PropertyBarName name;
	private int maxValue;
	private int currentValue;
	private int minValue;
	private List<IBarChangeListener> listeners;
	
	//Constructor
	public PropertyBar(PropertyBarName name) {
		this.name = name;
	}

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.maxValue = 100;
		this.minValue = 0;
		this.currentValue = 0;
		this.listeners = new ArrayList<IBarChangeListener>();
		
	}
	
	/*
	 * Activates all listeners attached to this property bar
	 * Inputs:
	 * 	newValue: property bars new value
	 */
	private void notifyListeners(int newValue) {
		for(IBarChangeListener oneListener : this.listeners) {
			oneListener.barChangeHappened(newValue);
		}
		
		
	}
	
	//For adding new listeners to this property bar
	public void addListener(IBarChangeListener newListener) {
		this.listeners.add(newListener);
	}
	
	//for removing listeners from this property bar
	public void removeListener(IBarChangeListener oldListener) {
		this.listeners.remove(oldListener);
	}
	
	//Setter for property bars max value
	public void setMaxValue(int maxValue) {
		this.maxValue = maxValue;
	}
	
	//Setter for property bars min value
	public void setMinValue(int minValue) {
		this.minValue = minValue;
	}
	
	//Method for filling up the property bar to its max value
	public void fillUp() {
		setCurrentValue(this.maxValue);
	}
	
	//Setter for current property bar value
	public void setCurrentValue(int value) {
		if(value > this.maxValue) {
			this.currentValue = this.maxValue;
		}
		else if ( value < this.minValue) {
			this.currentValue = this.minValue;
		}
		else {
			this.currentValue = value;
		}
		triggerChangeUpdates();
	}
	
	//Decreases the bars current value by designated value, wont go below min value
	public void decreaseByValue(int value) {
		if(this.currentValue - value < this.minValue) {
			this.currentValue = this.minValue;
		}
		else {
			this.currentValue = this.currentValue - value;
		}
		triggerChangeUpdates();
	}
	
	//Increases the bars current value by designated value, wont go above max value
	public void increaseByValue(int value) {
		if(this.currentValue + value > this.maxValue) {
			this.currentValue = this.maxValue;
		}
		else {
			this.currentValue = this.currentValue + value;
		}
		triggerChangeUpdates();
	}
	
	//Getter for the current value
	public int getCurrentValue() {
		return this.currentValue;
	}
	
	//Getter for bars max value
	public int getMaxValue() {
		return this.maxValue;
	}
	
	//Getter for the bars min value
	public int getMinValue() {
		return this.minValue;
	}
	
	//Check method if property bar is empty
	public boolean isEmpty() {
		if(this.currentValue > this.minValue) {
			return false;
		}
		return true;
	}
	
	//Check method if property bar is full
	public boolean isFull() {
		if(this.currentValue < this.maxValue) {
			return false;
		}
		return true;
	}
	
	//Getter for the bars name
	public PropertyBarName getName() {
		return this.name;
	}
	
	//toString method
	public String toString( ) {
		return this.name.toString();
	}
	
	//Compare method with property bar name value
	public boolean equals(PropertyBarName barName) {
		return this.name.equals(barName);
	}
	
	public ScreenBar generateVisualBar(int length, int height) {
		return this.mainFactory.getElemFactory().getScreenBar(length, height, this, this.name.getBarVisual(), "LightBlue");
	}
	
	//Umbrella command called once a change in the bar value happens
	private void triggerChangeUpdates() {
		this.notifyListeners(this.currentValue);
	}

}

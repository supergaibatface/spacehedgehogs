package factories;

import characterClassMatching.CharacterClassFinder;
import dataValues.CharValue;
import dialogueCharacters.IDialogueCharacter;
import gameCharacters.IGameCharacter;
import mainPartyCharacters.CharacterBob;
import mainPartyCharacters.CharacterBobDialogue;
import mainPartyCharacters.CharacterJack;
import mainPartyCharacters.CharacterJackDialogue;

public class CharacterFactory extends BaseMinorFactory {
	private CharacterClassFinder characterClassFinder;
	private IGameCharacter characterBob;
	private IDialogueCharacter bobDialogue;
	private IGameCharacter characterJack;
	private IDialogueCharacter jackDialogue;
	
	private void makeCharacterClassFinder() {
		this.characterClassFinder = setUpNewElement(new CharacterClassFinder());
	}
	
	private CharacterClassFinder getCharacterClassFinder() {
		if(this.characterClassFinder == null) {
			this.makeCharacterClassFinder();
		}
		return this.characterClassFinder;
	}
	
	private void makeCharacterBob() {
		this.characterBob = this.setUpNewElement(new CharacterBob());
	}
	
	public IGameCharacter getCharacterBob() {
		if(this.characterBob == null) {
			this.makeCharacterBob();
		}
		return this.characterBob;
	}
	
	private void makeDialogueBob() {
		this.bobDialogue = this.setUpNewElement(new CharacterBobDialogue());
	}
	
	public IDialogueCharacter getDialogueBob() {
		if(this.bobDialogue == null) {
			this.makeDialogueBob();
		}
		return this.bobDialogue;
	}
	
	public IGameCharacter getMatchClassFor(CharValue oneValue) {
		return this.getCharacterClassFinder().getClassForName(oneValue);
	}
	
	private void makeCharacterJack() {
		this.characterJack = this.setUpNewElement(new CharacterJack());
	}
	
	public IGameCharacter getCharacterJack() {
		if(this.characterJack == null) {
			this.makeCharacterJack();
		}
		return this.characterJack;
	}
	
	private void makeDialogueJack() {
		this.jackDialogue = this.setUpNewElement(new CharacterJackDialogue());
	}
	
	public IDialogueCharacter getDialogueJack() {
		if(this.jackDialogue == null) {
			this.makeDialogueJack();
		}
		return this.jackDialogue;
	}

}

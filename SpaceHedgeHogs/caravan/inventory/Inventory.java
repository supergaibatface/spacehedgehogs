package inventory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.event.EventListenerList;

import factories.IAutoMake;
import factories.MainFactory;
import inventoryChange.IInventoryListener;
import inventoryChange.InventoryNewItemEvent;
import inventoryException.ItemNotFoundException;
import itemBase.IGameItem;
import itemBase.ItemTagName;
import itemBase.TradeItemName;
import other.StaticFunctions;

//represents the collection of items that the player has at a time
public class Inventory implements IAutoMake{
	protected EventListenerList listenerList = new EventListenerList();
	private MainFactory mainFactory;
	private ArrayList<IGameItem> items;
	private boolean debug = false;

	//Setter for main factory
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
	}

	//Boot up method for a new object
	@Override
	public void bootUp() {
		this.items = new ArrayList<IGameItem>();
		this.mainFactory.getWorldTime().addTimeListener(this.mainFactory.getElemFactory().getInventoryTurnChangeListener());
	}
	
	/*
	 * Method for adding INventoryChangeListeners on inventory, must be a listener that implements IInventoryListener interface
	 * Input: 
	 * 	listener: inventoryListener
	 */
	public void addInventoryChangeEventListener(IInventoryListener listener) {
		listenerList.add(IInventoryListener.class, listener);
	}
	
	/*
	 * Method for removing ItemChangeListeners
	 */
	public void removeInventoryChangeEventListener(IInventoryListener listener) {
		listenerList.remove(IInventoryListener.class, listener);
	}
	
	/*
	 * triggers new item event
	 */
	private void makeNewItemEvent(IGameItem newItem) {
		this.fireNewItemEvent(new InventoryNewItemEvent(this, newItem));
	}
	
	//Method for sending a InventoryNewItemEvent to all the listeners, input evt: InventoryNewItemEvent style event
	private void fireNewItemEvent(InventoryNewItemEvent evt) {
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i + 2) {
			if (listeners[i] == IInventoryListener.class) {
				((IInventoryListener) listeners[i + 1]).inventoryNewItemEventOccured(evt);
			}
		}
	}
	
	/*
	 * Method that makes items out of an arrayList of itemValues
	 */
	/*public void makeItemsWithItemValues(ArrayList<ItemValue> values) {
		for(ItemValue oneValue : values) {
			//this.dealWithOneItemValues(oneValue);
			this.addItemToInventory(oneValue);
		}
	}*/
	
	public void clearInventory() {
		this.items.clear();
	}
	
	/*private IGameItem transformUnknownItemValueToItem(ItemValue oneValue) {
		return makeNewStandardItem(oneValue.getName(), oneValue.getAmount());
	}*/

	
	/*private IGameItem makeFoodItemFromItemValue(ItemValue sourceValue) {
		FoodValue asFoodValue = (FoodValue) sourceValue;
		return makeFoodItem(asFoodValue.getName(), asFoodValue.getAmount(), asFoodValue.getExpiration());
	}*/
	
	/*private IGameItem makeFoodItem(TradeItemName name, int amount, int expirationDate) {
		FoodItem newItem = this.mainFactory.getElemFactory().getFoodItem(name);
		newItem.addAmount(amount, expirationDate);
		return newItem;
	}*/

	
	/*private IGameItem makeNewStandardItem(TradeItemName name, int amount) {
		TradeItem newItem = this.mainFactory.getElemFactory().getTradeItem(name);
		newItem.addAmount(amount);
		return newItem;
	}*/
	
	//Method for announcing all items of a turn change
	public void announceTurnChangeToItems() {
		//this list get changed when something gets fully eaten
		for(IGameItem oneItem : new ArrayList<IGameItem>(this.items)) {
			oneItem.turnChanged();
		}
	}
	
	//getter for the current inventory, only gives items that the player has atleast one of
	public ArrayList<IGameItem> getCurrentInventory() {
		ArrayList<IGameItem> currentSupply = new ArrayList<IGameItem>();
		for(IGameItem oneItem: this.items) {
			if(oneItem.hasPossesion()) {
				currentSupply.add(oneItem);
			}
		}
		return currentSupply;
	}
	
	//Method for finding an item from the invetory by the items name
	public IGameItem findItem(TradeItemName name) {
		for(IGameItem oneItem: this.items) {
			if(oneItem.equals(name)) {
				return oneItem;
			}
		}
		return null;
	}
	
	public boolean hasItem(IGameItem checkItem) {
		return tryToFindItemStack(checkItem) != null;
		
	}
	
	private IGameItem tryToFindItemStack(IGameItem checkItem) {
		try {
			return findInventoryStackForItem(checkItem);
		}
		catch(Exception exception) {
			this.mainFactory.getExceptionManager().dealWithException(exception);
		}
		return null;
	}
	
	private IGameItem findInventoryStackForItem(IGameItem searchItemType) throws ItemNotFoundException {
		for(IGameItem oneItem : this.items) {
			if(oneItem.getClass() == searchItemType.getClass()) {
				return oneItem;
			}
		}
		throw new ItemNotFoundException(searchItemType);
	}
	
	public boolean hasItemWithEnoughSize(IGameItem checkItem) {
		IGameItem foundItem = this.tryToFindItemStack(checkItem);
		if(foundItem == null) {
			return false;
		}
		return foundItem.getSupply() >= checkItem.getSupply();
	}
	
	//Filter function that finds item with the specific tag
	public ArrayList<IGameItem> filterForTag(ItemTagName tagName) {
		ArrayList<IGameItem> filteredArray = new ArrayList<IGameItem>();
		for(IGameItem oneItem : this.items) {
			if(oneItem.hasTag(tagName)) {
				filteredArray.add(oneItem);
			}
		}
		return filteredArray;
	}
	
	/*
	 * Adds the specific amount of item to the inventory, if this is the first instance of this item
	 * will make a new entry in the inventory, if some the item already exists in inventory, increases the amount
	 * Inputs:
	 * 	name: item name
	 * 	amount: int value for the amount
	 */
	/*private void removeItemAmount(TradeItemName name, int amount) {
		IGameItem item = findItem(name);
		if(item == null) {
			System.out.println("Tried to remove item that doesn't exist");
		}
		else {
			item.addAmount(-amount);
			this.inventoryWeightChange(item);
		}
	}*/
	
	public void addMultipleItemsToInventory(List<IGameItem> addedItemList) {
		for(IGameItem oneItem : addedItemList) {
			this.addItemToInventory(oneItem);
		}
	}
	
	
	public void addItemToInventory(IGameItem addedItem) {
		this.newItemMasterAdded(addedItem);
	}
	
	/*public void addItemToInventory(ItemValue addedItem) {
		this.addItemToInventory(transformUnknownItemValueToItem(addedItem));
	}*/

	private void newItemMasterAdded(IGameItem newItem) {
		IGameItem searchResult = this.findItem(newItem.getName());
		if(searchResult == null) {
			this.addNewItemToList(newItem);
		}
		else {
			this.increaseItemAmountInInventory(newItem, searchResult);
		}
	}
	
	private void addNewItemToList(IGameItem addedItem) {
		IGameItem newItem = addedItem.makeCopy();
		this.items.add(newItem);
		this.makeNewItemEvent(newItem);
		this.inventoryWeightChange(newItem);
	}
	
	private void increaseItemAmountInInventory(IGameItem addedItem, IGameItem inventoryVersion) {
		inventoryVersion.merge(addedItem);
		this.inventoryWeightChange(inventoryVersion);
	}
	
	/*
	 * Removes the specific amount of item from the inventory, if this is the first instance of this item, will make a new entry
	 * Use outside methods to make sure that nothing goes into negative
	 * Inputs:
	 * 	name: trade item name
	 * 	amounts: amount of trade item to be removed in int, use positive values
	 */
	/*public void removeTradeItemAmount(TradeItemName name, int amount) {
		this.removeItemAmount(name, amount);
	}*/
	
	public boolean tryToRemoveItemWithAmount(IGameItem removableItem) {
		return this.tryToRemoveItemWithAmount(Collections.singletonList(removableItem));
	}
	
	public boolean tryToRemoveItemWithAmount(List<IGameItem> itemsToRemove) {
		return this.tryToRemoveItemWithAmountScript(itemsToRemove);
	}
	
	private boolean tryToRemoveItemWithAmountScript(List<IGameItem> itemsToRemove) {
		if(!checkIfCanRemoveListOfItemWithAmount(itemsToRemove)) {
			return false;
		}
		this.removeListOfItemsWithAmount(itemsToRemove);
		return true;
	}
	
	private boolean checkIfCanRemoveListOfItemWithAmount(List<IGameItem> itemsToCheck) {
		for(IGameItem oneItem : itemsToCheck) {
			if(!this.hasItemWithEnoughSize(oneItem)) {
				return false;
			}
		}
		return true;
	}
	
	private void removeListOfItemsWithAmount(List<IGameItem> itemsToRemove) {
		for(IGameItem oneItem : itemsToRemove) {
			this.removeItemWithAmount(oneItem);
		}
	}
	
	private void removeItemWithAmount(IGameItem removableItem) {
		IGameItem foundItem = this.tryToFindItemStack(removableItem);
		if(foundItem != null) {
			foundItem.removeAmount(removableItem.getSupply());
		}
	}
	
	
	
	//remove trade item amount version with trade good instance
	/*public void removeTradeItemAmount(IGameItem removeItem) {
		this.removeTradeItemAmount(removeItem.getName(), removeItem.getSupply());
	}*/
	
	//removes all items of certain type from inventory
	public void removeItemFromListing(IGameItem item) {
		if(this.items.remove(item)) {
			StaticFunctions.writeOutDebug(debug, "Removed " + item.getName()+ " from inventory listing while it had " + item.getSupply( ) + " amount");
		}
	}
	
	//getter for the full weight of the inventory
	public int getInventoryFullWeight() {
		int sum = 0;
		for(IGameItem oneItem : this.items) {
			sum = sum + oneItem.getFullWeight();
		}
		return sum;
	}
	
	//Method that is called when the total weight of the inventory changes, input is the item that is the reason for the latest change
	public void inventoryWeightChange(IGameItem changedItem) {
		this.mainFactory.getInventoryWeightController().visualUpdate();
	}

}

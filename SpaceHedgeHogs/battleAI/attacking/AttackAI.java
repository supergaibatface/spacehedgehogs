package attacking;

import calculations.FightCalc;
import combatFieldElements.FieldCharacter;
import combatFieldElements.MovementSquare;
import factories.IAutoMake;
import factories.MainFactory;
import propertyBars.PropertyBarName;

//Ai module that deals with performing specific attacks (dmg dealing)
public class AttackAI implements IAutoMake {
	private MainFactory mainFactory;
	private FightCalc fightCalculator;
	
	/*
	 * Main command that sets of the targeting logic in this module
	 * Inputs:
	 * 	controlChar: character the AI is controlling
	 * 	enemyChar: the deisgnated target for controlled char
	 */
	public void AttackMode(FieldCharacter controlChar, FieldCharacter enemyChar) {
		patternSwitch(controlChar, enemyChar);
	}
	
	/*
	 * Switch that will decide on attack logic depending on AI pattern
	 */
	private void patternSwitch(FieldCharacter controlChar, FieldCharacter enemyChar) {
		if(!controlChar.hasAIPatterns()) {
			return;
		}
		switch(controlChar.getAIPattern().getAttackPatern()) {
		case Standard:
			standardAttackMode(controlChar, enemyChar);
			break;
		case None:
			noneAttackMode();
			break;
		}
	}
	
	//attack logic for a "none" setting
	private void noneAttackMode() {
		return;
	}
	
	//Attack logic for a standard setting
	private void standardAttackMode(FieldCharacter controlChar, FieldCharacter enemyChar) {
		if(howManyAttacksLeft(controlChar) < 1) {
			return;
		}
		if(getDistance(controlChar, enemyChar) < 2) {
			performAttack(controlChar, enemyChar);
		}
	}
	
	//Finds out the distance between target and char being controlled
	private int getDistance(FieldCharacter controlChar, FieldCharacter enemyChar) {
		MovementSquare attackerLoc = controlChar.getLocation();
		MovementSquare defenderLoc = enemyChar.getLocation();
		return attackerLoc.getDistanceFrom(defenderLoc);
	}
	
	//finds out how many attacks the character can still do
	private int howManyAttacksLeft(FieldCharacter character) {
		return character.getMasterCharacter().getPropertyBar(PropertyBarName.Attacks).getCurrentValue();
	}
	
	//performs an actual attack move
	private void performAttack(FieldCharacter attacker, FieldCharacter defender) {
		this.fightCalculator.throwIntoBattle(attacker, defender, getDistance(attacker, defender));
	}

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.fightCalculator = this.mainFactory.getFightCalculator();
		
	}

}

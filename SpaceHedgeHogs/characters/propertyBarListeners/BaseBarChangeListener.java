package propertyBarListeners;

import factories.IAutoMake;
import factories.MainFactory;

//Base class for all property bar change listeners
public abstract class BaseBarChangeListener implements IBarChangeListener, IAutoMake {
	protected MainFactory mainFactory;

	@Override
	public abstract void barChangeHappened(int newValue);

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		
	}

}

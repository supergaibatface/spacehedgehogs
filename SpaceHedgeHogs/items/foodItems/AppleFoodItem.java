package foodItems;

import foodBase.FoodItemBase;
import itemBase.TradeItemName;

public class AppleFoodItem extends FoodItemBase {

	public AppleFoodItem() {
		super(TradeItemName.Apple);
	}

	@Override
	protected int bootGiveBaseExpiration() {
		return 20;
	}

	@Override
	protected FoodItemBase giveBaseClass() {
		return this.mainFactory.getItemFactory().getAppleFoodItem();
	}

	@Override
	protected String bootGiveItemImageName() {
		return this.bootGivePlaceholderImage();
	}

	@Override
	protected int bootGiveOneUnitWeight() {
		return 2;
	}

}

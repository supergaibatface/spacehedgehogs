package battleMaps;

import java.util.ArrayList;

import battle.SpawnArea;
import combatFieldElements.ControlFaction;
import combatFieldElements.MovementField;
import combatFieldElements.MovementLine;
import factories.IAutoMake;
import factories.MainFactory;

//Manager class for battle maps, makes and returns all maps
public class BattleMapManager implements IAutoMake {
	private MainFactory mainFactory;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	//Makes and returns tutorial map 1
	public MovementField getTutorialMovementField1() {
		MovementField battleField = this.mainFactory.getElemFactory().getMovementField();
		battleField.setUpAsSquare(1, 4);
		battleField.setLocation(500, 100);
		makeTutorial1SpawnAreas(battleField);
		return battleField;
	}
	
	//Makes and returns tutorial map 2
	public MovementField getTutorialMovementField2() {
		MovementField battleField = this.mainFactory.getElemFactory().getMovementField();
		battleField.addSquareWithCoords(0, 0);
		//battleField.addSquareWithCoords(3, 0);
		battleField.addHorizontalLineOfSquares(0, 2, 1);
		battleField.addHorizontalLineOfSquares(0, 2, 2);
		battleField.addHorizontalLineOfSquares(0, 2, 3);
		battleField.addHorizontalLineOfSquares(1, 2, 4);
		battleField.setLocation(500, 100);
		makeTutorial2SpawnAreas(battleField);
		return battleField;
	}
	
	//makes and returns tutorial map 3
	public MovementField getTutorialMovementField3() {
		MovementField battleField = this.mainFactory.getElemFactory().getMovementField();
		battleField.addHorizontalLineOfSquares(0, 8, 0);
		battleField.addHorizontalLineOfSquares(0, 8, 1);
		battleField.addHorizontalLineOfSquares(0, 8, 2);
		battleField.addHorizontalLineOfSquares(0, 8, 3);
		battleField.setLocation(200, 100);
		makeTutorial3SpawnAreas(battleField);
		return battleField;
	}
	
	//makes and returns a debug map
	public MovementField getDebugMovementField() {
		MovementField battleField = this.mainFactory.getElemFactory().getMovementField();
		battleField.setUpAsSquare(11, 7);
		makeTestSpawnAreas(battleField);
		return battleField;
	}
	
	//makes spawn areas for tutorial map 1
	private void makeTutorial1SpawnAreas(MovementField newField) {
		SpawnArea playerArea = this.mainFactory.getElemFactory().getSpawnArea(ControlFaction.PLAYER);
		playerArea.addNewSquare(newField.getSquareWithCoords(0, 3));
		newField.addSpawnArea(playerArea);
		
		SpawnArea compArea = this.mainFactory.getElemFactory().getSpawnArea(ControlFaction.COMPUTER);
		compArea.addNewSquare(newField.getSquareWithCoords(0, 0));
		newField.addSpawnArea(compArea);
		
	}
	
	//makes spawn areas for tutorial map 2
	private void makeTutorial2SpawnAreas(MovementField newField) {
		SpawnArea playerArea = this.mainFactory.getElemFactory().getSpawnArea(ControlFaction.PLAYER);
		playerArea.addNewSquare(newField.getSquareWithCoords(1, 4));
		playerArea.addNewSquare(newField.getSquareWithCoords(2, 4));
		newField.addSpawnArea(playerArea);
		
		SpawnArea compArea = this.mainFactory.getElemFactory().getSpawnArea(ControlFaction.COMPUTER);
		compArea.addNewSquare(newField.getSquareWithCoords(0, 0));
		compArea.addNewSquare(newField.getSquareWithCoords(2, 2));
		newField.addSpawnArea(compArea);
	}
	
	//makes spawn areas for tutorial map 3
	private void makeTutorial3SpawnAreas(MovementField newField) {
		SpawnArea playerArea = this.mainFactory.getElemFactory().getSpawnArea(ControlFaction.PLAYER);
		playerArea.addNewSquare(newField.getSquareWithCoords(0, 1));
		playerArea.addNewSquare(newField.getSquareWithCoords(0, 2));
		newField.addSpawnArea(playerArea);
		
		SpawnArea compArea = this.mainFactory.getElemFactory().getSpawnArea(ControlFaction.COMPUTER);
		compArea.addNewSquare(newField.getSquareWithCoords(8, 1));
		compArea.addNewSquare(newField.getSquareWithCoords(8, 2));
		newField.addSpawnArea(compArea);
	}
	
	/*
	 * Makes spawn areas for player and computer
	 */
	private void makeTestSpawnAreas(MovementField newField) {
		newField.addSpawnArea(makePlayerSpawnArea(newField));
		newField.addSpawnArea(makeComputerSpawnArea(newField));
	}
	
	/*
	 * Makes players spawn area (for test map)
	 */
	private SpawnArea makePlayerSpawnArea(MovementField battleField) {
		SpawnArea newArea = this.mainFactory.getElemFactory().getSpawnArea(ControlFaction.PLAYER);
		MovementLine oneLine = battleField.getLineNr(6);
		newArea.addNewSquare(oneLine.getSquareNr(5));
		newArea.addNewSquare(oneLine.getSquareNr(6));
		oneLine = battleField.getLineNr(5);
		newArea.addNewSquare(oneLine.getSquareNr(5));
		newArea.addNewSquare(oneLine.getSquareNr(6));
		return newArea;
	}
	
	/*
	 * Makes computer spawn area (for test map)
	 */
	private SpawnArea makeComputerSpawnArea(MovementField battleField) {
		SpawnArea newArea = this.mainFactory.getElemFactory().getSpawnArea(ControlFaction.COMPUTER);
		MovementLine oneLine = battleField.getLineNr(0);
		newArea.addNewSquare(oneLine.getSquareNr(3));
		newArea.addNewSquare(oneLine.getSquareNr(4));
		oneLine = battleField.getLineNr(1);
		newArea.addNewSquare(oneLine.getSquareNr(3));
		newArea.addNewSquare(oneLine.getSquareNr(4));
		return newArea;
	}

}

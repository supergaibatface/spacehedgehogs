package commandActionRightButton;

import commandAction.CABaseClass;
import sreenElementsSockets.ScreenSimpleItem;

//Right mouse Command action for opening extra command on screenSimpleItem
public class CARScreenItem extends CABaseClass<ScreenSimpleItem> {

	@Override
	public boolean inputIsCorrect() {
		return this.standardClickCheck();
	}

	@Override
	protected int giveMouseButtonNr() {
		return this.giveRightMouseButtonValue();
	}

	@Override
	public void actionImplement() {
		ScreenSimpleItem item = (ScreenSimpleItem) conditions.getClickedItem(this.hasItemTypeClicked());
		//System.out.println("clicked on char: "+character.getMaster().getName());
		this.mainFactory.getRightMouseInfoController().showBoxOnElement(this.mainFactory.getMouseConditionSet().getClickX(), this.mainFactory.getMouseConditionSet().getClickY(), item);
		
	}

	@Override
	public Class<ScreenSimpleItem> setClickTargetClass() {
		return ScreenSimpleItem.class;
	}

}

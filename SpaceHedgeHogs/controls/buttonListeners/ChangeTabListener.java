package buttonListeners;

import tabInterface.TabDirectory;
import tabInterface.TabWindow;

/*
 * Button listener that will trigger a change in visible tab in a tab directory
 */
public class ChangeTabListener extends BaseButtonListener {
	private TabDirectory masterDirectory;
	private TabWindow tab;
	
	/*
	 * Constructor
	 * Inputs
	 * 	masterDirectory: tab directory where the change will occur
	 * 	tab: tab that will be made active
	 */
	public ChangeTabListener(TabDirectory masterDirectory, TabWindow tab) {
		this.masterDirectory = masterDirectory;
		this.tab = tab;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		masterDirectory.setActiveTab(this.tab);
		
	}

	@Override
	public String toInfo() {
		return "Changes tab";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

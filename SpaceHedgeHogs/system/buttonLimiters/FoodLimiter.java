package buttonLimiters;


public class FoodLimiter extends BaseButtonLimiter {

	public FoodLimiter(boolean notifyOnFailure, boolean instantPopUp) {
		super(notifyOnFailure, instantPopUp);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean check() {
		return this.mainFactory.getFoodManager().hasEnoughFoodForTravel();
	}

	@Override
	public String reasonForFail() {
		return "Inventory doesn't have enough food";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doCost() {
		//System.out.println("doing cost for foodLimiter");
		this.mainFactory.getFoodManager().doOneTravelTurnFoodConsumption();
		
	}

}

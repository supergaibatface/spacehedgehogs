package buttonListeners;

import dialogueCharacters.IDialogueCharacter;
import view.CutsceneDataCollection;

public class DialogueFinishListener extends BaseButtonListener {
	private CutsceneDataCollection targetDialogue;
	private IDialogueCharacter targetNPC;
	
	public DialogueFinishListener(CutsceneDataCollection targetDialogue, IDialogueCharacter targetNPC) {
		this.targetDialogue = targetDialogue;
		this.targetNPC = targetNPC;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.targetNPC.finishedDialogue(targetDialogue);
		
	}

	@Override
	public String toInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

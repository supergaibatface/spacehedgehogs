package land;

import factories.IAutoMake;
import factories.MainFactory;
import screenElements.ScreenButton;

//Class that represents an land area that can be or was traveled on
public class LandArea implements IAutoMake {
	private MainFactory mainFactory;
	private double totalDistance;
	private TerrainName terrain;
	private double traveledDistance;
	private int expires;
	private ScreenButton assignedButton;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	terrain: the specific terrain element of this land area
	 * 	totalDistance: how much distance does this land area cover
	 */
	public LandArea(TerrainName terrain, double totalDistance) {
		this.terrain = terrain;
		this.totalDistance = totalDistance;
		
	}
	
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}
	
	@Override
	public void bootUp() {
		this.traveledDistance = 0;
		this.expires = 3;
	}
	
	//Set how much distance has been traveled on this land area
	public void setTraveledDistance(double distance) {
		if(distance < totalDistance) {
			this.traveledDistance = distance;
		}
		else {
			this.traveledDistance = totalDistance;
		}
	}
	
	//Advances the partys traveled disatance on this terrain by a value
	public void advance(double distance) {
		if(distance + this.traveledDistance > totalDistance) {
			this.traveledDistance = totalDistance;
		}
		else {
			this.traveledDistance = this.traveledDistance + distance;
		}
	}
	
	//Getter for the total distance of this land area
	public double getTotalDistance() {
		return this.totalDistance;
	}
	
	//getter for the currently traveled distance on this landArea
	public double getCurrentDistance() {
		return this.traveledDistance;
	}
	
	//Getter for the terrain of this landArea
	public TerrainName getTerrain() {
		return this.terrain;
	}
	
	/*
	 * Method for counting down towards expring a land area,
	 * its used to slowly make not traveled on Options dissapear from selection
	 * Outputs:
	 * 	true: this options should be deleted now
	 * 	false: this options should not yet be deleted
	 */
	public boolean countTowardsExpire() {
		this.expires --;
		if(this.expires < 1) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/*
	 * Returns how many turns of unuse should expire this option
	 */
	public int getTillExpires() {
		return this.expires;
	}
	
	/*
	 * toString method for land area that will give terrain toString
	 */
	public String toString() {
		return this.terrain.toString();
	}
	
	//Getter for how much land can still be covered on this landArea
	public double getRoomToAdvance() {
		return this.totalDistance - this.traveledDistance;
	}
	
	//Getter for the full traveling speed multiplier on this landArea
	public double getFullMultiplier() {
		return this.terrain.getTravelSpeedMultiplier();
	}
	
	/*
	 * Method for assigining a button for selecting this land area
	 */
	public void setButton(ScreenButton oneButton) {
		this.assignedButton = oneButton;
	}
	
	/*
	 * Checks if this land area has a button assigned
	 */
	public boolean hasButton() {
		return this.assignedButton != null;
	}
	
	/*
	 * Getter for assigned button
	 */
	public ScreenButton getButton() {
		return this.assignedButton;
	}

}

package spawning;

import factories.IAutoMake;
import factories.MainFactory;

//base class for spawn time modules
public abstract class BaseSpawnTimeModule implements ISpawnTimeModule, IAutoMake {
	protected MainFactory mainFactory;

	@Override
	public abstract boolean doWeSpawn();

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

}

package controllers;

import java.util.ArrayList;
import java.util.List;

import factories.IAutoMake;
import factories.MainFactory;
import screenElements.ScreenButton;
import screenElements.ScreenDataBox;
import screenElements.ScreenElement;
import sreenElementsSockets.ScreenSimpleChar;

//Controller class for right mouse info clicks
public class RightMouseInfoController implements IAutoMake {
	private MainFactory mainFactory;
	private ScreenDataBox infoSelection;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.infoSelection = this.mainFactory.getElemFactory().getScreenDataBox(0, 0, "Orange");
		this.infoSelection.fillTextOneLine("Right click Info box");
		this.infoSelection.updateSizeForTime();
		this.infoSelection.makeInvisible();
		this.addToGE();
		this.mainFactory.getMouseCommands().addPlayerActionListener(this.mainFactory.getElemFactory().getRightMouseInfoCloser());
		
	}
	
	/*
	 * public method for calling extra commands box for some element with specific coordinates
	 * Inputs:
	 * 	x: x coordinate for displaying location
	 * 	y: y coordinate for displaying location
	 * 	clickedElement: elemnt that will give buttons for the box
	 */
	public void showBoxOnElement(int x, int y, ScreenElement clickedElement) {
		this.clearExistingButtons();
		this.addCharNameAsText(clickedElement);
		setUpExtraCommandsButtons(clickedElement);
		setUpExtraCommandsBox(x, y);
		this.infoSelection.makeVisible();
	}
	
	/*
	 * Sets up the actual elements for the extra commands box
	 * Inputs:
	 * 	clickedElement: elemnt that will give buttons for the box
	 */
	private void setUpExtraCommandsButtons(ScreenElement clickedElement) {
		if(clickedElement.hasExtraCommands()) {
			for(ScreenButton oneButton : clickedElement.getExtraCommands()) {
				this.infoSelection.addButton(oneButton);
			}
		}
	
	}
	
	/*
	 * sets size and coordinates for the extra commands box
	 * Inputs:
	 * 	x: x coordinate for the box
	 * 	y: y coordinate for the box
	 */
	private void setUpExtraCommandsBox(int x, int y) {
		this.infoSelection.autoSize();
		this.infoSelection.setCoordinates(x, y);
	}
	
	//removes all previous buttons from the info selection
	private void clearExistingButtons() {
		this.infoSelection.removeAllButtons();
	}
	
	/*
	 * Adds selected character name as the top text for the extra commands box
	 * Inputs:
	 * 	clickedElement: element that was selected for the extra commands box
	 */
	private void addCharNameAsText(ScreenElement clickedElement) {
		if(clickedElement.getClass().equals(ScreenSimpleChar.class)) {
			ScreenSimpleChar character = (ScreenSimpleChar) clickedElement;
			addHeaderForInfoSelection(character.getMaster().getName());
		}
		else {
			addHeaderForInfoSelection("Right click Info box");
		}
	}
	
	private void addHeaderForInfoSelection(String oneLineText) {
		this.infoSelection.fillTextOneLine(oneLineText);
	}
	
	//Adds the extra commands box under graphical management
	private void addToGE() {
		this.mainFactory.getGElementManager().setRightClickDataBox(infoSelection);
	}
	
	//makes the extra commands box invisible
	public void hideBox() {

		this.infoSelection.makeInvisible();
	}
	
	//turns extra commands box visible
	public boolean isVisible() {
		return this.infoSelection.isVisible();
	}

}

package events;

import java.util.ArrayList;
import java.util.List;

import eventModules.IEventModule;
import factories.IAutoMake;
import factories.MainFactory;

//Manager for all the event type of objects in the game
public class EventManager implements IAutoMake {
	private MainFactory mainFactory;
	private ArrayList<GameEvent> events;
	
	//constructor
	public EventManager() {
		
	}

	//setter for main factory
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	//method for booting up a new instance of this class
	@Override
	public void bootUp() {
		this.events = new ArrayList<GameEvent>();
		//makeTestEvent();
	}
	
	//Method for making test events
	/*private void makeTestEvent() {
		makeOneTestEvent("Event nr 1", 1);
		makeOneTravelTestEvent("Event nr 2", 1);
		makeAddEnemiesTestEvent("Battle event nr 1", 1);
		
	}*/
	
	//Loads in events from an input module
	private void loadEventModule(IEventModule module) {
		this.events.addAll(module.getEvents());
	}
	
	//Combination method for proto show version events
	public void makeTutorialEvents() {
		makeFoodSearchEvents();
		//makeSupplySearchEvents();
		makeSettlementEvents();
	}
	
	//Hard coded method for getting food search events
	private void makeFoodSearchEvents() {
		this.loadEventModule(this.mainFactory.getElemFactory().getFoodSearchEvents1());
	}
	
	//Hard coded method for getting supply search events
	private void makeSupplySearchEvents() {
		this.loadEventModule(this.mainFactory.getElemFactory().getSupplySearchEvents1());
	}
	
	private void makeSettlementEvents() {
		this.loadEventModule(this.mainFactory.getElemFactory().getSettlementWaterSearchEvents());
	}
	
	//makes a test travel finish event
	public GameEvent getReachFortLoupeEvent() {
		GameEvent newEvent = makeOneEvent("You reached fort Loupe", EventTagName.TST, 0);
		newEvent.addEventImg("100X100Mark");
		EventChoice newChoice = getEventChoice(newEvent, "Enter");
		newChoice.addNewCustomButtonListener(this.mainFactory.getElemFactory().getGoSituationChangeListener(this.mainFactory.getSituationChangeFactory().getFromTTutorialToLoupe()));
		return newEvent;
	}
	
	/*
	 * Makes one new ingame event
	 * Inputs:
	 * 	text: string that will be displayed as introductionary text for the event
	 * 	tag: event tag that can be used to find same situation events
	 */
	private GameEvent makeOneEvent(String text, EventTagName tag, int nrOfChars) {
		GameEvent newEvent = addTextToNewEvent(text, nrOfChars);
		this.addTag(newEvent, tag);
		this.events.add(newEvent);
		return newEvent;
	}
	
	//Method for adding text to a event, inputs is the string format of the text
	private GameEvent addTextToNewEvent(String text, int nrOfChars) {
		GameEvent newEvent = this.mainFactory.getElemFactory().getGameEvent(nrOfChars);
		newEvent.addText(text);
		return newEvent;
	}
	
	/*
	 * gets a new event choice and attaches it to the event
	 * Inputs:
	 * 	attachTo: game event the choice is attached to
	 * 	choiceText: the text that will be displayed on the button that represents the choice
	 * Ouput:
	 * 	EventChoice: the choice that was made and got attached to the event already
	 */
	private EventChoice getEventChoice(GameEvent attachTo, String choiceText) {
		EventChoice newChoice = this.mainFactory.getElemFactory().getEventChoice(attachTo, choiceText);
		return newChoice;
	}
	
	/*
	 * Adds a tag to a game event
	 * Inputs:
	 * 	event: event that the tag will be added to
	 * 	name: tag name that will be added to the event
	 */
	public void addTag(GameEvent event, EventTagName name) {
		event.addTag(name);
	}
	
	//search function for events with a specific tag, return  a arraylist of those events
	public List<GameEvent> findEventsWithTag(EventTagName searchTag) {
		List<GameEvent> foundEvents = sortOutTags(searchTag, this.events);
		return foundEvents;
	}
	
	public List<GameEvent> findEventsWithTag(EventTagName searchTag, int maxParticipation) {
		List<GameEvent> foundEvents = sortOutTags(searchTag, this.events, maxParticipation);
		return foundEvents;
	}
	
	/*
	 * search function for events with tags:
	 * Inputs:
	 * 	searchTage: the tag that is searched for
	 * 	array: arrayList of elements that is searched for the tagged events
	 */
	private List<GameEvent> sortOutTags(EventTagName searchTag, List<GameEvent> array) {
		List<GameEvent> foundEvents = new ArrayList<GameEvent>();
		foundEvents = sortToTags(searchTag, array, foundEvents);
		return foundEvents;
	}
	
	
	/*
	 * search function for events with tags:
	 * Inputs:
	 * 	searchTage: the tag that is searched for
	 * 	array: arrayList of elements that is searched for the tagged events
	 * 	maxParticipation: sets a limit on how many participants can be for the event
	 */
	private List<GameEvent> sortOutTags(EventTagName searchTag, List<GameEvent> array, int maxParticipation) {
		List<GameEvent> foundEvents = new ArrayList<GameEvent>();
		foundEvents = sortToTags(searchTag, array, foundEvents, maxParticipation);
		return foundEvents;
	}
	
	/*
	 * Method for adding a tags events to an already existing list of events
	 * Inputs:
	 * 	searchTag: the tag for new events
	 * 	toArray: the array where the new events will be added
	 * Output:
	 * 	the same array list that was inputed
	 */
	public List<GameEvent> addAnotherTagEvents(EventTagName searchTag, List<GameEvent> toArray) {
		return sortToTags(searchTag, this.events, toArray);
	}
	
	/*
	 * Method for adding a tags events to an already existing list of events
	 * Inputs:
	 * 	searchTag: the tag for new events
	 * 	toArray: the array where the new events will be added
	 * 	maxParticipation: max amount of chars allowed for the event
	 * Output:
	 * 	the same array list that was inputed
	 */
	public List<GameEvent> addAnotherTagEvents(EventTagName searchTag, List<GameEvent> toArray, int maxParticipation) {
		return sortToTags(searchTag, this.events, toArray, maxParticipation);
	}
	
	
	/*
	 * Method for for finding events of a specific tag from one array list and adding them to another
	 * The found events will stay in the first list and will also be added to the second list
	 * Inputs:
	 * 	searchTag: the tag that is searched for
	 * 	fromArray: the array that will be searched from
	 * 	toArray: the array where found events will be added to
	 * Output:
	 * 	returns the inputted toArray
	 */
	private List<GameEvent> sortToTags(EventTagName searchTag, List<GameEvent> fromArray, List<GameEvent> toArray) {
		for (GameEvent oneEvent: fromArray) {
			if(oneEvent.hasTag(searchTag)) {
				toArray.add(oneEvent);
			}
		}
		return toArray;
	}
	
	/*
	 * Method for for finding events of a specific tag from one array list and adding them to another
	 * The found events will stay in the first list and will also be added to the second list
	 * Inputs:
	 * 	searchTag: the tag that is searched for
	 * 	fromArray: the array that will be searched from
	 * 	toArray: the array where found events will be added to
	 * 	maxParticipation: max nr of allowed participants for the events
	 * Output:
	 * 	returns the inputted toArray
	 */
	private List<GameEvent> sortToTags(EventTagName searchTag, List<GameEvent> fromArray, List<GameEvent> toArray, int maxParticipation) {
		for (GameEvent oneEvent: fromArray) {
			if(oneEvent.hasTag(searchTag)) {
				if(oneEvent.getCharAmount() <= maxParticipation) {
					toArray.add(oneEvent);
				}	
			}
		}
		return toArray;
	}
	
	/*
	 * Sorts out events with a specific participant number
	 * Inputs:
	 * 	searchNumber: the number that represents participant number that is searched for
	 * 	fromArray: the array that events are searched from
	 * Output:
	 * 	new array that only contains events with the specific participant number
	 */
	public List<GameEvent> getByCharNr(int searchNumber, List<GameEvent> fromArray) {
		ArrayList<GameEvent> newList = new ArrayList<GameEvent>();
		return sortToByCharNr(searchNumber, fromArray, newList);
	}
	
	/*
	 * Sorts out events with a specific participant number
	 * Inputs:
	 * 	searchNumber: the number that represents participant number that is searched for
	 * 	fromArray: the array that events are searched from
	 * 	toArray: the array that found events are added to
	 * Output:
	 *  the inputed toArray
	 */
	private List<GameEvent> sortToByCharNr(int searchNumber, List<GameEvent> fromArray, List<GameEvent> toArray) {
		for (GameEvent oneEvent: fromArray) {
			if(oneEvent.getCharAmount() == searchNumber) {
				toArray.add(oneEvent);
			}
		}
		return toArray;
	}

}

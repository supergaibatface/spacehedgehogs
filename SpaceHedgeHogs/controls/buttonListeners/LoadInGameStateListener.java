package buttonListeners;

import gameState.IGameState;

/*
 * Button listener that will load in a new gameState (Start of a game session)
 */
public class LoadInGameStateListener extends BaseButtonListener {
	private IGameState gameState;

	/*
	 * Constructor:
	 * Inputs
	 * 	gameState: gameState that will be loaded in
	 */
	public LoadInGameStateListener(IGameState gameState) {
		this.gameState = gameState;
	}
	
	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.mainFactory.getGameStateController().bootUpGameState(gameState);
		
	}

	@Override
	public String toInfo() {
		return "Loads in new Game";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

package buttonListeners;

import java.util.ArrayList;

import calculations.RangeCalculator;
import combatFieldElements.ControlFaction;
import combatFieldElements.FieldCharacter;
import combatFieldElements.MovementSquare;
import controllers.ActivityController;
import propertyBars.PropertyBarName;
import squareFilters.NoThisFactionChars;

//ButtonListener that will trigger showing attackSquares on a movementField
public class ShowAttackSquaresListener extends BaseButtonListener {
	private ActivityController activity;
	private RangeCalculator attackCalc;

	@Override
	public void bootUp() {
		this.activity = this.mainFactory.getActivityController();
		this.attackCalc = this.mainFactory.getAttackRangeCalculator();
		
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		FieldCharacter selectedChar = this.activity.getSelectedCharAsFieldChar();
		if(selectedChar.getMasterCharacter().getPropertyBar(PropertyBarName.Attacks).getCurrentValue() > 0) {
			findAndPaintSquares(selectedChar.getLocation());
		}

		
	}
	
	/*
	 * Method that gets attack squares from attack calculator and then paints them through game state controller
	 * Input:
	 * 	startSquare: square that the char stands on
	 */
	private void findAndPaintSquares(MovementSquare startSquare) {
		ArrayList<MovementSquare> attackSquares = this.attackCalc.getTargets(1, startSquare, 
				this.mainFactory.getGameStateController().getBattleController().getMovementField(), 
				new NoThisFactionChars(ControlFaction.PLAYER));
		this.mainFactory.getGameStateController().getBattleController().setAttackSquares(attackSquares);
	}

	@Override
	public String toInfo() {
		return "Shows possible attack locations";
	}

}

package settlement;

import java.util.List;

import screenElements.ScreenButton;

public interface ISettlement {
	
	public List<ScreenButton> getLocationButtons();

}

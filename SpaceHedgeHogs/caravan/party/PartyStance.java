package party;

//Enum that represents different stances that the players party can have
public enum PartyStance {
	Travel, Camp, Battle

}

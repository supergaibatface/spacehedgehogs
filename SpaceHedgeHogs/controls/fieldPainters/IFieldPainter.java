package fieldPainters;

import java.util.ArrayList;

import combatFieldElements.MovementSquare;

//Interface for field painters that sort out a required set of squares
public interface IFieldPainter {
	
	//returns the set of squares that it needs to find
	public ArrayList<MovementSquare> getActiveSquares();

}

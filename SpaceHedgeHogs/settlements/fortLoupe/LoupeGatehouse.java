package fortLoupe;

import java.util.List;

import dialogueCharacters.IDialogueCharacter;
import loupeNpcs.LoupeGateGuard;
import settlementLocation.BaseSettlementLocation;

public class LoupeGatehouse extends BaseSettlementLocation {

	public LoupeGatehouse() {
		super("Loupe gatehouse");
	}

	@Override
	protected void clickScript() {
		this.mainFactory.getGElementManager().openElementOnTopLayer(actionDisplayList);
		
	}

	@Override
	protected int giveButtonX() {
		return 100;
	}

	@Override
	protected int giveButtonY() {
		return 100;
	}

	@Override
	protected int giveButtonLength() {
		return 100;
	}

	@Override
	protected int giveButtonHeight() {
		return 100;
	}

	@Override
	protected String giveButtonImgName() {
		return "100X100Mark";
	}
	
	@Override
	protected  String giveButtonLabel() {
		return "Gatehouse";
	}

	@Override
	protected void setUpLocationCharacters(List<IDialogueCharacter> addLocation) {
		addLocation.add(this.mainFactory.getNPCFactory().getNPCLoupeGateGuard());
		
	}

	@Override
	protected boolean giveStartAccessabilityValue() {
		// TODO Auto-generated method stub
		return true;
	}

}

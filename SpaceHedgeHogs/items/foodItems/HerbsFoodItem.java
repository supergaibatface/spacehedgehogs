package foodItems;

import foodBase.FoodItemBase;
import itemBase.TradeItemName;

public class HerbsFoodItem extends FoodItemBase {

	public HerbsFoodItem() {
		super(TradeItemName.Herbs);
	}

	@Override
	protected int bootGiveBaseExpiration() {
		return 40;
	}

	@Override
	protected FoodItemBase giveBaseClass() {
		return this.mainFactory.getItemFactory().getHerbsFoodItem();
	}
	
	@Override
	protected String bootGiveItemImageName() {
		return this.bootGivePlaceholderImage();
	}

	@Override
	protected int bootGiveOneUnitWeight() {
		return 6;
	}

}

package mouseSituationSet;

//Represents a default situation set with basic button clicking functions
public class MSSDefault extends BaseMouseSituationSet {

	@Override
	public boolean checkSituation() {
		if(this.conditions.getSelectedItem() != null) {
			return false;
		}
		
		return true;
	}

	@Override
	protected void makeAllCAElements() {
		this.addNewCAElement(this.mainFactory.getCAButtonPress());
		this.addNewCAElement(this.mainFactory.getCACharSelect());
		this.addNewCAElement(this.mainFactory.getCARScreenCharacter());
		this.addNewCAElement(this.mainFactory.getCARScreenItem());
	}

}

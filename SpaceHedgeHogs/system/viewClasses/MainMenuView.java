package viewClasses;

import generalGraphics.ButtonLookType;
import screenElements.ScreenBackground;
import screenElements.ScreenButton;
import screenElements.ScreenDataBox;
import view.BaseGameView;
import view.ViewName;

public class MainMenuView extends BaseGameView {

	public MainMenuView() {
		super(ViewName.MainMenu);
	}

	@Override
	protected void bootElements() {
		ScreenDataBox menuBox = this.bootMakeMenuOptionsBox();
		this.addDataBox(menuBox, 700, 200);
		menuBox.addButton(this.bootMakeStartGameButton());
		menuBox.addButton(this.bootMakeDebugMenuButton());
		menuBox.autoSize();
		
	}
	
	@Override
	protected String bootGiveBackgroundImageName() {
		return "TestBG";
	}
	
	private ScreenDataBox bootMakeMenuOptionsBox() {
		ScreenDataBox menuBox = this.mainFactory.getElemFactory().getScreenDataBox(0, 0, "Orange");
		return menuBox;
	}
	
	private ScreenButton bootMakeStartGameButton() {
		ScreenButton startGame = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.POPUPSELECT, "Start game");
		startGame.addButtonEventListener(this.mainFactory.getElemFactory().getLoadInGameStateListener(this.mainFactory.getBattleTutorialState()));
		return startGame;
	}
	
	private ScreenButton bootMakeDebugMenuButton() {
		ScreenButton startGame = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.POPUPSELECT, "Debug modes");
		startGame.addButtonEventListener(this.mainFactory.getElemFactory().getViewChangeListener(ViewName.DebugMenu));
		return startGame;
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

}

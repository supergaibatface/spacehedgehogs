package buttonLimiters;

//Limiter that passes if there have passed enough turns
public class OverTurnLimiter extends BaseButtonLimiter {
	private int turnLimit;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	nrOfTurns: how many turns need to pass for the limiter to pass
	 */
	public OverTurnLimiter(boolean notifyOnFailure, boolean instantPopUp, int nrOfTurns) {
		super(notifyOnFailure, instantPopUp);
		this.turnLimit = nrOfTurns;
	}

	@Override
	public boolean check() {
		if(this.mainFactory.getBattleTime().getCurrenTurn() > this.turnLimit) {
			return true;
		}
		return false;
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doCost() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String reasonForFail() {
		return this.turnLimit+" turns have not passed yet";
	}

}

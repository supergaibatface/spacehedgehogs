package foodBase;

import java.util.HashMap;
import java.util.Map;

import factories.IAutoMake;
import factories.MainFactory;
import itemBase.TradeItemName;

/*
 * table that saves the expiration dates of food items
 * this should at some point be removed and its logic sent to itemNameTable
 */
public class FoodExpTable implements IAutoMake {
	private MainFactory mainFactory;
	private Map<TradeItemName, Integer> expirationTimes;
	
	//gives a placeholder expiration date in case main logic fails
	public int getDefaultFoodExpiration() {
		return 1;
	}

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}
	
	/*
	 * Returns the max epxiration date assigned to a food type
	 * Input:
	 * 	itemName: name of the item
	 */
	public int getMaxExpirationDateForFood(TradeItemName itemName) {
		if(this.expirationTimes.containsKey(itemName)) {
			return this.expirationTimes.get(itemName);
		}
		else {
			return this.getDefaultFoodExpiration();
		}
	}

	@Override
	public void bootUp() {
		this.expirationTimes = new HashMap<TradeItemName, Integer>();
		this.addValues();
		
	}
	
	//General function for adding the designated expiration times for all the items
	private void addValues() {
		//this.expirationTimes.put(TradeItemName.Berry, 8);
		//this.expirationTimes.put(TradeItemName.Mushroom, 16);
		//this.expirationTimes.put(TradeItemName.FreshMeat, 4);
		//this.expirationTimes.put(TradeItemName.Roots, 30);
		//this.expirationTimes.put(TradeItemName.Herbs, 40);
		//this.expirationTimes.put(TradeItemName.Apple, 20);
	}
	

}

package buttonListeners;

import gameCharacters.GameCharacter;

//Buttonlistener that triggers the end of a battle, and changes view
public class BattleEndListener extends BaseButtonListener {

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.mainFactory.getGameStateController().endBattle();
		
	}

	@Override
	public String toInfo() {
		return "leaves the battle view";
	}

}

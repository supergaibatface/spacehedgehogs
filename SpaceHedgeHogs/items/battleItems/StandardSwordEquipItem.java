package battleItems;

import battleItemBase.BattleItemBase;
import itemBase.IGameItem;
import itemBase.TradeItemName;

public class StandardSwordEquipItem extends BattleItemBase {

	public StandardSwordEquipItem() {
		super(TradeItemName.Sword);
	}

	@Override
	protected IGameItem giveBaseClass() {
		return this.mainFactory.setUpNewNonPerservativeValue(new StandardSwordEquipItem());
	}

	@Override
	protected String bootGiveItemImageName() {
		return "StandardSword";
	}

	@Override
	protected int bootGiveOneUnitWeight() {
		return 5;
	}

}

package battlePlacementItems;

import battlePlacementItemBase.BattlePlacementItemBase;
import itemBase.IGameItem;
import itemBase.TradeItemName;

public class BarricadeBattlePlacementItem extends BattlePlacementItemBase {

	public BarricadeBattlePlacementItem() {
		super(TradeItemName.Barricade);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected IGameItem giveBaseClass() {
		return this.mainFactory.setUpNewNonPerservativeValue(new BarricadeBattlePlacementItem());
	}

	@Override
	protected String bootGiveItemImageName() {
		return this.bootGivePlaceholderImage();
	}

	@Override
	protected int bootGiveOneUnitWeight() {
		return 5;
	}

}

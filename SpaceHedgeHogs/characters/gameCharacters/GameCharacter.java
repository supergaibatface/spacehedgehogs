package gameCharacters;

import java.util.ArrayList;
import java.util.List;

import dialogueCharacters.IDialogueCharacter;
import factories.IAutoMake;
import factories.MainFactory;
import propertyBars.PropertyBar;
import propertyBars.PropertyBarName;
import screenInfoPage.ScreenCharInfoPage;
import skills.Skill;
import skills.SkillName;
import sreenElementsSockets.ScreenSimpleChar;

//Class that represents a logical character in the game
public class GameCharacter extends BaseGameCharacter {	
	/*private String name;
	private String imgName;
	private MainFactory mainFactory;
	private ArrayList<Skill> skills;
	private ArrayList<PropertyBar> propertyBars;
	private ScreenSimpleChar socketAvatar;
	private ScreenCharInfoPage infoPage;
	private IDialogueCharacter dialogueElement;*/
	
	/*
	 * constructor, 
	 * inputs: 
	 * 	name: String version of the characters name
	 * 	ingName: String version of the pictures name (without file extension)
	 */
	public GameCharacter(String name, String imgName) {
		super(name, imgName);
		/*this.name = name;
		this.imgName = imgName;*/
	}

	@Override
	protected IDialogueCharacter bootDialogueElement() {
		return null;
	}
	
	//getter for the characters name
	/*public String getName() {
		return this.name;
	}*/
	
	//getter for the character image name
	/*public String getImgName() {
		return this.imgName;
	}*/

	//setter for main factory
	/*@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}*/

	//method for booting up an instance of this class
	/*@Override
	public void bootUp() {
		this.skills = new ArrayList<Skill>();
		this.mainFactory.getSkillManager().getSkillsForCharacter(this);
		this.propertyBars = new ArrayList<PropertyBar>();
		this.mainFactory.getCharacterManager().getPropertyBarsForChar(this);
		this.makeScreenCharInfoPage();
	}*/
	
	//makes a info page for this character
	/*private void makeScreenCharInfoPage() {
		this.infoPage = this.mainFactory.getElemFactory().getScreenCharInfoPage(this, "Orange");
	}*/
	
	//getter for this chatacters info page
	/*public ScreenCharInfoPage getInfoPage() {
		return this.infoPage;
	}*/
	
	/*
	 * method for adding a new skill to the character
	 * the skills should be gotten from skill manager
	 */
	/*public void addSkill(Skill newSkill) {
		this.skills.add(newSkill);
	}*/
	
	/*
	 * Method for addiong a property bar to the character
	 * Inputs:
	 * 	newBar: the property bar that will be added to the char
	 */
	/*public void addPropertyBar(PropertyBar newBar) {
		this.propertyBars.add(newBar);
	}*/
	
	//getter for a list of all the skills on this character
	/*public ArrayList<Skill> getAllSkill() {
		return this.skills;
	}*/
	
	//getter for the property bars assigned to this char
	/*public ArrayList<PropertyBar> getAllPropertyBars() {
		return this.propertyBars;
	}*/
	
	//getter for a specific porperty bar, searches by name
	/*public PropertyBar getPropertyBar(PropertyBarName name) {
		for(PropertyBar oneBar : this.propertyBars) {
			if(oneBar.equals(name)) {
				return oneBar;
			}
		}
		return null;
	}*/
	
	//method for adding experience to a skill
	/*public void improveSkill(SkillName skillname, int experience) {
		Skill rightSkill = this.findSkill(skillname);
		rightSkill.addExp(experience);
	}*/
	
	//Method for finding a specific skill on a character
	/*public Skill findSkill(SkillName name) {
		for(Skill oneSkill : this.skills) {
			if(oneSkill.equals(name)) {
				return oneSkill;
			}
		}
		return null;
	}*/
	
	//Checks if this gameCharacter already has a screen version of it made
	/*public boolean hasSocketAvatar() {
		return this.socketAvatar != null;
	}*/
	
	//Getter for GameCharacters screen avatar version
	/*public ScreenSimpleChar getSocketAvatar() {
		return this.socketAvatar;
	}*/
	
	//stter for the character screen avatar version
	/*public void setSocketAvatar(ScreenSimpleChar avatar) {
		this.socketAvatar = avatar;
	}*/
	
	//For safely deleting this element
	/*public void safeDelete() {
		this.socketAvatar.removeThisElementFromCurrentLocation();
		this.socketAvatar = null;
	}*/
	
	/*public boolean hasDialogueElement() {
		return this.dialogueElement != null;
	}*/
	
	/*public void addDialogueElement(IDialogueCharacter dialogueElement) {
		this.dialogueElement = dialogueElement;
	}*/

}

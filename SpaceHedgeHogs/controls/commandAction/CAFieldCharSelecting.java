package commandAction;

import combatFieldElements.ControlFaction;
import combatFieldElements.FieldCharacter;
import combatFieldElements.MovementSquare;
import screenElements.ScreenElement;

//Method for making a field character into the selected item
public class CAFieldCharSelecting extends CABaseClass<FieldCharacter> {

	@Override
	public boolean inputIsCorrect() {
		int nr = this.hasItemTypeClicked();
		if(nr<0) {
			return false;
		}
		if(this.castToClickTargetClass(conditions.getClickedItem(nr)).getFaction() != ControlFaction.PLAYER) {
			//System.out.println("failed at faction search");
			return false;
		}
		//System.out.println("got a true input value for clicking on: " +this.castToClickTargetClass(selectedItem).getMasterCharacter().getName());
		return true;
		
	}

	@Override
	public void actionImplement() {
		FieldCharacter selectedChar = this.castToClickTargetClass(conditions.getClickedItem(this.hasItemTypeClicked()));
		this.selection.makeActive(selectedChar);
	}

	@Override
	public Class<FieldCharacter> setClickTargetClass() {
		return FieldCharacter.class;
	}

	@Override
	protected int giveMouseButtonNr() {
		return this.giveLeftMouseButtonValue();
	}

}

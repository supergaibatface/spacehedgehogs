package loupeQuests;

import java.util.List;

import dialogueFlags.DialogueFlag;
import questBase.BaseQuest;
import questStepBase.IQuestStep;

public class RestingInLoupe extends BaseQuest {

	@Override
	protected String returnQuestName() {
		return "A moments rest between 4 walls";
	}

	@Override
	protected void addQuestSteps(List<IQuestStep> questStepList) {
		questStepList.add(this.makeFirstStep());
		questStepList.add(this.makeSecondStep());
		
	}
	
	private IQuestStep makeFirstStep() {
		IQuestStep newStep = this.mainFactory.getQuestFactory().getStandardQuestStep("Set up camp in Loupe", this);
		newStep.addNewStepObjective(this.mainFactory.getQuestFactory().getVisitLocationObjective(newStep, this.mainFactory.getLogicalLocationFactory().getLoupeCamp()));
		newStep.addQuestStepReward(this.mainFactory.getQuestFactory().getGainDialogueFlagReward(DialogueFlag.LoupeMainPartyPersonalQuests, this.mainFactory.getCharacterFactory().getDialogueJack()));
		return newStep;
	}
	
	private IQuestStep makeSecondStep() {
		IQuestStep newStep = this.mainFactory.getQuestFactory().getStandardQuestStep("Ask party members for ideas", this);
		newStep.addNewStepObjective(this.mainFactory.getQuestFactory().getTalkToNpcObjective(newStep, this.mainFactory.getCharacterFactory().getDialogueJack()));
		newStep.addQuestStepReward(this.mainFactory.getQuestFactory().getGainQuestASReward(this.mainFactory.getQuestFactory().getPreparingWeaponsJackQuest()));
		newStep.addQuestStepReward(this.mainFactory.getQuestFactory().getGainDialogueFlagReward(DialogueFlag.LoupeMainPartyPersonalQuests, this.mainFactory.getNPCFactory().getNPCLoupeArmoryChief()));
		return newStep;
	}

}

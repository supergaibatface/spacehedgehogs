package screenInfoPage;

import java.util.ArrayList;
import java.util.List;

import frameworks.TextFrame;
import itemBase.IGameItem;
import screenElements.ScreenButton;
import screenElements.ScreenSoloImage;

//Base class for item info pages
public abstract class ItemBasePage extends BaseInfoPage {
	protected TextFrame amountText;
	protected ScreenSoloImage itemPic;
	protected TextFrame weightText;

	//constructor
	public ItemBasePage() {
		super(1000, 900);
	}
	
	public void bootUp() {
		super.bootUp();
		this.setCoordinates(200, 50);
		this.makeWeightText();
		this.makeAmountTextFrame();
		this.addItemPic();
		this.setUpAmountUpdateListener();
	}

	@Override
	protected void setLocationForCloseButton(ScreenButton closeButton) {
		closeButton.setCoordinates(950, 20);
	}

	@Override
	protected void setLocationForNameField(TextFrame nameField) {
		nameField.setLocation(50, 80);
	}

	@Override
	protected void writeNameFieldLines(List<String> lines) {
		lines.add("Item: "+this.getMasterAsInterface().getName());
	}
	
	//Returns the master item as a IGameItem type object
	protected abstract IGameItem getMasterAsInterface();
	
	//Makes text frame that shows item amount
	protected void makeAmountTextFrame() {
		this.amountText = this.mainFactory.getElemFactory().getTextFrame(this);
		this.amountText.setLocation(50, 120);
		this.updateAmountText();
		this.addNewElement(amountText);
	}
	
	//public method for updating amount text
	public void updateAmountText() {
		this.writeAmountText();
	}
	
	//Writes amount text
	protected void writeAmountText() {
		List<String> lines = new ArrayList<String>();
		lines.add("amount: "+this.getMasterAsInterface().getSupply());
		this.amountText.fillTexts(lines);
	}
	
	//Sets up listener that updates amount text when needed
	protected void setUpAmountUpdateListener() {
		this.getMasterAsInterface().addItemChangeEventListener(this.mainFactory.getElemFactory().getItemInfoPageAmountListener(this));
	}
	
	//makes the item weight text element on the info page
	protected void makeWeightText() {
		this.weightText = this.mainFactory.getElemFactory().getTextFrame(this);
		this.addNewElement(this.weightText);
		this.weightText.setLocation(50, 140);
		this.updateWeightText();
	}
	
	//public method for updaying the value of the weight text
	public void updateWeightText() {
		this.writeWeightText(this.getWeightLines());
		
	}
	
	//Writes input list of string as the text on weight text element
	protected void writeWeightText(List<String> lines) {
		this.weightText.fillTexts(lines);
	}
	
	//getter for the list of strings that should be displayed as weight lines
	protected List<String> getWeightLines() {
		List<String> lines = new ArrayList<String>();
		lines.add("One unit weight: "+this.getMasterAsInterface().getOneElementWeight());
		lines.add("Full stack weight: "+this.getMasterAsInterface().getFullWeight());
		return lines;
	}
	
	//public method called when item amount and stack weight have changed
	public void itemAmountChangeCall() {
		this.updateAmountText();
		this.updateWeightText();
	}

	//Adds a pciture of the item on top of the info page
	protected void addItemPic() {
		this.itemPic = this.mainFactory.getElemFactory().getSoloImage(300, 300, this.getMasterAsInterface().getImageName());
		this.itemPic.setCoordinates(600, 80);
		this.addNewElement(this.itemPic);
	}

}

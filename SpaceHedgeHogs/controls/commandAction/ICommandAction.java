package commandAction;

//Interface for command actions (actions that a player can do)
public interface ICommandAction {
	
	//Method that checks if player done action is correct input for this command action
	public boolean inputIsCorrect();
	//Method that does a full check of everything needed to trigger the action
	public boolean fullCheck();
	//Command used to trigger the change that results from players action
	public void fullFillAction();

}

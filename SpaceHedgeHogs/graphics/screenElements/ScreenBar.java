package screenElements;

import java.util.ArrayList;
import java.util.List;

import frameworks.IStackable;
import propertyBarListeners.ScreenBarValueUpdater;
import propertyBars.PropertyBar;

//Visual representation of a propertybar
public class ScreenBar extends ScreenElement {
	private int bufferSize = 3;
	private ScreenSoloImage barImage;
	private PropertyBar masterBar;
	private String barVisual;
	private ScreenBarValueUpdater updaterListener;
	

	/*
	 * Constructor
	 * Inputs:
	 * 	length: length of the element
	 * 	height: height of the element
	 * 	masterBar: Property bar that this screen bar represents
	 */
	public ScreenBar(int length, int height, PropertyBar masterBar, String barVisual) {
		super(0, 0);
		this.setSize(length, height);
		this.masterBar = masterBar;
		this.barVisual = barVisual;
	}
	
	public void bootUp() {
		super.bootUp();
		this.xDataStart = this.bufferSize;
		this.yDataStart = this.bufferSize;
		this.barImage = this.mainFactory.getElemFactory().getSoloImage(getCurrentValueInVisual(), getBarVisualHeight(), this.barVisual);
		this.barImage.setWrapper(this);
		this.setUpListener();
	}
	
	//Sets up listener that 
	private void setUpListener() {
		this.updaterListener = this.mainFactory.getElemFactory().getScreenBarValueUpdater(this);
		this.masterBar.addListener(this.updaterListener);
	}
	
	//Getter for the current value
	private int getCurrentValueInBar() {
		return this.masterBar.getCurrentValue();
	}
	
	//Getter for the max value
	private int getMaxValueInBar() {
		return this.masterBar.getMaxValue();
	}
	
	//Getter for the min value
	private int getMinValueInBar() {
		return this.masterBar.getMinValue();
	}
	
	//Getter for the max value representation in visual size
	private int getMaxValueInVisual() {
		return this.getLength() - (bufferSize * 2);
	}
	
	//Getter for the bars visual height
	private int getBarVisualHeight() {
		return this.getHeight() - (bufferSize * 2);
	}
	
	//Getter for the current value represented in the visual size
	private int getCurrentValueInVisual() {
		int modifier = getCurrentValueInBar() * getMaxValueInVisual();
		int result =  modifier / getMaxValueInBar();
		if(result > 0) {
			return result;
		}
		else {
			return 1;
		}
	}
	
	//Method that updates screenBars visual look to the current bar value
	public void updateBarValue() {
		this.barImage.setSize(getCurrentValueInVisual(), getBarVisualHeight());
	}
	
	//Getter for this bars value showing elements image
	public ScreenSoloImage getBarImage() {
		return this.barImage;
	}

	@Override
	public void fillTopElementList(List<IStackable> list) {
		list.add(barImage);
		
	}

	@Override
	public void fillTopTextList(List<ScreenText> list) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void goingOffScreenScript() {
		// TODO Auto-generated method stub
		
	}

}

package fieldPainters;

import java.util.ArrayList;

import combatFieldElements.ControlFaction;
import combatFieldElements.FieldCharacter;
import combatFieldElements.MovementField;
import combatFieldElements.MovementSquare;
import factories.IAutoMake;
import factories.MainFactory;

/*
 * Field Painter that will find squares that one factions chars are standing on
 */
public class OnAlliedCharactersPainter implements IFieldPainter, IAutoMake {
	private MainFactory mainFactory;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<MovementSquare> getActiveSquares() {
		return findSquares();
	}
	
	/*
	 * Logic for finding designates squares
	 */
	private ArrayList<MovementSquare> findSquares() {
		ArrayList<MovementSquare> foundSquares = new ArrayList<MovementSquare>();
		for(FieldCharacter oneChar : this.mainFactory.getGameStateController().getBattleController().getMovementField().getFieldCharacters(ControlFaction.PLAYER)) {
			foundSquares.add(oneChar.getLocation());
		}
		return foundSquares;
		
	}

}

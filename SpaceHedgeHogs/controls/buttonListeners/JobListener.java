package buttonListeners;

//Button listener that triggers ingame jobs getting fulfilled, usually for turn change
public class JobListener extends BaseButtonListener {

	//event triggering method
	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.mainFactory.getCharacterManager().fullFillRoles();
	}

	//bootup method
	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String toInfo() {
		return "Will trigger jobs";
	}

}

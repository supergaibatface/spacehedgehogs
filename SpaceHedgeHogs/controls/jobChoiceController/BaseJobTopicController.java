package jobChoiceController;

import java.util.ArrayList;

import factories.IAutoMake;
import factories.MainFactory;
import jobs.Job;
import jobs.JobName;
import jobs.JobTopic;
import newJobs.IJob;
import other.ModifierPackage;
import other.StaticFunctions;
import sreenElementsSockets.ScrollCharSocket;

/*
 * Base class for job topic controllers
 */
public abstract class BaseJobTopicController implements IJobTopicController, IAutoMake{
	protected JobTopic assignedTopic;
	protected ModifierPackage modifierPackage;
	protected MainFactory mainFactory;
	protected boolean firstTime = true;
	protected boolean debug = true;
	
	/*
	 * Constructor:
	 * Inputs:
	 * 	newTopic: topic assigned to the controller
	 */
	public BaseJobTopicController(JobTopic newTopic) {
		this.assignTopic(newTopic);
	}
	
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}
	@Override
	public void bootUp() {
		bootUpModifierPackage();
		
	}
	
	/*
	 * Assignes a s job topic to the controller
	 */
	public void assignTopic(JobTopic newTopic) {
		this.assignedTopic = newTopic;
	}
	
	/*
	 * Updated sockets in the job topic
	 */
	public void updateSocketValues() {
		if(this.modifierPackage.modifierSetEquals(this.makeModifierValuePackage()) && !firstTime) {
			return;
		}
		this.modifierPackage.writeNewValues(this.makeModifierValuePackage());
		this.assignedTopic.resetTopic();
		this.addSockets();
		if(firstTime) {
			firstTime = false;
		}

	}
	
	/*
	 * Sets ready modifier package that will decide what sockets will be active
	 */
	protected void bootUpModifierPackage() {
		this.modifierPackage = new ModifierPackage();
		this.modifierPackage.bootUpTo(makeNullValuePackage(makeModifierValuePackage()));
	}
	
	/*
	 * Makes null values for modifiers used by controller
	 * Inputs:
	 * 	realModifiers: package of actual modifiers
	 */
	protected ArrayList<Object> makeNullValuePackage(ArrayList<Object> realModifiers) {
		ArrayList<Object> newArray = new ArrayList<Object>();
		for(Object oneObject : realModifiers) {
			newArray.add(null);
		}
		return newArray;
	}
	
	/*
	 * Makes a socket base
	 * Inputs:
	 * 	socketSize: visual size for the socket (in how many elements will be shown)
	 * 	imgName: socket visual base
	 * 	socketName: name of the socket
	 */
	protected ScrollCharSocket makeBaseSocket(int socketSize, String imgName, String socketName) {
		ScrollCharSocket workSocket = this.mainFactory.getElemFactory().getScrollCharSocket(0, 0, 
				this.mainFactory.getLengthAssigner().getSocketLengthForSize(1), 
				this.mainFactory.getLengthAssigner().getSocketLengthForSize(socketSize), imgName);//.getScreenSocket(0, 0, socketSize, imgName);
		workSocket.setTitle(socketName);
		return workSocket;
	}
	
	/*
	 * Assigns a job to a socket
	 * Inputs:
	 * 	charSocket: socket the job will assigned to
	 * 	jobName: name of the job that will be assigned to the job
	 */
	protected boolean assignJobToASocket(ScrollCharSocket charSocket, JobName jobName) {
		Job foundJob = this.mainFactory.getJobManager().findJob(jobName);
		if(foundJob == null) {
			StaticFunctions.writeOutDebug(debug, "Couldn't find a job with the name: " + jobName + " for socket a socket");
			return false;
		}
		charSocket.setJob(foundJob);
		return true;
	}
	
	/*
	 * Method that makes a new socket
	 * Inputs:
	 * 	size: size of the new socket
	 * 	img: name of the image the socket uses
	 * 	name: name of the socket
	 * 	jobName: name of the job that will be added to the new socket
	 */
	protected void prepareANewSocket(int size, String img, String name, JobName jobName) {
		ScrollCharSocket newSocket = this.makeBaseSocket(size, img, name);
		this.assignJobToASocket(newSocket, jobName);
		this.assignedTopic.AddNewSocket(newSocket);
	}
	
	/*
	 * Method that makes a new socket
	 * Inputs:
	 * 	size: size of the new socket
	 * 	img: name of the image the socket uses
	 * 	name: name of the socket
	 * 	job: job object that will be attached to the socket
	 */
	protected void prepareANewSocket(int size, String img, String name, IJob job) {
		ScrollCharSocket newSocket = this.makeBaseSocket(size, img, name);
		newSocket.setJob(job);
		this.assignedTopic.AddNewSocket(newSocket);
	}

	/*
	 * Method that has to return a package of modifiers that decide when to update jobs
	 */
	protected abstract ArrayList<Object> makeModifierValuePackage();
	
	/*
	 * Method that adds all the needed sockets to the jobTopic
	 */
	protected abstract void addSockets();
	

}

package timeListeners;

import inventory.Inventory;

//Turn change listener that informs inventory on turn change
public class InventoryTurnChangeListener extends BaseTimeListener {
	private Inventory inventory;

	@Override
	public void timeAdvanced(boolean inBattle) {
		if(!inBattle) {
			this.inventory.announceTurnChangeToItems();
		}
	}
	
	public void bootUp() {
		super.bootUp();
		this.inventory = this.mainFactory.getInventory();
	}

}

package cutsceneInstances;

import view.CutsceneData;
import view.CutsceneDataCollection;

public class ReachingLoupeCutscene extends BaseCutsceneInstance {

	@Override
	protected String giveCutsceneName() {
		return "ReacingLoupe";
	}

	@Override
	protected void makeCutsceneScenes(CutsceneDataCollection saveLocation) {
		CutsceneData newFrame = new CutsceneData("TestBG","You made it. You walked from the capital and climbed the lonely hill.");
		saveLocation.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "In front of you, the gates of fort Loupe. It was built as a border fort, but for anyone living now know it as a prison.");
		saveLocation.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "It has been holding prisoners that the emperor doesn't want in the capital.");
		saveLocation.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "And now its probably the closest defensible location to the fallen capital.");
		saveLocation.addNewCutScene(newFrame);
		newFrame = new CutsceneData("TestBG", "For your next step, you need to get to other side of these walls.");
		saveLocation.addNewCutScene(newFrame);
		
	}

}

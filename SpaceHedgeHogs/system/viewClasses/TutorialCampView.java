package viewClasses;

import generalGraphics.ButtonLookType;
import party.PartyStance;
import screenElements.ScreenBackground;
import screenElements.ScreenButton;
import screenElements.ScreenScrollPanel;
import socketStoring.CharSocketBuilder;
import socketStoring.FullCharacterListProviding;
import socketStoring.FullItemProviding;
import socketStoring.NewItemSocketBuilder;
import sreenElementsSockets.ScrollCharSocket;
import sreenElementsSockets.ScrollItemSocket;
import view.BaseGameView;
import view.ViewName;

public class TutorialCampView extends BaseGameView {

	public TutorialCampView() {
		super(ViewName.CampTutorial);
	}
	
	@Override
	protected String bootGiveBackgroundImageName() {
		return "campBG";
	}
	
	protected void bootElements() {
		makeTimeAndTravelManager();
		makeIdleForTabDirectory();
		makeInventoryForTabDirectory();
		makeScrollPanelForTravel();
		makeWaterDataBox();
		makeNoticeController();
		this.buttons.add(this.makeGoTravelButton());
		this.buttons.add(this.makeNextTurnButton());
	}
	
	//Makes time element and travel manager
	private void makeTimeAndTravelManager() {
		this.setGameTimeCoord(1500, 20);
		this.addDataBox(this.mainFactory.getTravelManager().getDataBox(), 1250, 150);
	}
	
	//Makes idle socket that is located in the tab directory
	private void makeIdleForTabDirectory() {
		ScrollCharSocket socket = this.mainFactory.getElemFactory().getScrollCharSocket(0, 0, 
				this.mainFactory.getLengthAssigner().getSocketLengthForSize(1), 
				this.mainFactory.getLengthAssigner().getSocketLengthForSize(3), "Orange");
		socket.setTitle("Idle");
		socket.designateAsIdleSocket();
		FullCharacterListProviding provider = this.mainFactory.getElemFactory().getFullCharacterListProvding();
		CharSocketBuilder socketBuilder = this.mainFactory.getElemFactory().getCharSocketBuilder(socket, provider);
		this.newSocketBuildersForTabs.add(socketBuilder);
	}
	
	private void makeInventoryForTabDirectory() {
		ScrollItemSocket socket = this.mainFactory.getElemFactory().getScrollItemSocket(0, 0,
				this.mainFactory.getLengthAssigner().getSocketLengthForSize(1), 
				this.mainFactory.getLengthAssigner().getSocketLengthForSize(3), "100Lime");
		socket.setTitle("Inventory");
		FullItemProviding provider = this.mainFactory.getElemFactory().getFullItemProviding();
		NewItemSocketBuilder socketBuilder = this.mainFactory.getElemFactory().getNewItemSocketBuilder(socket, provider);
		
		socketBuilder.setUpdateValue(true);
		
		this.newSocketBuildersForTabs.add(socketBuilder);
	}
	
	//makes a scrollpanel meant for travel view
	private void makeScrollPanelForTravel() {
		ScreenScrollPanel newPanel = this.mainFactory.getElemFactory().getNewScrollPanel(10, 10, 1200, 900, "20Opacity");
		this.mainScreen = newPanel;
		this.buttons.add(newPanel.getAScrollButtonForThisPanel(true, true));
		this.buttons.add(newPanel.getAScrollButtonForThisPanel(true, false));
		this.buttons.add(newPanel.getAScrollButtonForThisPanel(false, true));
		this.buttons.add(newPanel.getAScrollButtonForThisPanel(false, false));
		
		addJobPanelToScrollPanel(newPanel);
	}
	
	//adds a water info databox to a gameView
	private void makeWaterDataBox() {
		this.addDataBox(this.mainFactory.getWaterSupplyController().getWaterDisplayDataBox(), 1250, 20);
	}
	
	//adds a notice controller to a view
	private void makeNoticeController() {
		this.addDataBox(this.mainFactory.getNoticeController().getDataBox(), 1500, 85);
	}
	
	private ScreenButton makeGoTravelButton() {
		ScreenButton goTravel = this.mainFactory.getElemFactory().getScreenButton(1470, 900, ButtonLookType.NEXT, "Travel");
		goTravel.addButtonEventListener(this.mainFactory.getElemFactory().getViewChangeListener(ViewName.TravelTutorial));
		goTravel.addButtonEventListener(this.mainFactory.getElemFactory().getPartyStanceChangeListener(PartyStance.Travel));
		goTravel.addButtonEventListener(this.mainFactory.getElemFactory().getUpdateJobFrameListener());
		goTravel.addActionLimiter(this.mainFactory.getElemFactory().getEventLimiter(false, false));
		return goTravel;
	}
	
	private ScreenButton makeNextTurnButton() {
		ScreenButton nextTurn =  this.mainFactory.getElemFactory().getScreenButton(1610, 900, ButtonLookType.NEXT, "Next Turn");
		nextTurn.addButtonEventListener(this.mainFactory.getElemFactory().getClearAllNoticesListener());
		nextTurn.addButtonEventListener(this.mainFactory.getJobListener());
		nextTurn.addButtonEventListener(this.mainFactory.getElemFactory().getDecreaseWaterSupplyByTurnListener());
		nextTurn.addButtonEventListener(this.mainFactory.getElemFactory().getTurnChangeListener());
		nextTurn.addActionLimiter(this.mainFactory.getElemFactory().getEventLimiter(false, false));
		return nextTurn;
	}

	@Override
	protected void goingOnScreenScript() {
		// TODO Auto-generated method stub
		
	}
	
	private void addJobPanelToScrollPanel(ScreenScrollPanel saveLocation) {
		saveLocation.addNewElement(this.mainFactory.getJobPanelForCampTutorial());
	}

}

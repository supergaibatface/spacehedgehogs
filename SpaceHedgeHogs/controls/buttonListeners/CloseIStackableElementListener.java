package buttonListeners;

import frameworks.IStackable;

public class CloseIStackableElementListener extends BaseButtonListener {
	private IStackable target;
	
	public CloseIStackableElementListener(IStackable target) {
		this.target = target;
	}

	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.mainFactory.getGElementManager().removeElementFromScreen(target);
		
	}

	@Override
	public String toInfo() {
		return "Closes a specific " + this.target.getClass()+" type of object";
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

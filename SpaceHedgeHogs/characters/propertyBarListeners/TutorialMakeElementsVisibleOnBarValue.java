package propertyBarListeners;

import java.util.ArrayList;

import propertyBars.PropertyBar;
import screenElements.ScreenElement;

//Listener that is meant to turn elements visible once property bar hits specific value
public class TutorialMakeElementsVisibleOnBarValue extends BaseBarChangeListener {
	private ArrayList<ScreenElement> elementsToMakeVisible;
	private int onValue;
	private PropertyBar targetBar;
	
	/*
	 * Constructor
	 * Inputs:
	 * 	onValue: int value of the property bar when this listener is supposed to react
	 * 	elements: array of elements that will be turned visible on activation
	 * 	targetBar: property bar which value change is supposed to trigger the listener (not used)
	 */
	public TutorialMakeElementsVisibleOnBarValue(int onValue, ArrayList<ScreenElement> elements, PropertyBar targetBar) {
		this.elementsToMakeVisible = elements;
		this.onValue = onValue;
		this.targetBar = targetBar;
	}
	

	@Override
	public void barChangeHappened(int newValue) {
		if(newValue == onValue) {
			makeTargetsVisible();
		}
		
	}
	
	//command that makes the target bar visible when called
	private void makeTargetsVisible() {
		for(ScreenElement oneElement : this.elementsToMakeVisible) {
			oneElement.makeVisible();
		}
	}
	

}

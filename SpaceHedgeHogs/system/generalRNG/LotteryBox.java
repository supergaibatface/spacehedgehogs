package generalRNG;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import factories.IAutoMake;
import factories.MainFactory;
import other.StaticFunctions;

/*
 * Class for doing RNG situations, you can add a random amount of Lottery tickets into it
 * and then draw a random one
 */
public class LotteryBox implements IAutoMake {
	private MainFactory mainFactory;
	private List<LotteryTicket> tickets;
	private boolean debug = false;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.tickets = new ArrayList<LotteryTicket>();
		
	}
	
	//Empties the lottery box of lottery tickets
	public void clearBox() {
		this.tickets.clear();
	}
	
	/*
	 * Adds another lottery ticket to the box
	 * Input:
	 * 	holderObject: object that will represent the ticket
	 * 	nrOfTickets: amount of tickets dedicated to the object
	 */
	public void addAnotherItem(Object holderObject, int nrOfTickets) {
		this.tickets.add(new LotteryTicket(holderObject, getFirstNrOfNewItem(), nrOfTickets));
	}
	
	//private function for getting the start nr for a newly added lottery ticket
	private int getFirstNrOfNewItem() {
		if(this.tickets.isEmpty()) {
			return 0;
		}
		else {
			return tickets.get(tickets.size() - 1).getNumberAfterThis();
		}
	}
	
	/*
	 * this function draws a winning ticket from the current contents of the lottery box,
	 * doesn't remove the ticket
	 */
	public Object pullRandomObject() {
		if(this.tickets.isEmpty()) {
			System.out.println("Error: There are no tickets to pull from");
			return null;
		}
		int winNr = this.getRandomNumber(this.getFirstNrOfNewItem());
		StaticFunctions.writeOutDebug(debug, "win nr is: "+winNr);
		LotteryTicket winTicket = this.findMatchingTicket(winNr);
		if(winTicket == null) {
			System.out.println("Error: Winning ticket doesn't exist");
			return null;
		}
		if(this.debug) {
			winTicket.printOutTicketValue();
		}
		return winTicket.getHolder();
	}
	
	//gets a random number between 0 and the input number
	private int getRandomNumber(int lastBound) {
		return new Random().nextInt(lastBound);
	}
	
	//matches a ticket to input number, or null if not found
	private LotteryTicket findMatchingTicket(int nr) {
		for(LotteryTicket oneTicket : this.tickets) {
			if(oneTicket.isItThisNr(nr)) {
				return oneTicket;
			}
		}
		return null;
	}
	
	//checks if lottery box is empty
	public boolean isEmpty() {
		return this.tickets.isEmpty();
	}
	
	//debug function for printing out all the current tickets inside the lotterybox
	public void pringOutContents() {
		System.out.println("Contents of LotteryBox");
		System.out.println("***");
		for(LotteryTicket oneTicket : this.tickets) {
			oneTicket.printOutTicketValue();
			System.out.println("***********");
		}
		System.out.println("----------------------");
	}

}

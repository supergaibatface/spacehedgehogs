package baggageCarryItemBase;

import java.util.List;

import battleItemBase.BattleItemBase;
import itemBase.BaseItem;
import itemBase.IGameItem;
import itemBase.ItemTagName;
import itemBase.SingleStackItemBase;
import itemBase.TradeItemName;
import screenInfoPage.BaseInfoPage;
import screenInfoPage.ItemInfoPage;

public abstract class BaggageAnimalItemBase extends SingleStackItemBase {

	public BaggageAnimalItemBase(TradeItemName name) {
		super(name);
	}

	@Override
	protected ItemInfoPage bootGiveInfoPage() {
		return this.mainFactory.getElemFactory().getItemInfoPage(this, "Orange");
	}
	
	public void bootUp() {
		super.bootUp();
		this.addTags();
	}
	
	private void addTags() {
		addAnotherItemTag(ItemTagName.BaggageAnimal);
	}

}

package settlement;

import java.util.ArrayList;
import java.util.List;

import factories.IAutoMake;
import factories.MainFactory;
import screenElements.ScreenButton;
import settlementLocation.ISettlementLocation;

public abstract class BaseSettlement implements IAutoMake, ISettlement {
	protected MainFactory mainFactory;
	protected List<ScreenButton> locationButtons;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		this.makeLists();
		
	}
	
	protected void makeLists() {
		this.locationButtons = new ArrayList<ScreenButton>();
	}
	
	protected void registerLocationButton(ISettlementLocation newLocation) {
		this.locationButtons.add(newLocation.getLocationButton());
	}
	
	public List<ScreenButton> getLocationButtons() {
		return this.locationButtons;
	}

}

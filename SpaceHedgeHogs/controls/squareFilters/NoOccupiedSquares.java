package squareFilters;

import combatFieldElements.MovementSquare;

//Filter that passes through squares that dont have anything on them
public class NoOccupiedSquares implements ISquareFilter{

	@Override
	public boolean filterSquare(MovementSquare oneSquare) {
		if(oneSquare.isOccupied()) {
			return false;
		}
		return true;
	}

}

package questBase;

import java.util.List;

import questStepBase.IQuestStep;
import screenInfoPage.QuestInfoPage;

public interface IQuest {
	
	public String getName();
	
	public boolean isCompleted();
	
	public boolean isActive();
	
	public void setActive(boolean newValue);
	
	public QuestInfoPage getQuestInfoPage();
	
	public List<IQuestStep> getQuestSteps();
	
	public List<IQuestStep> getVisibleQuestSteps();
	
	public IQuestStep getCurrentQuestStep();
	
	public void recieveQuestStepProgressChange(IQuestStep sourceStep);
	
	//public void updateInfoPage();
	
	public boolean hasStep(IQuestStep questStep);

}

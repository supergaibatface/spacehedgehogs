package dialogueFlags;

import java.util.ArrayList;
import java.util.List;

import dialogueFlagListeners.IDialogueFlagListener;
import factories.IAutoMake;
import factories.MainFactory;

public abstract class BaseDialogueFlagStand implements IDialogueFlagStand, IAutoMake {
	protected MainFactory mainFactory;
	protected List<DialogueFlag> triggeredFlags;
	protected List<IDialogueFlagListener> listeners;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
	}

	@Override
	public void bootUp() {
		this.bootFlagList();
		this.bootListenerList();
	}
	
	protected void bootFlagList() {
		this.triggeredFlags = new ArrayList<DialogueFlag>();
	}
	
	protected void bootListenerList() {
		this.listeners = new ArrayList<IDialogueFlagListener>();
	}

	@Override
	public boolean raiseFlag(DialogueFlag flag) {
		if(this.canRaiseFlag(flag)) {
			this.addTrueFlagValue(flag);
			return true;
		}
		return false;
	}
	
	protected void addTrueFlagValue(DialogueFlag flag) {
		this.triggeredFlags.add(flag);
		this.announceFlagAdding(flag);
	}
	
	protected boolean canRaiseFlag(DialogueFlag flag) {
		if(hasFlag(flag)) {
			return false;
		}
		return true;
	}
	
	protected boolean hasFlag(DialogueFlag flag) {
		return this.triggeredFlags.contains(flag);
	}

	@Override
	public boolean checkFlagState(DialogueFlag flag) {
		return this.hasFlag(flag);
	}

	@Override
	public boolean takeDownFlag(DialogueFlag flag) {
		if(canTakeDownFlag(flag)) {
			
			return true;
		}
		return false;
	}
	
	protected boolean canTakeDownFlag(DialogueFlag flag) {
		if(hasFlag(flag)) {
			this.removeTrueFlagStatus(flag);
			return true;
		}
		return false;
	}
	
	protected void removeTrueFlagStatus(DialogueFlag flag) {
		this.triggeredFlags.remove(flag);
		this.announceFlagRemoval(flag);
	}

	@Override
	public boolean addListener(IDialogueFlagListener newListener) {
		if(!this.listeners.contains(newListener)) {
			this.listeners.add(newListener);
			return true;
		}
		return false;
	}

	@Override
	public boolean removeListener(IDialogueFlagListener oldListener) {
		if(this.listeners.contains(oldListener)) {
			this.listeners.remove(oldListener);
			return true;
		}
		return false;
	}
	
	protected void announceFlagAdding(DialogueFlag newFlag) {
		for(IDialogueFlagListener oneListener: this.listeners) {
			oneListener.triggerNewFlag(newFlag);
		}
	}
	
	protected void announceFlagRemoval(DialogueFlag oldFlag) {
		for(IDialogueFlagListener oneListener: this.listeners) {
			oneListener.triggerFlagRemoval(oldFlag);
		}
	}

}

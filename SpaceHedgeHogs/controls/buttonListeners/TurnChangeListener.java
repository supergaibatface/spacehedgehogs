package buttonListeners;

//Button event listener that changes a turn when triggered
public class TurnChangeListener extends BaseButtonListener {

	//Method for when the event is triggered
	@Override
	public void screenButtonEventOccured(ScreenButtonEvent evt) {
		this.mainFactory.getTurnController().nextTurn();
		//this.mainFactory.getNoticeController().clearAllNotices();
		this.battleSituationLogic();
	}

	//boot up method
	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String toInfo() {
		return "Changes the turn";
	}
	
	//Method that deals with a turn change in case of a battle view being active
	private void battleSituationLogic() {
		if(this.mainFactory.getGameStateController().inBattle()) {
			this.mainFactory.getGameStateController().getBattleController().clearVisuals();
			this.mainFactory.getMainAIHub().giveOneTurn();
		}
	}

}

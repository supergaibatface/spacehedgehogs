package mouseSituationSet;

import java.util.ArrayList;

import commandAction.CABaseClass;

/*
 * Interface for mouse situation sets, that govern the conditions of any possible player action
 * and also providing possible acceptable player actions that match the current situation
 */
public interface IMouseSituationSet {
	
	//Adds a new command action to possible actions to the current conditions
	public void addNewCAElement(CABaseClass newCommand);
	
	//returns a list of acceptable command actions
	public ArrayList<CABaseClass> getCommands();
	
	//Boolean check function that returns true if current situation matches this mouseSituationSet
	public boolean checkSituation();
	
	//Returns a list of element classes that will be searched for when a player performs a mouse click
	public ArrayList<Class> getSearchClasses(int mouseClick);

}

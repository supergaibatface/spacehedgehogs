package factories;

import screenElements.ScreenElement;

public abstract class BaseMinorFactory extends BaseFactory implements IAutoMake {
	protected MainFactory mainFactory;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected MainFactory giveMainFactory() {
		return this.mainFactory;
	}

}

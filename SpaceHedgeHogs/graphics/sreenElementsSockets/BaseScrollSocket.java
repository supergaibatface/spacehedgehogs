package sreenElementsSockets;

import java.util.ArrayList;
import java.util.List;

import frameworks.IStackable;
import frameworks.TextFrame;
import screenElements.ScreenElement;
import screenElements.ScreenScrollPanel;
import screenElements.ScreenText;

//Base class for scrollable sockets
public abstract class BaseScrollSocket<ElementType extends ScreenElement> extends ScreenScrollPanel {
	protected ScreenText emptyText;
	protected TextFrame titleFrame;

	/*
	 * Constructor
	 * Inputs:
	 * 	x: x coordinate of the socket
	 * 	y: y coordinate of the socket
	 * 	length: length of the socket
	 * 	height: height of the socket
	 */
	public BaseScrollSocket(int x, int y, int length, int height) {
		super(x, y, length, height);
	}
	
	public void bootUp() {
		super.bootUp();
		this.xDataStart = this.lenAss.getExtraRoomLength();
		this.yDataStart = this.lenAss.getExtraRoomLength();
		this.setUpElementFrame();
		this.setUpEmptyText();
	}
	
	//Method for setting up the frame that socket elements are stored in
	protected abstract void setUpElementFrame();
	
	//sets a text that will be displayed when the socket is empty
	protected void setUpEmptyText() {
		this.emptyText = this.mainFactory.getElemFactory().getText(this.emptyMessage());
		this.emptyText.setCoordinates(0, 20);
		this.emptyText.setWrapper(this);
	}
	
	//Checks and corrects the value of empty situation text
	protected void emptyTextVisiblityCheck() {
		if(this.getElements().isEmpty()) {
			if(!this.emptyText.isVisibile()) {
				this.emptyText.setVisibile(true);
			}
		}
		else {
			if(this.emptyText.isVisibile()) {
				this.emptyText.setVisibile(false);
			}
		}
	}
	
	//returns the text that should be displayed when the socket is empty
	protected abstract String emptyMessage();
	
	/*
	 * Method for adding new elements to the socket
	 */
	public boolean addNewElement(ScreenElement newElement) {
		if(this.elementAddingLogic(newElement)) {
			emptyTextVisiblityCheck();
			this.doingScrollCalc();
			return true;
		}
		else {
			return false;
		}
		
	}
	
	/*
	 * Method for removing elements from the socket
	 */
	public boolean removeElement(ScreenElement newElement) {
		if(this.elementRemovingLogic(newElement)) {
			emptyTextVisiblityCheck();
			this.doingScrollCalc();
			return true;
		}
		else {
			return false;
		}
	}
	
	/*
	 * Checks if the socket has room for a new element
	 */
	public boolean hasRoom() {
		return true;
	}
	
	//Logic for adding a new element
	protected abstract boolean elementAddingLogic(ScreenElement element);
	
	//Logic for adding a new element
	protected abstract boolean elementRemovingLogic(ScreenElement element);
	
	//Getter for displayBox length
	protected int getDisplayBoxLength() {
		int length = this.getLength() - (this.mainFactory.getLengthAssigner().getExtraRoomLength()*2);
		return length;
	}
	
	//getter for displayBox height
	protected int getDisplayBoxHeight() {
		int height = this.getHeight() - (this.mainFactory.getLengthAssigner().getExtraRoomLength()*2);
		return height;
	}
	
	/*
	 * sets a title for the screen socket
	 * Inputs:
	 * 	title: string version of the sockets title
	 */
	public void setTitle(String title) {
		List<String> titleLines = new ArrayList<String>();
		titleLines.add(title);
		this.setTitle(titleLines);
	}
	
	//sets the title of this socket
	public void setTitle(List<String> titleLines) {
		this.titleFrame = this.mainFactory.getElemFactory().getTextFrame(this);
		this.titleFrame.fillTexts(titleLines);
		this.titleFrame.setLocation(0,  - this.titleFrame.getFullHeight() -5);

		this.titleFrame.setForceNonScroll(true);
		correctSizeForTitle();

	}
	
	//getter for the title frame
	public TextFrame getTitleFrame() {
		return this.titleFrame;
	}
	
	//Corrects the socket size for when a title is added
	private void correctSizeForTitle() {
		this.yDataStart = this.titleFrame.getFullHeight() + 5 /*+ this.lenAss.getExtraRoomLength()*/;
		if(this.hasGrayElement()) {
			this.grayElement.setCoordinates( -this.xDataStart, -this.yDataStart);
		}
		this.setSize(this.getLength(), this.getHeight() + this.titleFrame.getFullHeight() + 5/*+ this.lenAss.getExtraRoomLength()*/);
	}
	
	@Override
	public void fillTopTextList(List<ScreenText> list) {
		super.fillTopTextList(list);
		//list.add(title);
		list.add(emptyText);
		
	}
	
	@Override
	public void fillTopElementList(List<IStackable> list) {
		super.fillTopElementList(list);
		list.add(this.titleFrame);
	}
	
	//Checks if the socket has a title set
	public boolean hasTitle() {
		return this.titleFrame != null;
	}
	
	//Getter for all the elements in this socket
	public abstract ArrayList<ElementType> getElements();
	
	//Removes all elements in this socket
	public abstract void removeAllElements();
	
	public abstract void safeDelete();

}

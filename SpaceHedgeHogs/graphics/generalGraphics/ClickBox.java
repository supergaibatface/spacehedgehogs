package generalGraphics;

import frameworks.IStackable;

//class that represents a clickable area on the screen
public class ClickBox {
	int xCoord;
	int yCoord;
	int length;
	int height;
	
	/*
	 * Constructor
	 * Input:
	 * 	x: clickboxes location x coordinate (upper left corner)
	 * 	y: clickboxes location y coordinate (upper left corner)
	 * 	length: clickboxes horizontal size
	 * 	height: clickboxes vertical size
	 */
	public ClickBox(int x, int y, int length, int height) {
		this.setCoords(x, y);
		this.setSize(length, height);
	}
	
	/*
	 * Constructor for a size 0 element
	 * Input:
	 * 	x: clickboxes location x coordinate (upper left corner)
	 * 	y: clickboxes location y coordinate (upper left corner)
	 */
	public ClickBox(int x, int y) {
		this.setCoords(x, y);
		this.setSize(0, 0);
	}
	
	//getter for x coordinate
	public int getX() {
		return this.xCoord;
	}
	
	//getter for y coordinate
	public int getY() {
		return this.yCoord;
	}
	
	/*
	 * Setter for clickboxes coordinates (upper left corner)
	 * 	x: x coordinate
	 * 	y: y coordinate
	 */
	public void setCoords(int x, int y) {
		this.xCoord = x;
		this.yCoord = y;
	}
	
	//setter for x coordinate, input is the new coordinate
	public void setX(int x) {
		this.xCoord = x;
	}
	
	//setter for y coordinate, input is the new coordinate
	public void setY(int y) {
		this.yCoord = y;
	}
	
	//getter for clickboxes vertical size
	public int getHeight() {
		return this.height;
	}
	
	//getter for clicboxes horizontal size
	public int getLength() {
		return this.length;
	}
	
	/*
	 * setter for clickboxes size
	 * 	length: new horizontal size
	 * 	height: new vertical size
	 */
	public void setSize(int length, int height) {
		this.length = length;
		this.height = height;
	}
	
	//Getter for the left sides X coordinate
	public int getLeftX() {
		return this.xCoord;
	}
	
	//Getter for the Right sides x coordinate
	public int getRightX() {
		return this.xCoord + this.length;
	}
	
	//Getter for top sides Y coordinate
	public int getTopY() {
		return this.yCoord;
	}
	
	//Getter for the bottom sides y Coordinate
	public int getBottomY() {
		return this.yCoord + this.height;
	}
	
	/*
	 * Calculates the overlap between this and another clickBox and returns that as a new clickBox
	 * Inputs:
	 * 	otherBox: the other clickBox with overlap
	 * Output:
	 * 	newBox: a new clickBox with overlap calculation coordinates, if there is no overlap then
	 * 		new sizes will be negative
	 */
	public ClickBox getOverlap(ClickBox otherBox) {
		int topY = findLowerTop(otherBox);
		int botY = findHigherBot(otherBox);
		int leftX = findLargerLeft(otherBox);
		int rightX = findSmallerRight(otherBox);
		int newHeight = botY - topY;
		int newLength = rightX - leftX;
		
		ClickBox newBox = new ClickBox(leftX, topY, newLength, newHeight);
		return newBox;
	}
	
	/*
	 * Debug method for printing out all the values of this clickBox
	 */
	public void printOutThisClickBox(String name) {
		System.out.println("Start of "+name);
		System.out.println("left x: "+ this.getLeftX());
		System.out.println("right x: "+ this.getRightX());
		System.out.println("top y: "+ this.getTopY());
		System.out.println("bottom y: "+ this.getBottomY());
		System.out.println("------------");
	}
	
	/*
	 * Method for localising the coordinates of this clickBox to one on top of inputElement
	 * Inputs
	 * 	localElement: element the clickbox will be localised to (meaning to a coordinate system on top of it)
	 */
	public ClickBox localiseClickBox(IStackable localElement) {
		return new ClickBox(
				this.xCoord - localElement.getX() - localElement.getXDataStart() + localElement.getXScroll(), 
				this.yCoord - localElement.getY() - localElement.getYDataStart() + localElement.getYScroll(), 
				this.getLength(), 
				this.getHeight());
	}
	
	//Checks if this clickBox has realistic values
	public boolean thisBoxIsRealistic() {
		if(this.height < 0) {
			return false;
		}
		if(this.length < 0) {
			return false;
		}
		return true;
	}
	
	//finds the lowest top coordinate between this and other input box
	private int findLowerTop(ClickBox otherBox) {
		if(this.getTopY() > otherBox.getTopY()) {
			return this.getTopY();
		}
		else {
			return otherBox.getTopY();
		}
	}
	
	//finds the highest bottom coordinate between this and other input box
	private int findHigherBot(ClickBox otherBox) {
		if(this.getBottomY() < otherBox.getBottomY()) {
			return this.getBottomY();
		}
		else {
			return otherBox.getBottomY();
		}
	}
	
	//Finds the larger value left point between this and other input box
	private int findLargerLeft(ClickBox otherBox) {
		if(this.getLeftX() > otherBox.getLeftX()) {
			return this.getLeftX();
		}
		else {
			return otherBox.getLeftX();
		}
	}
	
	//finds the smaller value right point between this and other input box
	private int findSmallerRight(ClickBox otherBox) {
		if(this.getRightX() < otherBox.getRightX()) {
			return this.getRightX();
		}
		else {
			return otherBox.getRightX();
		}
	}
	
	//Checks if this and other intput box have same values
	public boolean sameSquare(ClickBox otherBox) {
		if(!checkXCoord(otherBox)) {
			return false;
		}
		if(!checkYCoord(otherBox)) {
			return false;
		}
		if(!checkLength(otherBox)) {
			return false;
		}
		if(!checkHeight(otherBox)) {
			return false;
		}
		return true;
		
	}
	
	//checks if this and other boxes x coordinates match
	private boolean checkXCoord(ClickBox otherBox) {
		if(this.xCoord == otherBox.getX()) {
			return true;
		}
		return false;
	}
	
	//checks if this and other boxes y coordinates match
	private boolean checkYCoord(ClickBox otherBox) {
		if(this.yCoord == otherBox.getY()) {
			return true;
		}
		return false;
	}
	
	//checks if this and other boxes height match
	private boolean checkHeight(ClickBox otherBox) {
		if(this.height == otherBox.getHeight()) {
			return true;
		}
		return false;
	}
	
	//checks if this and other boxes length match
	private boolean checkLength(ClickBox otherBox) {
		if(this.length == otherBox.getLength()) {
			return true;
		}
		return false;
	}
	

}

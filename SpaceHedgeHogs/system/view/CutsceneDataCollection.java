package view;

import java.util.ArrayList;

import javax.swing.event.EventListenerList;

import buttonListeners.ISButtonListener;
import buttonListeners.ScreenButtonEvent;
import factories.IAutoMake;
import factories.MainFactory;
import screenElements.ScreenButton;

//Data collection that has many individual frame datas, makes up a full cutscene showing
public class CutsceneDataCollection implements IAutoMake {
	private MainFactory mainFactory;
	private String name;
	private ArrayList<CutsceneData> dataArray;
	private int bookmark;
	private EventListenerList finalButtonlistenerList = new EventListenerList();

	//constructor
	public CutsceneDataCollection(String name) {
		this.name = name;
	}
	
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}
	@Override
	public void bootUp() {
		this.dataArray = new ArrayList<CutsceneData>();
		this.bookmark = 0;
	}
	
	//Adds new scene to the collection
	public void addNewCutScene(CutsceneData newScene) {
		this.dataArray.add(newScene);
	}
	
	//gets the current bookmark for the cutscene
	public int getBookmark() {
		return this.bookmark;
	}
	
	//Resets the bookmark for current showing
	public void resetBookmark() {
		this.bookmark = 0;
	}
	
	//Checks if the cutscene showing has reached its end
	public boolean hasEnded() {
		return !(dataArray.size() > this.bookmark);
	}
	
	//Getter for current bookmarked scene
	public CutsceneData getCurrentData() {
		return this.dataArray.get(bookmark);
	}
	
	//moves bookmark to the next scene
	public void advanceBookmark() {
		this.bookmark++;
	}
	
	//Combined method that returns next scene and advances the bookmark
	public CutsceneData getAndAdvanceData() {
		CutsceneData returnData = this.getCurrentData();
		this.advanceBookmark();
		return returnData;
	}
	
	//Add button event listener that will be added to the last button of the cutscene
	public void addFinalButtonEventListener(ISButtonListener listener) {
		finalButtonlistenerList.add(ISButtonListener.class, listener);
	}
	
	//triggers all the listeners that have been assigned for the final button of the cutscene
	public void triggerFinalButtonListeners(ScreenButton button) {
		fireButtonEvent(new ScreenButtonEvent(button));
	}
	
	//Method for sending a ScreenButtonEvent to all the listeners, input evt: ScreenButtonEvent style event
	private void fireButtonEvent(ScreenButtonEvent evt) {
		Object[] listeners = finalButtonlistenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i + 2) {
			if (listeners[i] == ISButtonListener.class) {
				((ISButtonListener) listeners[i + 1]).screenButtonEventOccured(evt);
			}
		}
	}
	

}

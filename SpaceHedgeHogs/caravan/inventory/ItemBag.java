package inventory;

import java.util.ArrayList;

import factories.IAutoMake;
import factories.MainFactory;
import itemBase.IGameItem;
import itemBase.TradeItemName;

//Field characters bag that will contain items on him/her
public class ItemBag implements IAutoMake {
	private MainFactory mainFactory;
	private ArrayList<IGameItem> items;
	
	//constructor
	public ItemBag() {
		
	}
	
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}
	@Override
	public void bootUp() {
		this.items = new ArrayList<IGameItem>();
		
	}
	
	//Finds an item from the bag or returns null
	public IGameItem findItem(TradeItemName name) {
		for(IGameItem oneItem: this.items) {
			if(oneItem.equals(name)) {
				return oneItem;
			}
		}
		return null;
	}
	
	/*
	 * Adds the specific amount of item to the inventory, if this is the first instance of this item
	 * will make a new entry in the inventory, if some the item already exists in inventory, increases the amount
	 * Inputs:
	 * 	name: item name
	 * 	amount: int value for the amount
	 */
	public void addTradeItemAmount(IGameItem oneItem) {
		IGameItem item = findItem(oneItem.getName());
		if(item == null) {
			this.items.add(oneItem);
		}
		else {
			item.addAmount(oneItem.getSupply());
		}
	}

}

package characterClassMatching;

import characterExceptions.NoCustomCharacterClassException;
import dataValues.CharValue;
import factories.CharacterFactory;
import factories.IAutoMake;
import factories.MainFactory;
import gameCharacters.IGameCharacter;

public class CharacterClassFinder implements IAutoMake {
	private MainFactory mainFactory;

	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}
	
	public IGameCharacter getClassForName(CharValue oneValues) {
		try {
			return matchCustomList(oneValues.getName());
		}
		catch(Exception exception) {
			this.exceptionResponding(exception);
		}
		return this.getDefaultClass(oneValues);
	}
	
	private void exceptionResponding(Exception exception) {
		this.mainFactory.getExceptionManager().dealWithException(exception);
	}
	
	private IGameCharacter getDefaultClass(CharValue oneValue) {
		return this.mainFactory.getElemFactory().getGameCharacter(oneValue.getName(), oneValue.getImgName());
	}
	
	private CharacterFactory accessCharFactory() {
		return this.mainFactory.getCharacterFactory();
	}
	
	private IGameCharacter matchCustomList(String name) throws NoCustomCharacterClassException {
		if(name.equals("Bob")) {
			return this.accessCharFactory().getCharacterBob();
		}
		else if(name.equals("Jack")) {
			return this.accessCharFactory().getCharacterJack();
		}
		
		throw new NoCustomCharacterClassException(name);
	}

}

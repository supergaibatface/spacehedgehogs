package npcListeners;

import stepObjective.TalkToNpcObjective;
import view.CutsceneDataCollection;

public class EndDialogueQuestNPCListener extends BaseNPCListener {
	private TalkToNpcObjective targetObjective;
	
	public EndDialogueQuestNPCListener(TalkToNpcObjective targetObjective) {
		this.targetObjective = targetObjective;
	}

	@Override
	public void callEndOfADialogue(CutsceneDataCollection endedDialogue) {
		this.targetObjective.announceObjectiveCompletion();
		
	}

}

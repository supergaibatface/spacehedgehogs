package eventSuccessFactors;

import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;
import skills.SkillName;

//success factor that depents on a character skill
public class SimpleSkillFactor extends BaseEventSuccessFactor {
	private SkillName skillName;
	
	/*
	 * constructor
	 * Input:
	 * 	skillName: name of the skill that will be giving a boost to success
	 */
	public SimpleSkillFactor(SkillName skillName) {
		this.skillName = skillName;
	}

	@Override
	public boolean getValueType(IGameCharacter oneChar) {
		return true;
	}

	@Override
	public int getValueFactor(IGameCharacter oneChar) {
		return oneChar.findSkill(skillName).getLevel();
	}

	@Override
	public void bootUp() {
		// TODO Auto-generated method stub
		
	}

}

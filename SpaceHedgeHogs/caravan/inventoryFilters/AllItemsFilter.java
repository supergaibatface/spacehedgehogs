package inventoryFilters;

import java.util.List;

import itemBase.IGameItem;

//inventory filter that returns all items
public class AllItemsFilter extends BaseInventoryFilter {

	@Override
	public List<IGameItem> getFilteredItems() {
		return this.mainFactory.getInventory().getCurrentInventory();
	}

	@Override
	protected boolean itemFilterScript(IGameItem checkItem) {
		return true;
	}

}

package timedSystems;

import java.util.TimerTask;

//Counter task that counts down on a timed object
public class TimeObjectCountTask extends TimerTask {
	private ITimedObject timedObject;
	
	//constructor
	public TimeObjectCountTask(ITimedObject object) {
		this.timedObject = object;
	}

	@Override
	public void run() {
		if(timedObject.checkIfDelete()) {
			
			this.timedObject = null;
			this.cancel();
		}
		else {
			timedObject.countDown();
		}
		
	}

}

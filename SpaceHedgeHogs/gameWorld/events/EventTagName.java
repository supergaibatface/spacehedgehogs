package events;

//Enum for event tag names
public enum EventTagName {
	TST, 
	TST2, 
	WTS, 
	SCOT, 
	FORF, 
	FORW,
	PATR, 
	REST, 
	DEFN, 
	FORH,
	SearchForFood,
	SearchForWater,
	SearchForWaterSettlement,
	SearchForSupply,
	SearchForPath,
	TrainingFighting,
	Resting,
	Crafting,
	ItemRepairing,
	BaggageManagement

}

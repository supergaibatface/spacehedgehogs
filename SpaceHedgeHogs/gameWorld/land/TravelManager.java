package land;

import java.util.ArrayList;

import factories.IAutoMake;
import factories.MainFactory;
import generalGraphics.ButtonLookType;
import screenElements.ScreenButton;
import screenElements.ScreenDataBox;
import travelGoals.GoalMarker;

//Object that manages caravans traveling
public class TravelManager implements IAutoMake{
	private MainFactory mainFactory;
	private LandArea lastArea;
	private LandArea selectedArea;
	private ArrayList<LandArea> coveredDistance;
	private ArrayList<LandArea> travelOptions;
	private ScreenDataBox dataBox;
	private ArrayList<GoalMarker> goalMarkers;
	
	//constructor
	public TravelManager() {
		
	}
	
	@Override
	public void setMainFactory(MainFactory mainFactory) {
		this.mainFactory = mainFactory;
		
	}
	@Override
	public void bootUp() {
		this.coveredDistance = new ArrayList<LandArea>();
		this.travelOptions = new ArrayList<LandArea>();
		this.dataBox = this.mainFactory.getElemFactory().getScreenDataBox(0, 0, "LightBlue");
		this.dataBox.setCustomId("travelBox");
		this.goalMarkers = new ArrayList<GoalMarker>();
	}
	
	//For adding a goal marker that acts as a location to reach
	public void addGoalMarker(GoalMarker newMarker) {
		this.goalMarkers.add(newMarker);
		if(newMarker.isShowDestination()) {
			updateDataBox();
		}
	}
	
	/*
	 * Method for adding traveling options
	 */
	public void addTravelOption(LandArea area) {
		if(this.travelOptions.isEmpty()) {
			setFirstTravelOption(area);
			standardTravelOptionBranch(area);
			this.dataBox.addRadioFunctionToButtons();
		}
		else {
			standardTravelOptionBranch(area);
		}
	}
	
	/*
	 * Checks if there are goal markers that have been reached
	 * if a goal has been reached it will set it to be deleted
	 */
	private void checkGoalMarkers() {
		ArrayList<GoalMarker> markersToDelete = new ArrayList<GoalMarker>();
		for(GoalMarker oneMarker : this.goalMarkers) {
			if(oneMarker.hasReachedGoal(getTotalTraveledDistance())) {
				markersToDelete.add(oneMarker);
			}
		}
		if(!markersToDelete.isEmpty()) {
			this.goalMarkers.removeAll(markersToDelete);
		}
	}
	
	/*
	 * Method that adds a travel option to the player
	 */
	private void standardTravelOptionBranch(LandArea area) {
		this.travelOptions.add(area);
		makeNewButtonForLandArea(area);
		updateDataBox();
	}
	
	/*
	 * Method for adding the first travel option to the player
	 */
	private void setFirstTravelOption(LandArea area) {
		this.setCurrentArea(area);
		this.selectedArea = area;
	}
	
	/*
	 * Test method for generating a land area
	 */
	public LandArea makeAndGetTestLandArea(TerrainName name, int distance) {
		return this.getTestLandArea(name, distance);
	}
	
	/*
	 * Makes a test land area
	 * Inputs:
	 * 	name: name of the terrain
	 * 	speed: speed modiefier 0 to 1
	 * 	distance: how much land can be covered on this
	 */
	private LandArea getTestLandArea(TerrainName name, int distance) {
		LandArea newLandArea = this.mainFactory.getElemFactory().getLandArea(name, distance);
		return newLandArea;
	}
	
	//Setter for current land area
	public void setCurrentArea(LandArea currentArea) {
		this.lastArea = currentArea;
	}
	
	/*
	 * For choosing an active land area
	 * Inputs:
	 * 	selectedArea: the area that will be made active
	 */
	public void setSelectedArea(LandArea selectedArea) {
		this.selectedArea = selectedArea;
		updateDataBox();
	}
	
	//Adds a land area under already covered land areas
	public void addToCoveredDistance(LandArea pastArea) {
		this.coveredDistance.add(pastArea);
	}
	
	//Updates the databox that represents traveling
	public void updateDataBox() {
		ArrayList<String> lines = new ArrayList<String>();
		addGoalMarkerInfo(lines);
		addTerrainInfo(lines);
		this.dataBox.fillDataBoxWithTexts(lines);
		this.dataBox.updateSizeForTime();
	}
	
	//Adds a goal marker data to travel info
	private void addGoalMarkerInfo(ArrayList<String> lines) {
		int linesStart = lines.size();
		for(GoalMarker oneMarker : this.goalMarkers) {
			if(oneMarker.isShowDestination()) {
				lines.add("Destination: "+oneMarker.getDestinationDescription());
				lines.add((oneMarker.getGoalDistance() - this.getTotalTraveledDistance()) + " left to travel");
				lines.add("");
			}
		}
		if(linesStart != lines.size()) {
			lines.add(linesStart, "Destinations: ");
		}
	}
	
	//for adding lines about terrain to an array of strings
	private void addTerrainInfo(ArrayList<String> lines) {
		lines.add("Active terrain");
		lines.add("Terrain: " + this.lastArea.toString());
		lines.add("Traveled Distance: " + getTotalTraveledDistance());
		lines.add(" ");
		lines.add("Selected terrain: " + this.selectedArea.toString());
	}
	
	//Makes test buttons for the travel text box
	public void dataBoxTestButtons() {
		ScreenButton testButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.TRAVELSELECT, "Test");
		this.dataBox.addButton(testButton);
		this.dataBox.updateSizeForTime();
	}
	
	/*
	 * Makes buttons for all the traveling options that the game currently has
	 */
	private void makeButtonsFromChoices() {
		for (LandArea oneArea: this.travelOptions) {
			makeNewButtonForLandArea(oneArea);
		}
		this.dataBox.updateSizeForTime();
	}
	
	/*
	 * Makes a button for a one specific land area option
	 * Input:
	 * 	oneArea: the land area that will have a button made for it
	 */
	private void makeNewButtonForLandArea(LandArea oneArea) {
		ScreenButton testButton = this.mainFactory.getElemFactory().getScreenButton(0, 0, ButtonLookType.TRAVELSELECT, getStringForButton(oneArea));
		testButton.addButtonEventListener(this.mainFactory.getElemFactory().getLandAreaChangeListener(oneArea));
		testButton.addButtonEventListener(this.mainFactory.getElemFactory().getUpdateJobFrameListener());
		oneArea.setButton(testButton);
		this.dataBox.addButton(testButton);
	}
	
	//Getter for current land area
	public LandArea getCurrentArea() {
		return this.lastArea;
	}
	
	public LandArea getSelectedArea() {
		return this.selectedArea;
	}
	
	//getter for the databox that represents traveling
	public ScreenDataBox getDataBox() {
		return this.dataBox;
	}
	
	//Getter for the total traveled distance in this instance
	public double getTotalTraveledDistance() {
		double traveledDistance = 0;
		for(LandArea oneLandArea : this.coveredDistance) {
			traveledDistance = traveledDistance + oneLandArea.getCurrentDistance();
		}
		traveledDistance = traveledDistance + this.lastArea.getCurrentDistance();
		return traveledDistance;
	}
	
	//Checks if its allowed to travel forward
	public boolean canAdvance() {
		if(this.selectedArea == null) {
			return false;
		}
		if(!(this.selectedArea.getRoomToAdvance() > 0)) {
			return false;
		}
		return true;
	}
	
	//Method for advancing forward by a distance
	public void advance(double distance) {
		if(!selectedArea.equals(lastArea)) {
			switchLastArea();
		}
		if(canAdvance()) {
			ArrayList<LandArea> toBeRemoved = expireAreas(this.travelOptions, this.selectedArea);
			removeExpiredAreas(toBeRemoved);
		}
		this.lastArea.advance(distance);
		this.checkGoalMarkers();
		
		updateDataBox();
	}
	
	/*
	 * Switches to a different active travel area, adds the old area to history
	 */
	private void switchLastArea() {
		this.coveredDistance.add(lastArea);
		setCurrentArea(this.selectedArea);
	}
	
	/*
	 * Expires not active land area options by one turn
	 * Inputs:
	 * 	selection: list of possible land areas
	 * 	inUseArea: the one active land area
	 */
	private ArrayList<LandArea> expireAreas(ArrayList<LandArea> selection, LandArea inUseArea) {
		ArrayList<LandArea> toBeRemoved = new ArrayList<LandArea>();
		for(LandArea oneOption: selection) {
			if(!oneOption.equals(inUseArea)) {
				if(oneOption.countTowardsExpire()) {
					toBeRemoved.add(oneOption);
				}
			}
		}
		return toBeRemoved;
	}
	
	/*
	 * Removes land area options that are given to this function
	 * Inputs:
	 * 	areas: arraylist of all the options that will be removed
	 */
	private void removeExpiredAreas(ArrayList<LandArea> areas) {
		for(LandArea oneOption : areas) {
			removeLandAreaOption(oneOption);
		}
	}
	
	/*
	 * removes one land area option from choices
	 * Inputs:
	 *  landArea: the land area that will be removed
	 */
	private void removeLandAreaOption(LandArea landArea) {
		if(landArea.hasButton()) {
			this.dataBox.removeButton(landArea.getButton());
			this.travelOptions.remove(landArea);
		}
		
	}
	
	//Calculates the distance that the caravan can advance in one turn on this land area
	public double calculateDistance(double distance) {
		double calculated = this.lastArea.getFullMultiplier() * distance;
		return calculated;
	}
	
	//Method for getting a string label for land area button
	private String getStringForButton(LandArea landArea) {
		return landArea.getTerrain().toString();
	}
	
	//For deleting all travel history
	public void clearTravelHistory() {
		lastArea = null;
		selectedArea = null;
		coveredDistance.clear();
		travelOptions.clear();
		if(this.dataBox != null) {
			if(this.dataBox.hasButtons()) {
				this.dataBox.getButtonFrame().removeAllButtons();
			}
		}
		goalMarkers.clear();;
	}

}

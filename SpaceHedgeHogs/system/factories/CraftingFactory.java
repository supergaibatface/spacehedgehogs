package factories;

import craftingBluePrints.CraftingProject;
import craftingBluePrints.IBlueprint;
import craftingBluePrints.SwordBlueprint;
import craftingCenter.CraftingInfoPage;
import craftingCenter.CraftingManager;
import craftingManagerListeners.CraftingInfoPageListUpdater;
import craftingManagerListeners.ICraftingManagerListener;
import screenInfoPage.BaseInfoPage;

public class CraftingFactory extends BaseMinorFactory {
	private CraftingManager craftingManager;
	
	public CraftingManager getCraftingManager() {
		if(this.craftingManager == null) {
			this.craftingManager = this.setUpNewElement(new CraftingManager());
		}
		return this.craftingManager;
	}
	
	public IBlueprint getSwordBluePrint() {
		return this.setUpNewElement(new SwordBlueprint());
	}
	
	public BaseInfoPage getCraftingPage(CraftingManager masterManager, String pageImage) {
		return this.setUpNewGraphicalElement(new CraftingInfoPage(masterManager), pageImage);
	}
	
	public CraftingProject getNewCraftingProject(IBlueprint targetBlueprint) {
		return this.setUpNewElement(new CraftingProject(targetBlueprint));
	}
	
	public ICraftingManagerListener getCraftingInfoPageListUpdater(CraftingInfoPage masterPage) {
		return this.setUpNewElement(new CraftingInfoPageListUpdater(masterPage));
	}

}

package propertyBars;

import java.util.ArrayList;
import java.util.List;

import gameCharacters.GameCharacter;
import gameCharacters.IGameCharacter;

//Umbrella object for all the saved property bars meants for a specific one character
public class SavedPropertySheet {
	private List<SavedPropertyBar> savedBars;
	
	//constructor
	public SavedPropertySheet() {
		this.savedBars = new ArrayList<SavedPropertyBar>();
	}
	
	//For adding a new saved property bar to the sheet
	public void addNewSavedPropertyBar(SavedPropertyBar newBar) {
		this.savedBars.add(newBar);
	}
	
	/*
	 * for making a new saved property bar from given values
	 * Inputs:
	 * 	name: property bar name
	 * 	currentValue: value of the saved bar
	 */
	public void generateAndSaveNewBar(PropertyBarName name, int currentValue) {
		this.savedBars.add(new SavedPropertyBar(name, currentValue));
	}
	
	//for setting all saved character bar values on one specific char
	public void fillBars(IGameCharacter targetChar) {
		for(SavedPropertyBar oneBar : this.savedBars) {
			oneBar.fillBarOnChar(targetChar);
		}
	}

}
